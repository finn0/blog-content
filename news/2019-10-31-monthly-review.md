+++
title = "Monthly review – October 2019"
author = "Benjamin"
date = "2019-10-31T06:00:00+01:00"
tags = [ "simjacker", "https", "tls", "mintotp", "2fa" ]
categories = [ "Monthly review" ]
ogdescription = "Our monthly review of October 2019."
slug = "2019-10-31-monthly-review"
aliases = [ "/blog/2019-10-monthly-review" ]
banner = "banners/monthly-review"
toc = true
syntax = true
news = true
+++

Each month, we publish a review that covers essential activities of the last 30 days. This month, we talk about Simjacker (again), web browser support for TLS, important security updates, MinTOTP, and more.
<!--more-->
{{< rssbox >}}

## News of the month {#notm}
In October 2019, we read the following news:

### Simjacker
We mentioned the [Simjacker attack]({{< relref "#links" >}}) before. It can be used to locate phones, or retrieve information about them (IMEI, battery, network, language). This month, a report listing affected countries was released. In Europe, at least one mobile operator in Italy, Cyprus, and Bulgaria is/was affected. At least 61 mobile operators are/were affected worldwide. In total, about 861 million SIM cards are affected according to the report.

### Web browser disabled TLS 1.0 and 1.1 by default
Mozilla and Google started to remove support for TLS 1.0 and 1.1 in their web browsers (Firefox, Chromium, Chrome). During a transition period, users will likely be able to re-enable TLS 1.0/1.1 in their web browsers. The goal is to completely switch to TLS 1.2 or later for more secure transport encryption. Keep in mind that mobile apps and other web clients may still use legacy TLS. Besides, Mozilla changed the look of security indicators (similar to Google). You likely realize the removal of "https://" and the green lock icon in the address bar. On the other hand, "http://" is always marked as insecure now.

### Important security updates
* PuTTY 0.73 was released on September 29, fixing three different security vulnerabilities.
* Then, Exim Internet Mailer 4.92.3 was released, fixing a heap-based buffer overflow in Exim 4.92 to 4.92.2 (see [CVE-2019-16928]({{< relref "#links" >}})).
* Another notable release is tcpdump 4.9.3, which fixes nearly 30 security vulnerabilities. Some of them are rated "critical."

### Data breaches and leaks
Moreover, there were some data breaches. Have I Been Pwned added information about the following breaches:

* Sephora (breached in January 2017)
* StreetEasy (breached in June 2016)

Check if you were affected, and change your credentials.

## Tool of the month {#toolotm}
This month, we look at a minimal Python 3 script to generate [OATH-TOTP]({{< ref "/glossary.md#oath-totp" >}}) and [OATH-HOTP]({{< ref "/glossary.md#oath-hotp" >}}) codes. Web applications commonly use TOTP for [two-factor authentication]({{< ref "/glossary.md#2fa" >}}).

The script is called [MinTOTP]({{< relref "#links" >}}). You need Python 3.4 or later installed on your device. We like the simplicity of the script that consists of 30 lines of code. There is also comprehensive documentation available on GitHub.

To use the script, you can either download the py file or install MinTOTP via pip ({{< kbd "pip install mintotp" >}}).

Afterward, you can run {{< kbd "mintotp <<< SECRETSECRETSECRET" >}} (if you installed it using pip) or {{< kbd "python3 totp.py <<< SECRETSECRETSECRET" >}} (if you directly use the py file) to get a TOTP code.

More examples are shown on the GitHub page of [MinTOTP]({{< relref "#links" >}}). Keep in mind that the secret (e.g., SECRETSECRETSECRET) is a secret! Store it like a password and only use a second device to generate your OTPs. Do not use the device which you use for login. Do not store the shared OTP secret and the password in the same database!

Tip: You can also use zbarimg and oathtool to generate TOTP codes, as [shown on our Terminal tips page]({{< ref "/terminal-tips.md#zbarimg-oathtool" >}}).

## Tip of the month {#tipotm}
This month's tip is about processes and organization. If you are a frequent reader of the InfoSec Handbook, you likely know that information security consists of technology, processes/organization, and people. Only focusing on technology doesn't give you more than 33% security.

An example in the physical world: There is a new fire extinguisher on the floor of your apartment building. Is it safe in case of fire now? No. Residents need information about the location of the fire extinguisher, and they need to know how to use it (raising awareness of people). Then, somebody has to maintain the fire extinguisher (checks, refills, replacement). Who is responsible here? You need processes and organization.

Some questions regarding information security at your home are:

* How do I manage credentials (e.g., [passwords]({{< ref "/glossary.md#password" >}}), [passphrases]({{< ref "/glossary.md#passphrase" >}}), [2FA]({{< ref "/glossary.md#2fa" >}}) codes, [U2F]({{< ref "/glossary.md#u2f" >}}) tokens)?
* How do I update my devices? (e.g., software on your router, the firmware of IoT devices, software packages on your computer, apps on your smartphone)
* How do I check the security settings of software and web applications I use? (Keep in mind that updates may reset or change previous settings.)
* How do I learn about data breaches that likely affect my [personal data]({{< ref "/glossary.md#personal-data" >}})? (Leaked data makes you more vulnerable to [phishing]({{< ref "/glossary.md#phishing" >}}).)
* Which devices are in my home network? Do they need internet access? Is there a way to put certain types of hardware or users in separate VLANs (e.g., guests using your internet access, or IoT devices)?
* How do I monitor my home network for suspicious network traffic? How do I get alerts? How do I react in case of suspicious network traffic?
* Do I have backups of my data? What happens in case of malware infection, esp. ransomware?

These are only several examples. A crucial part of information security is thinking about it. How do you react in certain situations? How is something managed? Who is responsible? Thinking about all of this results in new questions and a better understanding of technology and threats. Think about it, ask questions, and discuss it. Thinking is far more helpful than just installing arbitrary "security" software.

## Readers' questions of the month {#rqotm}
We share some of our replies to frequent questions of our readers:

### "What is the current state of gzip and side-channel attacks?"
This question is likely about the 2013 BREACH attack. BREACH works with any HTTP compression, even the new br (Brotli) compression. There is a difference between TLS compression and HTTP compression. TLS compression is always considered insecure (turn it off), HTTP compression is in a state of "it depends." Disabling HTTP compression dramatically reduces the performance of websites, so the majority of web servers didn't disable it. There is no fix, but there are mitigations. For our website, this isn't an issue since we don't transmit any personal or sensitive data.

### "What is the best way to learn Kali Linux/infosec/ethical hacking?"
For Kali, there is a book on kali.training (and some paid training). For ethical hacking, look for certifications like CEH v10, or CompTIA PenTest+. There are many books, videos, and training to get certified. Use [Hack The Box]({{< ref "/as-htb.md" >}}) for practice. Information security itself is a broad topic. You likely look for something like Security+. You don't need to get certified; however, you get a structured approach this way. Read our post ["Your career in information security."]({{< ref "/blog/infosec-career.md" >}})

### "Should I switch from search engine A to search engine B for more privacy?"
Switching from an online service provider to another online service provider is about trusting other parties. This is true for many different services, like blogs, forums, search engines, VPNs, mail servers, and so on. The problem is that you can't check most of the server-side configuration, as mentioned in ["Pros and cons of online assessment tools for web server security."]({{< ref "/blog/online-assessment-tools.md" >}}) If you can't check this, you have to trust the service provider. In the end, it is all about trust, not about actual control. So be sure that you trust the service provider of the other search engine. Keep in mind that you can't validate most of the provider's statements like "we don't log."

Just send us your questions. Maybe, we answer your question in the next monthly review.

## Our activities of the month {#ishotm}
In October, we published three new articles within the context of this year's European Cyber Security Month:

* [ECSM 2019 – Tips for your cyber hygiene]({{< ref "/blog/ecsm2019-cyber-hygiene.md" >}}): This article addresses the first topic of the ECSM 2019 (Cyber Hygiene). We present several quick actions to keep or improve your level of information security.
* [ECSM 2019 – Preparing and hosting a security CTF contest]({{< ref "/blog/ecsm2019-ctf.md" >}}): Some of us were heavily involved in preparing and hosting a public CTF contest. In the article, we give an insight into preparations and hosting of the CTF event, and discuss some lessons learned.
* [ECSM 2019 – Securing emerging technology (IoT) at home]({{< ref "/blog/ecsm2019-emerging-technology.md" >}}): In this article, we address the second topic of the ECSM 2019 (Emerging Technology) by discussing the security of IoT devices. We show ways to secure your IoT devices.

Besides, we moved the Git repository containing our content from GitHub to [codeberg.org]({{< relref "#links" >}}). There, you can see every single change of our articles for transparency reasons. We will publish more information regarding codeberg.org soon.

## Closing words {#cw}
After two restless months, we will proceed to revise our [Web server security series]({{< ref "/as-wss.md" >}}) as announced before. Our goal is to split general content and software-specific content for better maintainability.

## External links {#links}
* Simjacker attack: {{< extlink "https://www.adaptivemobile.com/blog/simjacker-frequently-asked-questions" "Simjacker – Frequently Asked Questions and Demos" >}}
* {{< extlink "https://www.exim.org/static/doc/security/CVE-2019-16928.txt" "CVE-2019-16928 in Exim" >}}
* {{< extlink "https://github.com/susam/mintotp" "MinTOTP" >}}
* {{< extlink "https://codeberg.org/infosechandbook/blog-content" "Content of infosec-handbook.eu on codeberg.org" >}}
