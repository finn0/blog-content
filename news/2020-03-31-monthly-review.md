+++
title = "Monthly review – March 2020"
author = "Benjamin"
date = "2020-03-31T20:53:07+02:00"
tags = [ "covid19", "lets-encrypt", "tls", "tor" ]
categories = [ "Monthly review" ]
ogdescription = "Our monthly review of March 2020."
slug = "2020-03-31-monthly-review"
aliases = [ "/blog/2020-3-monthly-review" ]
banner = "banners/monthly-review"
toc = true
syntax = true
news = true
+++

Each month, we publish a review that covers essential activities of the last 30 days. This month, we talk about coronavirus-related news, Let's Encrypt's certificate revocation, new attacks on hardware, issues with Tor Browser, and more.
<!--more-->
{{< rssbox >}}

## News of the month {#notm}
In March 2020, we read the following reports:

### 2019–20 coronavirus pandemic {#covid19}
Of course, the entire world talks about the current pandemic these days. Giving any medical advice is out-of-scope of our website. Follow the instructions of your national public health authorities, help people in need, and stay healthy (easier said than done).

In the digital world, different groups milk the topic for their interests. Criminals drop malware on faked virus maps, large organizations offer their services (temporarily) for free, and smaller groups try to convince people to use their "alternatives." The goal is to either hack you or get new customers.

Be aware of such activities. Check links sent by your family and friends, as usual. Think twice before you register new accounts or use new services. Look for missing or incomplete privacy policies (read ["GDPR: How to identify incomplete privacy policies?”]({{< ref "/blog/guide-privacy-policy.md" >}})). Keep in mind that so-called "alternatives" aren't necessarily better for your privacy and may expose lots of your personal data on the server. Don't act with precipitation.

We generally recommend that you read our post ["Tips for your cyber hygiene"]({{< ref "/blog/ecsm2019-cyber-hygiene.md" >}}), which we published last October.

### Let's Encrypt's certificate revocation
In February, Let's Encrypt (a widely-known non-profit certificate authority) announced that they issued their billionth certificate. Several days later, they found a bug in their CAA code. CAA means "Certification Authority Authorization." CAA is part of the server-side DNS configuration and tells certificate authorities if they are authorized to issue certificates for a given domain name (read our article on ["Server-side DNS security configuration"]({{< ref "/blog/wss5-dns-configuration.md#caa" >}})).

Due to the bug, Let's Encrypt didn't check CAA records within 8 hours before the issuance of a new certificate. This check is mandatory for CAs. Therefore, Let's Encrypt decided to revoke more than 3 million certificates in early March. Later, Let's Encrypt only revoked 1.7 million certificates, and decided that revoking more could be disruptive to web users. The investigation showed that Let's Encrypt issued 445 certificates despite CAA records that forbid the issuance.

We think this again shows that all of us heavily depend on the security of certificate authorities. Large parts of web security can be compromised if CAs fail to comply with standards.

### New attacks on hardware
In March, security researchers announced several further attacks on hardware:

* ["Take A Way: Exploring the Security Implications of AMD's Cache Way Predictors"]({{< relref "#links" >}}) describes new side-channel attacks affecting AMD CPUs from 2011 to 2019. The attacks (Collide+Probe and Load+Reload) exploit the L1D cache way predictor to access secret information. Interestingly, AMD stated that these aren't new attacks in any way.
* In the case of Intel, researches presented the Meltdown-based (and theoretical) ["Load Value Injection"]({{< relref "#links" >}}) attack. Intel released firmware updates that fix the vulnerability. The researches stated that the attack could also affect other CPUs.
* Finally, ["TRRespass"]({{< relref "#links" >}}) was titled "Return of the Rowhammer attack". Researches showed that most current DRAM modules are vulnerable to this attack, esp. DDR4 and LPDDR4.

In general, it is a good indication that researchers uncover more and more hardware vulnerabilities. It is also a good sign when hardware manufacturers timely provide security updates. When you choose hardware, keep in mind that many attacks on Intel CPUs, AMD CPUs, and other hardware components are purely theoretical. Numerous of these security vulnerabilities are either hard to exploit or can only be exploited locally (the attacker has to be in the same place as the CPU). Other attacks are only relevant for hosting companies when multiple virtual server instances share the same physical CPU.

### Firefox 74.0: The death and revival of TLS 1.0 and 1.1
Mozilla published [Firefox 74.0]({{< relref "#links" >}}) in March. As previously announced, they disabled TLS 1.0 and 1.1 by default. TLS 1.0 and 1.1 are widely considered insecure and shouldn't be used. However, there are still web servers and web applications that don't support TLS 1.2 or 1.3. Due to the current pandemic, Mozilla re-enabled TLS 1.0 and 1.1 for an indefinite period.

If you don't need TLS 1.0 and 1.1, you can manually disable it by changing the configuration of your Firefox. In Firefox, go to {{< kbd "about:config" >}}. Click "Accept the Risk and Continue." Search for "security.tls.version.min" and set it to {{< kbd "3" >}}. "3" means "TLS 1.2", "4" means "TLS 1.3." While using TLS 1.3 is the most secure option, many web servers and web applications don't support it at the moment.

Besides, Mozilla announced to disable FTP (File Transfer Protocol) in Firefox 77.

### Tor Browser's issues with JavaScript
The last report, which we selected for you, addresses a security vulnerability related to JavaScript in the Tor Browser. In some cases, the Tor Browser executes JavaScript even if you set the security level to "Safest." In Tor Browser 9.0.6, JavaScript remained enabled, while the developers tried to mitigate the vulnerability with a NoScript update. Later, they identified that JavaScript could still be executed on the "Safest" level.

Due to this, Tor Browser 9.0.7 is shipped with JavaScript disabled by default if you chose the "Safest" level. You can't temporarily enable JavaScript via NoScript. If you need JavaScript, you must change the configuration of your Tor Browser. However, re-enabling JavaScript can lead to unwanted execution on some websites.

Further, the updated version of HTTPS Everywhere introduced the EASE (Encrypt All Sites Eligible) mode. The developers of the Tor Browser explicitly state that you shouldn't enable EASE as this can be used to identify you.

### Data breaches and leaks
Moreover, there were some data breaches and leaks. Have I Been Pwned added information about the following breaches and leaks:

* AnimeGame (breached in February 2020)
* The Halloween Spot (breached in September 2019)
* PropTiger (breached in January 2018)
* Tamodo (breached in February 2020)
* Dueling Network (breached in March 2017)

Check if you were affected, and change your credentials.

## Tool of the month {#toolotm}
This month, we present [USBGuard]({{< ref "/blog/tool-usbguard.md" >}}) to protect against malicious USB devices.

## Our activities of the month {#ishotm}
In March, we published one article:

* [Social engineering: The story of Jessika]({{< ref "/blog/social-engineering-jessika.md" >}}): We gave another lecture on social engineering for Master students. This time, the students had to invade the privacy of the fictional character Jessika to exploit her.

Besides, we changed several things on our website:

* We started to improve the style, grammar, and wording of our articles. You can call it spring cleaning.
* We added [a link to key.openpgp.org]({{< ref "/contact-details.md#encrypted-e-mail" >}}) as another mirror for retrieving our current OpenPGP key.

## External links {#links}
* {{< extlink "https://community.letsencrypt.org/t/revoking-certain-certificates-on-march-4/114864" "Let's Encrypt: Revoking certain certificates on March 4" >}}
* {{< extlink "https://arstechnica.com/information-technology/2020/03/lets-encrypt-holds-off-on-revocation-of-certificates/" "Let’s Encrypt changes course on certificate revocation" >}}
* {{< extlink "https://mlq.me/download/takeaway.pdf" "Take A Way: Exploring the Security Implications of AMD's Cache Way Predictors" >}} (PDF file)
* {{< extlink "https://lviattack.eu/" "LVI - Hijacking Transient Execution with Load Value Injection" >}}
* {{< extlink "https://www.vusec.net/projects/trrespass/" "TRRespass" >}}
* {{< extlink "https://www.mozilla.org/en-US/firefox/74.0/releasenotes/" "Firefox 74.0 release notes" >}}
