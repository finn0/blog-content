+++
title = "Monthly review – January and February 2020"
author = "Benjamin"
date = "2020-02-29T11:22:09+01:00"
tags = [ "36c3", "sha1", "crlite", "kr00k", "cutycapt", "curl" ]
categories = [ "Monthly review" ]
ogdescription = "Our monthly review of January and February 2020."
slug = "2020-02-29-monthly-review"
aliases = [ "/blog/2020-2-monthly-review" ]
banner = "banners/monthly-review"
toc = true
syntax = true
news = true
+++

Each month, we publish a review that covers the most important activities of the last 30 days. This time, our monthly review actually covers the first two months of 2020. We talk about the 36C3, the end of SHA-1, CRLite in Firefox, the Kr00k attack, and more.
<!--more-->
{{< rssbox >}}

## News of the month {#notm}
In January and February 2020, we read the following news:

### 36C3
The 36th Chaos Communication Congress (36C3) took place in December 2019. In January 2020, recordings of most talks became available. We recommend the following talks if you didn't watch them so far:

* [SIM card technology from A-X]({{< relref "#links" >}})
* [How to Break PDFs]({{< relref "#links" >}})
* [Cryptography demystified]({{< relref "#links" >}})
* [Ansible all the Things]({{< relref "#links" >}}) (German)

There is also an [InfoSec survival guide]({{< relref "#links" >}}) that could be helpful.

### The end of SHA-1 {#sha1}
One big (but already expected) story was [SHA-1 is a Shambles]({{< relref "#links" >}}) in January. Researches demonstrated the first chosen-prefix collision for SHA-1, for instance, an [impersonation attack]({{< ref "/glossary.md#impersonation" >}}) using [GnuPG]({{< ref "/glossary.md#gnupg" >}}) and SHA-1.

In summary, don't use SHA-1 for any cryptographic purposes anymore. Use SHA-2, SHA-3, or Poly1305 if available.

### Firefox tries CRLite {#firefox-crlite}
In January, Mozilla announced to introduce CRLite in Firefox Nightly. Revocation of [digital certificates]({{< ref "/glossary.md#certificate" >}}) is still a big problem for today's internet security. Only a small number of web browsers support standards like [OCSP]({{< ref "/glossary.md#ocsp" >}}), and each standard comes with disadvantages. On the other hand, traditional Certificate Revocation Lists (CRLs) are too big and too static for reliable revocation information.

CRLite is a new technology proposed by a group of researches in May 2017. [Mozilla published several blog articles on Firefox and its experimental use of CRLite]({{< relref "#links" >}}). Their results show that CRLite seems to be 99% faster of the time when compared with OCSP so that CRLite might replace OCSP in Firefox in the future.

### OpenSSH supports U2F {#openssh-u2f}
In February, [OpenSSH 8.2 was released]({{< relref "#links" >}}). This release introduced support for [U2F]({{< ref "/glossary.md#u2f" >}}). For U2F, OpenSSH 8.2 adds new public key types ("ecdsa-sk" and "ed25519-sk"). Furthermore, they removed a key signature algorithm due to the first chosen-prefix collision for SHA-1.

We will update our [Web server security series]({{< ref "/as-wss.md" >}}) accordingly in early 2020.

### Kr00k – a new attack on WPA2 {#kr00k}
Finally, some researches presented their new [Kr00k attack on current WPA2 encryption for Wi-Fi]({{< relref "#links" >}}) (CVE-2019-15126). Under certain conditions, Wi-Fi chips manufactured by Broadcom and Cypress can be forced to use an all-zero encryption key for WPA2-Personal and WPA2-Enterprise. This attack affects the latest AES-CCMP encryption.

Since an attacker needs to trigger disassociations repeatedly, this attack is very likely detected by the user due to continuously losing the Wi-Fi connection. Network traffic that is already encrypted on higher layers (like TLS or SSH) isn't affected by this attack.

Some affected devices were Amazon's Echo (2nd generation), Apple's iPhone (6, 6S, 8, XR), and the Raspberry Pi 3. If affected, install system updates as soon as possible.

### Data breaches and leaks
Moreover, there were some data breaches and leaks. Have I Been Pwned added information about the following breaches and leaks:

* Universarium (breached in November 2019)
* Indian Railways (leaked in November 2019)
* Go Games (breached in October 2015)
* BtoBet (breached in December 2019)
* Planet Calypso (breached in July 2019)
* europa.jobs (breached in August 2019)
* Tout (breached in September 2014)
* DailyObjects (breached in January 2018)
* MGM Resorts (breached before July 2019)
* Slickwraps (breached in February 2020)
* Straffic (leaked in February 2020)

Check if you were affected, and change your credentials.

## Tool of the month {#toolotm}
This time, we look at CutyCapt. CutyCapt is a handy tool that uses WebKit's rendering to store snapshots of websites in different formats (e.g., PNG, JPG, PDF). This tool requires Qt 4.4.0 or higher. It should be available for most operating systems. For instance, on Arch Linux, you find it in the AUR. Kali Linux recently added this tool to their repository.

To store a snapshot of a website, enter {{< kbd "cutycapt --url=[a-website] --out=[file.png]" >}} in your terminal. For example, the following command stores a snapshot of our website's landing page: {{< kbd "cutycapt --url=https://infosec-handbook.eu --out=ish-snapshot.png" >}}.

You may realize that this snapshot only has a narrow width. You can change the width by setting the "--min-width" flag. We change the command to: {{< kbd "cutycapt --url=https://infosec-handbook.eu --min-width=1200 --out=ish-snapshot.png" >}}. Now, the width of the snapshot is at least 1200px.

CutyCapt also supports setting HTTP methods, headers, the user-agent, and zooming. You can even enable and disable JavaScript and Java execution. To see all supported commands, enter {{< kbd "cutycapt --help" >}}.

## Tip of the month {#tipotm}
This month's tip is about detecting "report-to" and "report-uri" directives using curl. curl is a popular tool for transferring data using various network protocols. It is a Swiss Army knife, and you likely have curl already installed on your system.

Several HTTP response headers allow server admins to set the (older) "report-uri" or the (upcoming) "report-to" (W3C Reporting API) directives. As soon as a web client with support for these directives comes across such headers and produces an event (e.g., violation of the CSP, network-level error), the web client transparently sends data to a (third-party) reporting server (e.g., report-uri.com or uriports.com). This data includes the client's IP address, User-Agent, and other protocol-/error-specific information. "Transparently" means that network traffic to the reporting server is invisible for most users. They must actively enable the developer options of their web browser or monitor their network traffic with other tools to detect this traffic.

Therefore, you should consider detecting such potential behavior when it comes to privacy and data protection. We suggested such detection for the online assessment tool Webbkoll in October 2019.

At the moment, you can simply detect these directives using curl on your local client. Open your terminal and enter {{< kbd "curl -I -Ss [url] | grep report" >}}. This command means:

* "-I": Tells curl to only fetch the HTTP header using the HTTP method HEAD.
* "-Ss": Hides the progress meter of curl, but tells curl to print any errors that occur.
* "grep report": Looks at the output and only prints lines that contain "report."

If you get any output, the web server sets one or both directives. This means that your "normal" web client (e.g., Chrome, Firefox, Safari) may send any error reports to the URL defined by these directives.

If there is no output, the web server didn't set these directives. So there won't be any report-to/report-uri data transmissions in the background.

Three examples:

* {{< kbd "curl -I -Ss https://infosec-handbook.eu | grep report" >}}: There should be no output since we didn't set the directives.
* {{< kbd "curl -I -Ss https://scotthelme.co.uk/ | grep report" >}}: You should see the Content Security Policy, the Network Error Logging (NEL) header, the X-XSS-Protection header, and the Report-To header. The directives point to report-uri.com, one of the common third-party services that collect such reports.
* {{< kbd "curl -I -Ss https://cyberciti.biz | grep report" >}}: You should see the Expect-CT header. This server sends errors related to this header to cloudflare.com.

Several server operators claim that these directives are purely security-relevant and don't affect privacy at all. However, for us, this is another data transmission. If third parties are involved, the website should mention and explain this in their privacy policy.

Keep in mind that such reports are only sent if there is some kind of violation detected. For example, the web server's Content Security Policy tells your web browser that the execution of JavaScript is forbidden. If there is JavaScript on the website (either added by the web server itself or injected in your client), your client recognizes the violation and send a report to the specified collector.

## Our activities of the last two months {#ishotm}
In January and February, we did not publish any articles. For us, 2020 started with a lot of security work (network traffic analysis, redesign, and reconfiguration of production networks, awareness training, further education, and more) in different European countries (Austria, Germany, UK, Czech Republic, Belgium).

Due to this, we had to reduce our activities on our website. Reduced activity also means that we—unfortunately again—have to postpone the significant updates of our [Web server security series]({{< ref "/as-wss.md" >}}) and [Home network security series]({{< ref "/as-hns.md" >}}). However, we documented all planned changes and won't forget this. There are also many requests by our readers for content to be added so that we won’t die of boredom. 😉

## External links {#links}
* 36C3 talk: {{< extlink "https://media.ccc.de/v/36c3-10737-sim_card_technology_from_a-z" "SIM card technology from A-X" >}}
* 36C3 talk: {{< extlink "https://media.ccc.de/v/36c3-10832-how_to_break_pdfs" "How to Break PDFs" >}}
* 36C3 talk: {{< extlink "https://media.ccc.de/v/36c3-10627-cryptography_demystified" "Cryptography demystified" >}}
* 36C3 talk: {{< extlink "https://media.ccc.de/v/36c3-90-ansible-all-the-things" "Ansible all the Things" >}}
* 36C3 guide: {{< extlink "https://events.ccc.de/congress/2019/wiki/index.php/Static:Information_Security" "InfoSec survival guide" >}}
* {{< extlink "https://sha-mbles.github.io/" "SHA-1 is a Shambles" >}}
* {{< extlink "https://blog.mozilla.org/security/tag/crlite/" "CRLite articles on blog.mozilla.org" >}}
* {{< extlink "https://obj.umiacs.umd.edu/papers_for_stories/crlite_oakland17.pdf" "CRLite scientific paper: CRLite: A Scalable System for Pushing All TLS Revocations to All Browsers (PDF file)" >}}
* {{< extlink "https://www.openssh.com/txt/release-8.2" "OpenSSH 8.2 changelog" >}}
* {{< extlink "https://www.welivesecurity.com/wp-content/uploads/2020/02/ESET_Kr00k.pdf" "Kr00k attack on WPA2 (PDF file)" >}}
