+++
title = "Firefox 80: Improved HTTPS-Only settings"
author = "Benjamin"
date = "2020-08-26T19:01:07+02:00"
lastmod = "2020-11-18"
tags = [ "firefox", "https" ]
ogdescription = "Firefox 80 adds improved HTTPS-Only settings."
slug = "2020-08-26-firefox80-https-only"
banner = "banners/ish-news"
news = true
syntax = true
+++

In Firefox 76, Mozilla added an experimental HTTPS-Only Mode that could replace add-ons like HTTPS Everywhere. You can go to {{< kbd "about:config" >}} and set "dom.security.https_only_mode" to {{< kbd "True" >}}. If you try to open an HTTP-only website, Firefox shows a warning message that no HTTPS is available.

In Firefox 80, released a short while ago, Mozilla introduced a new setting called "browser.preferences.exposeHTTPSOnly." After setting it to {{< kbd "True" >}}, you will see new settings in the preferences of Firefox. The HTTPS-Only Mode lets you enable in all windows, only in private windows, or disable it entirely.

{{< webpimg "news-img/2020-08-26-firefox80-https-only-1.png" "The new HTTPS-Only Mode in the Firefox settings" "the HTTPS-Only Mode settings in Firefox 80." >}}

In Firefox 83, Mozilla officially released the feature for everybody: {{< extlink "https://blog.mozilla.org/security/2020/11/17/firefox-83-introduces-https-only-mode/" "Firefox 83 introduces HTTPS-Only Mode" >}}.

If a website only supports HTTP, you can temporarily or permanently add the website to a list of exceptions. For the exceptions, the global settings of the HTTPS-Only Mode are ignored.

## Changelog
* Nov 18, 2020: Added information about the final feature release.
