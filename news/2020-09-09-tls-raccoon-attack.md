+++
title = "Raccoon Attack: A timing vulnerability in the TLS 1.2 specification"
author = "Benjamin"
date = "2020-09-09T19:06:07+02:00"
lastmod = "2020-09-10"
tags = [ "openssl", "tls", "https" ]
ogdescription = "Raccoon is a newly-discovered vulnerability in the TLS specification."
slug = "2020-09-09-tls-raccoon-attack"
banner = "banners/ish-news"
news = true
+++

Researchers presented the "Raccoon Attack" that exploits a weakness in the TLS 1.2 (or below) specification to decrypt TLS-protected network traffic.

While this may sound critical, it only affects specific TLS cipher suites, and the actual exploitability varies from implementation to implementation. The affected TLS cipher suites are static Diffie-Hellman (DH) cipher suites, or ephemeral DH (EDH or DHE) cipher suites if the ephemeral keys are reused.

Elliptic-curve Diffie–Hellman and TLS 1.3 aren't affected.

## OpenSSL (CVE-2020-1968) {#openssl}
OpenSSL confirmed the following:

* OpenSSL 1.1.1 isn't vulnerable to the Raccoon Attack.
* OpenSSL 1.1.0 is out of support. It may be vulnerable.
* OpenSSL 1.0.2f and above are vulnerable if a static DH cipher suite is used. The affected cipher suites start with "TLS_DH_."
* OpenSSL 1.0.2e and below are vulnerable unless the "SSL_OP_SINGLE_DH_USE" was configured. The vulnerability affects EDH- and DH-based cipher suites, beginning with "TLS_DH_" or "TLS_DHE_."

OpenSSL 1.0.2w (only available for premium support customers) moves the affected cipher suites to the weak cipher suites that aren't compiled by default.

## Other TLS implementations
* BearSSL and BoringSSL aren't affected as they don't support the vulnerable cipher suites.
* Botan, Mbed TLS, and s2n aren't affected as they don't support static DH cipher suites and don't allow DHE key reuse.
* It is currently unknown whether other implementations like GnuTLS are also affected.

## What to do?
As mentioned above, most servers won't be affected by this. Keep the software on your server up-to-date and enable strong TLS cipher suites only.

If you operate legacy systems that need affected implementations, you should at least ensure that DHE key reuse is disabled. You should disable static DH completely.

* {{< extlink "https://raccoon-attack.com/" "Raccoon Attack (main page)" >}}
* {{< extlink "https://raccoon-attack.com/RacoonAttack.pdf" "Raccoon Attack: Finding and Exploiting Most-Significant-Bit-Oracles in TLS-DH(E)" >}} (PDF file)
* {{< extlink "https://www.openssl.org/news/secadv/20200909.txt" "OpenSSL Security Advisory – Raccoon Attack (CVE-2020-1968)" >}}

## Changelog
* Sep 10, 2020: Added information regarding BearSSL, BoringSSL, Botan, Mbed TLS, and s2n.
