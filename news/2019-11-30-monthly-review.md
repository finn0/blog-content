+++
title = "Monthly review – November 2019"
author = "Benjamin"
date = "2019-11-30T14:10:00+01:00"
tags = [ "rcs", "vnc", "ultravnc" ]
categories = [ "Monthly review" ]
ogdescription = "Our monthly review of November 2019."
slug = "2019-11-30-monthly-review"
aliases = [ "/blog/2019-11-monthly-review" ]
banner = "banners/monthly-review"
toc = true
news = true
+++

Each month, we publish a review that covers essential activities of the last 30 days. This month, we talk about attacks on RCS, security vulnerabilities in VNC software, a massive data leak, and more.
<!--more-->
{{< rssbox >}}

## News of the month {#notm}
In November 2019, we read the following news:

### Attacks on the SMS successor "RCS"
SRLabs, the German company that offers SnoopSnitch and SIMtester, [published a new report]({{< relref "#links" >}}) about attacks on RCS (Rich Communication Services). RCS is a communication protocol that aims to replace SMS in the future. It brings new features and is already supported by dozens of mobile phone carriers around the globe.

Their report shows that during the provisioning process, attackers can take over user accounts by stealing RCS configuration files and injecting other credentials. There are some other issues like insufficient validation of user identities, user tracking, and possible caller ID spoofing. The exploitability of these vulnerabilities largely depends on your mobile phone carrier. After the Simjacker attack, these vulnerabilities again show that you can't fully control your smartphone.

### Security vulnerabilities in VNC software
VNC (Virtual Network Computing) is a desktop sharing system that allows you to control other computers remotely. It is based on the RFB (Remote Frame Buffer) protocol. RFB itself isn't a secure protocol. The current version 3.8, released in 2007, only supports a very weak authentication scheme, and there is no encryption.

This month, Kaspersky Lab [released a report]({{< relref "#links" >}}) on 37 security vulnerabilities in LibVNC, TightVNC 1, TurboVNC, and UltraVNC. Some of the vulnerabilities resided in the open-source code for many years, again [showing that open-source software isn't automatically more secure than closed-source software]({{< ref "/blog/myths-software-security.md#m1" >}}). Some of the reported vulnerabilities are already publicly known for several months, as written in ["UltraVNC – a security nightmare."]({{< ref "/blog/uvnc-vulnerabilities.md" >}})

If you are a user of VNC, check whether you are affected and install updates if available. Keep in mind that you could be affected if your VNC implementation uses LibVNC. Besides, TightVNC 1 is unsupported and won't get any update.

### Massive data leak
Finally, Data Viper, a threat intelligence platform, [released a report]({{< relref "#links" >}}) about a massive data leak that likely originated from People Data Labs and OxyData. PDL and OxyData are so-called data enrichment companies that collect data about people from different sources, combine it, and sell it to their customers.

The leak allegedly contains data about 1.2 billion unique people. Have I Been Pwned added 622 million affected e-mail addresses to their databases. While there are no credentials included, such data (e-mail addresses, phone numbers, social media profiles, and job history data) can be misused for [spear phishing]({{< ref "/glossary.md#spear-phishing" >}}) and [impersonation]({{< ref "/glossary.md#impersonation" >}}).

### Data breaches and leaks
Moreover, there were some data breaches. Have I Been Pwned added information about the following breaches:

* Vedantu (breached in mind-2019)
* ToonDoo (breached in August 2019)
* GPS Underground (breached in early 2017)
* EpicBot (breached in September 2019)
* GateHub (breached in October 2019)
* People Data Labs (leaked in October 2019), [see above]({{< relref "#huge-data-leak" >}})

Check if you were affected, and change your credentials.

## Tool of the month {#toolotm}
This month, we present [fscrypt]({{< ref "/blog/tool-fscrypt.md" >}}) to manage Linux filesystem encryption.

## Readers' questions of the month {#rqotm}
We share some of our replies to frequent questions of our readers:

### "Why do we see Elasticsearch leaks all the time?"
Internal services unknowingly exposed to the internet are the problem. Elasticsearch is only one example. There are also exposed IP cameras, PLCs in industrial environments, and office printers. Regularly scan your public and private IP address ranges to detect open ports and services and apply usual security practices. Never just assume something is configured – check and document it.

### "Which cloud storage provider do you recommend?"
We don't recommend any specific cloud storage provider. Some providers may offer additional server-side encryption of some kind; however, you can never be sure about this if you don't control the server. Keep in mind that TLS encryption is only relevant for encrypting/authenticating data between your computer and the remote server. TLS doesn't protect your uploaded data on the server. You may use tools like [Cryptomator]({{< relref "#links" >}}) to encrypt your data locally before sending it to the server.

### "Which search engine do you recommend?"
There are no real benefits of switching your search engine. In all cases, you can't check if a search engine doesn't track you. You must always trust the statements of the provider. The most important feature of a search engine is likely to find answers for your search query. Some people favor metasearch engines like MetaGer or SearX, while others state that metasearch engines are unusable. Others like to use DuckDuckGo or Startpage. So we can't recommend anything special here.

Just send us your questions. Maybe, we answer your question in the next monthly review.

## Our activities of the month {#ishotm}
In November, we published one new tutorial:

* [How to redact sensitive information with LibreOffice 6.3+]({{< ref "/blog/libreoffice63-redaction.md" >}}): This tutorial shows the risks of leaking information in documents and the new redaction feature of LibreOffice 6.3.

Besides, we changed several things on our website:

* We updated our policies ([privacy policy]({{< ref "privacy-policy.md" >}}), [security/disclosure policy]({{< ref "security.md" >}})) to improve readability and to provide more details.
* We centralized Git pushes to codeberg.org. Due to this, we can provide OpenPGP-signed and verified Git repositories for transparency. (Beforehand, our commits were already signed, but the key wasn't connected to the account.)
* We added information on how to [support the InfoSec Handbook]({{< ref "/support-us.md" >}}).
* Hugo, the tool we use to generate our static website, introduced a new Markdown parser. Due to this, we had to change the layout of several pages. Furthermore, we re-enabled the automatic rendering of the table of contents on most pages. If you spot any strangely-looking content on our website, [just let us know]({{< ref "/contact-details.md" >}}). We will happily fix it!

## Closing words {#cw}
November is already over. Our original plan was to update some articles of our [Web server security series]({{< ref "/as-wss.md" >}}) this month, but there was no time for this left. So we again plan to release some content updates in December.

We will also check our [Home network security series]({{< ref "/as-hns.md" >}}) and plan future articles for this series.

Other current activities are:

* Verena does some web accessibility checks and will write down suggestions to improve it,
* Benjamin improves our server infrastructure (we will post an article about this), and
* Jakub prepares some new articles on "security myths."

## External links {#links}
* RCS attacks: {{< extlink "https://srlabs.de/bites/rcs-hacking/" "New RCS technology exposes most mobile users to hacking" >}}
* VNC security vulnerabilities: {{< extlink "https://ics-cert.kaspersky.com/reports/2019/11/22/vnc-vulnerability-research/" "VNC vulnerability research" >}}
* PDL data leak: {{< extlink "https://www.dataviper.io/blog/2019/pdl-data-exposure-billion-people/" "Personal And Social Information Of 1.2 Billion People Discovered In Massive Data Leak" >}}
* {{< extlink "https://cryptomator.org/" "Cryptomator" >}}
