+++
title = "TLS usage of InfoSec Handbook readers"
author = "Jakub"
date = "2020-08-02T18:10:18+02:00"
tags = [ "infosechandbook", "tls" ]
ogdescription = "We provide insight into TLS usage of our readers."
slug = "2020-08-02-tls-insights"
banner = "banners/ish-news"
news = true
syntax = true
+++

We logged the TLS versions and cipher suites of recent client requests to see the share of different versions and cipher suites.

| # of requests | TLS version | TLS cipher suite              |
|---------------|-------------|-------------------------------|
| 129,258       | TLS 1.3     | TLS_AES_256_GCM_SHA384        |
| 74,328        | TLS 1.3     | TLS_AES_128_GCM_SHA256        |
| 43,814        | TLS 1.2     | ECDHE-ECDSA-AES256-GCM-SHA384 |
| 17,166        | TLS 1.2     | ECDHE-ECDSA-AES128-GCM-SHA256 |
| 4,870         | TLS 1.3     | TLS_CHACHA20_POLY1305_SHA256  |
| 1,816         | TLS 1.2     | ECDHE-ECDSA-CHACHA20-POLY1305 |

Some interesting numbers:

* Already **77%** of all client requests used **TLS 1.3**.
* **98%** of the client requests used **AES** instead of ChaCha20.
* **64%** of the client requests used **AES-256-GCM**.

Our web server doesn't enforce server-side cipher preference since we only allow a small set of strong TLS cipher suites that are considered secure. Client-side cipher preference is also recommended by Mozilla for modern TLS configuration: {{< extlink "https://wiki.mozilla.org/Security/Server_Side_TLS#Modern_compatibility" "Mozilla Security/Server Side TLS: Modern compatibility" >}}

{{< webpimg "news-img/2020-08-02-tls-insights-1.png" "TLS usage of our readers" "a pie chart that shows the TLS usage of our readers." >}}

Tip: If you log cipher suites and get one request per line, you can sort the output with {{< kbd "sort [tls-logfile] | uniq -c | sort -bgr" >}}.
