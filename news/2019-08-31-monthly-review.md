+++
title = "Monthly review – August 2019"
author = "Benjamin"
date = "2019-08-31T15:56:21+02:00"
tags = [ "knob", "bluetooth", "dejablue", "gnupg" ]
categories = [ "Monthly review" ]
ogdescription = "Our monthly review of August 2019."
slug = "2019-08-31-monthly-review"
aliases = [ "/blog/2019-8-monthly-review" ]
banner = "banners/monthly-review"
toc = true
syntax = true
news = true
+++

Each month, we publish a review that covers essential activities of the last 30 days. This month, we talk about the KNOB attack, DejaBlue, and more.
<!--more-->
{{< rssbox >}}

## News of the month {#notm}
In August 2019, two major [security vulnerabilities]({{< ref "/glossary.md#vulnerability" >}}) were disclosed:

### KNOB attack
The [KNOB attack]({{< relref "#links" >}}) (CVE-2019-9506) likely affects all Bluetooth devices (except devices using Bluetooth Low Energy, or BLE). The vulnerability allows attackers to inject packages during the negotiation process of the encryption key to enforce only 1 byte of [entropy]({{< ref "/glossary.md#entropy" >}}) used for encrypting Bluetooth traffic. Attackers must be in the range of the Bluetooth devices. Since attackers force both devices to use smaller but valid key sizes, it is a [downgrade attack]({{< ref "/glossary.md#downgrade-attack" >}}). Afterward, the remaining key space can be easily [brute-forced]({{< ref "/glossary.md#brute-force-attack" >}}) to decrypt all of the traffic between the devices during that session. Countermeasures are:

* Enforcing 16 bytes of entropy in the Bluetooth firmware (requires firmware update),
* checking the key size upon connection (requires software update),
* securing communication on the layers above.
* Besides, you can use BLE devices only, or turn of Bluetooth if not in use.

### DejaBlue
Then, we had [DejaBlue]({{< relref "#links" >}}) (CVE-2019-1181, CVE-2019-1182, CVE-2019-1222, CVE-2019-1226), a newly-discovered BlueKeep-like security vulnerability in the Remote Desktop Services (RDS) of current versions of Microsoft Windows. An unauthenticated attacker can send specially crafted packets to a vulnerable host to execute arbitrary code on the host without any user interaction. Microsoft released patches to address the vulnerability. Besides patching, you can turn off Remote Desktop Services, block TCP port 3389 in your firewall, and enable Network Level Authentication (NLA). Affected are:

* Windows 7 SP1 (only affected if either RDP 8.0 or RDP 8.1 is installed),
* Windows 8.1 and Windows RT 8.1,
* Windows 10,
* Windows Server 2008 R2 SP1 (only affected if either RDP 8.0 or RDP 8.1 is installed),
* Windows Server 2012 and Windows Server 2012 R2,
* Windows Server 2016,
* Windows Server 2019,
* and all Windows-10-based Windows Server editions.

### Data breaches and leaks
Moreover, there were also some data breaches. Have I Been Pwned added information about the following breaches:

* CafePress (breached in February 2019)
* Canva (breached in May 2019)
* StockX (breached in July 2019)
* Cracked.to (breached in July 2019)
* Chegg (breached in April 2018)
* Coinmama (breached in August 2017)

Check if you were affected, and change your credentials.

## Tool of the month {#toolotm}
This month, we present [Minisign]({{< ref "/blog/tool-minisign.md" >}}) to sign and verify files.

## Tip of the month {#tipotm}
This month, our tip is about faster generation of [OpenPGP]({{< ref "/glossary.md#openpgp" >}}) keys using [GnuPG]({{< ref "/glossary.md#gnupg" >}}). We regularly change our GnuPG keys. Instead of generating each key manually, we use the following command (gpg 2.2.17):

{{< kbd "gpg --yes --quick-gen-key '[your-name] <[your-e-mail-address]>' future-default default $(date --iso-8601 --date='3 months')" >}}

Let's look at the command in detail:

* "--yes": Tells GPG to proceed even if there is already a key for the provided ID.
* "--quick-gen-key": This generates a standard key with one user ID. "quick" means that there is less user interaction required.
  * "'[your-name] <[your-e-mail-address]>'": This is your user ID (name and e-mail address).
  * "future-default default": "future-default" is an alias for the algorithm that will likely be used as the default algorithm in future versions of GPG. Currently, "future-default" means [Ed25519]({{< ref "/glossary.md#ed25519" >}}) for signing, and [Curve25519]({{< ref "/glossary.md#curve25519" >}}) for encryption. "default" also creates a subkey for encryption.
  * "$(date --iso-8601 --date='3 months')": This runs "date" and returns an ISO 8601 formatted date (in 3 months from now). The result is an OpenPGP key that is valid for three months.

In summary, this command allows you to create a fresh OpenPGP key pair using modern algorithms quickly.

## Readers' questions of the month {#rqotm}
We share some of our replies to frequent questions of our readers:

### "Is there any downside of setting legacy HTTP headers?"
There shouldn't be any downsides. The biggest problem is the false sense of security: Server admins set headers like X-Frame-Options, X-Xss-Protection, or HPKP, because some outdated online guides mention them. At the same time, every modern web browser ignores these headers.

### "Which operating system do you use at home?"
Most of us use Linux (Arch, Ubuntu 18.04, Debian 10, Parrot OS).

### "Which e-mail provider do you recommend, or should I host my own mail server?"
Keep in mind that 'hosting yourself' always means continuous monitoring, fast and regular updating, and much more. If you can't ensure this or if you want convenience, use a well-known provider. We use mailbox.org, Tutanota, and Protonmail.

### "Do you plan to continue your series about the OpenWRT-based router Turris Omnia?"
Yes, we continue the series next year. We still think that this is one of the best routers you can get.

### "I want to become a security professional. How do I start?"
Read [Your career in information security]({{< ref "/blog/infosec-career.md" >}}).

Just send us your questions. Maybe, we answer your question in the next monthly review.

## Our activities of the month {#ishotm}
In August, we published four new articles and updated many others. New articles are:

* [The false sense of security]({{< ref "/blog/discussion-false-sense.md" >}}): In this article, we discuss three typical situations that result in a false sense of security.
* [The current state of the LineageOS-based /e/ ROM]({{< ref "/blog/e-foundation-second-look.md" >}}): This is a follow-up [of our first look]({{< ref "/blog/e-foundation-first-look.md" >}}) in March, requested by many readers.
* [Binary thinking is for boolean data types, not for information security or privacy]({{< ref "/blog/discussion-filter-bubbles.md" >}}): Here, we addressed the "binary thinking" of several people that results in filter bubbles, and neither improves information security nor privacy.
* [Web server security – Part 8: Basic log file analysis]({{< ref "/blog/wss8-log-file-analysis.md" >}}): In part 8 of our [Web server security series]({{< ref "/as-wss.md" >}}), we present a tool to analyze log files more easily.

Besides, we fixed a bug that occurred in Chrome, Chromium, and Edge by adding a CSS-based workaround. Chroma-based syntax highlighting seems to be broken in either Hugo or Chroma at the moment. Moreover, we added some new tools to our [terminal tips]({{< ref "/terminal-tips.md" >}}) page. Another change was migrating our mailbox from Protonmail to Tutanota.

Finally, a responsible reader from Canada privately reported several files on infosec-handbook.eu that contained metadata which shouldn't be there. This person received about 430 Lumens (€25) for observing the [testing requirements and code of conduct]({{< ref "/security.md#tr-and-coc" >}}) of [our disclosure policy]({{< ref "/security.md#disclosure-policy" >}}). Thanks again.

## Closing words {#cw}
In September, we will mainly proceed to add information for nginx and Caddy users to our [Web server security series]({{< ref "/as-wss.md" >}}) since our poll "Which web server software do you prefer?" showed that many people are using nginx. On the other hand, Caddy seems to be a good choice for a web server in certain use cases. Thanks to everybody who voted this time.

## External links {#links}
* KNOB attack: {{< extlink "https://knobattack.com/" "Key Negotiation of Bluetooth Attack: Breaking Bluetooth Security" >}}
* DejaBlue: {{< extlink "https://portal.msrc.microsoft.com/en-US/security-guidance/advisory/CVE-2019-1181" "CVE-2019-1181" >}}, {{< extlink "https://portal.msrc.microsoft.com/en-US/security-guidance/advisory/CVE-2019-1182" "CVE-2019-1182" >}}, {{< extlink "https://portal.msrc.microsoft.com/en-US/security-guidance/advisory/CVE-2019-1222" "CVE-2019-1222" >}}, and {{< extlink "https://portal.msrc.microsoft.com/en-US/security-guidance/advisory/CVE-2019-1226" "CVE-2019-1226" >}}
