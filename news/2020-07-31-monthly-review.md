+++
title = "Monthly review – July 2020"
author = "Benjamin"
date = "2020-07-31T09:43:07+02:00"
tags = [ "signal", "ama" ]
categories = [ "Monthly review" ]
ogdescription = "Our monthly review of July 2020."
slug = "2020-07-31-monthly-review"
aliases = [ "/blog/2020-7-monthly-review" ]
banner = "banners/monthly-review"
toc = true
notice = true
news = true
+++

Each month, we publish a review that covers essential activities of the last 30 days. This month, we talk about the discussions regarding the Signal PIN, our AMA event, and upcoming content.
<!--more-->
{{< rssbox >}}

## The Signal PIN
This month, people started discussing the Signal PIN. In this Monthly Review, we dedicate our news section to these discussions to clarify some points.

### What is the Signal PIN?
[The Signal PIN]({{< relref "#links" >}}) is a password for your Signal account. At the moment, your Signal account uses a telephone number as an identifier; however, Signal plans to change this in the future. To support accounts that aren't based on phone numbers, Signal needs to store your contacts. Currently, Signal uses your phone's address book to do so. However, this isn't possible when migrating to the new model. The new model stores your encrypted profile, settings, and contacts on Signal servers. Contrary to this, your message history isn't stored on their servers.

### What is all the recent fuss about?
For server-side protection, Signal relies on [Intel SGX (Software Guard Extensions)]({{< relref "#links" >}}). A recent security vulnerability ([SGAxe]({{< relref "#links" >}})) demonstrated that vulnerable Signal servers could learn about contacts of Signal users and reduce the security of a user account to the strength of the Signal PIN. Since the minimum length of the PIN is four digits, some people argued that Signal accounts "can be hacked instantly." Ultimately, this means a server-side attacker could see all contacts of a Signal user, their profile information, and settings.

### What needs to be considered?
To decrypt the user data, the attacker needs to extract the hashed user PIN from the main memory of unpatched Signal servers. Furthermore, users need to set weak PINs because it remains infeasible to guess strong PINs. Some statements seem to simplify the whole attacker's perspective significantly so that Signal appears "completely insecure." It is quite evident that this isn't a trivial attack.

### What is the solution?
As an average user, you should set a strong PIN. This rule is valid for any service that requires passwords. In the best case, you use a password manager like [KeePassXC]({{< ref "/blog/keepassxc-password-management-basics.md" >}}) to create a long password or passphrase and store it securely.

If it doesn't matter that you may lose access to your Signal account when losing your device, Signal introduced an option to disable the Signal PIN. In this case, encrypted data is still stored on Signal servers; however, the user-provided PIN is replaced by a strong key that never leaves the user's device. The strong key ensures that server-side parties can't decrypt user data, even if the SGX protection fails.

### What is bad about the whole story?
Some people started to convince Signal users to switch to messengers that come with no server-side protection. To put it in perspective: Signal stores some encrypted data on their servers, while many other messengers store much more data on their servers in cleartext (including the complete message history). In our opinion, there is absolutely no reason to panic.

## Data breaches and leaks
In July 2020, the total number of breached accounts on Have I Been Pwned **exceeded 10 billion**. The latest breaches are:

* Wattpad (breached in June 2020)
* Promo (breached in July 2020)
* Dave (breached in June 2020)
* Hurb (breached in March 2019)
* Drizly (breached in July 2020)
* Dunzo (breached in June 2019)
* Chatbooks (breached in March 2020)
* Scentbird (breached in June 2020)
* Appen (breached in June 2020)
* Swvl (breached in June 2020)

Check if you were affected, and change your credentials.

## Our activities of the month {#ishotm}
### Ask Me Anything {#ama-july-2020}
From July 13 to July 23, we hosted an "Ask Me Anything" (AMA; technically "Ask Us Anything") event. We answered more than 200 questions. [50 categorized questions and answers](/categories/ask-us-anything/) are available in three articles.

All in all, this was our first AMA event, and we enjoyed the vast amount of good questions and inspiring discussions with our readers.

### New and updated content
In July, we published one article:

* [KeePassXC for beginners – setup and basic usage]({{< ref "/blog/keepassxc-password-management-basics.md" >}}): Some of our readers asked for a basic tutorial on setting up KeePassXC. We satisfied their wish and hope that more people migrate from weak passwords, stored in their brains, to strong passwords, stored in a password database.

Besides, we changed several things on our website:

* We updated our [Recommendations]({{< ref "/recommendations.md" >}}): We added "Darknet Diaries" (a bi-weekly podcast about "hacker" stories), a new book (CompTIA Network+ Certification All-in-One Exam Guide), and KeePassXC as an alternative to KeePass 2.
* We switched to inclusive terms. For instance, we now use "allowlist" instead of "whitelist." We know that this doesn't make sense for some people; however, nobody is hurt by this, and terms change all the time. 😉
* For admins: We disabled the "Permissions-Policy" (formerly named "Feature-Policy") due to no real client-side support. Besides, we added the "Cross-Origin-Opener-Policy" for testing purposes.

## Closing words {#cw}
In August, we work on integrating a news section into our website, [as explained in last month's Monthly Review]({{< ref "/news/2020-06-30-monthly-review.md#2020-spring-cleaning-part-2" >}}). **Update (August 2020):** We added a ["News" section](/news/).

## External links {#links}
* {{< extlink "https://support.signal.org/hc/en-us/articles/360007059792-Signal-PINs" "Signal PINs" >}}
* {{< extlink "https://en.wikipedia.org/wiki/Software_Guard_Extensions" "Intel SGX" >}}
* {{< extlink "https://sgaxe.com/files/SGAxe.pdf" "SGAxe: How SGX Fails in Practice" >}} (PDF file)
