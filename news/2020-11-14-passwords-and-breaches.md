+++
title = "'What breach?'"
author = "Benjamin"
date = "2020-11-14T19:09:07+01:00"
tags = [ "password" ]
ogdescription = "Most people don't change their passwords after data breaches."
slug = "2020-11-14-passwords-and-breaches"
banner = "banners/ish-news"
news = true
syntax = true
+++

In June, [we pointed to a research paper]({{< ref "/news/2020-06-30-monthly-review.md#changing-passwords" >}}). The research paper shows that only 33% of users affected by a data breach change their password afterward. 13% of these users did so within _three_ months after the breach became publicly known.

A new awareness study presents that only 16% of participants visited web pages related to notable data breaches. Few participants read web pages about data breaches that likely affected them.

>"Without adequate awareness, it is unlikely that people will act to improve their security."

As a reader of our website, you are likely aware of security incidents and information security; most people are not. They aren't interested in these topics, overlooking that information security affects all of us every day. For us, it is vital to impart InfoSec knowledge so that you can improve your security. So share your knowledge, too. Show others how to protect themselves.

Regarding passwords: In our experience, many users are still overwhelmed when we recommend password managers as they don't know how password managers work. After trying [a password manager like KeePassXC]({{< ref "/blog/keepassxc-password-management-basics.md" >}}) for some time, they ultimately fall back to the classic "one password for all" scheme. However, password managers for unique and strong passwords plus [two-factor authentication]({{< ref "/blog/modern-credential-management.md#mfa" >}}) are the best we have in 2020.

The research papers:

* {{< extlink "https://www.ieee-security.org/TC/SPW2020/ConPro/papers/bhagavatula-conpro20.pdf" "(How) Do People Change Their Passwords After a Breach?" >}} (PDF file)
* {{< extlink "https://arxiv.org/pdf/2010.09843.pdf" "What breach? Measuring online awareness of security incidents by studying real-world browsing behavior" >}} (PDF file)
