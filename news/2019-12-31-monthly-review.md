+++
title = "Monthly review – December 2019"
author = "Benjamin"
date = "2019-12-31T14:07:36+01:00"
tags = [ "python", "signal", "ios", "webauthn" ]
categories = [ "Monthly review" ]
ogdescription = "Our monthly review of December 2019."
slug = "2019-12-31-monthly-review"
aliases = [ "/blog/2019-12-monthly-review" ]
banner = "banners/monthly-review"
toc = true
news = true
+++

Each month, we publish a review that covers essential activities of the last 30 days. This month, we talk about Python 2 EOL, malicious Python libraries, technical previews by Signal, WebAuthn for iOS, and more.
<!--more-->
{{< rssbox >}}

## News of the month {#notm}
In December 2019, we read the following reports:

### Python 2 EOL
Python 2 reaches its end of life (EOL) on January 1, 2020. (_If you read this on Dec 31, 2019, then this is tomorrow._) [The EOL date]({{< relref "#links" >}}) isn't news but known for more than five years. There won't be any planned security updates for Python 2 in the future. The final version of Python 2 will be 2.7.18 in mid-April 2020.

As a user, check whether any of your applications still require Python 2. If you don't need Python 2 anymore, remove it from your devices. If some applications still require Python 2, check whether there are any announcements regarding migrating to Python 3. In case of doubt, look for alternatives.

As a developer, immediately consider migrating to Python 3 as the EOL date is known for years, and Python 2 won't get any planned security updates in the future.

### Malicious Python libraries stealing OpenPGP and SSH keys
Once again, [people found malicious libraries for Python]({{< relref "#links" >}}). The malicious libraries, "python3-dateutil" and "jeIlyfish," try to copy SSH and OpenPGP keys to remote IP addresses. This is the third time the PyPI team intervened to remove typo-squatted malicious Python libraries from the official repository.

As a user, you should always be sure that you need specific libraries and remove unused libraries. This is not only true for Python libraries but for any other source of user-provided content (e.g., custom repositories for F-Droid, Arch AUR, NPM packages).

### Signal released two technical previews
This month, Signal published two blog posts:

* {{< extlink "https://signal.org/blog/signal-private-group-system/" "Technology Preview: Signal Private Group System" >}}
* {{< extlink "https://signal.org/blog/secure-value-recovery/" "Technology Preview for secure value recovery" >}}

Both posts and a related scientific paper describe concepts for a group system and an account recovery system, which rely on servers while being cryptographically secured. Implementing these concepts could help Signal to overcome certain limitations of their client-side account management.

### iOS and iPadOS support FIDO-compliant security keys
Finally, iOS and iPadOS 13.3 introduce support for FIDO-compliant security keys. After updating, you should be able to use your security token over Lightning, USB, or NFC for [WebAuthn]({{< ref "/glossary.md#webauthn" >}}).

Security key-based biometrics or PIN (without the use of username and password) are not supported yet.

### Data breaches and leaks
Moreover, there were some data breaches. Have I Been Pwned added information about the following breaches:

* AgusiQ-Torrents.pl (breached in September 2019)
* Zynga (breached in September 2019)
* Factual (breached in March 2017)

Check if you were affected, and change your credentials.

## Tip of the month {#tipotm}
This month's tip is about pages on our website that aren't posts. If you only read our RSS/Atom feed, you probably don't know these pages:

* **[Glossary]({{< ref "/glossary.md" >}})**: Our glossary already contains definitions for more than 130 terms related to information security and data protection.
* **[Recommendations]({{< ref "/recommendations.md" >}})**: This page lists recommendations for different uses cases.
* **[Terminal tips]({{< ref "/terminal-tips.md" >}})**: Finally, this growing collection lists CLI tools for different use cases.

Other pages are:

* [About us]({{< ref "/about.md" >}}): This page contains information about our project "InfoSec Handbook" and frequent contributors.
* [Contact details]({{< ref "/contact-details.md" >}}): This page lists contact information like our e-mail address, OpenPGP keys, and our accounts on other websites.
* [Copyright]({{< ref "/copyright.md" >}}): This page contains our copyright information.
* [Privacy policy]({{< ref "/privacy-policy.md" >}}): This is our privacy policy for our website. In short, we never track you, we don't collect any of your personal data, and we usually don't keep any log files.
* [RSS/Atom](/index.xml): This is our full-text RSS feed.
* [Security and disclosure policy]({{< ref "/security.md" >}}): This is our security and disclosure policy. This page also contains our bug bounty program.
* [Series of articles]({{< ref "/soa.md" >}}): This central page contains links to our series.
* [Support us]({{< ref "/support-us.md" >}}): This page shows different ways to support our work.

If you miss something or in case of any other ideas, feel free to contact us.

## Readers' questions of the month {#rqotm}
We share some of our replies to frequent questions of our readers:

### "Do you have a CPU purchasing guide?" {#cpu-purchasing-guide}
We don't provide any recommendations regarding buying CPUs. This information is quickly outdated, and there is likely no perfect CPU for everybody. Buy the CPU that fits your use cases best. If you read about security vulnerabilities in CPUs, keep in mind that most of them aren't relevant for non-virtualized systems or current attacks are purely theoretical.

### "Do you plan to send findings of online assessment tools to organization XYZ?" {#oat}
In general, we contact organizations and even private bloggers if we identify issues with their servers (e.g., vulnerable software, insecure configuration, incomplete privacy policies). We not only include arbitrary results but check if the findings are relevant. As of December 2019, we sent reports to about 120 different parties.

However, we won't send any random findings of online assessment tools to anybody. We already covered the limitations of these tools in different articles on our website. All online assessment tools have a very limited scope and don't understand the context of their findings. The findings require security professionals looking into it. We could do this, but we are already happily employed. 😉

### Not a question: "I found a security vulnerability on your website. If you want me to tell you, please pay me a bug bounty first." {#bug-bounty-first}
We frequently get e-mails from "security researchers" who claim that they found some security issues on our website. Instead of telling us, they demand their bug bounty first. Most of them neither read our [disclosure policy]({{< ref "/security.md#disclosure-policy" >}}) nor provide any details.

We pay [bug bounties]({{< ref "/security.md#scope" >}}). However, we won't pay anything in advance. So if you found a security vulnerability, stay with our [disclosure process]({{< ref "/security.md#disclosure-process" >}}). Besides, understand and use OpenPGP. Using OpenPGP shouldn't be a problem for "security researchers." 😉

## Our activities of the month {#ishotm}
In December, we published six new articles:

* [European GDPR myths – Part 2]({{< ref "/blog/myths-gdpr2.md" >}}): In the second post about GDPR myths, we debunk three more myths (obligation to inform customers within 72 hours after a data breach, declaring GDPR violations, and interpreting GDPR Articles). You can also read [European GDPR myths – Part 1]({{< ref "/blog/myths-gdpr.md" >}}).
* [3 Don'ts of penetration testing and security assessments]({{< ref "/blog/dont-penetration-testing.md" >}}): If done wrong, penetration testing results in a list of arbitrary problems that aren't necessarily related to security. We show three things good penetration testers don't do in this article.
* [Yubico Security Key vs. Nitrokey FIDO2]({{< ref "/blog/yubico-security-key-nitrokey-fido2.md" >}}): After already comparing different security tokens offered by Yubico and Nitrokey, we compare the Yubico Security Key and Nitrokey FIDO2.
* [The state of the LineageOS-based /e/ ROM in December 2019]({{< ref "/blog/e-foundation-final-look.md" >}}): In March and August, we checked the state of the /e/ ROM. In this article, we recheck our findings for the last time.
* [Using a YubiKey as a second factor for LUKS]({{< ref "/blog/yubikey-luks.md" >}}): We use the challenge-response feature of a YubiKey to add two-factor authentication (2FA) to a device already protected by the Linux Unified Key Setup (LUKS).
* [Cryptography myths]({{< ref "/blog/myths-crypto.md" >}}): In our last post in 2019, we debunk three myths regarding cryptography (applying cryptography makes everything secure, applying cryptography makes everything private, AES is secure).

Besides, we changed several things on our website:

* The release of Hugo 0.60 forced us to get rid of inline HTML in Markdown files. The main reason for this was Hugo's migration to Goldmark for rendering Markdown files. On the positive side, we introduced absolute file paths. This allowed us to offer [another mirror of our website on codeberg.org]({{< relref "#links" >}}).
* Furthermore, we changed the styles, content, and layout of our website to comply with the W3C Web Content Accessibility Guidelines 2.1 (WCAG 2.1). Improved accessibility is beneficial for all of our readers.

## Closing words {#cw}
In 2019, we published 28 new posts (excluding monthly reviews) and updated 22 already-existing posts.

This year, **we voluntarily spent 298.5 hours** (equivalent to 37.5 workdays) writing and updating our posts, improving our technical setup and replying to questions of readers and forum posts. We offer our content for free without any tracking, sponsoring, or advertisements. This way, we remain 100% independent and 100% self-funded.

In early 2020, you can expect our updated [Web server security series]({{< ref "/as-wss.md" >}}) and new content for our [Home network security series]({{< ref "/as-hns.md" >}}). There will also be an article on our server infrastructure.

We wish you a secure new year!

## External links {#links}
* Python 2 EOL: {{< extlink "https://www.python.org/dev/peps/pep-0373/#maintenance-releases" "Python 2.7 Release Schedule" >}}
* Malicious Python libraries: {{< extlink "https://zdnet.com/article/two-malicious-python-libraries-removed-from-pypi/" "Two malicious Python libraries caught stealing SSH and GPG keys" >}}
* {{< extlink "https://codeberg.org/infosechandbook/blog-content/commits/branch/master" "Our repository on codeberg.org" >}}
* {{< extlink "https://pages.codeberg.org/infosechandbook/index.html" "Mirror of InfoSec Handbook on codeberg.org" >}}
