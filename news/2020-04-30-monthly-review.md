+++
title = "Monthly review – April 2020"
author = "Benjamin"
date = "2020-04-30T18:22:01+02:00"
tags = [ "side-channel-attack", "python", "ftp", "jitsi-meet" ]
categories = [ "Monthly review" ]
ogdescription = "Our monthly review of April 2020."
slug = "2020-04-30-monthly-review"
aliases = [ "/blog/2020-4-monthly-review" ]
banner = "banners/monthly-review"
toc = true
news = true
+++

Each month, we publish a review that covers essential activities of the last 30 days. This month, we talk about side-channel attacks, Python 2 EOL (again), Jitsi Meet, and more.
<!--more-->
{{< rssbox >}}

## News of the month {#notm}
In April 2020, we read the following reports:

### Side-channel attacks
In mid-April, security researchers reported [a new technique to extract data from air-gapped systems]({{< relref "#links" >}}), called "AiR-ViBeR." The side-channel attack relies on vibrations of PC fans, like GPU fans and CPU fans.

The [related ZDNet article]({{< relref "#links" >}}) lists a wide range of side-channel attacks on air-gapped systems:

* LED-it-Go – exfiltrate data from air-gapped systems via an HDD's activity LED
* USBee – force a USB connector's data bus give out electromagnetic emissions that can be used to exfiltrate data
* AirHopper – use the local GPU card to emit electromagnetic signals to a nearby mobile phone, also used to steal data
* Fansmitter – steal data from air-gapped PCs using sounds emanated by a computer's GPU fan
* DiskFiltration – use controlled read/write HDD operations to steal data via sound waves
* BitWhisper – exfiltrate data from non-networked computers using heat emanations
* Unnamed attack – uses flatbed scanners to relay commands to malware infested PCs or to exfiltrate data from compromised systems
* xLED – use router or switch LEDs to exfiltrate data
* aIR-Jumper – use a security camera's infrared capabilities to steal data from air-gapped networks
* HVACKer – use HVAC systems to control malware on air-gapped systems
* MAGNETO & ODINI – steal data from Faraday cage-protected systems
* MOSQUITO – steal data from PCs using attached speakers and headphones
* PowerHammer – steal data from air-gapped systems using power lines
* CTRL-ALT-LED – steal data from air-gapped systems using keyboard LEDs
* BRIGHTNESS – steal data from air-gapped systems using screen brightness variations

At first glance, such lists look like "everything is insecure." However, it is crucial to understand the requirements of such attacks. Some attacks require physical access to the device; other security vulnerabilities are hard to exploit or only exist in theory. The same is true for the famous Spectre and Meltdown vulnerabilities in CPUs.

So if you look at security vulnerabilities, always try to understand how they are exploited and evaluate if these ways work in your environment.

### Python 2 EOL
In our monthly review of last December, we pointed out the [upcoming end of life (EOL) of Python 2]({{< ref "/news/2019-12-31-monthly-review.md#python-2-eol" >}}). Python 2 reached its EOL on January 1, 2020. In April, the Python developers released the final version of Python 2 (2.7.18). Therefore, we repeat our advice:

As a user, check whether any of your applications still require Python 2. If you don’t need Python 2 anymore, remove it from your devices. If some applications still require Python 2, check whether there are any announcements regarding migrating to Python 3. In case of doubt, look for alternatives.

As a developer, immediately consider migrating to Python 3 as the EOL date is known for years, and Python 2 won’t get any planned security updates in the future.

### Jitsi Meet – the "secure and privacy-friendly" alternative?
Many online publications presented "the best" video conferencing tools during the last weeks. Some of them recommended Jitsi Meet as a "secure and privacy-friendly" alternative to Zoom, Microsoft Teams, and other famous tools.

Our brief analysis showed several issues with Jitsi Meet. For instance, Jitsi Meet isn't end-to-end encrypted by default but claims to be "fully encrypted." We asked readers in the Fediverse for their understanding of "fully encrypted." 74% said that they understand "fully encrypted" as "end-to-end encrypted." Based on this poll, we suggested to change the wording, but a Jitsi developer stated that "No unencrypted data is ever transported. So it's fully encrypted in transit, and since nothing is stored at rest, that's all there is to it. We are not saying it's E2EE, because it's not, yet." We (and many readers) are still convinced that the wording "fully encrypted" is misleading.

Moreover, the mobile apps of Jitsi contain lots of trackers, and there are several issues with Jitsi's privacy policy. Since parties can host their own Jitsi instances, there are many Jitsi instances without any privacy policy or current security-related HTTP response headers. Some of these instances use third-party servers for STUN (Session Traversal Utilities for NAT) and TURN (Traversal Using Relays around NAT), so these third parties may get personal data of Jitsi users.

Finally, a security vulnerability in the Docker variant of Jitsi Meet showed that it is impossible to contact several operators of Jitsi instances due to missing contact details. Besides, basic good practices of information security were violated (the Jitsi developers chose "passw0rd" as a default password).

In summary, Jitsi Meet is not more or less secure or privacy-friendly than any other video conferencing tool. It has pros and cons, and you must consider your personal use cases and threat models.

### Web browsers: The postponed deaths of insecure protocols
Similar to [Mozilla's decision to revive TLS 1.0 and TLS 1.1 due to the current pandemic]({{< ref "/news/2020-03-31-monthly-review.md#firefox-740-the-death-and-revival-of-tls-10-and-11" >}}), Google and other web browser developers postponed the removal of insecure TLS protocol versions.

This month, Google decided to postpone the removal of FTP from Chrome. Originally, Google planned to disable FTP in Chrome 81 and remove it in Chrome 82. Mozilla plans to remove FTP support, too. They announced to disable FTP in Firefox 77 by default, and plan to remove it in 2021.

We will likely see more delays of stricter security measures in web browsers since the major web browser developers fear that these measures could prevent people from accessing important pandemic-related websites and online resources.

### Data breaches and leaks
Moreover, there were some data breaches and leaks. Have I Been Pwned added information about the following breaches and leaks:

* OGUsers (breached in April 2020)
* HTC Mania (breached in January 2020)
* Aptoide (breached in April 2020)
* Vianet (breached in April 2020)

Check if you were affected, and change your credentials.

## Readers' questions of the month {#rqotm}
We share some of our replies to frequent questions of our readers:

### "Can you provide a template for privacy policies?"
No, for simple reasons:

1. We aren't lawyers who know all aspects and snares of current privacy laws and regulations.
2. There is no "single privacy law" for everybody (even the GDPR isn't the same for every EU country).
3. We don't know your setup, so our template will be likely incomplete for your use case.
4. We saw other blogs that provided a template or stated "just use our policy as a template," but their policy was incomplete.

Therefore, we only provide general guidance on privacy policies. For instance, we wrote [an article on (privacy) policies in general]({{< ref "/blog/wss7-policies-contact.md" >}}), and another [article on identifying incomplete privacy policies]({{< ref "/blog/guide-privacy-policy.md" >}}). You are responsible for your privacy policy, so you must write and understand its content.

### "What do you think about BigBlueButton/Jami/Briar/…?"
Contrary to other blogs, we do not recommend any products or services that we don't use ourselves. So we can't provide any meaningful statements for these solutions. We are convinced that recommendations based on hearsay or [binary thinking]({{< ref "/blog/discussion-filter-bubbles.md" >}}) won't help our readers. Keep in mind that every solution has its pros and cons, and talk about benefits and drawbacks.

We list our recommendations on our [recommendations page]({{< ref "/recommendations.md" >}}).

### "Can you host a Mastodon/Jitsi instance?"
No, we do not plan to host any other services than our website. We want to minimize our attack surface and process the least amount of personal data possible. Both can't be realized with most services as they rely on databases, PHP, JavaScript, and your personal data.

Just send us your questions. Maybe, we answer your question in the next monthly review.

## Our activities of the month {#ishotm}
In April, we updated the content, style, grammar, and wording of several articles. This process is still ongoing.

Moreover, we [documented the upcoming changes of our Web server security series](https://codeberg.org/infosechandbook/blog-content/issues/13#issue-21699) on codeberg.org. Feel free to comment on our plan.

Other activities included the analysis of Jitsi Meet instances. Besides, we contacted several server administrators and notified them about a security vulnerability in their instance.

## External links {#links}
* {{< extlink "https://arxiv.org/pdf/2004.06195v1" "AiR-ViBeR: Exfiltrating Data from Air-Gapped Computers via Covert Surface ViBrAtIoNs" >}} (PDF file)
* {{< extlink "https://www.zdnet.com/article/academics-steal-data-from-air-gapped-systems-using-pc-fan-vibrations/" "ZDNet: Academics steal data from air-gapped systems using PC fan vibrations" >}}
