+++
title = "Our experience with the Fediverse, and why we left"
author = "Benjamin"
date = "2020-12-05T08:08:01+01:00"
tags = [ "infosechandbook" ]
ogdescription = "We left the Fediverse permanently."
slug = "2020-12-05-leaving-the-fediverse"
banner = "banners/ish-news"
news = true
syntax = true
+++

The "Fediverse" is a loose aggregation of federated servers that mainly speak the ActivityPub protocol. One of the most famous services in the Fediverse is "Mastodon." We joined the Fediverse in the early days of Mastodon to share short-lived information that didn't fit into a separate article on our website.

## The issues that we discussed earlier this year
In May, [we reconsidered our presence in the Fediverse]({{< ref "/news/2020-05-31-monthly-review.md#fediverse" >}}). Back then, the main issues for us were:

1. You can't migrate your posts if you need to migrate to another Mastodon server.
2. You can't update your posts without losing shares and comments.
3. All posts have a character limit, so we can't say everything we want in a single post.
4. Many Fediverse users claim to be the "better and open-minded alternative to Twitter." We think that this doesn't reflect reality, as explained below.

In June, we addressed issues 1 to 3 by introducing a "News" section on our website.

## Further issues in the Fediverse and other social networks
We also participated in discussions and tried to encourage dialogs in the Fediverse and other forums instead of only posting information. However, these discussions revealed consistent issues:

### The toxic "us vs. them" ideology
For some groups, it is vital to make a bogeyman out of opposing parties. In the Fediverse, some individuals and groups continuously spread misinformation about "the others" to gain likes and followers. While most of the toxic behavior targets the big tech companies in the USA, some individuals targetting us.

### The "reverse burden of proof"
When discussing with others, we often encountered the "reverse burden of proof." Somebody posts negative claims about a product or service. Instead of providing (technical) proof for the claim, the only supporting "fact" is, "everybody knows that this is true." On the contrary, this person doesn't accept any (technical) proof that debunks their initial claim. For us, it looks like you don't need to provide any real facts as long as you satisfy the ideology of some people.

### The "I read headlines only" problem
Many people read the headlines of a post and then guess what is written in the remaining article. Such behavior results in misinterpreted information and persistent myths. On the other hand, it became a standard tool for some blogs to gain more readers: Writing, "Critical security vulnerability could leak all of your data" results in more clicks and shares than, "Company fixed potential security vulnerability months ago, which could have leaked some of your data if ever exploited." This problem kills meaningful discussions instantly.

### The "security and privacy by assumption" problem
Most of you know "security by obscurity," when security relies on keeping the implementation secret. We see another common issue: "security and privacy by assumption." Many people share security and privacy tips without ever checking whether these tips work. They just assume that somebody else checked it before.

### And finally, "comparing apples to oranges"
For instance, some people claim the Signal messenger became insecure after introducing the Signal PIN because a server-side attacker could now access your data. The base for this claim is the assumption that a server-side party extracts encrypted information from the volatile main memory of a server, exploits an unpatched security vulnerability in Intel CPUs to extract a key, and successfully guesses a user-provided password.

While the attack looks anything but trivial as these people describe it, they then argue that their favorite messenger is far more secure due to being decentralized. They compare the encryption mechanism of instant messenger A with something else. Suppose you look at the same feature of their favorite messenger. In that case, you see that their messenger doesn’t offer any server-side protection. In their case, a server-side party can directly access your data in cleartext—this is trivial.

### "I'm privacy-friendly; please donate"
The issues mentioned above are also prevalent on other social networks like Twitter. Just like on Twitter, you see "privacy-friendly" companies trying to gain more paying customers or some bloggers heavily involved in influencer marketing. While most of them claim that this is for a "good cause," it is ultimately all about money.

### Decentralization still isn't a privacy feature
Now, some people may insist that at least security and privacy are far better "due to the decentralized nature" of the Fediverse. As we wrote several times before, "decentralization" isn't a privacy feature.

Search engines like Google can discover and index all of your public posts in the Fediverse. By default, you can see the social graphs of all accounts, as followers are public. Especially Mastodon servers come with privacy policies that don't meet the European GDPR requirements. Many (Mastodon) servers seem to run an outdated version, indicating inadequate security practices.

## Conclusion
Should you become part of the Fediverse by creating an account? It isn't up to us to decide this, of course.

If you only want to read the content of others, we recommend subscribing to their RSS feeds. Your RSS subscriptions are private and more decentralized than any Fediverse account. And you need to manage one account less on the internet.

If you want to be an active part of the Fediverse, be prepared for the same problems as on any other social network. We decided to leave the Fediverse for now.
