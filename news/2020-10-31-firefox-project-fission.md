+++
title = "Project Fission: The upcoming site isolation in Firefox"
author = "Benjamin"
date = "2020-10-31T09:30:39+01:00"
tags = [ "firefox", "isolation", "sandbox" ]
ogdescription = "Project Fission is the upcoming site isolation in Firefox."
slug = "2020-10-31-firefox-project-fission"
banner = "banners/ish-news"
news = true
syntax = true
+++

Project Fission started in early 2019 as a project "for revamping and strengthening the architecture of Firefox browser." Today, it is "Mozilla's implementation of Site Isolation in Firefox."

The site isolation feature isolates websites and frames (as the name implies) so that they can't access each other's data. Firefox achieves isolation by running websites and frames in isolated processes.

Currently, Project Fission is still experimental. You can enable it in Firefox Nightly: Go to {{< kbd "about:config" >}} in Firefox Nightly. Then, set "fission.autostart" and "gfx.webrender.all" to {{< kbd "True" >}}. Do not change any other "fission.…" or "gfx.webrender.…" settings. Restart Firefox.

You can check whether site isolation is enabled by hovering over a tab in your web browser. If it is enabled, the tab's tooltip shows an "[F]."

## Links
* {{< extlink "https://wiki.mozilla.org/Project_Fission" "Project Fission (mozilla wiki)" >}}
