+++
title = "A brief look at web protocols"
author = "Benjamin"
date = "2020-09-29T07:19:06+02:00"
tags = [ "tls", "https" ]
ogdescription = "Raccoon is a newly-discovered vulnerability in the TLS specification."
slug = "2020-09-29-web-protocol-clutter"
banner = "banners/ish-news"
news = true
+++

Some readers get confused by protocols used on the internet, like HTTP, HTTPS, and TLS. So, what is the meaning of these abbreviations?

## HTTP
The Hypertext Transfer Protocol is the foundation of the World Wide Web. Most communication in your web browser happens via HTTP.

* HTTP/1.1: Version 1.1 of HTTP was documented in 1997. It was the standard for HTTP communication for nearly 20 years.
* HTTP/2 (SPDY): Version 2 of HTTP was published in 2015. HTTP/2 changes the way of transmitting data between web servers and clients to minimize requests and speed up communication. Most other semantics of HTTP/1.1 stayed the same. HTTP/2 was derived from Google's experimental SPDY protocol.
* HTTP/3 (QUIC): Version 3 of HTTP is currently available as a draft. It is derived from Google's experimental QUIC protocol. Contrary to HTTP/2, there are more changes to speed up web communications. For instance, HTTP/3 uses UDP as the underlying transport protocol. The original QUIC protocol also integrates the key exchange and cryptographic handshake in the connection's initial handshake, further speeding up HTTPS.

## HTTPS
The Hypertext Transfer Protocol Secure is an extension of HTTP. Nowadays, it uses TLS (Transport Layer Security) for security. In other words: HTTPS is HTTP running on top of TLS.

* SSL: All versions of SSL are outdated. Do not use it.
* TLS 1.0 and 1.1: TLS 1.0 and 1.1 are also outdated. Do not use them.
* TLS 1.2: TLS 1.2 was introduced in 2008. While TLS 1.2 can still be considered "secure," it comes with many legacy cipher suites that are insecure or weak. As a server admin, you need to limit the TLS cipher suites to the small number of cipher suites that support [AEAD]({{< ref "/glossary.md#aead" >}}).
* TLS 1.3: TLS 1.3 is the latest TLS protocol, introduced in 2018. TLS 1.3 introduces modern cryptographic primitives and removes legacy cryptography by default. As a server admin, you don't need to limit cipher suites.

## Links
* {{< extlink "https://tools.ietf.org/html/rfc7540" "RFC 7540 – Hypertext Transfer Protocol Version 2 (HTTP/2)" >}}
* {{< extlink "https://quicwg.org/base-drafts/draft-ietf-quic-http.html" "Hypertext Transfer Protocol Version 3 (HTTP/3) (draft)" >}}
* {{< extlink "https://tools.ietf.org/html/rfc8446" "RFC 8446 – The Transport Layer Security (TLS) Protocol Version 1.3" >}}
