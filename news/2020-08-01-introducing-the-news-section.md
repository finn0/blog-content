+++
title = "Introducing the 'News' section"
author = "Benjamin"
date = "2020-08-01T15:20:00+02:00"
tags = [ "infosechandbook" ]
ogdescription = "InfoSec Handbook introduces a news section."
slug = "2020-08-01-introducing-the-news-section"
banner = "banners/ish-news"
news = true
+++

This is our first 'News' post on infosec-handbook.eu. [As explained in our Monthly Review of June 2020]({{< ref "/news/2020-06-30-monthly-review.md#2020-spring-cleaning-part-2" >}}), we want to use this section to discuss recent developments in information security. This section allows us to post without any character limit, we can update all posts, and the posts are preserved permanently. We want to include additional links, information, and comments.

Currently, the tags of this section are the same as the tags in "normal" posts. So clicking the tags shows news posts alongside posts. We may change this, though.
