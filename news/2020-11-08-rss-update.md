+++
title = "Update of our RSS/Atom feed"
author = "Benjamin"
date = "2020-11-08T20:43:01+01:00"
tags = [ "infosechandbook" ]
ogdescription = "We changed our RSS feed."
slug = "2020-11-08-rss-update"
banner = "banners/ish-news"
news = true
syntax = true
+++

Hello InfoSec lovers! 😀

Just a small update: As requested [during our 2020 Fall Survey]({{< ref "/news/2020-10-16-2020-fall-survey.md" >}}), we updated our RSS/Atom feed. There is now a single feed for articles and news posts. So you don't have to subscribe to two separate feeds anymore.

We spent some hours testing these changes; however, if you encounter any issues (esp., "404 not found"), please [e-mail us]({{< ref "/contact-details.md" >}}). 👍

Besides, we decided to drop our Changelog page in favor of {{< extlink "https://codeberg.org/infosechandbook/blog-content/commits/branch/master" "our blog-content repository on codeberg.org" >}}. We didn't configure a redirect for the former Changelog page or feed because we don't want to send you to a third-party website without warning. We will describe significant changes of the InfoSec Handbook in news posts on our website.
