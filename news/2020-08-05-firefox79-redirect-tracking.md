+++
title = "Firefox 79: Introducing redirect tracking protection"
author = "Benjamin"
date = "2020-08-05T18:23:19+02:00"
tags = [ "firefox", "privacy" ]
ogdescription = "Firefox 79 protects against redirect tracking."
slug = "2020-08-05-firefox79-redirect-tracking"
banner = "banners/ish-news"
news = true
syntax = true
+++

Mozilla explained their new "Enhanced Tracking Protection 2.0" that is included in Firefox 79 and later: _"ETP 2.0 clears cookies and site data from tracking sites every 24 hours, except for those you regularly interact with. We’ll be rolling ETP 2.0 out to all Firefox users over the course of the next few weeks."_

Redirect tracking means that you click a link on a website to another website. Instead of directly navigating to the expected website, your web browser first navigates to the tracking website and is then instantly redirected to the expected website. The redirection allows the tracking party to set cookies as a first party. Your web browser doesn't usually block first-party cookies. Firefox 79 periodically removes these cookies in the future.

While this feature will be gradually turned on by Mozilla, you can enable it today by navigating to {{< kbd "about:config" >}} and setting "privacy.purge_trackers.enabled" to {{< kbd "True" >}}. However, the "network.cookie.cookieBehavior" needs to be set to {{< kbd "4" >}} or {{< kbd "5" >}} to allow regular cleaning of these cookies.

* {{< extlink "https://www.mozilla.org/en-US/firefox/79.0/releasenotes/" "Firefox 79 release notes" >}}
* {{< extlink "https://blog.mozilla.org/security/2020/08/04/firefox-79-includes-protections-against-redirect-tracking/" "Firefox 79 includes protections against redirect tracking" >}}
* {{< extlink "https://developer.mozilla.org/en-US/docs/Mozilla/Firefox/Privacy/Redirect_Tracking_Protection" "Redirect tracking protection (technical background)" >}}
