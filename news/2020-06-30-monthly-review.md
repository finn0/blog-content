+++
title = "Monthly review – June 2020"
author = "Benjamin"
date = "2020-06-30T17:24:07+02:00"
tags = [ "password", "webbkoll", "turris-omnia" ]
categories = [ "Monthly review" ]
ogdescription = "Our monthly review of June 2020."
slug = "2020-06-30-monthly-review"
aliases = [ "/blog/2020-6-monthly-review" ]
banner = "banners/monthly-review"
toc = true
notice = true
news = true
+++

Each month, we publish a review that covers essential activities of the last 30 days. This month, we talk about passwords, a new Webbkoll feature, Turris OS 5, and more.
<!--more-->
{{< rssbox >}}

## News of the month {#notm}
In June 2020, we read the following reports:

### Changing passwords
In 2017, the National Institute of Standards and Technology published their updated "Authentication and Lifecycle Management" guideline (SP 800-63B). The guideline states, "Verifiers SHOULD NOT require memorized secrets to be changed arbitrarily (e.g., periodically). However, verifiers SHALL force a change if there is evidence of compromise of the authenticator." In Appendix A of the same document, the NIST explains that humans "often choose passwords that can be easily guessed," and analysis suggests that most password rules don't increase the strength of the passwords while severely impacting the "usability and memorability."

In other terms, software shouldn't force users to change their passwords from time to time. However, the reason for this isn't that changing passwords technically results in weak passwords, but users try to cope with enforced rules by creatively bypassing them.

[A recent paper]({{< relref "#links" >}}) shows that forcing users to change their passwords after a data breach isn't better: Only 33% of affected users changed their passwords after a data breach; 13% did so within three months after the breach became publicly known. Moreover, most passwords were weaker or of equal strength. Thus, both ways likely result in weak passwords due to the human factor. Besides, you have to consider that the vast majority of data breaches are detected after a long period.

### Webbkoll now detects reporting directives
In October 2019, [we requested that Webbkoll detects reporting directives]({{< relref "#links" >}}) in HTTP response headers. In June 2020, the developer of Webbkoll added the feature.

Several HTTP response headers allow server operators to set either "report-to" or "report-uri" directives. As soon as web clients (e.g., your web browser) run into an issue with the website, they may produce an error report and send it to a reporting API. However, the reporting API can be provided by a third party. In this case, your web browser sends the error report, including personal data (like your IP address), to the third party. You can only detect this by inspecting your network traffic.

Some HTTP response headers that support these directives are the Content Security Policy, X-XSS-Protection, NEL (Network Error Logging), and Expect-CT.

Webbkoll detects reporting directives in several HTTP response headers and shows directives pointing to third-party APIs.

### Turris OS 5.0 released
In June, the team behind the Czech Turris routers released Turris OS 5.0. Their latest open-source operating system is based on OpenWrt 19.07. Turris OS 5 allows you to use WPA3 ([see our guide]({{< ref "/blog/hns1-hello-world.md#to-wlan" >}})).

Turris OS 4 was automatically upgraded to 5, while automatic migration from Turris OS 3 is planned. At present, you can manually upgrade Turris OS 3 to 5. However, manually upgrading is only recommended for advanced users.

### Data breaches and leaks
Moreover, there were some breaches and leaks. Have I Been Pwned added information about the following breaches and leaks:

* Lead Hunter (breached in March 2020)
* Zoomcar (breached in July 2018)
* Mathway (breached in January 2020)
* Foodora (breached in April 2016)
* Quidd (breached in 2019)

Check if you were affected, and change your credentials.

## 2020 spring cleaning (part 2)
In May, [we shut down several accounts and services]({{< ref "/news/2020-05-31-monthly-review.md#2020-spring-cleaning" >}}) (GitLab, Keybase, Session, Threema, and our Dat mirror). Additionally, [we reconsidered our presence in the Fediverse]({{< ref "/news/2020-05-31-monthly-review.md#fediverse" >}}).

At present, we think that we will add a "News" section to our website that contains the content of our Fediverse feed. This section shouldn't be a simple mirror, but provide additional links, information, and comments. **Update (August 2020):** We added a ["News" section](/news/). **Update (November 2020):** We left the Fediverse permanently.

This approach ensures that the content remains available if our Mastodon server disappears (happened twice before). There is no character limit for posts, and we can update the post later. In total, this approach addresses 3 out of 4 issues that we presented last month. This approach also ensures that our account in the Fediverse stays active.

If you have any suggestions, feel free to send us a message.

## Ask Me Anything {#ama-july-2020}
Beginning tomorrow (July 13, 2020), we host our first "Ask Me Anything" (AMA) event. The rules are simple: Ask us anything; don't be afraid to ask. Jakub and Benjamin will answer your questions.

The event at a glance:

* **Start:** Monday, July 13
* **End:** Thursday, July 23
* **What can I ask?** "Anything." However, a focus on information security is appreciated.
* **Where can I ask?**: E-mail us.
* **Who answers my questions?**: Jakub and Benjamin.

## Our activities of the month {#ishotm}
In June, we started updating our [Home network security series](/as-hns/) that shows ways to secure your network at home. A friendly reminder: We [documented the upcoming changes of our Web server security series](https://codeberg.org/infosechandbook/blog-content/issues/13#issue-21699) on codeberg.org. Feel free to comment on this.

## External links {#links}
* {{< extlink "https://www.ieee-security.org/TC/SPW2020/ConPro/papers/bhagavatula-conpro20.pdf" "(How) Do People Change Their Passwords After a Breach?" >}} (PDF file)
* {{< extlink "https://github.com/andersju/webbkoll/issues/19" "Webbkoll – [Feature Request] Detect use of report-uri and report-to directives" >}}
* {{< extlink "https://www.turris.com/en/news/news-list/turris-os-5-0/" "Turris OS 5.0" >}}
