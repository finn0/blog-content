+++
title = "Monthly review – September 2019"
author = "Benjamin"
date = "2019-09-30T19:16:13+02:00"
tags = [ "simjacker", "wibattack", "pdfex", "privacytools" ]
categories = [ "Monthly review" ]
ogdescription = "Our monthly review of September 2019."
slug = "2019-09-30-monthly-review"
aliases = [ "/blog/2019-9-monthly-review" ]
banner = "banners/monthly-review"
toc = true
news = true
+++

Each month, we publish a review that covers essential activities of the last 30 days. This month, we talk about Simjacker/WIBattack, PDFex, privacytools.io, and more.
<!--more-->
{{< rssbox >}}

## News of the month {#notm}
In September 2019, two major [security vulnerabilities]({{< ref "/glossary.md#vulnerability" >}}) were disclosed:

### Simjacker
The [Simjacker attack and WIBattack]({{< relref "#links" >}}) both rely on software on the SIM card of targeted phones. These attacks can be used to locate phones, or retrieve information about them (IMEI, battery, network, language). The problem here is that such software on SIM cards is 100% controlled by the mobile network operator, and can be transparently installed, uninstalled, enabled, or disabled by mobile network operators. The owner of a SIM card can't control such software or spot these changes. However, these attacks rely on special commands that are sent to the SIM card of the victim. There seem to be some apps that can detect such SMS like [SnoopSnitch]({{< relref "#links" >}}). Then, there is [SIMtester]({{< relref "#links" >}}), an app that can detect various security vulnerabilities of SIM cards. Kindly note that these apps are only for detection. Blocking these commands can also be done by mobile network operators.

In summary, such attacks demonstrate that you can't "take back control" of your phone by just installing a custom operating system on your device. Such attacks will remain possible due to proprietary chips that can't be controlled by the phone's operating system (e.g., Android, iOS). Even a smartphone that comes with 100% open hardware relies on proprietary SIM cards.

### PDFex
[PDFex]({{< relref "#links" >}}) demonstrates several attacks on (legacy) PDF encryption. On the one hand, an attacker can manipulate parts of an encrypted PDF file without knowing the password used for encryption. On the other hand, the insecure Cipher Block Chaining (CBC) encryption mode allows ciphertext malleability since there are no integrity checks. Researchers showed that 23 out of 27 tested PDF viewers were vulnerable to these attacks. Furthermore, research suggests that PDF signing is also vulnerable to several attacks, allowing attackers to change the contents of signed PDF files without invalidating the signatures.

### Data breaches and leaks
Moreover, there were some data breaches. Have I Been Pwned added information about the following breaches:

* XKCD (breached in July 2019)
* Mastercard Priceless Specials (breached in August 2019)
* Poshmark (breached in mid-2018)
* void.to (breached in June 2019)
* Minehut (breached in May 2019)
* KiwiFarms (breached in September 2019)
* Lumin PDF (breached in April 2019)
* Wanelo (breached in December 2018)

Check if you were affected, and change your credentials.

## Tool of the month {#toolotm}
This month, we present [signal-cli]({{< ref "/blog/tool-signal-cli.md" >}}) that allows you to use Signal in your terminal.

## Tip of the month {#tipotm}
This month, our tip is about a [privacytools.io]({{< relref "#links" >}}). Privacytools.io is focused on providing privacy-oriented alternatives to well-known apps and services. Additionally, they are hosting some services like Gitea, Mastodon, Matrix, Searx, and PrivateBin. There is also a [forum]({{< relref "#links" >}}). Unlike other websites, which recommend alternatives, recommendations on privacytools.io are based on community discussions and feedback. So everybody can help improving recommendations and share their opinions.

Feel free to check their websites if you are interested in sharing some privacy tips or discuss it. (As always, no sponsoring involved! 😉)

## Readers' questions of the month {#rqotm}
We share some of our replies to frequent questions of our readers:

### "Is infosec-handbook.eu a reliable source for Wikipedia?"
This depends on the edition of Wikipedia since the rules for "reliable sources" differ. In the case of the English edition, infosec-handbook.eu (and most other personal blogs) doesn't meet the requirements of a primary source. While we always try to provide high-quality content, our content can't be used as the sole source for Wikipedia. In our opinion, this is good. Wikipedia (as an online encyclopedia) should contain only information that was reviewed by many different people. On the other hand, personal blogs aren't encyclopedias.

### "Which software do you use for your blog?"
We use different applications for different purposes. The core of our website is Hugo, a static website generator (see [this article for more information]({{< ref "/blog/static-blogging.md" >}})). Hugo takes different files as input and produces our website as output. Then, we use OpenSSH and rsync to upload all files of our website to our server. Our server runs Nginx (web server) to serve our content. Besides, we use Git to back up our content, and OpenPGP to sign each commit. Other tools are pngcrush, ImageMagick (image compression), and minify (content compression). Of course, there are more tools involved, like Inkscape (drawing icons) or Atom (editor).

Our setup requires less server-side software than popular content management systems. For instance, we don't use content management systems like WordPress or PHP, drastically improving the security of our website.

### "Do you plan to implement 'OpenPGP Web Key Directory' for your OpenPGP keys?"
No, not at the moment since it is still an informational draft, not a standard or recommendation. Then, the vast majority of readers doesn't send us encrypted e-mails (even companies, which sell OpenPGP products, didn't manage to send encrypted e-mails), or contact us via other encrypted channels that don't make use of our OpenPGP keys.

### "Will you publish content in other languages, e.g., in German?"
No, this isn't planned at the moment. Publishing content in multiple languages makes updating content much more time-consuming.

Just send us your questions. Maybe, we answer your question in the next monthly review.

## Our activities of the month {#ishotm}
In September, we published only one new article since most of us were on vacation:

* [GnuPG for e-mail encryption and signing]({{< ref "/blog/gpg-for-emails.md" >}}): In this article, we show recent security vulnerabilities in OpenPGP/GPG. Furthermore, we talk about the basic workflow when using GPG for e-mail encryption, and show several alternatives and their use cases.

Besides, we split the main Git repository of our website and [published the Git repo]({{< relref "#links" >}}) that contains the content of our website. You can see every single change of our articles on GitHub now. We did this mainly for transparency reasons; however, readers with a GitHub account can also use this way to report issues or suggestions for improvements.

Moreover, we tweaked the CSS style of the website a little bit and added [additional ways to contact us]({{< ref "/contact-details.md" >}}).

## Closing words {#cw}
In October, we will proceed to revise our [Web server security series]({{< ref "/as-wss.md" >}}), as announced last month. The revision is a challenging process that takes some time. We intend to split general content and software-specific content for better maintainability.

Then, it is already October. Every year, October is the European Cyber Security Month (ECSM). As announced on some ECSM-related websites, we will attend by publishing several shorter articles that address topics of this year's ECSM.

## External links {#links}
* Simjacker attack: {{< extlink "https://www.adaptivemobile.com/blog/simjacker-next-generation-spying-over-mobile" "Simjacker – Next Generation Spying Over Mobile" >}}
* WIBattack: {{< extlink "https://ginnoslab.org/2019/09/21/wibattack-vulnerability-in-wib-sim-browser-can-let-attackers-globally-take-control-of-hundreds-of-millions-of-the-victim-mobile-phones-worldwide-to-make-a-phone-call-send-sms-to-any-phone-numbers/" "WIBattack: Vulnerability in WIB sim-browser" >}}
* Simjacker/WIBattack – Simjacker/WIBattack – protection tools now available: {{< extlink "https://srlabs.de/bites/sim_attacks_demystified/" "New SIM attacks de-mystified, protection tools now available" >}}
* {{< extlink "https://opensource.srlabs.de/projects/snoopsnitch" "SnoopSnitch" >}}
* {{< extlink "https://opensource.srlabs.de/projects/simtester" "SIMtester" >}}
* {{< extlink "https://pdf-insecurity.org/" "PDFex: how to break PDF encryption" >}}, and {{< extlink "https://pdf-insecurity.org/encryption/evaluation_2019.html" "PDFex: Evaluation of PDF viewers" >}}
* {{< extlink "https://www.privacytools.io" "privacytools.io" >}} ({{< extlink "http://privacy2zbidut4m4jyj3ksdqidzkw3uoip2vhvhbvwxbqux5xy5obyd.onion" "via Tor Browser" >}}), {{< extlink "https://forum.privacytools.io/" "privacytools.io – forum" >}}
* {{< extlink "https://github.com/infosec-handbook/blog-content" "Content of infosec-handbook.eu on GitHub" >}}
