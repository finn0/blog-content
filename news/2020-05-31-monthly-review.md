+++
title = "Monthly review – May 2020"
author = "Benjamin"
date = "2020-05-31T09:40:13+02:00"
tags = [ "side-channel-attack", "luks", "blogging", "bluetooth" ]
categories = [ "Monthly review" ]
ogdescription = "Our monthly review of May 2020."
slug = "2020-05-31-monthly-review"
aliases = [ "/blog/2020-5-monthly-review" ]
banner = "banners/monthly-review"
toc = true
notice = true
syntax = true
news = true
+++

Each month, we publish a review that covers essential activities of the last 30 days. This month, we talk about attacks on hardware, the Permissions Policy, auto-mounting a secondary LUKS-protected disk, and more.
<!--more-->
{{< rssbox >}}

## News of the month {#notm}
In May 2020, we read the following reports:

### Thunderspy, BIAS, POWER-SUPPLaY, and USBFuzz (or more attacks on hardware)
Last month, we talked about [a growing number of publicly-known side-channel attacks on hardware]({{< ref "/news/2020-04-30-monthly-review.md#side-channel-attacks" >}}). In May, researchers presented the following novel attacks and tools (for links to the sources, [see the section "external links" below]({{< relref "#links" >}})):

* Thunderspy, "a series of attacks that break all primary security claims for Thunderbolt 1, 2, and 3."
* BIAS, a series of impersonation attacks on Bluetooth devices.
* POWER-SUPPLaY that turns the power supply unit of a device into an audio speaker to extract secrets.
* USBFuzz, a new fuzzing tool that "discovered a total of 26 new bugs, including 16 memory bugs of high security impact in various Linux subsystems."

While researchers should report such vulnerabilities and the industry must address them, it is also necessary to understand the requirements for such attacks. We already wrote this last month; however, every new security vulnerability in the media leads to random lists of "security tips" to avoid these attacks. The "tips" range from "buy AMD only" and "disable USB, Bluetooth, and WiFi" to "everything is insecure, so don't use your device for anything."

We recommend that you always read the original paper. Even if you do not understand everything, you can learn about the setting of the research and its requirements. In our opinion, avoiding every security vulnerability is impossible.

### The Feature Policy is dead, long live the Permissions Policy!
Similar to the Content Security Policy, the (former) Feature Policy allows server administrators to set a list of client-side features that should be disabled. Clients, like web browsers, get the list and disable their features accordingly. Of course, clients must support and observe such policies.

In May, the Feature Policy was renamed to the Permissions Policy. The Permissions Policy is still a public draft, so there is no need to set one on the server. You can [access the draft on GitHub]({{< relref "#links" >}}).

### Data breaches and leaks
Moreover, there were more than 10 data breaches and leaks. Have I Been Pwned added information about the following breaches and leaks:

* Tokopedia (breached in April 2020)
* TaiLieu (breached in November 2019)
* Elanic (breached in January 2020)
* Ulmon (breached in January 2020)
* Covve (data dump appeared as "db8151dd" in February 2020)
* Nulled.ch (breached in May 2020)
* Lifebear (appeared in early 2019)
* Artsy (breached in April 2018)
* PetFlow (breached in December 2017)
* LiveJournal (breached in 2017)
* Wishbone (breached in January 2020 again)

Check if you were affected, and change your credentials.

## Tip of the month {#tipotm}
This month's tip is about auto-mounting a secondary LUKS-protected disk. In this scenario, you use at least two internal disks. Your primary disk, which contains your Linux operating system, can be encrypted using LUKS independently. If you don't know LUKS, feel free to read our article "[Using a YubiKey as a second factor for LUKS]({{< ref "/blog/yubikey-luks.md" >}})." The article provides guidance on protecting your primary disk and shows the limitations of LUKS.

{{< noticebox "warning" "We assume that you already formatted your secondary disk and set a LUKS password. If you auto-mounted the secondary disk before, check your local file systems table: '/etc/fstab.' You may have to remove the entry that mounted your secondary disk before. If you forget this, your system might fail to boot in the future. In this case, you can boot from a recovery medium to fix your 'fstab' file." >}}

Mount the secondary disk that you prepared. There are several CLI tools for this, like "udisksctl" or "cryptsetup." After that, enter {{< kbd "cat /etc/mounts" >}} in your terminal to see your mounted devices.

In our case, the LUKS-protected secondary disk was mounted as "/dev/mapper/secret /run/media/angela/Data ext4 rw,nosuid,nodev,noexec,noatime 0 0."

These settings are:

* "/dev/mapper/secret": This is the specification of the filesystem.
* "/run/media/angela/Data": This is the mount point of the disk.
* "ext4": ext4 is the filesystem of the disk.
* "rw": The filesystem is mounted as "read write."
* "nosuid": "suid" and "sgid" bits are blocked.
* "nodev": This filesystem can't contain special devices.
* "noexec": Binaries on the disk can't be executed.
* "noatime": The last access time isn't stored.
* "0 0": The first 0 is the backup frequency of the "dump" tool (0 means never); the second 0 indicates when the tool "fsck" should check the filesystem for errors (0 means never).

Then, you can enter {{< kbd "lsblk" >}} to see the underlying device, like "/dev/sda." You can check if the LUKS password for this device is set by entering {{< kbd "sudo cryptsetup luksDump /dev/sda" >}} to view the LUKS header. You should see output similar to:

{{< samp "Key Slot 0: ENABLED" >}}

The remaining 7 key slots should be disabled.

Now you can add the device to your "fstab" file, or you can create a local key file to unlock your secondary disk when booting your system.

For generating a key file that contains random bytes, enter {{< kbd "sudo dd bs=128 if=/dev/random of=/root/devicekey count=1" >}}. The command uses the "dd" tool to write 128 random bytes from "/dev/random" to "/root/devicekey." You can also use a YubiKey for this, as shown in our article "[Using a YubiKey as a second factor for LUKS]({{< ref "/blog/yubikey-luks.md" >}})."

After creating the key file, set the file permissions to read-only for the root account: {{< kbd "sudo chmod 400 /root/devicekey" >}}. Then, you need to add the key file to a free key slot of your LUKS-protected disk: {{< kbd "sudo cryptsetup luksAddKey /dev/sda /root/devicekey" >}}. You need to enter your previous LUKS password to add the key file.

Recheck the key slots: {{< kbd "sudo cryptsetup luksDump /dev/sda" >}}. You should see output similar to:

{{< samp "Key Slot 0 and 1: ENABLED" >}}

The remaining 6 key slots should be disabled.

Finally, you need to add the secret to your "crypttab" file that contains the configuration for encrypted block devices. Open the file {{< kbd "nano /etc/crypttab" >}}, and add "secret /dev/sda /root/devicekey."

Recheck all of the modified files, and reboot your system. The secondary disk should be automatically mounted and unlocked when booting your system.

{{< noticebox "note" "As always, this is an example. Understand each command and setting, and modify everything according to your threat model and use cases." >}}

## 2020 spring cleaning
In May, we shut down several accounts and services. This activity is still ongoing. We removed our accounts on GitLab, Keybase, Session, and Threema. Additionally, we shut down our Dat mirror. Currently, we reconsider our presence in the Fediverse.

### Goodbye to GitLab, Keybase, Session, and Threema
We said goodbye to our accounts on GitLab, Keybase, Session, and Threema. About 350 accounts followed these accounts.

As we previously wrote in the Fediverse, there are several reasons for this:

1. Using more and more third-party services leads to an increased attack surface and requires some level of community management. Unfortunately, we don't have resources for interacting with people on every social platform that is considered "the latest."
2. These services don't offer any additional features that we need at the moment.
3. Only 3% of our readers' communication happened via these services (see the pie chart below).

All readers affected by this can still [subscribe to our RSS feeds](/index.xml) and [contact us]({{< ref "contact-details.md" >}}).

{{< webpimg "art-img/2020-5-mr-ish-contact.png" "How people contact the InfoSec Handbook team" "a pie chart about how people contact the InfoSec Handbook team. E-mail 84%, Fediverse 7%, Signal 6%, Keybase 2%, Others 1%." >}}

### Goodbye to our Dat mirror {#dat-mirror}
Additionally, we shut down our Dat mirror for the following reasons:

1. The peer-to-peer [Dat protocol (dat://)]({{< relref "#links" >}}) is still a niche protocol without broad client-side support. People need to install additional software to use it. On the other hand, only a few websites are accessible via Dat.
2. We primarily aimed to improve our content's availability in case of technical problems or attacks on our servers. Therefore, we provide a [mirror on codeberg.org](https://pages.codeberg.org/infosechandbook/index.html). This mirror is accessible by everyone using their standard web browser.
3. While some people say that P2P protocols improve your privacy, we don't know to which IP addresses you connect when you access our content via the Dat protocol. Some third-party Dat peers learn about your browsing behavior just by sharing our content.

Third parties may still share our content via Dat; however, keep in mind that this content gets more and more outdated.

### What about the Fediverse? {#fediverse}
Finally, we want to talk about the Fediverse. We were among the early adopters of Mastodon. Back then, other bloggers claimed that Mastodon is a day fly that nobody needs. Nowadays, Mastodon (and the Fediverse) is anything but a dead community.

On the other hand, there are several things we don't like:

1. We can't migrate our posts to another Mastodon server. We experienced the shutdown of our Mastodon instance twice. So, we migrated from securitymastod.one to mastodon.at, and then to chaos.social. Each time, we lost all of our posts, leaving behind a considerable number of dead links.
2. We can't easily update our posts. From time to time, there are typos in our posts (despite checking them before), or there is another important reason for updating a post. Mastodon only allows us to delete and repost content. However, deleting and reposting also unlinks all comments, shares, and so on.
3. There is a limit of _n_ characters for each post. While the limit helps to focus on the core message, it also leads to misunderstandings or prevents us from posting. Occasionally, there is content that can't be condensed, but there is no reason to write an article. So, we don't post it.
4. Some parties in the Fediverse demand "self-censorship." Especially when we talk about particular services or products, individuals contact us and demand that we delete our posts. "We shouldn't talk about the topic," so other people don't start to use these "evil" services and products. In our opinion, such demands contradict the claim of being an "open-minded community."

**Update (August 2020):** We added a ["News" section](/news/) to address the issues mentioned above.

**Update (November 2020):** We left the Fediverse permanently.

Besides, we want to address some recent comments:

* Followers: Some readers pointed out the number of our followers in the Fediverse. Currently, there are more than 2,100 accounts following us in the Fediverse. However, we all don't know how many of these accounts are still actively in use. How many accounts are already abandoned? How many accounts are backup accounts? How many accounts are just bots? We don't know, but we think that the actual number of "real" readers in the Fediverse is way smaller than 2,100. This is also true for other accounts with many followers, like Mastodon admins. Furthermore, we aren't motivated by the number of followers.
* Neglecting minority groups: Another reader wrote that dropping services due to little contact is neglecting minority groups. However, we can't directly address every community, as written above.
* Bot: Following our announcement that we reconsidered our presence in the Fediverse, some readers suggested we turn our Mastodon account into a bot. At first glance, operating a bot account may look less time-consuming; however, keeping accounts secure means that we have to monitor bot accounts, too. So, the effort remains nearly the same, while there is less interaction with our readers.
* Patreon/donations: Some readers further suggested crowdfunding. As we pointed out before, we don't want to accept money or any commodity contributions. Crowdfunding introduces the new overhead of managing finances (including taxes). Consider that InfoSec Handbook isn't a legal entity, and all contributors of the InfoSec Handbook reside in different European countries. Besides, taking money doesn't lead to more time for our website, but creates new dependencies.

All in all, we still evaluate different possibilities to provide useful content that remains accessible to everybody.

## Our activities of the month {#ishotm}
In May, we removed several accounts and our Dat mirror, as written above. Moreover, we updated many articles and pages.

A friendly reminder: We [documented the upcoming changes of our Web server security series](https://codeberg.org/infosechandbook/blog-content/issues/13#issue-21699) on codeberg.org. Feel free to comment on this.

Besides, we asked our readers in the Fediverse about an "Ask Me Anything" (AMA) event in July. 60 readers participated, and 86% chose "Yes, do it" (see the pie chart below). We publish more details in upcoming posts.

{{< webpimg "art-img/2020-5-mr-ish-ama-poll.png" "Our poll: What do you think about an 'Ask Me Anything' event in mid-July?" "a pie chart about what people think about an Ask Me Anything event in mid-July. 86% support the idea of an AMA event." >}}

## External links {#links}
* {{< extlink "https://thunderspy.io/" "Thunderspy – When Lightning Strikes Thrice: Breaking Thunderbolt 3 Security" >}}
* {{< extlink "https://francozappa.github.io/about-bias/publication/antonioli-20-bias/antonioli-20-bias.pdf" "BIAS: Bluetooth Impersonation AttackS" >}} (PDF file)
* {{< extlink "https://arxiv.org/pdf/2005.00395.pdf" "POWER-SUPPLaY: Leaking Data from Air-Gapped Systems by Turning the Power-Supplies Into Speakers" >}} (PDF file)
* {{< extlink "https://nebelwelt.net/publications/files/20SEC3.pdf" "USBFuzz: A Framework for Fuzzing USB Drivers by Device Emulation" >}} (PDF file)
* {{< extlink "https://w3c.github.io/webappsec-feature-policy/" "Permissions Policy" >}}
* {{< extlink "https://dat.foundation/" "dat:// – A distributed data community for the next generation Web." >}}
