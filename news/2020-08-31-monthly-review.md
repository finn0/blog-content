+++
title = "Monthly review – August 2020"
author = "Benjamin"
date = "2020-08-31T19:29:59+02:00"
tags = [ "grub", "openpgp", "apache" ]
categories = [ "Monthly review" ]
ogdescription = "Our monthly review of August 2020."
slug = "2020-08-31-monthly-review"
banner = "banners/monthly-review"
toc = true
notice = true
news = true
+++

Each month, we publish a review that covers essential activities of the last 30 days. This month, we discuss vulnerabilities in GRUB2, OpenPGP and S/MIME implementations, and Apache, plus our recent activities.
<!--more-->
{{< rssbox >}}

## News of the month {#notm}
In August 2020, we read the following reports:

### BootHole vulnerability in GRUB2 {#boothole}
At the end of July, [the BootHole vulnerability]({{< relref "#links" >}}) surfaced. BootHole (CVE-2020-10713) is a vulnerability in the GRUB2 boot loader that affects Linux and Windows systems. An attacker could bypass the Secure Boot feature of the system if exploited. However, to exploit BootHole, an attacker already needs write permissions to modify the grub.cfg configuration file.

The problem with BootHole is that there is no easy fix since vendors of affected operating systems need to update the vulnerable GRUB2 version and add the vulnerable ones to the official UEFI Revocation List. Until then, the vulnerability remains exploitable.

After learning about BootHole, the Canonical security team checked the GRUB2 code and discovered seven additional vulnerabilities (CVE-2020-14308, CVE-2020-14309, CVE-2020-14310, CVE-2020-14311, CVE-2020-15705, CVE-2020-15706, CVE-2020-15707). So make sure that you installed the most recent version of GRUB2 on all of your (Linux) systems.

### Mailto vulnerabilities in OpenPGP and S/MIME implementations {#mailto}
A team of security researches published a paper called ["Mailto: Me Your Secrets."]({{< relref "#links" >}}) Their research shows three vulnerabilities in OpenPGP and S/MIME implementations that can be exploited to replace S/MIME certificates, sign arbitrary messages, or exfiltrate private OpenPGP keys.

As security researchers usually work with affected vendors to fix the vulnerabilities before releasing their findings, the latest software should be patched. Check that you run the latest versions of the affected implementations.

### Apache HTTP Server vulnerabilities {#apache}
Finally, [three vulnerabilities in Apache HTTP Server]({{< relref "#links" >}}) were reported. One vulnerability (CVE-2020-11984) affects the module "mod_proxy_uwsgi," and can be exploited for remote code execution. The remaining vulnerabilities (CVE-2020-9490, CVE-2020-11993) affect HTTP/2, and can be exploited for denial of service attacks.

Check that your server runs Apache HTTP Server 2.4.44 or newer (or the version of your operating system that backported the patches).

### Data breaches and leaks
As every month, Have I Been Pwned added information about some newly-discovered breaches and leaks:

* Havenly (breached in June 2020)
* Vakinha (breached in June 2020)
* (Korean interior decoration website) (breached in March 2020)
* TrueFire (breached in February 2020)
* Kreditplus (breached in June 2020)
* ProctorU (breached in June 2020)
* Sonicbirds (breached in December 2019)
* Catho (breached in March 2020)
* Utah Gun Exchange (breached in July 2020)
* Unico Campania (breached in August 2020)
* LiveAuctioneers (breached in June 2020)

Check if you were affected, and change your credentials.

## Our activities of the month {#ishotm}
### The News section
As discussed before, we finally added a [News section](/news/) that separates (longer) handbook-style articles from (shorter) news-style posts. The News section allows us to update these posts, bypass Mastodon's character limit, and provide more information. We decided to release our Monthly Reviews as news posts from now on.

### Theme improvements
As suggested by a reader, we introduced the HTML tags "kbd" and "samp" instead of using "code" tags everywhere. In the case that you don't know these HTML tags, these are their purposes:

* "kbd" represents user input (e.g., voice input, textual input from a keyboard, single keystrokes).
* "samp" represents sample output from a program.
* "code" represents computer code.

This change should improve the readability and accessibility of our articles. Be aware that there might be some places on our website where we didn't update the tags so far; however, most articles should be updated by now.

### Smaller file sizes and less resource consumption
As another improvement, we revised our content's building process and added a new PNG compression algorithm that reduces the file size of PNG and WebP files by another 5 to 10%. Additionally, we separated some CSS code and only load it when needed. Finally, we pre-compress our content now so that our web server directly serves the pre-compressed files instead of compressing files on the fly.

All in all, these three changes should reduce the already small size of our content even more and reduce the resource consumption of our web server.

## Closing words {#cw}
We plan to split all previous Monthly Review so that the "news" part goes to the News section, and the remaining content goes to dedicated articles.

## External links {#links}
* {{< extlink "https://eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/" "There's a Hole in the Boot" >}}
* {{< extlink "https://www.nds.ruhr-uni-bochum.de/media/nds/veroeffentlichungen/2020/08/15/mailto-paper.pdf" "Mailto: Me Your Secrets. On Bugs and Features in Email End-to-End Encryption" >}} (PDF file)
* {{< extlink "https://httpd.apache.org/security/vulnerabilities_24.html" "Apache HTTP Server 2.4 vulnerabilities" >}}
