+++
title = "Security and disclosure policy"
ogdescription = "Security-related information and the disclosure policy of InfoSec Handbook"
aliases = [ "security-policy", "disclosure-policy" ]
nodateline = true
noprevnext = true
toc = true
+++

This page is about our security and disclosure policy. Have a look at our [privacy policy]({{< ref "privacy-policy.md" >}}), if you are looking for privacy-related topics.

## Security contact
* We provide a [security.txt file](https://infosec-handbook.eu/.well-known/security.txt) for structured security contact information.
* See our [contact page]({{< ref "contact-details.md" >}}) for contact details and our OpenPGP key.

## For us, security and privacy take top priority {#our-security}
✅ No logging by default – ✅ Minimal data processing

We decided to choose the best protection for your [personal data]({{< ref "glossary.md#personal-data" >}}): We simply do not collect it. You don't have to trust us, because you keep your data. By default, we do not log anything, and we concluded a data processing agreement according to Article 28 GDPR with our server provider (see our [privacy policy]({{< ref "privacy-policy.md" >}})). We do not track you, and we do not set any cookies.

✅ Single-purpose server – ✅ No databases

For security, we provide our website using a dedicated virtual server. There aren't any other public services on this server (e.g., no database server, no mail server, no messaging server).

✅ Security monitoring – ✅ Strong authentication – ✅ Defined processes

We permanently monitor our server to check for modified files and login attempts. [Two-factor authentication]({{< ref "glossary.md#2fa" >}}) is needed to access our server. The core of our server is a [hardened]({{< ref "glossary.md#hardening" >}}) Linux installation. Hardening means that we removed unnecessary packages and applied strict configuration at the kernel level. Finally, we implemented processes to ensure the installation of security updates within a narrow time frame and quick reaction to reported potential security vulnerabilities.

✅ 100% static content – ✅ No CMS, PHP, or JavaScript – ✅ No 3rd party content

Our website consists of 100% static content. There is no content management system (CMS) installed, and there is no dynamically-served content like PHP or JavaScript. We do not embed any third-party content, and all links to third-party websites are visually marked. If you navigate to other websites from the InfoSec Handbook, the new browser tab runs in a separate process in your web browser, and we strip any Referrer information.

✅ 100% transparency – ✅ Available on archive.org – ✅ No hidden changes

You find all changes on InfoSec Handbook on {{< extlink "https://codeberg.org/infosechandbook/blog-content/commits/branch/master" "codeberg.org" >}}. Our commits are cryptographically signed. When we update our content, we add a small changelog to the bottom of the post, listing the most significant changes. Moreover, our website is listed on {{< extlink "https://web.archive.org/web/https://infosec-handbook.eu" "archive.org" >}}. This way, you can go back in history and check our changes.

---

## Disclosure policy
Did you find a potential security vulnerability? You find our [security-related contact details]({{< relref "#security-contact" >}}) above. We won't take legal action against you as a penetration tester if you observe the law, and we won't publish your identity by default.

Besides, we run a bug bounty program to ensure the highest level of security and privacy. Everyone is eligible to participate in the program as described by this policy.

### Disclosure process
We are big fans of "coordinated disclosure." Due to this, we stay with the following process:

#### 1. You start to test for security vulnerabilities
First of all, thanks for helping us to improve the security of the InfoSec Handbook. Please [look at the scope]({{< relref "#scope" >}}) and [observe the testing requirements]({{< relref "#tr-and-coc" >}}). If you have any further questions, please do not hesitate to [contact us]({{< ref "/contact-details.md#e-mail" >}}).

#### 2. You send us a private report
You privately report a potential security vulnerability. Use the [communication channels mentioned above]({{< relref "#security-contact" >}}). **Use our OpenPGP key, and provide your OpenPGP key!**

You may submit your report anonymously; however, we can't get in touch with you in this case.

#### 3. We check your report and you get our feedback
We check your initial report. Depending on our investigation, we either:

* fix the vulnerability and get in touch with you regarding your bug bounty and coordinated disclosure, or
* get in touch with you to request additional information, or
* inform you about the ineligibility of your report.

Expect our initial feedback within 5 days.

#### 4. We wait for your feedback
After sending our feedback to you, we wait up to 30 days for your response.

#### 5. We publish information about your report
The final step of the coordinated disclosure process can be:

* We agree on coordinated disclosure of the fixed vulnerability. Upon request, we add your name to our [Acknowledgments]({{< relref "#acknowledgments" >}}) section.
* We publish information regarding an invalid vulnerability to inform future testers.

### Scope and possible bug bounties {#scope}
The disclosure policy on this page is valid for the following domain names (and underlying servers):

| Domain name| Eligible for bug bounties |
|------------|---------------------------|
| https://infosec-handbook.eu/ | yes |
| All other domains operated by us | no |

The following bounties are only a guideline. We include the actual bug bounty in our responses. **If all testing requirements were met**, we offer the following bounties:

| Type of vulnerability                      | Bug bounty up to |
|--------------------------------------------|------------------|
| Security-relevant configuration weakness   | Acknowledgment   |
| Information leakage (except personal data) | €75              |
| Code injection (e.g., HTML, JS)            | €100             |
| Unauthorized access (user-level)           | €100             |
| Remote Code Execution (RCE)                | €150             |
| Leakage of personal data                   | €175             |
| Unauthorized access (root-level)           | €175             |

Out-of-scope are vulnerabilities of software that we don't use, vulnerabilities that require physical access to our servers, and recently disclosed 0-day vulnerabilities. If you report out-of-scope vulnerabilities, you may still be eligible to be listed [below]({{< relref "#acknowledgments" >}}).

Bug bounties can only be paid via bank wire transfer (EU countries only) or Stellar Lumens (XLM). There may exist additional legal regulations and requirements regarding payments and bug bounties in your country.

### Testing requirements and code of conduct {#tr-and-coc}
Please observe our testing requirements and code of conduct:

#### 1. Check whether you are the first reporter
You must be the first reporter of a potential vulnerability. Please {{< extlink "https://codeberg.org/infosechandbook/blog-content/issues?q=&type=all&labels=813" "go to our issue tracker" >}} BEFORE reporting anything, and check whether somebody already reported the potential vulnerability.

#### 2. Check the scope
The reported vulnerability and the domain name must [be in scope]({{< relref "#scope" >}}).

#### 3. Provide a report
Please include the following in your report:

1. A brief description of the security vulnerability (Which software is affected? What is the issue?)
2. A brief description of risks originating from the security vulnerability (What are risks for our website?)
3. A step-by-step guide that allows us to reproduce the issue

If necessary, add screenshots or proof of concept code.

#### 4. Do not act unprofessionally
* Do not randomly attack our server with automated tools. Flooding our servers with millions of requests or executing random attacks neither is something a professional penetration tester does nor something that we want to see.
* Do not leak, manipulate, or destroy any data on our servers.
* Do not publish anything regarding a confirmed and unpatched vulnerability without our prior permission.
* Do not use abusive language, act criminally, or impersonate us.
* Do not demand a bug bounty, or try to press us for money.

## Acknowledgments
We would like to thank the following researchers and testers:

| Date       | Name        | Vulnerability                     | Bounty |
|------------|-------------|-----------------------------------|-----|
| 2019-08-28 | Undisclosed | Unintended metadata in some files | €25 |

## Changelog
We updated this page on May 28, 2020. For transparency, we provide a complete changelog of this page on {{< extlink "https://codeberg.org/infosechandbook/blog-content/commits/branch/master/security.md" "codeberg.org" >}}.
