+++
title = "Contact information"
ogdescription = "Your reason for contacting us"
aliases = [ "contact-general" ]
nodateline = true
noprevnext = true
+++

Before contacting us, please take notice of the following table:

| Contact us to                      | Don't contact us to        |
|------------------------------------|----------------------------|
| ✔️ Ask questions about our content  | ❌ Offer sponsoring         |
| ✔️ Report outdated content          | ❌ Request pentests        |
| ✔️ Report other issues              | ❌ Offer "donations"        |
| ✔️ Report security vulnerabilities  | ❌ Offer ghostwriting       |
| ✔️ Report fake accounts             | ❌ Ask general IT questions|
| ✔️ Say thanks                       | ❌ Offer jobs               |

[Proceed to see our e-mail address …]({{< ref "/contact-details.md" >}})
