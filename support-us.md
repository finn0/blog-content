+++
title = "Support us"
ogdescription = "Ways you can support InfoSec Handbook."
nodateline = true
noprevnext = true
+++

InfoSec Handbook is a growing community of European information security professionals and privacy activists. We don’t accept any form of financial donations or sponsorship, allowing us to remain 100% independent and self-funded.

There are still ways to support our work:

1. Report spelling mistakes and grammar errors. [Via e-mail]({{< ref "contact-details.md" >}}) or [on codeberg.org](https://codeberg.org/infosechandbook/blog-content/issues).
2. Report outdated content in our articles. [Via e-mail]({{< ref "contact-details.md" >}}) or [on codeberg.org](https://codeberg.org/infosechandbook/blog-content/issues).
3. [Send us news and topics that could be interesting for everyone.]({{< ref "contact-details.md" >}})
4. [Report security vulnerabilities.]({{< ref "security.md#security-contact" >}})
5. Tell your friends and family members about InfoSec Handbook and help to make things more secure.

Thank you very much for supporting our work!
