+++
title = "Terminal tips"
ogdescription = "Security-related commands tips which we use often."
nodateline = true
noprevnext = true
toc = true
syntax = true
+++

This page contains useful security-related command-line tools and their commands, tested on Arch Linux. Commands on other Linux-based operating systems or Windows might differ and aren't included.

## bash aliases
Bash aliases aren't a tool but helpful when you want to execute some commands quickly. You can directly set aliases in your "~/.bashrc" file. However, we recommend creating a new file (e.g., "~/.bash_aliases") to store your aliases.

To create an alias, just add {{< kbd "alias [shortcut]='[command]'" >}} to your "~/.bashrc" file. For instance, on Debian-like operating systems, you can add {{< kbd "alias ua='sudo apt update && sudo apt upgrade && sudo apt autoremove'" >}}. Then, restart your terminal window. If you enter {{< kbd "ua" >}} now, your system executes all the three commands in succession.

If you define your aliases in a separate file, you need to add a link to your "~/.bashrc" file. To do so, add the following to your "~/.bashrc" file:

```bash
if [ -f ~/.bash_aliases ]; then
  . ~/.bash_aliases
fi
```

Change "~/.bash_aliases" to the name of your file. Kindly note that this file is executed to add your commands.

## cheat
cheat is a small tool to create and view interactive cheat sheets using the command line. For instance, enter {{< kbd "cheat gpg" >}}, {{< kbd "cheat git" >}}, {{< kbd "cheat openssl" >}}, or {{< kbd "cheat nmap" >}}. By default, cheat sheets are stored at "~/.cheat" and can be modified.

## chromium
The web browser Chromium can be configured by changing so-called switches. The switches allow you to restrict [cipher suites]({{< ref "glossary.md#cipher-suites" >}}) and the [TLS]({{< ref "glossary.md#tls" >}}) version used by Chromium. You could change the configuration in the terminal; however, future updates will overwrite this.

Therefore, create "~/.config/chromium-flags.conf" and add:

* {{< kbd "--cipher-suite-blacklist=0x009c,0x009d,0x002f,0x0035,0x000a,0xc013,0xc014" >}} (disables weak cipher suites)
* {{< kbd "--ssl-version-min=tls1.2" >}} (disables all TLS versions except TLS 1.2)

Use [Qualys' SSL Client Test](https://clienttest.ssllabs.com:8443/ssltest/viewMyClient.html) to check if all weak cipher suites are disabled.

## curl
curl is a "command line tool and library for transferring data with URLs." It is a very handy multitool that supports many network protocols. Some basic commands are:

* {{< kbd "curl --head [domainname]" >}} displays HTTP response headers (including security-relevant headers).
* {{< kbd "curl --header '[header]' [domainname]" >}} adds the '[header]' to your request.
* {{< kbd "curl --insecure https://[domainname]" >}} connects to the domain and ignores any certificate errors.
* {{< kbd "curl --sslv3 https://[domainname]" >}} connects to the domain using insecure SSLv3 (also works for other insecure SSL/TLS versions).
* {{< kbd "curl -u user:password -O ftp://[domainname]/[file]" >}} downloads a file using username and password authentication via FTP.

There are many other options. Just test it and look at {{< kbd "man curl" >}}.

## dig
dig is part of [BIND](https://www.isc.org/downloads/bind/) and can be used to check domains for DNSSEC:

* {{< kbd "dig [domain-name] +multiline" >}}
  * "status" should be "NOERROR" ("SERVFAIL" means that there is a problem with the DNS server configuration, e.g., DNSSEC configuration is broken)
  * "flags" must contain "ad" (authentic data)
* {{< kbd "dig [domain-name] +multiline +dnssec" >}}
  * This query sets the "DNSSEC OK" (DO) bit and requests DNSSEC records to be sent, if available
  * Look for "RRSIG" resource records
* {{< kbd "dig [domain-name] +trace" >}}
  * This query emulates a DNS resolver. It starts from the root of the DNS hierarchy and works down using iterative DNS queries.

## fscrypt
fscrypt is a high-level tool for the management of Linux filesystem encryption. fscrypt manages metadata, key generation, key wrapping, PAM integration, and provides a uniform interface for creating and modifying encrypted directories. See our [article on fscrypt]({{< ref "/blog/tool-fscrypt.md" >}}) for guidance.

## gpg
[GnuPG]({{< ref "glossary.md#gnupg" >}}) is often already installed on your machine and can be used for e-mail encryption and signing. You can use it in your terminal, of course. GPG itself is an implementation of [OpenPGP]({{< ref "/glossary.md#openpgp" >}}).

### How to create an OpenPGP key {#gpg-gen}
This section describes how to create a [Curve25519]({{< ref "/glossary.md#curve25519" >}}) key using gpg 2.2.13. If you don't know your version, open a terminal and enter {{< kbd "gpg --version" >}}.

* Open your terminal.
* Enter {{< kbd "gpg --expert --full-generate-key" >}}.
* Select {{< kbd "(9) ECC and ECC" >}}.
* Select {{< kbd "(1) Curve 25519" >}}.
* Enter the validity period, and confirm it by entering {{< kbd "y" >}}.
  * For e-mails, we recommend {{< kbd "6m" >}} (= six months).
  * for Git signing, we recommend {{< kbd "1y" >}} (= one year).
  * for internal use, we recommend less than {{< kbd "2y" >}} (= two years).
* Enter your full name.
* Enter your e-mail address.
  * For e-mails, you need to enter your real e-mail address.
  * For Git signing/internal use, you can enter an arbitrary address (e.g., git(at)lenka.laptop).
* Enter a comment if needed.
* Check everything, and confirm it.
* Enter a passphrase used to encrypt your OpenPGP key locally. You must enter this password every time you want to decrypt/sign something.
* After creation, save the location of the revocation certificate. You need the certificate to revoke your key if you lose access to your private OpenPGP key.
* Your new OpenPGP key pair is ready now. Enter {{< kbd "gpg --list-secret-keys" >}} to see it.
* You can export your public OpenPGP key by entering {{< kbd "gpg --armor --export [key-id] > my-public-gpg-key.asc" >}}.

You can also quickly create a new modern OpenPGP key, as shown in our ["Monthly review – August 2019"]({{< ref "/news/2019-08-31-monthly-review.md#tipotm" >}}).

### Asymmetric encryption {#gpg-as}
We save our cleartext as "clear.txt." You can also use {{< kbd "echo 'your message'" >}}, of course. The ciphertext is stored as "cipher.txt."

* Encrypt and sign: {{< kbd "cat clear.txt | gpg -esar [key-id-of-the-recipient] -u [your-key-id] > cipher.txt" >}}
  * "-e" means encrypt
  * "-s" means sign
  * "-a" means ASCII format
  * "-r" means encrypt for the following key id of the recipient
  * "-u" means use the following (your) key id for signing
* Decrypt: {{< kbd "cat cipher.txt | gpg -d > clear.txt" >}}
  * "-d" means decrypt

### Symmetric encryption {#gpg-s}
gpg can be used to symmetrically encrypt data, too:

* Encrypt: {{< kbd "gpg -c --cipher-algo AES256 clear.txt" >}}
  * "-c" means symmetrically encrypt
  * "--cipher-algo AES256" means use [AES]({{< ref "glossary.md#aes" >}})-256 for encryption
* Decrypt: {{< kbd "gpg -d ciphertext.gpg > clear.txt" >}}
  * "-d" means decrypt

Please note that your device temporarily caches the key used for encryption/decryption. When you are running gpg 2.2.7 or newer, you can turn off caching by adding {{< kbd "--no-symkey-cache" >}}.

## imagemagick
Well-known tools use imagemagick, so it is likely that imagemagick is already installed on your machine. You can use it to remove metadata from photos:

* Remove metadata: {{< kbd "mogrify -strip [filename]" >}}
  * "-strip" means "strip the image of any profiles, comments or these PNG chunks: bKGD, cHRM, EXIF, gAMA, iCCP, iTXt, sRGB, tEXt, zCCP, zTXt and date"
* View metadata: {{< kbd "identify -format '%[EXIF:*]' [filename]" >}}
  * shows Exif metadata in the file

## minisign
Minisign is a small tool that uses Ed25519 for cryptographic signing. See our [article on Minisign]({{< ref "/blog/tool-minisign.md" >}}) for examples.

## openssl
You can use openssl for many purposes. For example, whenever you need pseudo-random bytes:

* Print bytes to terminal: {{< kbd "openssl rand [number-of-bytes]" >}}
* Hex format: {{< kbd "openssl rand -hex [number-of-bytes]" >}}

Then, there is OpenSSL's SSL/TLS client program:

* {{< kbd "openssl s_client -connect infosec-handbook.eu:443" >}} connects to the domain. The output contains information about certificates, TLS parameters, and TLS session tickets.

## pwgen
Do you need a password now? Use pwgen:

* Create passwords containing upper-case and lower-case chars, digits and special chars: {{< kbd "pwgen -scyn1 [number-of-characters] [number-of-passwords]" >}}
* Create passwords containing upper-case and lower-case chars and digits: {{< kbd "pwgen -scn1 [number-of-characters] [number-of-passwords]" >}}

## qrencode
qrencode can be used to transform arbitrary strings into QR codes:

* {{< kbd "qrencode -o [qr-filename].png '[string]'" >}}
* Change the pixel size: {{< kbd "qrencode -o [qr-filename].png -s [pixel-size] '[string]'" >}}

## signal-cli
signal-cli can be used as a Signal messenger client in the terminal. See our [article on signal-cli]({{< ref "/blog/tool-signal-cli.md" >}}) for examples.

## subnetcalc
subnetcalc is a CLI-based calculator for subnets of IPv4 and IPv6 networks.

* {{< kbd "subnetcalc 192.168.1.1/24" >}} prints network, netmask, broadcast address, max. hosts, properties, and more.
* {{< kbd "subnetcalc infosec-handbook.eu" >}} prints IP addresses, properties, geo-information about the IP address, and more.

## ykman
The official YubiKey Manager can be used to manage the features of a YubiKey. For all commands, see {{< extlink "https://support.yubico.com/support/solutions/articles/15000012643-yubikey-manager-cli-ykman-user-guide" "YubiKey Manager CLI (ykman) User Manual" >}}.

### General commands
* {{< kbd "ykman list" >}}: List all YubiKeys connected to the device.
* {{< kbd "ykman info" >}}: Show details of a connected YubiKey, including its serial number.

### ykman config
The "config" command can be used to enable or disable applications (features) on the YubiKey.

* {{< kbd "ykman config [nfc|usb] -e [OTP|U2F|OPGP|PIV|OATH|FIDO2]" >}}: Enable features of your YubiKey over NFC or USB.
* {{< kbd "ykman config [nfc|usb] -d [OTP|U2F|OPGP|PIV|OATH|FIDO2]" >}}: Disable features of your YubiKey over NFC or USB.
* {{< kbd "ykman config set-lock-code [application]" >}}: Set a lock code to protect the configuration of an application (up to 32 characters).

### ykman fido
If you own a normal YubiKey, the following commands are only relevant for WebAuthn. If you own a FIPS-certified YubiKey, the same commands can be used for U2F.

* {{< kbd "ykman fido list" >}}: List all resident credentials (WebAuthn) on the YubiKey.
* {{< kbd "ykman fido set-pin" >}}: Set a PIN to protect resident credentials (4 and 128 characters).
* {{< kbd "ykman fido reset" >}}: Reset FIDO-related credentials (WebAuthn and U2F).

### ykman oath
The "oath" command can be used to manage [OATH-TOTP]({{< ref "/glossary.md#oath-totp" >}}) credentials. You can use it instead of the Yubico Authenticator.

* {{< kbd "ykman oath add -i [issuer] [name] [secret]" >}}: Register OATH-TOTP issued by [issuer] using the [secret] (provided by the issuer) and store it as [name]. (By default, TOTPs have six digits, use SHA-1, and are valid for 30 seconds.)
* {{< kbd "ykman oath code" >}}: Show TOTPs for all accounts on the YubiKey.
* {{< kbd "ykman oath code -s [name]" >}}: Show the TOTP for the specified account.
* {{< kbd "ykman oath delete [name]" >}}: Remove the specified account.
* {{< kbd "ykman oath reset" >}}: Reset OATH-related credentials (HOTP and TOTP).
* {{< kbd "ykman oath set-password" >}}: Set a password to protect OATH-related credentials (HOTP and TOTP).

## zbarimg + oathtool {#zbarimg-oathtool}
Do you want to use [two-factor authentication]({{< ref "glossary.md#2fa" >}}) in the terminal? You can use [OATH-TOTP]({{< ref "glossary.md#oath-totp" >}}) with zbarimg + oathtool:

* Enable 2FA on the website. Normally, you will see a QR code. Save this QR code.
* Use {{< kbd "zbarimg [file-containing-qr-code]" >}} to show the string representation of the QR code. This looks like {{< samp "QR-Code:otpauth://totp/[blabla]?secret=T2LAELPYIS2NGNYE&issuer=[website-owner]&algorithm=SHA1&digits=6&period=30" >}}
  * "secret=T2LAELPYIS2NGNYE" is the important part here!
  * "algorithm=SHA1" is the used [hash function]({{< ref "glossary.md#hash-function" >}})
  * "digits=6" means that the [OTP]({{< ref "glossary.md#otp" >}}) is 6 digits long
  * "period=30" means that the OTP changes every 30 seconds
* Use {{< kbd "oathtool --base32 --totp 'T2LAELPYIS2NGNYE'" >}} to get your OTP each time.
* The output is like "608166."

**WARNING**: The secret (e.g., T2LAELPYIS2NGNYE) is a secret! Store it like a password and use a second device to generate your OTPs. Do not use the device that you use to log in! Do not store the shared OTP secret and the password in the same database!

There is also the Python 3 script "MinTOTP" that can be used to generate TOTP codes. See our ["Monthly review – October 2019"]({{< ref "/news/2019-10-31-monthly-review.md#toolotm" >}}).

## zmap
zmap is a fast network scanning tool. It is very useful for large-scale (internet-wide) scans. One example command is {{< kbd "zmap -p [port] -o [output-filename].csv [network]" >}}. You can add "--dryrun" to test your command without actually running it.
