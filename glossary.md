+++
title = "Glossary"
ogdescription = "Definitions of terms in information security and data protection"
nodateline = true
noprevnext = true
+++

This glossary defines terms often used on infosec-handbook.eu.

[2]({{< relref "#2" >}}) | [A]({{< relref "#a" >}}) | [B]({{< relref "#b" >}}) | [C]({{< relref "#c" >}}) | [D]({{< relref "#d" >}}) | [E]({{< relref "#e" >}}) | [F]({{< relref "#f" >}}) | [G]({{< relref "#g" >}}) | [H]({{< relref "#h" >}}) | [I]({{< relref "#i" >}}) | [K]({{< relref "#k" >}}) | [M]({{< relref "#m" >}}) | [N]({{< relref "#n" >}}) | [O]({{< relref "#o" >}}) | [P]({{< relref "#p" >}}) | [R]({{< relref "#r" >}}) | [S]({{< relref "#s" >}}) | [T]({{< relref "#t" >}}) | [U]({{< relref "#u" >}}) | [V]({{< relref "#v" >}}) | [W]({{< relref "#w" >}}) | [X]({{< relref "#x" >}}) | [Y]({{< relref "#y" >}}) | [Z]({{< relref "#z" >}})

## 2
### 2FA
{{< dfnl "Two-factor authentication" "2FA" >}} requires individuals or systems to prove their identity by providing two different factors: something they have, something they know, or [something they are]({{< relref "#biometrics" >}}). For example, you must provide your credit card (sth. you have) and PIN (sth. you know) when you withdraw money. On the internet, common 2FA standards are [OATH-TOTP]({{< relref "#oath-totp" >}}) and the [W3C WebAuthn API]({{< relref "#webauthn" >}}).

## A
### Accountability
{{< dfns "Accountability" >}} is a [security goal]({{< relref "#security-goal" >}}) of [RMIAS]({{< relref "#rmias" >}}). It means that a system can hold users responsible for their actions.

### Advanced persistent threat
{{< dfnl "Advanced persistent threat" "APT" >}} means that an attacker infiltrates a system over a long period. He adapts his actions to his victims to pass undetected and gain a permanent foothold. Therefore, APTs are very customized attacks.

### AEAD
{{< dfns "Authenticated Encryption with Associated Data" >}} (AEAD) includes Authenticated Encryption (AE). AE combines encryption and [MAC]({{< relref "#mac" >}}) to accomplish [confidentiality]({{< relref "#confidentiality" >}}), [integrity]({{< relref "#integrity" >}}), and [authenticity]({{< relref "#authenticity" >}}). AEAD allows its users to transfer additional unencrypted but authenticated data. AD achieves the [security goals]({{< relref "#security-goal" >}}) of integrity and authenticity. For example, modern [TLS]({{< relref "#tls" >}}) [cipher suites]({{< relref "#cipher-suites" >}}) implement AEAD.

### AES
{{< dfnl "Advanced Encryption Standard" "AES" >}} is a widespread [symmetric]({{< relref "#symmetric-cryptography" >}}) encryption algorithm.

### Allowlist
An {{< dfns "allowlist" >}} explicitly defines actions that are allowed. Undefined actions are denied by default. Other terms are whitelist and passlist. For instance, "application whitelisting" means that only authorized applications can be executed on the system. The opposite is a [denylist]({{< relref "#denylist" >}}).

### ALPN
{{< dfns "Application-Layer Protocol Negotiation" >}} (ALPN) is a [TLS]({{< relref "#tls" >}}) extension. It is needed by HTTP/2 to improve the compression of web pages and to reduce latency. ALPN replaced Next Protocol Negotiation (NPN).

### Argon2
{{< dfns "Argon2" >}} is a [key derivation function]({{< relref "#kdf" >}}). It uses a [password]({{< relref "#password" >}}) and additional parameters to derive a stronger cryptographic key. This process is called [key stretching]({{< relref "#key-stretching" >}}) and makes [brute-force attacks]({{< relref "#brute-force-attack" >}}) less feasible. Argon2 can be used to store passwords securely in a database. Another widespread KDF is [PBKDF2]({{< relref "#pbkdf" >}}).

### Attack tree
{{< dfns "Attack trees" >}} are diagrams to show how something (the root of the tree) can be attacked. The root of the tree is the ultimate goal of the attacker. Leaves and their children show different attack paths. All child nodes of a particular node must be satisfied to make the parent node "true." This process allows the exclusion of nodes when there are protection measures in place.

### Audit
{{< dfns "Auditing" >}} means basically to compare the desired condition of something with its actual state. For instance, there are code audits to find bugs and [vulnerabilities]({{< relref "#vulnerability" >}}). Companies, their subsidiaries, or single data centers can also be audited, for example, to get an ISO/IEC 27001 certification.

### Auditability
{{< dfns "Auditability" >}} is a [security goal]({{< relref "#security-goal" >}}) of [RMIAS]({{< relref "#rmias" >}}). It means that a system can conduct persistent, non-bypassable monitoring of all actions performed by humans or machines within the system.

### Authentication
{{< dfns "Authentication" >}} means that a system/individual confirms the identity of a system/individual. Usually, this is done by providing some kind of proof (something you have, something you know, or [something you are]({{< relref "#biometrics" >}}), and the verifier knows that an identity is linked to this proof. For example, you must provide your credit card (sth. you have) and PIN (sth. you know) when you withdraw money. It is called [two-factor authentication]({{< relref "#2fa" >}}) if you must provide different proofs for authentication.

### Authenticity
{{< dfns "Authenticity" >}} (also called "trustworthiness") is a [security goal]({{< relref "#security-goal" >}}) of [RMIAS]({{< relref "#rmias" >}}). It means that a system can verify the identity of a third party and establish trust in a third party and in the information it provides. An attack on this security goal is the [replay attack]({{< relref "#replay-attack" >}}).

### Availability
{{< dfns "Availability" >}} is a [security goal]({{< relref "#security-goal" >}}) of [RMIAS]({{< relref "#rmias" >}}) and the [CIA triad]({{< relref "#cia-triad" >}}). It means that a system is available when expected. Availability also means that a system (e.g., mail server) can be down for maintenance when previously announced. For instance, attackers can conduct a [DDoS attack]({{< relref "#ddos-attack" >}}) to affect the availability of a system/service.

### Awareness
There is no clear definition of {{< dfns "awareness" >}} in the context of _Information Security Awareness_. It means to raise awareness of threats to information security and to change the behavior of people. Raising awareness remains an integral part of information security since [social engineering]({{< relref "#social-engineering" >}}) attacks target humans who can unwittingly disable security measures or leak information.

## B
### Backdoor
A {{< dfns "backdoor" >}} in software or hardware allows an unauthorized party to bypass access control. For instance, an undocumented developer account in a router will enable developers of this product to bypass the login form. Third parties can also use backdoors to access software/hardware.

### Backporting
{{< dfns "Backporting" >}} in terms of [security vulnerabilities]({{< relref "#vulnerability" >}}) means that someone takes security updates for supported software versions and applies these updates to unsupported software versions. For backporting security updates, one must isolate the actual security update from other changes and ensure that there are no side effects after applying the changes to the unsupported software version. Of course, backported security updates can result in new security [risks]({{< relref "#risk" >}}) like any other modification of software.

### Biometrics
{{< dfns "Biometrics" >}} refers to metrics related to human characteristics and is used for [authentication]({{< relref "#authentication" >}}) (sth. you are). However, biometrics as a single factor for authentication is still considered insecure.

### Block cipher
{{< dfns "Block ciphers" >}} are algorithms to transform fixed-length blocks (groups of bits) using [symmetric keys]({{< relref "#symmetric-cryptography" >}}). There are different modes of operation. Some modes are considered insecure (like ECB). We recommend only modes that combine [confidentiality]({{< relref "#confidentiality" >}}) and [authenticity]({{< relref "#authenticity" >}}) (authenticated encryption).

### Body area network
A {{< dfns "body area network" >}} (BAN; or "wireless body area network" (WBAN)) connects wearable devices of one single person. For instance, an activity tracker is connected with a smartphone using Bluetooth. BANs are smaller than [PANs]({{< relref "#personal-area-network" >}}).

### Botnet
After being taken over (e.g., due to [malware]({{< relref "#malware" >}}) infection), a system (bot) can become part of a large remotely controlled network of bots ({{< dfns "botnet" >}}). Attackers can use these networks for [DDoS attacks]({{< relref "#ddos-attack" >}}) or [phishing]({{< relref "#phishing" >}}).

### Brute-force attack {#brute-force-attack}
An attacker who "simply" tries every possible key to access a service or decrypt a file uses "brute force." This process is called {{< dfns "brute-force attack" >}}. Brute-force attacks become more feasible due to more efficient computers. More efficient machines require the implementation of better algorithms to slow down the process of guessing.

### Buffer overflow
A {{< dfns "buffer overflow" >}} occurs when data is written to a buffer that is too small. The data overruns the boundary of the buffer and overwrites adjacent memory areas then. Buffer overflows are a widespread type of attack, and there are several protective countermeasures available (e.g., DEP, ASLR, or stack canaries).

## C
### CAA
{{< dfnl "DNS Certification Authority Authorization" "CAA" >}} means "DNS Certification Authority Authorization." Domain name holders can define which certificate authorities should be able to issue certificates for this domain. The idea is to prevent unauthorized certificate issuance. However, certificate authorities must support CAA, and some reports were showing that certificate authorities ignored this policy. As of April 2018, only 3.1% of the 150,000 most popular websites implemented CAA (according to Qualys).

### CAPEC
{{< dfnl "Common Attack Pattern Enumeration and Classification" "CAPEC" >}} stands for "Common Attack Pattern Enumeration and Classification," and is currently maintained by the Mitre Corporation, a US-based not-for-profit organization. Like [CVE]({{< relref "#cve" >}}) and [CWE]({{< relref "#cwe" >}}), Mitre created the CAPEC system to structure and define attack patterns.

### Certificate
A {{< dfns "digital certificate" >}} is issued and signed by a trustworthy certificate authority (CA). It contains information like the [public key]({{< relref "#public-key-cryptography" >}}) of the owner, its [fingerprint]({{< relref "#fingerprint" >}}), and the validity period of the certificate. A certificate that is signed by a public CA allows the verifier to check whether the public key is valid and also trustworthy ([integrity]({{< relref "#integrity" >}}) and [authenticity]({{< relref "#authenticity" >}})). One problem with certificates is to check their revocation status (see [CRL]({{< relref "#crl" >}}) and [OCSP]({{< relref "#ocsp" >}})).

### Certificate Transparency
{{< dfns "Certificate Transparency" >}} (CT) is a more complex system that logs information about all [certificates]({{< relref "#certificate" >}}) issued by trustworthy certificate authorities. CT allows clients and other parties to validate certificates provided by servers.

### Challenge-response authentication {#challenge-response-authentication}
The {{< dfns "challenge-response authentication" >}} works as follows: The verifier sends a challenge to the prover. Then, the prover sends his response to the verifier. Finally, the verifier checks whether the response matches the expected one. However, the actual implementation is more complicated: Both parties usually share a secret, and an attacker can capture exchanged messages between both parties. So challenge-response authentication requires protection against [replay attacks]({{< relref "#replay-attack" >}}) and [brute-force attacks]({{< relref "#brute-force-attack" >}}). It is common practice to send a [nonce]({{< relref "#nonce" >}}) (which is only valid for a short time) to the prover who includes it in his response.

### CIA triad
The {{< dfns "CIA triad" >}} is a core concept of [information security]({{< relref "#information-security" >}}). Its elements are [confidentiality]({{< relref "#confidentiality" >}}), [integrity]({{< relref "#integrity" >}}), and [availability]({{< relref "#availability" >}}). However, this view is very limited to information which is why there are newer concepts like [RMIAS]({{< relref "#rmias" >}}).

### Cipher suites
{{< dfns "Cipher suites" >}} (as used in [TLS]({{< relref "#tls" >}})) are sets of algorithms used for key exchange, [authentication]({{< relref "#authentication" >}}), encryption, and [MAC]({{< relref "#mac" >}}). Client and server can support different cipher suites, but they must have at least one cipher suite in common to establish connections.

### Clickbaiting
Typically, {{< dfns "clickbait" >}} is a short text or a headline that is designed to make readers curious, so they want to access linked content. While it is primarily a marketing technique, it can be seen as [social engineering]({{< relref "#social-engineering" >}}) in the broadest sense.

### Clickjacking
{{< dfns "Clickjacking" >}} tricks the user into clicking on concealed links. A clickjacked website looks normal; however, there is an invisible layer over the legitimate website. Users who think that they click the buttons of the authentic website interact with the hidden layer. Known [exploits]({{< relref "#exploit" >}}) are downloading and running [malware]({{< relref "#malware" >}}), sharing links on social media, or enabling the victim's webcam or microphone.

### Confidentiality
{{< dfns "Confidentiality" >}} is a [security goal]({{< relref "#security-goal" >}}) of [RMIAS]({{< relref "#rmias" >}}) and the [CIA triad]({{< relref "#cia-triad" >}}). It means that only authorized individuals/systems can read/modify confidential messages.

### Cookie
HTTP/HTTPS is stateless. Being stateless means that web browsers need other ways to store data when necessary. {{< dfns "Cookies" >}} are small files stored client-side which serve this purpose. However, cookies are sometimes used for user tracking. Therefore, we recommend denying cookies by default (create an [allowlist]({{< relref "#allowlist" >}})) and delete all cookies when you close your web browser.

### Credential stuffing
{{< dfns "Credential stuffing" >}} means an attacker uses previously leaked or stolen lists of valid user credentials (e.g., e-mail addresses and the corresponding passwords) to gain access to other accounts. Contrary to [brute-force attacks]({{< relref "#brute-force-attack" >}}), attackers only use (previously) valid user credentials. Users reusing the same credentials for different accounts are prone to this attack.

### CRL
{{< dfns "Certificate Revocation Lists" >}} (CRL) are simple lists that contain revoked [certificates]({{< relref "#certificate" >}}). However, there are different problems with CRLs. Due to this, some web browsers implement [OCSP]({{< relref "#ocsp" >}}), while Chrome uses a custom mechanism.

### CSP
{{< dfnl "Content Security Policy" "CSP" >}} means "Content Security Policy." Website owners can set a CSP to tell clients (like web browsers) how they should handle the content of the website. For example, a CSP can forbid loading JavaScript, images, and fonts from other web servers. Website owners shouldn't use "unsafe-inline" directives at all.

### CSRF
{{< dfns "Cross-site request forgery" >}} (CSRF or XSRF) is an attack that executes unwanted actions on a website in which the victim is currently authenticated. For the website, it looks like the action originated from the authenticated user. A server-side countermeasure is to use and validate [CSRF tokens]({{< ref "/glossary.md#csrf-token" >}}).

### CSRF token
{{< dfns "CSRF tokens" >}} are unique and unpredictable values to defend against [cross-site request forgery]({{< ref "/glossary.md#csrf" >}}). The server-side application generates tokens and sends them to the client. The client must include the token in a subsequent HTTP request. If the token is invalid or missing, the server requests the request.

### Curve25519
{{< dfns "Curve25519" >}} is an [elliptic curve]({{< relref "#ecc" >}}) that offers 128 bits of security. Its reference implementation is public domain, and it is widely supported. Curve25519 is a SafeCurve (see https://safecurves.cr.yp.to/ for further information).

### CVE
{{< dfnl "Common Vulnerabilities and Exposures" "CVE" >}} stands for "Common Vulnerabilities and Exposures," and is currently maintained by the Mitre Corporation, a US-based not-for-profit organization. Like [CAPEC]({{< relref "#capec" >}}) and [CWE]({{< relref "#cwe" >}}), Mitre created the CVE system to create globally unique identifiers for security [vulnerabilities]({{< relref "#vulnerability" >}}). Identifiers look like "CVE-YEAR-NUMBER." The year included in the identifier is the year when the CVE ID was assigned, not the year when the vulnerability became publicly known.

### CWE
{{< dfnl "Common Weakness Enumeration" "CWE" >}} stands for "Common Weakness Enumeration," and is currently maintained by the Mitre Corporation, a US-based not-for-profit organization. Like [CVE]({{< relref "#cve" >}}) and [CAPEC]({{< relref "#capec" >}}), Mitre created the CWE system to provide a structured list of clearly defined software weaknesses.

### CVSS
The {{< dfns "Common Vulnerability Scoring System" >}} is an open standard to rate the severity of [vulnerabilities]({{< relref "#vulnerability" >}}). CVSS v3.0 provides a score (0 to 10.0, 10.0 means most severe) and a vector string based on a formula that evaluates several metrics to approximate ease and impact of [exploits]({{< relref "#exploit" >}}). There are three metrics: base (is calculated once), temporal (changes over time), and environmental (allows organizations and individuals to adjust the scoring by considering their infrastructure).

## D
### Data protection
{{< dfns "Data protection" >}} is the protection of [personal data]({{< relref "#personal-data" >}}) so that the processor only lawfully processes it, and third parties aren't able to access this data. However, there is more data in companies that must be protected (see [information security]({{< relref "#information-security" >}})).

### DDoS attack
A {{< dfns "DDoS attack" >}} (Distributed Denial of Service attack) tries to overload or crash services on the targeted system by sending millions of requests from numerous sources. The goal of DDoS attacks is to affect the [availability]({{< relref "#availability" >}}) of a service or system, e.g., making a web server unreachable for web browsers.

### Defacement attack
Breaking into a web server to modify (add, change, delete) content of the hosted website is called {{< dfns "website defacement" >}}. Typically, it's easy to spot defacement since defacers want to arouse attention.

### Denylist
A {{< dfns "denylist" >}} explicitly defines actions that are denied. Undefined actions are allowed by default. Other terms are blacklist and blocklist. For instance, a custom e-mail spam filter explicitly blocks defined e-mail addresses. The opposite is an [allowlist]({{< relref "#allowlist" >}}).

### Diceware
{{< dfns "Diceware" >}} is a technique to generate [passphrases]({{< relref "#passphrase" >}}) using dice as a hardware random number generator. A group of five digits represents a word on a word list. Sufficiently long Diceware passphrases aren't vulnerable to [dictionary attacks]({{< relref "#dictionary-attack" >}}) because there is the same probability for every word on the list to be chosen, and words are picked randomly. You randomly generate numbers and replace these numbers with words to be easily readable.

### Dictionary attack
Iterating over a word list (= dictionary) and trying every word to access a service or decrypt a file, is a {{< dfns "dictionary attack" >}}. A suitable [hash function]({{< relref "#hash-function" >}}) and [salt]({{< relref "#salt" >}}) can defeat this attack as long as the attacker doesn't have an appropriate word list and sufficient computing power.

### Digital signature
{{< dfns "Digital signatures" >}} base on [public-key cryptography]({{< relref "#public-key-cryptography" >}}) and are used to provide [non-repudiation]({{< relref "#non-repudiation" >}}), [authenticity]({{< relref "#authenticity" >}}), and [integrity]({{< relref "#integrity" >}}). A user uses a private key to digitally sign data, while the corresponding public key is used by third parties to validate the signature of the user. It's imperative to verify the owner of the public key.

### DMZ
The {{< dfns "demilitarized zone" >}} (DMZ) is a physical or logical subnetwork that contains the external-facing services of a network. External hosts on the internet can only connect to servers/services in the DMZ. However, they can't connect to hosts of the private network outside the DMZ since a [firewall]({{< relref "#firewall" >}}) protects the private part.

### DNSSEC
{{< dfnl "Domain Name System Security Extensions" "DNSSEC" >}} stands for "Domain Name System Security Extensions." Its primary purpose is [authentication]({{< relref "#authentication" >}}) by signing DNS data, so DNS resolvers can check if DNS records remained unchanged. DNSSEC responses are only signed, not encrypted. It allows [integrity]({{< relref "#integrity" >}}) checks but does not provide [confidentiality]({{< relref "#confidentiality" >}}) of data.

### Downgrade attack
A protocol that allows different levels of security can be vulnerable to {{< dfns "downgrade attacks" >}}. An attacker tries to downgrade the security level to the lowest one, so it is easier for him to attack. A well-known example is POODLE.

### Doxing
Publicly releasing [private data]({{< relref "#personal-data" >}}) about an individual or organization is called {{< dfns "doxing" >}}. Before publication, the person conducting doxing uses public databases, social media, or [social engineering]({{< relref "#social-engineering" >}}) to acquire information.

## E
### ECC
{{< dfns "Elliptic-curve cryptography" >}} (ECC) is an approach to [public-key cryptography]({{< relref "#public-key-cryptography" >}}) based on the algebraic structure of elliptic curves over finite fields. ECC requires smaller keys compared to non-EC cryptography (like used by [RSA]({{< relref "#rsa" >}})) to provide equivalent security.

### ECDSA
The {{< dfns "Elliptic Curve Digital Signature Algorithm" >}} (ECDSA) is a variant of the Digital Signature Algorithm (DSA). It uses [elliptic-curve cryptography]({{< relref "#ecc" >}}) to sign data digitally.

### Ed25519
{{< dfns "Ed25519" >}} is an [EdDSA]({{< relref "#eddsa" >}}) signature scheme using SHA-512 and [Curve25519]({{< relref "#curve25519" >}}). It offers roughly the same security as NIST curve P-256, [RSA]({{< relref "#rsa" >}}) with 3000-bit keys, or 128-bit [block ciphers]({{< relref "#block-cipher" >}}). Ed25519 [signatures]({{< relref "#digital-signature" >}}) fit into 64 bytes, and public keys consume only 32 bytes.

### EdDSA
{{< dfnl "Edwards-curve Digital Signature Algorithm" "EdDSA" >}} stands for "Edwards-curve Digital Signature Algorithm." It uses [elliptic curves]({{< relref "#ecc" >}}) for [digital signatures]({{< relref "#digital-signature" >}}). One famous variant is [Ed25519]({{< relref "#ed25519" >}}).

### End-to-end encryption {#end-to-end-encryption}
{{< dfns "End-to-end encryption" >}} (also E2EE) ensures that all communication between two endpoints is encrypted and can only be decrypted by the endpoints (e.g., [Signal messenger]({{< relref "#signal-protocol" >}}), [GnuPG]({{< relref "#gnupg" >}})).

### Entropy
{{< dfns "Entropy" >}} describes the strength of keys in bits. One bit represents two possible outcomes (0 or 1). A key with 100-bit entropy is equal to 2 to the power of 100 possibilities to create this key. Every extra bit duplicates the number of possibilities.

### Exploit
An {{< dfns "exploit" >}} is code to exploit a [vulnerability]({{< relref "#vulnerability" >}}). Even worse are [zero-day exploits]({{< relref "#zero-day-exploit" >}}).

## F
### Federation
{{< dfns "Federation" >}} means that users of a network can communicate with users of another network without being part of the other network. For instance, Facebook isn't federated because all users have to be on Facebook to communicate with each other. By contrast, e-mail is federated because a Gmail user can send an e-mail to a mailbox.org user.

### Firewall
A {{< dfns "firewall" >}} is software that limits access between two networks or systems and follows a security policy. Firewalls can be network-based or host-based. There are different types of firewalls, like packet filters, stateful filters, and application layer firewalls. Another specific firewall is a [web application firewall]({{< relref "#web-application-firewall" >}}).

### FIDO2
The {{< dfnl "Fast IDentity Online 2" "FIDO2" >}} Project is an effort to create a new FIDO [authentication]({{< relref "#authentication" >}}) standard that incorporates the upcoming [W3C WebAuthn API]({{< relref "#webauthn" >}}) and the Client-to-Authenticator Protocol (CTAP) developed by the FIDO Alliance.

### Fingerprint
A {{< dfns "digital fingerprint" >}} is a checksum. You can use [hash functions]({{< relref "#hash-function" >}}) to create fingerprints. One advantage is that you only need to check whether the (shorter) fingerprint matches. For example, you calculate the hash sum of a PDF file and send the fingerprint and file to a friend. Your friend only needs to calculate the hash sum and check if it matches yours. Fingerprints are often used in cryptography (e.g., in [certificates]({{< relref "#certificate" >}}) or to verify [public keys]({{< relref "#public-key-cryptography" >}}) in general).

Besides, the term {{< dfns "fingerprint" >}} is used in conjunction with a machine, device, or web browser (e.g., device fingerprint). Such fingerprints consist of (unique) information about devices and can be used to identify individual devices or even users.

### Forward secrecy
{{< dfns "Forward secrecy" >}} (FS; also known as "Perfect Forward Secrecy") combines a system of long-term keys and session keys to protect encrypted communications against key compromise in the future. An attacker who can record every encrypted message ([man-in-the-middle]({{< relref "#man-in-the-middle-attack" >}})) won't be able to decrypt these messages when keys are compromised in the future. Modern encryption protocols like [TLS]({{< relref "#tls" >}}) 1.3 and [Signal Protocol]({{< relref "#signal-protocol" >}}) offer FS.

## G
### GnuPG
{{< dfnl "GNU Privacy Guard" "GnuPG" >}} implements the [OpenPGP]({{< relref "#openpgp" >}}) standard. GPG provides cryptographic functions to encrypt, decrypt, and sign e-mail content, files, etc. [Metadata]({{< relref "#metadata" >}}) (like e-mail addresses or subject) remains unencrypted.

## H
### Hardening
{{< dfns "Hardening" >}} is a generic term for the process of securing systems against attacks. Hardening includes deactivating unused interfaces like USB ports and appropriate rights management.

### Hash function
A {{< dfns "hash function" >}} maps input (e.g., files or data) of arbitrary size to an output of fixed size (e.g., 128-bit string). In cryptography, hash functions must be infeasible to invert (one-way function) and have several additional properties. Hash functions can be used to check the [integrity]({{< relref "#integrity" >}}) of data.

### HMAC
{{< dfnl "keyed-hash message authentication code" "HMAC" >}} means "keyed-hash message authentication code." HMAC combines [MAC]({{< relref "#mac" >}}), a secret key, and a cryptographic [hash function]({{< relref "#hash-function" >}}). It can be used to check the [authenticity]({{< relref "#authenticity" >}}) and [integrity]({{< relref "#integrity" >}}) of data. Unlike MACs, HMACs aren't prone to length extension attacks.

### HSTS
The {{< dfnl "HTTP Strict Transport Security" "HSTS" >}} header tells clients to always use HTTPS connections for this domain name. HSTS becomes only beneficial when sent over HTTPS. HSTS can be misused for user tracking; however, since HTTPS-only became a best practice for most websites, server admins should set this header.

## I
### Industrial control systems
{{< dfns "Industrial control systems" >}} (ICS) are control systems used for industrial process control. They contain Supervisory Control and Data Acquisition (SCADA) systems or distributed control systems (DCS) and programmable logic controllers (PLCs). ICS differ from "traditional" information technology. Most technical security practices can't be simply deployed in ICS environments. Therefore, ICS are considered [operational technology]({{< relref "#ot" >}}).

### IDN homograph attack
{{< dfns "Homoglyphs" >}} are characters with shapes that appear identical or very similar. Attackers make use of homoglyphs to create internationalized domain names (IDN) that look similar to well-known domain names. For example, "infosес-handbook.eu" looks similar to "infosec-handbook.eu." However, the first domain name contains Cyrillic е and с. The visual similarity enables attackers to [impersonate]({{< relref "#impersonation" >}}) individuals and domain names.

### Impersonation
In terms of information security, an {{< dfns "impersonator" >}} is somebody who pretends to be another person (identity theft) to commit fraud or other illegal activities. It is another [social engineering]({{< relref "#social-engineering" >}}) technique.

### Information security
According to Wikipedia, "{{< dfns "[i]nformation security" >}}, sometimes shortened to InfoSec, is the practice of preventing unauthorized access, use, disclosure, disruption, modification, inspection, recording or destruction of information. It is a general term that can be used regardless of the form the data may take (e.g., electronic, physical)."

On the contrary, {{< dfns "IT security" >}} is focused on the protection of computer systems (hardware, software, information). Therefore, IT security is a subset of information security.

The term {{< dfns "information" >}} doesn't include [personal data]({{< relref "#personal-data" >}}). Securing personal data is called [data protection]({{< relref "#data-protection" >}}).

### Integrity
{{< dfns "Integrity" >}} is a [security goal]({{< relref "#security-goal" >}}) of [RMIAS]({{< relref "#rmias" >}}) and the [CIA triad]({{< relref "#cia-triad" >}}). It means that one can determine whether a particular resource was modified compared with the original resource.

### Intrusion detection system
An {{< dfns "intrusion detection system" >}} (IDS) monitors networks (network IDS, NIDS) or hosts (host-based IDS, HIDS) for malicious activity or policy violations. Mostly, detected events are reported to a centralized security monitoring solution.

IDS that can respond to malicious activity or policy violations are referred to as intrusion prevention systems (IPS).

### IPsec
{{< dfnl "Internet Protocol Security" "IPsec" >}} is a secure network protocol suite that authenticates and encrypts the packets of data sent over an internet protocol network. It is used in [virtual private networks]({{< relref "#vpn" >}}). IPsec can be operated in transport mode (only the payload of the IP packet is encrypted and authenticated) or in tunnel mode (the entire IP packet is encrypted and authenticated).

Contrary to [TLS]({{< relref "#tls" >}}), IPsec operates on the Internet Layer.

## K
### KDF
A {{< dfnl "key derivation function" "KDF" >}} derives at least one secret key from an input like a [password]({{< relref "#password" >}}). Some KDFs can be used for [key stretching]({{< relref "#key-stretching" >}}).

### Kerckhoffs's principle
{{< dfns "Kerckhoffs's principle" >}} is derived from six design principles for military ciphers written by Dutch linguist and cryptographer Auguste Kerckhoffs. It says that "A cryptosystem should be secure even if everything about the system, except the key, is public knowledge."

### Key stretching
{{< dfns "Key stretching" >}} is used to make [brute-force attacks]({{< relref "#brute-force-attack" >}}) more difficult by increasing the time it takes to test each possible key. Two widespread key stretching algorithms are [PBKDF2]({{< relref "#pbkdf" >}}) and [Argon2]({{< relref "#argon2" >}}).

### Kill chain
The term {{< dfns "kill chain" >}} originates from a military concept to structure a physical attack. In information security, the {{< dfns "cyber kill chain" >}} is a framework developed by the Lockheed-Martin corporation to structure a digital attack. The cyber kill chain describes different phases of an attack. However, it has some downsides (e.g., several phases happen outside the defended organization, or it is focused on malware-based attacks). Due to this, there is the newer {{< dfns "unified kill chain" >}} based on the cyber kill chain and MITRE’s ATT&CK framework. The unified kill chain describes 18 phases that may occur in digital attacks.

## M
### MAC
A {{< dfnl "message authentication code" "MAC" >}} is a short value used to check the [authenticity]({{< relref "#authenticity" >}}) and [integrity]({{< relref "#integrity" >}}) of data. It protects against message forgery by anyone who doesn't know the secret key. Since this [key is shared among the sender and receiver]({{< relref "#symmetric-cryptography" >}}) of a message, MACs don't provide [non-repudiation]({{< relref "#non-repudiation" >}}).

### Malware
{{< dfns "Malware" >}} (malicious software) is a generic term for software containing unwanted or malicious functions. Malware includes [ransomware]({{< relref "#ransomware" >}}), Trojan horses, computer viruses, worms, spyware, scareware, adware, etc. Nowadays, malware can't be categorized because sophisticated malware often combines properties of different categories. For instance, WannaCry propagated like a worm but encrypted files and demanded ransom (ransomware).

### Man-in-the-middle attack {#man-in-the-middle-attack}
While Alice communicates with Bob via the internet, Eve (Eavesdropper) joins the conversation "in the middle" and becomes {{< dfns "man-in-the-middle" >}}. Eve can modify, insert, [replay]({{< relref "#replay-attack" >}}), or read messages at will. Protective measures are encryption ([confidentiality]({{< relref "#confidentiality" >}})) and checking the [authenticity]({{< relref "#authenticity" >}}) and [integrity]({{< relref "#integrity" >}}) of all messages. However, one must also ensure that one is communicating with the expected party. For instance, when you use [GnuPG]({{< relref "#gnupg" >}}) (or [public-key cryptography]({{< relref "#public-key-cryptography" >}}) in general), you have to verify that you own the real public key of the respective recipient.

### Metadata
{{< dfns "Metadata" >}} is data that provides information about other data. For instance, a JPG file contains the actual picture (data) but also metadata like creation date, type of camera, GPS coordinates, etc. Metadata can be valuable for attackers (e.g., finding appropriate exploits for outdated software used by the victim), government agencies (e.g., collecting information about people to create social graphs), and other parties (e.g., show location-based advertisements). As soon as you use any computer (like your smartphone, laptop, PC, IP camera, smart refrigerator), you very likely leave metadata behind.

## N
### Nitrokey
The {{< dfns "Nitrokey" >}} is an open-source USB key produced by the Nitrokey UG in Germany. It implements [OpenPGP]({{< relref "#openpgp" >}}) card algorithms. One can generate and store [OpenPGP]({{< relref "#openpgp" >}}) key pairs on it. Some models also support creating [OATH-TOTP]({{< relref "#oath-totp" >}}) codes, contain secure password storage, secure data storage, and other cryptographic functions.

### Nonce
In cryptography, a {{< dfns "nonce" >}} is a random number that is only used once. Nonces are used to prevent [replay attacks]({{< relref "#replay-attack" >}}).

### Non-repudiation {#non-repudiation}
{{< dfns "Non-repudiation" >}} is a [security goal]({{< relref "#security-goal" >}}) of [RMIAS]({{< relref "#rmias" >}}). It means that a system can prove (with legal validity) occurrence/non-occurrence of an event or participation/non-participation of a party in an event.

## O
### OATH-HOTP {#oath-hotp}
{{< dfnl "OATH HMAC-based one-time password algorithm" "OATH-HOTP" >}} stands for "OATH [HMAC]({{< relref "#hmac" >}})-based [one-time password]({{< relref "#otp" >}}) algorithm." This algorithm generates OTPs based on a secret key, a counter value, and an HMAC.

### OATH-TOTP {#oath-totp}
{{< dfnl "OATH time-based one-time password algorithm" "OATH-TOTP" >}} stands for "OATH time-based [one-time password]({{< relref "#otp" >}}) algorithm." In addition to [OATH-HOTP]({{< relref "#oath-hotp" >}}), the current timestamp is included to create an OTP.

### OCSP
The {{< dfnl "Online Certificate Status Protocol" "OCSP" >}} can be used to obtain the revocation status of digital [certificates]({{< relref "#certificate" >}}). It's an alternative to [CRL]({{< relref "#crl" >}}). Pure {{< dfns "OCSP" >}} exposes the client's IP address to the OCSP responder while {{< dfns "OCSP stapling" >}} enables the server to store pre-authenticated OCSP information to avoid this. {{< dfns "OCSP Must-Staple" >}} is a certificate extension that allows the client to learn about the presence of OCSP information during the [TLS]({{< relref "#tls" >}}) handshake. Most web browsers support OCSP; however, Chrome uses a custom mechanism to obtain revocation information.

### OMEMO
{{< dfns "OMEMO" >}} (XEP-0384) is an _experimental_ extension to XMPP that allows [end-to-end encrypted]({{< relref "#end-to-end-encryption" >}}) communication and offers [forward secrecy]({{< relref "#forward-secrecy" >}}). It is based on the [Signal Protocol]({{< relref "#signal-protocol" >}}).

### OpenPGP
{{< dfns "OpenPGP" >}} is an open standard, introduced several years after the original PGP (Pretty Good Privacy). OpenPGP is specified in RFC 4880 and some additional RFCs like RFC 6637. OpenPGP-compliant software provides several security services for electronic communications and data storage. Private users mostly use OpenPGP-compliant software like [GnuPG]({{< relref "#gnupg" >}}) for encrypting and [signing]({{< relref "#digital-signature" >}}) e-mails. Other current implementations are NeoPG, Sequoia PGP, and OpenPGP.js. Mailvelope and ProtonMail use the latter.

### OT
{{< dfnl "Operational technology" "OT" >}} includes hardware and software that is used to monitor or modify the physical state of a system. The term OT is used to demonstrate differences between information technology (IT) and [ICS]({{< relref "#industrial-control-systems" >}}) environments.

### OTP
{{< dfnl "one-time password" "OTP" >}} stands for "one-time password." This [password]({{< relref "#password" >}}) is only valid for single use. Standard algorithms to generate OTPs are [OATH-HOTP]({{< relref "#oath-hotp" >}}) and [OATH-TOTP]({{< relref "#oath-totp" >}}).

### OTR
{{< dfnl "Off-the-Record Messaging" "OTR" >}} stands for "Off-the-Record Messaging." It allows [end-to-end encrypted]({{< relref "#end-to-end-encryption" >}}) communication and offers [forward secrecy]({{< relref "#forward-secrecy" >}}). However, it is only suitable for single-client use and synchronous messaging. Both parties must be online at the same time to communicate.

## P
### Passphrase
A {{< dfns "passphrase" >}} is similar to [passwords]({{< relref "#password" >}}); however, it consists of words instead of characters. You can create passphrases with [Diceware]({{< relref "#diceware" >}}).

### Password
A {{< dfns "password" >}} is a string of characters used for [authentication]({{< relref "#authentication" >}}). A strong password consists of randomly-chosen characters that all have an identical probability of occurrence.

### Password spraying
{{< dfns "Password spraying" >}} is a [brute-force attack]({{< relref "#brute-force-attack" >}}). Typically, an attacker tries to guess the password for a fixed username. For instance, the attacker sets "root" as the username and iterates over a long list of different [passwords]({{< relref "#password" >}}). Of course, [intrusion detection systems]({{< relref "#intrusion-detection-system" >}}) (IDS) could easily detect a vast number of unsuccessful login attempts.

Contrary to this, password spraying sets a fixed password (e.g., "123456") and iterates over a long list of different usernames. So, there is only one failed login per username and iteration. As a result, IDS aren't triggered. The attack requires a list of (likely) valid usernames and a list of common passwords. Countermeasures are strong credentials and [two-factor authentication]({{< relref "#2fa" >}}).

### PBKDF
{{< dfnl "Password-Based Key Derivation Function" "PBKDF" >}} (Password-Based [Key Derivation Function]({{< relref "#kdf" >}})) creates cryptographic keys based on a [password]({{< relref "#password" >}}), [HMAC]({{< relref "#hmac" >}}), iterations, and [salt]({{< relref "#salt" >}}). For instance, [WPA2]({{< relref "#wpa" >}}) uses PBKDF2. The aim is to reduce the vulnerability of keys to [brute-force attacks]({{< relref "#brute-force-attack" >}}). According to RFC 8018 section 4.2., "an iteration count of 10,000,000 may be appropriate."

### Penetration test
{{< dfns "Penetration tests" >}} are documented checks and scans on applications, systems, or websites to identify [vulnerabilities]({{< relref "#vulnerability" >}}). Penetration tests are either black box (pen tester has no inside knowledge), gray box (limited knowledge), or white box (pen tester has inside knowledge) tests. Typically, pen test reports include identified vulnerabilities, guidance, and severity of each vulnerability.

### Personal area network
A {{< dfns "personal area network" >}} (PAN; or "wireless personal area network" (WPAN)) is a small network that connects personal devices in the direct workspace of a single person. Technologies used for WPANs are Bluetooth, ZigBee, IrDA, and Wireless USB. Sometimes, one device in the PAN is used to connect all other devices to another network. PANs are smaller than LANs but bigger than [BANs]({{< relref "#body-area-network" >}}).

### Personal data
According to Article 4 of the European General Data Protection Regulation (GDPR), "'{{< dfns "personal data" >}}' means any information relating to an identified or identifiable natural person ('data subject')." An identifiable natural person "is one who can be identified, directly or indirectly, in particular by reference to an identifier such as a name, … an online identifier or … one or more factors specific to the … natural person."

For instance, company names, addresses of authorities, or secret manufacturing data isn't personal data. However, it can also be necessary to protect this non-personal data (see [information security]({{< relref "#information-security" >}})).

A similar but non-synonymous term is {{< dfns "personally identifiable information" >}} (PII) in the US. According to NIST Special Publication 800-122, PII is "any information about an individual … such as name, social security number, date and place of birth, mother‘s maiden name, or biometric records; and … any other information that is linked or
linkable to an individual …."

### Phishing
{{< dfns "Phishing" >}} is a [social engineering]({{< relref "#social-engineering" >}}) technique. Attackers send forged SMS, e-mails, chat messages, etc. to their victims to get their [personal data]({{< relref "#personal-data" >}}). After that, attackers can try to [impersonate]({{< relref "#impersonation" >}}) their victims or do something criminal. [Spear phishing]({{< relref "#spear-phishing" >}}) is a more sophisticated phishing technique.

### Plausible deniability
{{< dfns "Plausible deniability" >}} can be another [security goal]({{< relref "#security-goal" >}}). It is accomplished if you can’t prove that a person/system sent a particular message. Then, this person/system can plausibly deny being the sender of the message.

### Privacy
{{< dfns "Privacy" >}} is a [security goal]({{< relref "#security-goal" >}}) of [RMIAS]({{< relref "#rmias" >}}). It means that a system should obey privacy legislation, and it should enable individuals to control, where feasible, their personal information (user-involvement).

Sometimes, [data protection]({{< relref "#data-protection" >}}) is also called "data privacy."

### Public-key cryptography {#public-key-cryptography}
{{< dfns "Public-key cryptography" >}} (or asymmetric cryptography) is the opposite of [symmetric cryptography]({{< relref "#symmetric-cryptography" >}}). Every party has two keys (public and private). The private one must be kept secret and is used for decryption while the public one has to be published and is used for encryption. All other parties must verify that a published public key belongs to the anticipated owner to avoid [man-in-the-middle attacks]({{< relref "#man-in-the-middle-attack" >}}).

There are different approaches to public-key cryptography. For example, some cryptosystems are [based on the algebraic structure of elliptic curves over finite fields]({{< relref "#ecc" >}}). Others are [based on the difficulty of the factorization of the product of two large prime numbers]({{< relref "#rsa" >}}).

Public-key cryptography can also be used for [digital signatures]({{< relref "#digital-signature" >}}).

## R
### Ransomware
{{< dfns "Ransomware" >}} is a type of [malware]({{< relref "#malware" >}}). There are different subtypes of ransomware. The idea is to press victims for money by threatening them with doing something harmful. One scheme is publishing confidential data in x hours. Another more popular method is encrypting vital data using [public-key cryptography]({{< relref "#public-key-cryptography" >}}) and threaten to delete the corresponding private key after x hours, rendering the encrypted data useless). An important countermeasure is to back up all of your data regularly.

### Referrer
The {{< dfns "referrer" >}} is part of an HTTP request. Clients send the referrer header to the server. There are different use cases for this. However, it can be used for tracking users. Most web browsers allow you to disable referrer headers.

### Replay attack
{{< dfns "Replay attacks" >}} are attacks on [authenticity]({{< relref "#authenticity" >}}). An attacker records messages and resends them (replay). The recipient can't be sure whether the second message (sent by the attacker) was sent by the sender of the first (identical) message or someone else. Using current timestamps, [nonces]({{< relref "#nonce" >}}), and [end-to-end encryption]({{< relref "#end-to-end-encryption" >}}) are primary countermeasures.

### Risk
A {{< dfns "risk" >}} is the level of impact on operations, assets, or individuals. It is based on the impact of a threat and the likelihood of that threat occurring.

### Risk assessment
A {{< dfns "risk assessment" >}} (or {{< dfns "risk analysis" >}}) is the process of identifying [risks]({{< relref "#risk" >}}) to operations, assets, or individuals by determining the probability of occurrence, the resulting impact, and controls that would mitigate this impact.

### Risk management
{{< dfns "Risk management" >}} is the process of the identification, measurement, control, and minimization of [risks]({{< relref "#risk" >}}). It includes [assessing risks]({{< relref "#risk-assessment" >}}), taking actions to reduce risks to an acceptable level, and maintaining risks at an acceptable level.

### RMIAS
{{< dfnl "A Reference Model of Information Assurance & Security" "RMIAS" >}} is a reference model introduced in 2013 which consists of four dimensions: _Security Life Cycle_, _Information Taxonomy_, _Security Goals_, and _Security Countermeasures_. The goal of this model is to overcome restrictions of prior models like the [CIA triad]({{< relref "#cia-triad" >}}) and meet the needs of new trends. Besides the "traditional" [security goals]({{< relref "#security-goal" >}}) of the CIA triad, this model also contains [authenticity/trustworthiness]({{< relref "#authenticity" >}}), [privacy]({{< relref "#privacy" >}}), [accountability]({{< relref "#accountability" >}}), [auditability]({{< relref "#auditability" >}}), and [non-repudiation]({{< relref "#non-repudiation" >}}). These security goals are viewed in the context of components of an information system that are information, people, processes, hardware, software, and networks.

### RSA
{{< dfnl "Rivest–Shamir–Adleman" "RSA" >}} is a well-known [public-key cryptosystem]({{< relref "#public-key-cryptography" >}}). It is based on the practical difficulty of the factorization of the product of two large prime numbers. For instance, RSA is used as part of [OpenPGP]({{< relref "#openpgp" >}}) for e-mail encryption and signing.

## S
### Salt
In cryptography, {{< dfns "salt" >}} is random data. Commonly, salt is appended to a key and then processed with a [hash function]({{< relref "#hash-function" >}}). Finally, the output and salt are stored in a database. A long salt, which is randomly generated for each key, protects against [dictionary attacks]({{< relref "#dictionary-attack" >}}).

### Sandboxing
{{< dfns "Sandboxing" >}} is software-based isolation of applications to mitigate system failures or [vulnerabilities]({{< relref "#vulnerability" >}}).

### Security goal
Concepts in [information security]({{< relref "#information-security" >}}) like the [CIA triad]({{< relref "#cia-triad" >}}) or [RMIAS]({{< relref "#rmias" >}}) define {{< dfns "security goals" >}} that have to be fulfilled. Well-known security goals are [confidentiality]({{< relref "#confidentiality" >}}), [integrity]({{< relref "#integrity" >}}), and [availability]({{< relref "#availability" >}}).

### Signal Protocol
The {{< dfns "Signal Protocol" >}} (formerly _TextSecure Protocol_ or _Axolotl Protocol_) is a modern cryptographic protocol allowing [end-to-end encrypted]({{< relref "#end-to-end-encryption" >}}) communication. Contrary to [OTR]({{< relref "#otr" >}}), asynchronous and multi-client communication is possible. [Forward secrecy]({{< relref "#forward-secrecy" >}}) is also supported.

### SIM swapping
{{< dfns "SIM swapping" >}} is a [social engineering]({{< relref "#social-engineering" >}}) attack. An attacker gathers information about the victim. Afterward, the attacker convinces the victim's mobile phone service provider to port the victim's telephone number to a SIM card controlled by the attacker by [impersonating]({{< relref "#impersonation" >}}) the victim. If successful, the attacker has full control of the victim's telephone number. This way, the attacker can bypass [two-factor authentication]({{< relref "#2fa" >}}) by directly intercepting [one-time passwords]({{< relref "#otp" >}}) in text messages or phone calls.

### Social engineering
{{< dfns "Social engineering" >}} is a generic term for the psychological manipulation of humans into performing actions. Social engineering isn't dependent on technology and quite common in everyday life. For example, children cry to manipulate their parents, or commercials manipulate their viewers. In [information security]({{< relref "#information-security" >}}), [phishing]({{< relref "#phishing" >}}) is a widespread social engineering technique.

### Spear phishing
{{< dfns "Spear phishing" >}} is more sophisticated than [phishing]({{< relref "#phishing" >}}). Attackers customize their forged messages and send them to a smaller amount of potential victims. Spear phishing requires more research on the attacker's side; however, the success rate of spear-phishing attacks is higher than the success rate of phishing attacks.

### SSRF
{{< dfnl "Server-side request forgery" "SSRF" >}} is an attack that exploits a server-side application to execute unwanted actions. For other server applications (including the exploited application itself), it looks like the action originated from the trustworthy server application.

### Stream cipher
{{< dfns "Stream ciphers" >}} are [symmetric]({{< relref "#symmetric-cryptography" >}}) algorithms to combine plaintext with a pseudorandom keystream. Each digit is encrypted/decrypted one at a time with the corresponding keystream digit. It's crucial to use different keys each time.

### Subresource Integrity
{{< dfnl "Subresource Integrity" "SRI" >}} can be used to ensure the [integrity]({{< relref "#integrity" >}}) of third-party content embedded on a website. Websites which don't embed any third-party content don't need SRI. The basic idea is that one [hash]({{< relref "#hash-function" >}}) per external resource is provided. Modifying the external resource results in a different hash. The client gets a new hash value that differs from the hash value provided by the web server. Ultimately, the client discards the modified resource.

### Supply-chain attack {#supply-chain-attack}
A {{< dfns "supply-chain attack" >}} can affect any user of IT/[OT]({{< relref "#ot" >}}) components (hardware or software). Attackers manipulate a component during its manufacturing process. In most cases, the actual attack happens before the targeted user possesses the manipulated component. Examples are manipulated compilers or firmware, and attacks like Stuxnet or NotPetya.

### Symmetric cryptography
{{< dfns "Symmetric cryptography" >}} is the opposite of [public-key cryptography]({{< relref "#public-key-cryptography" >}}). Two parties need the same private key to communicate. Both of them use this key for encryption and decryption. Symmetric encryption is faster than public-key encryption; however, you have to exchange keys securely. [AES]({{< relref "#aes" >}}) is a well-known representative of symmetric cryptography.

Symmetric cryptography uses either [block ciphers]({{< relref "#block-cipher" >}}) or [stream ciphers]({{< relref "#stream-cipher" >}}).

## T
### TLS
{{< dfnl "Transport Layer Security" "TLS" >}} allows secure data transfer via the internet. Nowadays, operators of servers should only allow TLS 1.2/TLS 1.3 and [cipher suites]({{< relref "#cipher-suites" >}}) supporting [forward secrecy]({{< relref "#forward-secrecy" >}}) as well as [AEAD]({{< relref "#aead" >}}). The TLS 1.3 standard only offers cipher suites that support FS and AEAD.

## U
### U2F
{{< dfnl "Universal 2nd Factor" "U2F" >}} is an [authentication]({{< relref "#authentication" >}}) standard initially developed by Google and Yubico. The FIDO Alliance currently hosts the standard. U2F tokens can be used for [two-factor authentication]({{< relref "#2fa" >}}). These tokens contain a unique secret key which can't be extracted. The specification allows unlimited accounts per U2F token since web applications that offer U2F authentication generate and store their own private/public key pair for each U2F token in use.

## V
### VPN
A {{< dfnl "virtual private network" "VPN" >}} extends a private network (e.g., your network at home) across a public network (like the internet). Devices connected to the VPN are logically part of the private network, even if there are physically somewhere else. Applications using a VPN are subject to the functionality, security, and management of the private network. [IPsec]({{< relref "#ipsec" >}}) or [TLS]({{< relref "#tls" >}}) are widely in use to secure VPNs.

### Vulnerability
Commonly, {{< dfns "vulnerabilities" >}} are [exploitable]({{< relref "#exploit" >}}) security flaws in software or hardware. Well-known vulnerabilities receive names like Heartbleed, Shellshock, Spectre, or Stagefright and at least one [CVE]({{< relref "#cve" >}}) identifier. There aren't always exploits available for vulnerabilities. A well-known system to classify the severity of vulnerabilities is [CVSS]({{< relref "#cvss" >}}).

## W
### Web application firewall
A {{< dfns "web application firewall" >}} (WAF) is an application layer [firewall]({{< relref "#firewall" >}}) that filters, monitors, and blocks HTTP traffic to and from web applications. For instance, this can prevent SQL injection or [XSS]({{< relref "#xss" >}}) attacks.

### WebAuthn
{{< dfnl "Web Authentication: An API for accessing Public Key Credentials Level 1" "WebAuthn" >}} is a specification developed by W3C that defines the creation and use of strong, attested, scoped, public key-based credentials by web applications. It is also part of [FIDO2]({{< relref "#fido2" >}}). As of December 2019, a Level 2 specification is under development.

### Wi-Fi
{{< dfns "Wi-Fi" >}} is a family of wireless networking technologies and a trademark of the Wi-Fi Alliance. It is based on the IEEE 802.11 specifications that define protocols for implementing wireless local area networks (WLANs). The current Wi-Fi generation is 6, referring to the 802.11ax protocol. Former generations are Wi-Fi 5 (802.11ac), Wi-Fi 4 (802.11n), Wi-Fi 3 (802.11g), Wi-Fi 2 (802.11a), and Wi-Fi 1 (802.11b).

For protection, [WPA]({{< relref "#wpa" >}}) is used.

### WPA
{{< dfnl "Wi-Fi Protected Access" "WPA" >}} means [Wi-Fi]({{< relref "#wi-fi" >}}) Protected Access, and it is used to secure wireless networks. The current generation is WPA3. Former generations are WPA2 (only use CCMP/[AES]({{< relref "#aes" >}}) for encryption; TKIP is insecure), and WPA (insecure). WPA offers WPA-Personal (for private users) and WPA-Enterprise (for companies). For WPA-Personal, [PBKDF2]({{< relref "#pbkdf" >}}) is used to derive a 256 bit key from the pre-shared key entered by the user.

## X
### XSS
{{< dfnl "cross-site scripting" "XSS" >}} is a [vulnerability]({{< relref "#vulnerability" >}}) found in web applications. There are different types of XSS attacks. Mostly, the attacker can inject client-side scripts into websites. Code injection exploits the trust a user has in a website. The opposite is [CSRF/XSRF]({{< relref "#csrf" >}}).

## Y
### YubiKey
The {{< dfns "YubiKey" >}} is a closed source USB key produced by Yubico in the US and Sweden. It implements [OpenPGP]({{< relref "#openpgp" >}}) card algorithms. One can generate and store [OpenPGP]({{< relref "#openpgp" >}}) key pairs on it. Some models also support creating [OATH-TOTP]({{< relref "#oath-totp" >}}) codes, U2F, and other cryptographic functions.

## Z
### Zero-day exploit {#zero-day-exploit}
A {{< dfns "zero-day exploit" >}} [exploits]({{< relref "#exploit" >}}) a [vulnerability]({{< relref "#vulnerability" >}}) in software or hardware, and this vulnerability is unknown to the public, publisher, or other parties who would typically mitigate it.
