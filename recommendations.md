+++
title = "Recommendations"
ogdescription = "Hardware, software, services and blogs we recommend."
nodateline = true
noprevnext = true
notice = true
+++

This page contains security-related recommendations. Kindly note that we _exclusively_ recommend hardware, software, and services that we use and own. We do not endorse any products based on sponsoring or things we only know from hearsay.

[General information security topics]({{< relref "#gst" >}}) | [Home network]({{< relref "#hn" >}}) | [Disk and file encryption]({{< relref "#dfe" >}}) | [DNS]({{< relref "#dns" >}}) | [Instant messaging]({{< relref "#im" >}}) | [Operating systems]({{< relref "#os" >}}) | [Repositories]({{< relref "#repo" >}}) | [Secure key and password storage]({{< relref "#skps" >}})

## General information security topics {#gst}
The following resources are useful to learn about InfoSec in general:

### Blogs {#gst-blogs}
* [Scott Helme](https://scotthelme.co.uk/) (InfoSec blog, focused on web application security)
* [n-o-d-e](https://n-o-d-e.net/) (various hardware projects)

### Podcasts {#gst-podcasts}
* [Security Now](https://twit.tv/shows/security-now) (weekly podcast with Steve Gibson and Leo Laporte)
* [StormCast](https://isc.sans.edu/podcast.html) (daily 5-10 minute podcast about current InfoSec topics)
* [Darknet Diaries](https://darknetdiaries.com/) (bi-weekly podcast about "hacker" stories)

### Q&A websites/forums {#gst-qa}
* [Information Security Stack Exchange](https://security.stackexchange.com/) (Q&A website for information security professionals)

### Other useful websites {#gst-misc}
* [EFF Security Education Companion](https://sec.eff.org/) (for digital security educators)
* [EFF Surveillance Self-Defense](https://ssd.eff.org/) (tips, tools, and how-tos for more secure online communications)
* [IT and Information Security Cheat Sheets](https://zeltser.com/cheat-sheets/) (cheat sheets on numerous topics)
* [OWASP Cheat Sheet Series](https://cheatsheetseries.owasp.org/) (cheat sheets on multiple topics)

---

## Home network {#hn}
Your home network connects you and your family to the internet. The most vulnerable point is your router since it has to fulfill different functions and is the primary point of entry for a remote attacker. Feel free to read our [home network security series](/as-hns/).

### Books {#hn-books}
* [Introducing Basic Network Concepts](https://www3.nd.edu/~cpoellab/teaching/cse40814_fall14/networks.pdf) (PDF file)
* [Peterson/Davie: Computer Networks: A Systems Approach](https://book.systemsapproach.org/) (free online access)
* Meyers: CompTIA Network+ Certification All-in-One Exam Guide (Exam N10-007), ISBN 978-1-26-012238-1
* Kizza: Guide to Computer Network Security, ISBN 978-3-319-55606-2
* Lowe: Networking for dummies, ISBN 978-1-119-25777-6

### Blogs {#hn-blogs}
* [Router security blog by Michael Horowitz](https://routersecurity.org/index.php)

### Hardware {#hn-hardware}
* [Turris Omnia](https://www.turris.com/en/omnia/overview/) (open hardware and open-source router) ([#Turris-Omnia](/tags/turris-omnia/))

---

## Disk and file encryption {#dfe}
We recommend the following applications or standards. Some recommendations are based on a talk of Mr. Schumacher from _Magdeburger Institut für Sicherheitsforschung_. Only use well-maintained and well-tested software for cryptography. Otherwise, your data could be exposed in some way, or you could lose your data.

### Full-disk encryption
* [LUKS](https://gitlab.com/cryptsetup/cryptsetup/) (Linux; see our [article on using a YubiKey for two-factor authentication]({{< ref "/blog/yubikey-luks.md" >}}))
* [VeraCrypt](https://www.veracrypt.fr/en/Home.html) (open-source disk encryption software for Windows, Mac OSX and Linux)

### Built-in file encryption
The Linux file systems ext4, F2FS, and UBIFS natively support file encryption. See our [article on fscrypt]({{< ref "/blog/tool-fscrypt.md" >}}).

### Other software
* [GoCryptFS](https://nuetzlich.net/gocryptfs/) (uses modern crypto but leaks metadata)
* [CryFS](https://www.cryfs.org/) (uses modern crypto and hides metadata but is slower than GoCryptFS)

---

## DNS
Many private users are focused on HTTPS and forget about their insecure DNS traffic. Cleartext DNS traffic can be modified or logged, and third parties can learn about your surfing habits. People who are familiar with network protocols and DNS can configure DNSSEC as well as DNS-over-TLS. If set correctly, you get validated DNS responses, and your DNS traffic is authenticated and encrypted.

Check our [DNS-related articles](/tags/dns/).

### Websites {#dns-websites}
* [DNS Privacy Project](https://dnsprivacy.org/wiki/) (collaborative open project to promote, implement and deploy DNS Privacy)
* [DNS leak test](https://www.dnsleaktest.com/) (see the DNS server that is used by your client)
* [List of public recursive name servers on Wikipedia](https://en.wikipedia.org/wiki/Public_recursive_name_server)
* [DNS Privacy Public Resolvers](https://dnsprivacy.org/wiki/display/DP/DNS+Privacy+Public+Resolvers)
* [DNS Privacy Test Servers](https://dnsprivacy.org/wiki/display/DP/DNS+Privacy+Test+Servers)

---

## Instant messaging {#im}
Ask ten people about their preferred instant messenger, and you'll get 15 recommendations. Some people say that federation is best for privacy ([no, this is wrong]({{< ref "/blog/myths-federation.md#m2" >}})), some recommend closed-source messengers like Threema, and most people keep on using WhatsApp. We aren't interested in wars of opinions and stay with the facts.

When it comes to security, privacy, usability, and support for different operating systems, [Signal](https://signal.org/download/) is the clear winner. See also our [articles on Signal](/tags/signal/).

If you still want to use XMPP-based messengers like Conversations, Gajim, Dino, and so on, keep in mind that server-side parties can [access and manipulate]({{< ref "/blog/xmpp-aitm.md" >}}) everything. We strongly recommend running your own XMPP server in this case. If you don't know how to do this, use a messenger like Signal. Unlike many XMPP-based messengers, Signal uses client-side account management and enforces end-to-end encryption by default.

---

## Operating systems {#os}
We recommend [Arch Linux](https://www.archlinux.org/) for advanced users. It allows you to set up a minimal operating system that can be highly customized. Besides, you get current software packages. Try to avoid unmaintained packages from the AUR (Arch User Repository) to keep your system stable and secure.

---

## Repositories {#repo}
The following repositories contain useful resources and links:

* [Awesome Cellular Hacking](https://github.com/W00t3k/Awesome-Cellular-Hacking)
* [Awesome Infosec](https://github.com/onlurking/awesome-infosec)
* [Awesome Hacking](https://github.com/Hack-with-Github/Awesome-Hacking)
* [Awesome Security](https://github.com/sbilly/awesome-security)
* [Awesome Social Engineering](https://github.com/v2-dev/awesome-social-engineering)
* [Awesome Web Security](https://github.com/qazbnm456/awesome-web-security)
* [Probable Wordlists](https://github.com/berzerk0/Probable-Wordlists)

---

## Secure key and password storage {#skps}
If you use OpenPGP, SSH, etc., you probably store your keys on your computer. We recommend storing private keys on dedicated security hardware. Furthermore, we recommend using password management software. If available, enable and use [two-factor authentication]({{< ref "/glossary.md#2fa" >}}) for online services ([WebAuthn]({{< ref "/glossary.md#webauthn" >}}), [U2F]({{< ref "/glossary.md#u2f" >}}), [OATH-TOTP]({{< ref "/glossary.md#oath-totp" >}})).

* [Nitrokey Pro 2](https://www.nitrokey.com/) (and its predecessor) ([#Nitrokey](/tags/nitrokey/))
* [YubiKey 5](https://www.yubico.com/) (and its predecessor) ([#YubiKey](/tags/yubikey/))
* [Yubico Security Key](https://www.yubico.com/) (supports FIDO2, WebAuthn, and U2F) ([#YubiKey](/tags/yubikey/))
* [KeePass Password Safe 2](https://keepass.info/) (well-tested password manager)
* [KeePassXC](https://keepassxc.org/) (password manager similar to KeePass 2) ([#KeePassXC](/tags/keepassxc/))
