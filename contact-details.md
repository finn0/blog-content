+++
title = "Contact information"
ogdescription = "Our contact information"
nodateline = true
noprevnext = true
chip = true
syntax = true
+++

[E-mail]({{< relref "#e-mail" >}}) | [Encrypted e-mail]({{< relref "#encrypted-e-mail" >}}) | [Expired or revoked OpenPGP keys]({{< relref "#eol-keys" >}})

## E-mail
If you voluntarily e-mail us, **you consent that our mail provider processes your personal data**. Please read our [privacy policy]({{< ref "privacy-policy.md#emailprovider" >}}) (the "Tutao GmbH" section).

{{< chip "img/mail-icon.png" "E-mail icon" "ish-team@tuta.io" "mailto:ish-team@tuta.io" >}}

We do not use any other e-mail addresses.

## Encrypted e-mail
If you know how to use [GnuPG]({{< ref "/glossary.md#gnupg" >}}) (or compatible implementations of [OpenPGP]({{< ref "/glossary.md#openpgp" >}})), use our public OpenPGP key to encrypt the content of your e-mail. You don't need to sign your e-mails, so you don't have to create an OpenPGP key to communicate with us. If you want to get encrypted replies, please provide your public OpenPGP key or fingerprint. Have a look at our [short guide]({{< ref "/terminal-tips.md#gpg-gen" >}}) to create a modern [Curve25519]({{< ref "/glossary.md#curve25519" >}}) key.

{{< chip "img/mail-enc-icon.png" "Encrypted e-mail icon" "Our public OpenPGP key" "https://infosec-handbook.eu/gpg.asc" >}}

The current fingerprint of our public OpenPGP key is:

valid from | valid to   | Fingerprint/**Key ID**
-----------|------------|-----------------------
2020-11-27 | 2021-02-27 | [EC90 12A6 A133 A535 A776 4A97 **484C F313 4F43 716B**](https://infosec-handbook.eu/gpg.asc)

You can also use curl (or similar tools) to download our key:

* {{< kbd "curl https://infosec-handbook.eu/gpg.asc | gpg --import" >}} (direct download)
* {{< kbd "curl https://keys.openpgp.org/vks/v1/by-email/ish-team%40tuta.io | gpg --import" >}} (keys.openpgp.org mirror)

## Expired or revoked OpenPGP keys {#eol-keys}
To reduce risks of long-lived OpenPGP keys circulating on the internet, we regularly renew our key. The table below lists expired and revoked keys.

valid from     | valid to       | Fingerprint/**Key ID**
---------------|----------------|-----------------------
~~2020-09-03~~ | ~~2020-12-03~~ | ~~980B A1DE 6ADA D6C6 4A6F EE7C **25A5 9B1C A3CA 30F7**~~
~~2020-06-13~~ | ~~2020-09-13~~ | ~~98EA 019B C2BB F8A8 AB88 53D2 **4AAF CD54 71C5 A8E6**~~
~~2020-03-20~~ | ~~2020-06-22~~ | ~~B372 4DF5 F610 D8B6 3974 922A **0329 34A0 5D11 D587**~~
~~2019-12-30~~ | ~~2020-03-30~~ | ~~4808 D6CD 65D6 A23E DE3D 7143 **8F26 DC6C D8AB B20C**~~
~~2019-08-29~~ | ~~2019-12-31~~ | ~~F8CE AD90 8841 1EBE 722C 347A **3179 E817 703F 5D25**~~
~~2019-08-22~~ | ~~2019-10-31~~ | ~~F6FB F3E9 9DC0 FD3E FE45 BEDC **6B68 98C3 32D1 70F7**~~
~~2019-05-26~~ | ~~2019-08-24~~ | ~~FE38 23F5 7A02 600E F080 9E58 **BCF4 BFD9 4ABF F017**~~
~~2019-03-03~~ | ~~2019-06-01~~ | ~~1C3D DFF7 2961 7BFC 7544 3E3B **E545 30DB 4072 2A8A**~~
~~2018-12-11~~ | ~~2019-03-11~~ | ~~4726 F48E 5607 B8A6 06CA F4F9 **09D3 4CFA C102 617D**~~
~~2018-09-25~~ | ~~2018-12-24~~ | ~~ED21 9FFD 58F0 B467 223A 6312 **D9FE 497B B424 11ED**~~
~~2018-06-12~~ | ~~2018-09-10~~ | ~~708C A4E3 8A9B E257 748D 01C9 **172B AD3E 40AA EA08**~~
