+++
title = "Privacy policy"
ogdescription = "Privacy policy of infosec-handbook.eu"
aliases = [ "privacy" ]
nodateline = true
noprevnext = true
+++

Thank you for your interest in our privacy policy. This policy contains information about how we process your personal data and about your rights according to the European GDPR (General Data Protection Regulation). References below to "we" or "us" refer to the operator of this website. Our website and this privacy policy are provided under Czech and European law.

## Scope
The following privacy policy is valid for https://infosec-handbook.eu/.

---

## A short version of our privacy policy {#short-pp}
* By default, our web server processes your IP address. Processing your IP address is technically necessary to send our content to your client.
* By default, we do not log any personal data of you. Log files are automatically encrypted after one day and stored in encrypted format for ten days.
* We do not track your browsing behavior or anything else. We do not try to identify you. We do not collect statistics. We do not set any cookies. We do not serve ads.
* Your rights are explained in Articles 15–21 and 77 of the European GDPR.
* In case of any questions related to this privacy policy, feel free to [contact us]({{< ref "contact-details.md#e-mail" >}}).
* For further information about our security measures, read our [security policy]({{< ref "security.md" >}}).

---

## Contact details
We are private individuals domiciled in different European countries, operating this website and its web server. Our server is physically located in Germany.

The controller in terms of the GDPR is:

{{< contactinfo >}}
[Contact details]({{< ref "contact-details.md#e-mail" >}})

---

## Contents
* [Definitions]({{< relref "#definitions-gdpr" >}})
* [Personal data we process]({{< relref "#processing-gdpr" >}})
* [Personal data third parties process for us]({{< relref "#processing-gdpr-3p" >}})
* [Accessing our website using mirrors and archives]({{< relref "#mirrors" >}})
* [Your rights (Articles 15–20 GDPR)]({{< relref "#rights-gdpr" >}})
* [Right to object (Article 21 GDPR)]({{< relref "#21-gdpr" >}})
* [Right to lodge a complaint with a supervisory authority (Article 77 GDPR)]({{< relref "#77-gdpr" >}})
* [Changelog]({{< relref "#changelog" >}})

---

## Definitions {#definitions-gdpr}
There are several definitions in the GDPR. The most important definitions are:

* ‘personal data’ means any information relating to an identified or identifiable _natural_ person
* ‘processing’ means any operation […] on personal data […] such as collection, recording, organisation, structuring, storage, adaptation or alteration, retrieval, consultation, use, disclosure by transmission, dissemination or otherwise making available, alignment or combination, restriction, erasure or destruction

If we talk about your personal data in the following, we mean anything that can be used to identify you. Examples are your name, e-mail address, and IP address. When we talk about "processing of personal data," we mean any type of processing.

---

## Personal data we process {#processing-gdpr}
When you visit our website, your **IP address** and user-agent are automatically processed by our web server. We automatically get this data from your client (e.g., your web browser or RSS/Atom feed reader). Our web server needs your IP address to send our contents back to your client. By default, we do not process any other personal data from you. Furthermore, our web server doesn't store your IP address permanently (e.g., in log files).

The legal basis for processing your personal data, as explained above, is Article 6(1) f GDPR. Our legitimate interest is providing our content.

### Logging {#logging}
Our web server writes information about particular client-side requests to so-called log files. We use these log files to detect attack-like behavior and to improve our services. Our web server automatically encrypts all log files after one day using [public-key cryptography]({{< ref "/glossary.md#public-key-cryptography" >}}). The encrypted log files are automatically deleted after ten days.

In case of abnormal requests (for technical people: all HTTP status codes except 200, 302, 304), we only log the following. Unusual requests include repeated attempts to access [denied]({{< relref "#denylist" >}}) files or other attack-like behavior:

* timestamp ("[31/Dec/2016:12:01:10 +0100]")
* IP address ("123.123.123.123")
* HTTP status code ("403")
* bytes transmitted ("157")
* first line of request for each request/HTTP version ("GET /secrets.bak HTTP/1.1)
* user-agent ("Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0")
* in some cases, our web application firewall also logs full client-side requests and full server-side responses

We use this data to identify new attacks, audit blocked requests, and unblock legitimate users, if necessary. We store blocked IP addresses for 14 days. We also use this data to identify broken links (for technical people: HTTP status code 404).

The legal basis for processing your personal data, as explained in this section, is Article 6(1) f GDPR. Our legitimate interests are blocking attacks and improving our services.

---

## Personal data third parties process for us {#processing-gdpr-3p}
The following third parties process personal data for us:

### netcup GmbH, Germany {#ng}
The netcup GmbH ([read their privacy policy](https://www.netcup.eu/kontakt/datenschutzerklaerung.php)) provides our servers. The netcup GmbH may log access attempts (IP address, user-agent) for all of its customers (including us) to detect DDoS attacks, attack-like behavior, and so on.

We concluded a data processing agreement according to Article 28 GDPR with netcup GmbH.

The legal basis for processing your personal data is Article 6(1) f GDPR. Our legitimate interest and the interest of the netcup GmbH is detecting/blocking attack-like behavior and proving our content.

### Tutao GmbH, Germany (e-mail only) {#emailprovider}
The Tutao GmbH ([read their privacy policy](https://tutanota.com/privacy)) provides our mail server. It isn't necessary to send us any e-mails to access our website. **If you decide to contact us, you agree that Tutao GmbH and we process your personal data (e.g., name, e-mail address) to answer your request.** We do not use your e-mail address for marketing purposes or tracking. We immediately delete your e-mails after your request is answered.

The legal basis for processing your personal data is Article 6(1) a GDPR. You may withdraw your consent with this at any time.

---

## Accessing our website using mirrors and archives {#mirrors}
Third parties may provide our content as a mirror (reflecting the current content) or as an archived website (reflecting outdated content). Kindly note that this privacy policy doesn't cover such mirrors or archives.

---

## Your rights (Articles 15–20 GDPR) {#rights-gdpr}
According to Articles 15 to 20 of the GDPR, you have several rights concerning your personal data processed by us:

* Art. 15: Right of access
* Art. 16: Right to rectification
* Art. 17: Right to erasure
* Art. 18: Right to restriction of processing
* Art. 19: Notification obligation regarding rectification or erasure of personal data or restriction of processing
* Art. 20: Right to data portability

You may exercise your rights by [contacting us]({{< ref "contact-details.md#e-mail" >}}).

## Right to object (Article 21 GDPR) {#21-gdpr}
**You have the right to object, on grounds relating to your particular situation, at any time to processing of personal data concerning you which is based on point e or f of Article 6(1) GDPR, including profiling based on those provisions. We no longer process the personal data unless we demonstrate compelling legitimate grounds for the processing which override the interests, rights and freedoms of you or for the establishment, exercise or defence of legal claims. This doesn't affect the lawfulness of processing based on consent before its withdrawal (point c of Article 13(2) GDPR).**

## Right to lodge a complaint with a supervisory authority (Article 77 GDPR) {#77-gdpr}
Without prejudice to any other administrative or judicial remedy, you have the right to lodge a complaint with a supervisory authority, in particular in the Member State of your habitual residence, place of work or place of the alleged infringement if you consider that the processing of personal data relating to you infringes the GDPR.

---

## Changelog
We updated this page on May 28, 2020. For transparency, we provide a complete changelog of this page on {{< extlink "https://codeberg.org/infosechandbook/blog-content/commits/branch/master/privacy-policy.md" "codeberg.org" >}}.
