+++
title = "Copyright and license information"
ogdescription = "Copyright and license information of InfoSec Handbook"
nodateline = true
noprevnext = true
+++

Our content (articles, icons, images, and so on) is subject to copyright laws of different European countries.

**Please do not use any content of our website without the prior written consent of us.** See our [contact page]({{< ref "contact-details.md" >}}) for contact details.

## Third-party elements we use {#tpe}
We use the following third-party elements:

* **Fonts:** _Orkney Regular_, designed by Samuel Oakes and Alfredo Marco Pradil ({{< extlink "http://scripts.sil.org/OFL" "SIL OFL" >}})
* **Theme:** _Hugo Icarus Theme_, Copyright © 2015 ZHANG Ruipeng / Copyright © 2015 Digitalcraftsman ({{< extlink "https://raw.githubusercontent.com/digitalcraftsman/hugo-icarus-theme/master/LICENSE.md" "MIT" >}} license)
