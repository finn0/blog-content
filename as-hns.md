+++
title = "Home network security series"
ogdescription = "The Home network security series shows you how you can improve the security of your home network. This series is based on the Czech open-source router Turris Omnia."
nodateline = true
noprevnext = true
notice = true
+++

{{< picture "img/as-hns.png" "Home network security series" >}}

In this series, we show ways to secure your network at home. The series contains configuration examples for the Czech open-source router **Turris Omnia**, running **Turris OS 5.x** (released in June 2020). Turris OS 5.x is based on OpenWrt 19.07.

You can use most examples to configure other Turris-OS-based or OpenWrt-based routers.

{{< noticebox "info" "June 2020: We update the series at the moment. Some examples in the series are outdated since they were written for Turris OS 3.x. In case of doubt, read the documentation provided by CZ.NIC or OpenWrt. (We will remove this note as soon as all articles of this series are up-to-date.)" >}}

* [Part 1: Hello World]({{< ref "/blog/hns1-hello-world.md" >}})
* [Part 2: HTTPS and TLS hardening]({{< ref "/blog/hns2-tls-hardening.md" >}}) ([harden SSH using this guide]({{< ref "/blog/wss1-basic-hardening.md#s3" >}}))
* Part 3: NAS (network-attached storage)
  * [Part 3a: Turris Omnia as network-attached storage via SMB]({{< ref "/blog/hns3-nas.md" >}})
  * [Part 3b: Turris Omnia as network-attached storage via Nextcloud]({{< ref "/blog/hns3-nextcloud.md" >}})
* [Part 4: Turris Omnia as an ad blocker]({{< ref "/blog/hns4-adblocking.md" >}})
* [Part 5: Client-side DNS security features]({{< ref "/blog/hns5-dns-configuration.md" >}})
