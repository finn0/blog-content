+++
title = "Web server security series"
ogdescription = "The Web server security series shows you how you can secure your web server (using Debian and Apache)."
nodateline = true
noprevnext = true
+++

{{< picture "img/as-wss.png" "Home network security series" >}}

In this series, we show ways to secure your web server. We will use **Debian 9 and Apache httpd 2.4.25** in our examples; however, you can convert most configuration to other operating systems or web servers.

* [Part 0: How to start]({{< ref "/blog/wss0-how-to-start.md" >}})
* [Part 1: Basic hardening]({{< ref "/blog/wss1-basic-hardening.md" >}})
* [Part 2: Harden the web server]({{< ref "/blog/wss2-webserver-hardening.md" >}})
* [Part 3: TLS and security headers]({{< ref "/blog/wss3-tls-headers.md" >}})
* [Part 4: WAF ModSecurity and IPS Fail2ban]({{< ref "/blog/wss4-modsecurity-fail2ban.md" >}})
* [Part 5: Server-side DNS security features]({{< ref "/blog/wss5-dns-configuration.md" >}})
* [Part 6: GDPR-friendly logging, and server monitoring]({{< ref "/blog/wss6-logging-monitoring.md" >}})
* [Part 7: Policies, and security contact]({{< ref "/blog/wss7-policies-contact.md" >}})
* [Part 8: Basic log file analysis]({{< ref "/blog/wss8-log-file-analysis.md" >}})

Upcoming parts of this series will be about getting ECDSA certificates, tools for server monitoring, specific HTTP response header directives, cookie security, and more.
