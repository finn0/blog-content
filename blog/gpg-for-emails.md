+++
title = "GnuPG for e-mail encryption and signing"
author = "Benjamin"
date = "2019-09-01T16:51:31+02:00"
tags = [ "metadata", "gnupg", "encryption", "privacy" ]
categories = [ "privacy" ]
ogdescription = "An overview of the current state of GnuPG for e-mails."
slug = "gpg-for-emails"
banner = "banners/gpg-for-emails"
notice = true
toc = true
+++

GnuPG, originally released 20 years ago, offers encryption for everyone. However, like every piece of software, it neither is flawless nor perfect. Recent attacks like ROCA, SigSpoof, Efail, and signature flooding revived the discussions about its security.

In this article, we present an overview of the current state of GnuPG for e-mail encryption and signing.
<!--more-->
{{< rssbox >}}

{{< noticebox "note" "For simplicity, we use the terms GPG, GnuPG, OpenPGP, PGP etc. interchangeably in the following. In our experience, most people don't know the differences between them. Besides, some people consider GnuPG to be the reference implementation of OpenPGP (RFC 4880). So other implementations of OpenPGP are/were also affected by attacks and issues mentioned below." >}}

## Introduction
GnuPG is already 20 years old. However, most private individuals never used it, and won't use it in future as shown by different polls. In the following, we show several recent attacks related to GPG, and some important issues.

### Recent attacks
The following attacks are not necessarily in the software GnuPG itself but related to it.

#### ROCA (CVE-2017-15361)
In October 2017, Czech security researchers publicly disclosed ROCA ("Return of Coppersmith's attack"), a vulnerability in RSALib. They originally discovered this cryptographic weakness in early 2017. ROCA allows an attacker to recover the [RSA]({{< ref "/glossary.md#rsa" >}}) private key from the corresponding public key if the vulnerable software library was used to generate the RSA key pair. RSALib is used by many smart cards, ID cards, and TPM (Trusted Platform Module) implementations. ROCA also affected GPG keys generated on Yubico's YubiKeys at that time.

#### SigSpoof (CVE-2018-12020), and more spoofing
There were multiple vulnerabilities directly in GnuPG that allowed spoofing of signatures (e.g., CVE-2006-0049, CVE-2007-1263).

More recently, security researches discovered bugs and vulnerabilities in e-mail clients (not directly in GnuPG) that allow attackers to spoof signatures, spoof senders, or inject code. One is called "Mailsploit" (2017, "a new way to easily spoof email addresses"), another one ROPEMAKER (2017, "a malicious actor can change the displayed content in an email at will").

In 2018, a German security researcher published a possibility to create messages that look encrypted in GnuPG and e-mail clients while being plaintext. This was fixed in GnuPG. One month later, he published SigSpoof (CVE-2018-12020). SigSpoof allows an attacker to spoof messages that look signed while not being signed.

In 2019, other German researches published "'Johnny, you are fired!' – Spoofing OpenPGP and S/MIME Signatures in Emails". This paper describes different attacks to spoof signatures. 14 out of 20 tested OpenPGP-capable e-mail clients were affected.

#### Efail (2018)
Then, there was infamous EFAIL, a collection of different security vulnerabilities that affected GnuPG and S/MIME.

Unfortunately, some people tried to downplay the vulnerabilities since EFAIL was widely covered by media, and initially misinterpreted as a "major PGP vulnerability that endangers all encrypted communication", or basically "PGP is broken". Other people then called the GPG developers "irresponsible" for not taking any action, and ignoring previous reports of the researches.

In the end, many e-mail clients and GnuPG (MDC is mandatory since 2.2.8) got updated. EFAIL also cast light on the security of HTML e-mails. While many e-mail clients send HTML e-mails by default, some researchers see HTML e-mails as a big risk for security.

#### Flooding of SKS keyservers (Certificate Spamming Attack)
In June 2019, a new attack surfaced. This time, an unknown party started to flood the SKS (Synchronizing Key Server) network with certificates. This resulted in a crash of GnuPG each time it loaded affected public keys.

As one of many consequences, GnuPG got updated to ignore all key signatures received from key servers (2.2.17). At about the same time, a new key server emerged that isn't part of the SKS network. While this server fixes some issues, it is centralized now, and there are several new issues (e.g., "no user ID" issues in GPG, no signatures anymore, or only one key per e-mail address).

### Issues
The following issues are discussed on many other websites, so we don't provide in-depth explanations in the following.

* No [forward secrecy]({{< ref "/glossary.md#forward-secrecy" >}}): GnuPG doesn't support FS. If you always use the same private key for encryption, it becomes more likely that somebody is able to decrypt every single message encrypted with this key. For instance, leaking your private key, or vulnerabilities like ROCA allow attackers to decrypt every single message. The importance of FS was one lesson learned from the Snowden Leaks. Interestingly, there is an expired draft called "Forward Secrecy Extensions for OpenPGP" from 2001. It never became a standard.
* Surreptitious Forwarding (2001): The "Sign & Encrypt" workflow can be misused to re-encrypt an already signed message. For example, Alice writes a message, and signs it using her private signing key. Then, she uses Bob's public encryption key to encrypt the signed message for him. Bob gets the message, decrypts it, and re-encrypts the message (still signed by Alice) with the public encryption key of Charlie. Finally, Charlie gets an encrypted message that was signed by Alice. For Charlie, it looks like Alice directly sent this message to him.
* Metadata: For instance, SUBJECT and FROM are not signed, and visible to third parties. In general, only the content of an e-mail is signed and encrypted. There are some addons that try to fix this ("Memory Hole Protected E-mail Headers"), however, this requires everybody to install these addons.
* Complex key management: Mainly tech-savvy people insist that key management is extremely easy in GPG and similar systems. In September 2017, even the Adobe PSIRT managed to accidentally leak their fresh GPG private key.
* Verifying fingerprints is also risky: In 2014, the "Evil 32" attack was published. This attack showed that 32 bit key IDs are too short to be secure. So you must compare the 64 bit key ID at least. Furthermore, the attack showed that GPG didn't verify key IDs properly. This was fixed.
* Usability aspects: The presence of hundreds of "how to start with GPG" guides on the internet show that it is everything but easy to use GPG. Several companies and projects tried to address this by providing easier-to-use software (e.g., Protonmail, Mailvelope, p≡p, Enigmail). However, GPG is still far from easy-to-use as there are many pitfalls. For instance, German media reported that Enigmail actually sent all e-mails in cleartext while telling the user that the content was encrypted if "junior mode" was enabled.
* Leak of other recipients: GPG-encrypted messages contain the 32-bit key ID of each recipient, allowing determining its recipients.
* Personal data on the internet forever: Until 2019, GPG users synchronized their GPG keys using the SKS network. One feature of the network is that nobody can delete any keys once uploaded. Of course, this also means that you can never remove personal data (e.g., your name, your e-mail address, data of people who signed your key) anymore. The new GPG key server, launched in June 2019, tries to address the issue.
* No current standard for publishing your public key: As mentioned before, there are some issues with the old SKS network and the new centralized key server that is currently recommended by many websites. At the moment, there is an informational draft "OpenPGP Web Key Directory (version 08)", and an experimental RFC 7929 (DNS-Based Authentication of Named Entities (DANE) Bindings for OpenPGP). Other people (and InfoSec Handbook) publish their keys via their own web servers, or services like Keybase.
* Presence of legacy algorithms: GPG tries to provide backwards compatibility. As seen in TLS 1.2 (and before), backwards compatibility reintroduces legacy encryption and other legacy algorithms. Besides, backwards compatibility requires legacy code.

## Other things to consider
### Algorithms and key sizes
Besides attacks and issues mentioned above, there are contradictory recommendations on the internet. For instance, some people say that you shouldn't create keys based on [elliptic-curve cryptography (ECC)]({{< ref "/glossary.md#ecc" >}}) since they should be less secure than RSA keys. Other people say that you should create 8192 bit RSA keys while some guides still state that 2048 bit is sufficient.

>We recommend ECC-based keys (Ed25519, Curve25519) in 2019. Curve25519 is widely used by many different software projects like Signal.

We recommend using ECC-based keys in 2019. On the one hand, they are currently considered "future-default" (the algorithm which will likely be used as default algorithm in future versions of gpg). On the other hand, they are faster and smaller than RSA keys.

With "ECC-based keys" we mean [Ed25519]({{< ref "/glossary.md#ed25519" >}})/[Curve25519]({{< ref "/glossary.md#curve25519" >}}). Curve25519 was first released by cryptologist Daniel J. Bernstein in 2005. Nowadays, it is widely used by many different software projects (e.g., libgcrypt, libssh, NaCl, libsodium, Signal Protocol, TLS, and WireGuard). Curve25519's level of security is comparable with a 3000 bit (3kB) RSA key. 4kB or 8kB RSA keys don't offer any considerable increase in security today. Never only look at key sizes if you talk about security. Cryptography is far more complex (and to complex to explain this here).

See [our terminal tips page]({{< ref "/terminal-tips.md#gpg-gen" >}}) to create a fresh ECC-based key pair. Advanced users can use [a single command to create one key pair]({{< ref "/news/2019-08-31-monthly-review.md#tipotm" >}}). Keep in mind that some security tokens can't manage ECC-based GPG keys.

### Legacy recommendations, and the "insecure smartphone" {#legacy}
Then, there is the problem of [legacy configuration]({{< ref "/blog/discussion-false-sense.md#r1" >}}): Guides and recommendations are snapshots in time. If you look at GPG guides from 2005, 2010, 2015, and 2019, you will probably find contradictory recommendations. This is quite normal, however, some new users may not realize this.

On the other hand, some people tell you not to use GPG on your smartphone since your smartphone is "inherently insecure". For the same reason, these people tell you not to use any banking apps. Contrary to this, the same people recommend instant messengers like Signal or XMPP-based messengers that store secrets on your smartphone (like GPG, or banking apps).

>Some people tell you not to use GPG or banking apps on your smartphone since it is "inherently insecure". At the same time, they recommend instant messengers that create and store secrets on your phone – like GPG or banking apps.

So these inconsistent warnings don't make sense. Moreover, they ignore the fact that many people (esp. in developing countries) solely use smartphones nowadays. If your smartphone is secure enough for instant messengers, it is also secure enough for everything else.

## Basic workflow of using GPG for e-mails {#workflow}
In this section, we briefly show how you can use GPG nowadays. However, we won't provide another comprehensive "how to use GPG" guide as there are already hundreds on the internet.

1. Choose a client with GPG support. It is impossible to address all of the issues mentioned above by just recommending the "right" e-mail client. Choose the e-mail client that fits your needs. Don't choose an e-mail client only because someone on the internet told you that this one is the "most secure" one.
2. Create a GPG key pair, and set an expiry date (e.g., after 1 year). See [our terminal tips page]({{< ref "/terminal-tips.md#gpg-gen" >}}) to create a fresh ECC-based key pair on Linux.
3. Manage your key pair as described in [Modern credential management: security tokens, password managers, and a simple spreadsheet]({{< ref "/blog/modern-credential-management.md#manage-have" >}}).
4. Back up your private key and the password to decrypt it. If you lose either your private key or the password to decrypt it, you lose access to every e-mail that was encrypted using the corresponding public key.
5. Publish your public key, and its fingerprint. Tell everybody about its existence. People won't magically realize this. They won't magically encrypt e-mails for you.
6. Use your private key to sign/decrypt messages, and the public key of others to encrypt messages for them or verify their signatures.
7. If you get signed messages from others, carefully check the fingerprint of the keys that were used.
8. If you ever lose your private key (or if you accidentally leak it), revoke it.

Tip: You don't need an e-mail client with GPG support to use GPG. You can directly encrypt, decrypt, sign, or verify with GnuPG itself. See [our terminal tips page]({{< ref "/terminal-tips.md#gpg" >}}). So if you don't trust your e-mail server provider, just do the crypto stuff on your local machine, and copy already encrypted messages to your e-mail client.

## Alternatives
GPG is some kind of Swiss Army knife. This fact contributes to many more lines of code, and a heavyweight tool. For instance, many people don't know that GPG also supports [symmetric encryption]({{< ref "/glossary.md#symmetric-cryptography" >}}) ([AES]({{< ref "/glossary.md#aes" >}})), and more. However, if you only want to encrypt your e-mails, you don't need all of these features. Due to this, we recommend some tools and projects for different use cases.

* Encrypted communication: Use instant messengers based on the Signal Protocol. One messenger is the [Signal]({{< relref "#links" >}}) messenger itself.
* Encrypted file exchange: You can use [Magic Wormhole]({{< relref "#links" >}}) for this.
* Encrypted backups: Here, you can use [BorgBackup]({{< relref "#links" >}}).
* File signing: [Minisign]({{< relref "#links" >}}) is a lightweight single-purpose tool for this. We provide an [article on Minisign]({{< ref "/blog/tool-minisign.md" >}}). An alternative is [OpenBSD's Signify]({{< ref "/blog/tool-signify.md" >}}).
* Encrypted mailboxes: [Userli]({{< relref "#links" >}}) is an interesting project for mailboxes that encrypts them using the password of their owners.

Regarding _opmsg_ ("a replacement for gpg which can encrypt/sign/verify your mails or create/verify detached signatures of local files"): We tried it and planned to write an article about it. However, it is even more complicated than GPG itself, it offers legacy crypto, uses insecure cipher modes by default (AES with CBC mode), and there is nearly no development. There is also no independent security audit or any other audit.

## Summary
In our experience, nearly nobody uses GPG on a daily basis. Over the course of the last three years, the vast majority of e-mails sent to InfoSec Handbook and its predecessor were in cleartext. Even a German company, which manufactures security tokens that can be used for GPG, didn't manage to encrypt their e-mails using our public key. On the other hand, private individuals mostly use instant messengers for daily communications. So it is unlikely to see a sudden rise in usage of GPG.

In general, we don't recommend using GPG for e-mails if you mostly communicate via instant messengers. Modern instant messengers come with state-of-the-art encryption, and some of them (like Signal) reduce the amount of metadata while actually enforcing encryption. On the other hand, the vast majority of companies and organizations don't support GPG. Therefore, it is in doubt whether you can use your GPG keys often.

## Sources
* {{< extlink "https://crocs.fi.muni.cz/public/papers/rsa_ccs17" "ROCA (CVE-2017-15361)" >}}
* {{< extlink "https://www.mailsploit.com/index" "Mailsploit" >}}
* {{< extlink "https://neopg.io/blog/encryption-spoof" "Not everything that looks encrypted, is encrypted" >}}
* {{< extlink "https://neopg.io/blog/gpg-signature-spoof/" "SigSpoof: Spoofing signatures in GnuPG, Enigmail, GPGTools and python-gnupg (CVE-2018-12020)" >}}
* {{< extlink "https://github.com/RUB-NDS/Johnny-You-Are-Fired/raw/master/paper/johnny-fired.pdf" "'Johnny, you are fired!' – Spoofing OpenPGP and S/MIME Signatures in Emails" >}}
* {{< extlink "https://efail.de/" "EFAIL" >}}
* {{< extlink "https://evil32.com/" "Evil 32: Check Your GPG Fingerprints" >}}
* {{< extlink "https://tools.ietf.org/html/draft-koch-openpgp-webkey-service-08" "OpenPGP Web Key Directory – draft-koch-openpgp-webkey-service-08" >}}
* {{< extlink "https://tools.ietf.org/html/rfc7929" "RFC 7929: DNS-Based Authentication of Named Entities (DANE) Bindings for OpenPGP" >}}

## External links {#links}
* {{< extlink "https://signal.org/" "Signal messenger – Fast, simple, secure." >}}
* {{< extlink "https://github.com/warner/magic-wormhole" "Magic Wormhole – Get things from one computer to another, safely." >}}
* {{< extlink "https://borgbackup.readthedocs.io/en/stable/" "BorgBackup – a deduplicating backup program." >}}
* {{< extlink "https://jedisct1.github.io/minisign/" "Minisign – A dead simple tool to sign files and verify signatures." >}}
* {{< extlink "https://systemli.github.io/userli/" "Userli – Web application to (self-) manage email users and encrypt their mailboxes." >}} ({{< extlink "https://media.ccc.de/v/thms-34-e-mail-privatheit-und-mailbox-verschlsselung" "German talk about Userli" >}})
