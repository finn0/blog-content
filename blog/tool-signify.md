+++
title = "Signify"
author = "Benjamin"
date = "2020-09-28T18:41:41+02:00"
tags = [ "signify" ]
categories = [ "tutorial" ]
ogdescription = "Signify can be used to sign and verify files."
slug = "tool-signify"
banner = "banners/ish-tools"
notice = true
syntax = true
+++

We look at [OpenBSD's Signify]({{< relref "#external-links" >}}). You can use Signify as an alternative to [GnuPG]({{< ref "/glossary.md#gnupg" >}}) or [Minisign]({{< ref "/blog/tool-minisign.md" >}}) for [signing]({{< ref "/glossary.md#digital-signature" >}}) and verifying files.
<!--more-->
{{< rssbox >}}

Signify uses [Ed25519]({{< ref "/glossary.md#ed25519" >}}) for cryptographic signing and verification. OpenBSD developers use Signify extensively for signing. Actually, Ted Unangst developed the tool to sign and verify OpenBSD's files. Besides, some other projects rely on Signify, like Wireguard, radare2, or LibreSSL. The current version of Signify is v30, released on September 24, 2020.

After downloading and installing Signify on your platform, you enter {{< kbd "signify -G -p signify.pub -s signify.sec" >}} to create a key pair in the current directory.

As you see, we specify "-p" for the public verification key "signify.pub," and "-s" for the secret signing key "signify.sec." We suggest using a password manager to store your passphrase for "signify.sec."

{{< noticebox "warning" "If you create a new key pair, you get two files: The file that ends with \".sec\" is your private signing key. Do not share it. The file that ends with \".pub\" is your public verification key. Share this file with everybody who needs to verify your signatures." >}}

The contents of {{< kbd "cat signify.pub" >}} look like:

{{< samp "untrusted comment: signify public key of InfoSec Handbook" >}}
{{< samp "RWSdP65piDd+OZWjsPeIWQKHCOBbF0XSDRIA6uby560mpcZVFaCU8USG" >}}

The first line is an "untrusted" comment. "Untrusted" means that it isn't signed and can be changed. The second line is the Base64 encoded public key.

After creating a key pair, the workflow is similar to tools like GnuPG or Minisign: You publish your public key "signify.pub" and use your local private key "signify.sec" to sign files.

To sign files, just enter: {{< kbd "signify -S -s signify.sec -m [file-to-sign] -x [signature-file]" >}}. After entering the passphrase for the private key, Signify signs the "[file-to-sign]" and stores the signature in "[signature-file]." If you don't specify "-x," Signify uses "[file-to-sign].sig."

Another person can verify the file's signature by entering: {{< kbd "signify -V -p signify.pub -m [file-to-sign]" >}}. Again, "-x" can be specified for a custom signature file. If the signature is correct, you see "Signature Verified." If the provided file differs from the original one, you get "signify: signature verification failed." If the signature file is corrupted, you see "signify: unable to parse [signature-file]."

The signature file looks like:

{{< samp "untrusted comment: verify with signify.pub" >}}
{{< samp "RWSdP65piDd+OVoglh1oEzICs3q/OIVN4p5DC0TscWfy/kjtC2wdDKGtBbW2/uKbxLcw5qvA/jTr8YNNe2X0T8xY/n0wjItd9gs=" >}}

Rename your "signify.pub" and "signify.sec" files if you need numerous key pairs.

## Conclusion
OpenBSD's Signify is a modern tool that allows you to sign and verify files. In our opinion, it should be a very stable alternative since OpenBSD extensively uses it.

## External links
* {{< extlink "https://github.com/aperezdc/signify" "Signify - Sign and Verify" >}}
