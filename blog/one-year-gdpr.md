+++
title = "One year GDPR: looking at privacy policies of websites operated by private individuals"
author = "Benjamin"
date = "2019-05-28T05:34:37+02:00"
tags = [ "gdpr", "privacy-policy", "policy", "xmpp", "matrix", "mastodon", "blogging" ]
categories = [ "privacy" ]
ogdescription = "We look at the privacy policies of 20 websites to check whether they provide obligatory information."
slug = "one-year-gdpr"
banner = "banners/gdpr-myths"
notice = true
toc = true
+++

One year ago, the European General Data Protection Regulation (GDPR) became enforceable after a two-year transition period. In the wake of this event, the media reported about several administrators who decided to permanently shut down their (small) websites while many non-EU websites started to block all EU-based IP addresses. Some [people spread myths]({{< ref "/myths-gdpr.md" >}}), and soon afterward the media lost interest in the GDPR. Taken as a whole, the GDPR caused confusion despite the fact that former national privacy laws were mostly as strict as the GDPR itself.

In this article, we look at the privacy policies of 20 websites (operated by private individuals) to check whether they provide information for their users according to Articles 12 and 13 (GDPR).
<!--more-->
{{< rssbox >}}

{{< noticebox "warning" "Please note: We aren't lawyers and don't pretend to be lawyers. Please do not rely on any information provided below. Contact your data protection lawyer if you have GDPR-specific questions." >}}

## Does your small website really need a privacy policy? {#need}
The short answer is: "Yes, always!".

"But I don't store [personal data]({{< ref "/glossary.md#personal-data" >}})!" is a typical answer in this case. According to Article 4 (GDPR), "processing" personal data is _anything_ you do with it. This includes just collecting and using personal data. If you operate a publicly accessible server (e.g., web server, mail server, XMPP server), your server always processes at least IP addresses, which are widely classed as personal data.

According to Article 12 (1) (GDPR), you "shall take appropriate measures to provide **any** information referred to in Articles 13 and 14 and **any** communication under Articles 15 to 22 and 34 **relating to processing** to the data subject in a **concise, transparent, intelligible and easily accessible form, using clear and plain language**, in particular for any information addressed specifically to a child". You must provide this information in writing, and you may add standardized icons to make it more understandable.

The above mentioned Articles contain the following:

* [Article 13]({{< relref "#article13" >}}): Information you must provide if you retrieve personal data directly from a private individual (so, this is the most important Article in many cases).
* Article 14: Information you must provide if you retrieve personal data from other sources than the affected private individual.
* Articles 15–21: Rights of private individuals (access, rectification, erasure, restriction of processing, notification obligation, data portability, object).
* Article 22: Information you must provide if you use automated individual decision making (including profiling).
* Article 34: Communication of data breaches to affected private individuals.

In summary, you must at least provide information according to Article 13 of the GDPR, and mention Articles 15–21 (the rights of a private individual). The GDPR includes more obligations, however, we only check if websites provide this information in the following.

## Article 13 in detail {#article13}
As mentioned above, Article 13 is often the most important Article regarding informing private individuals about data processing. We look at the content of Article 13:

### Basic information
If you directly get personal data from private users, include the following:

* Your **contact details**.
* The **contact details of a data protection officer** (read Article 37 to check if you need a DPO!).
* The **purposes of processing** personal data and the **legal basis for processing**; if you select point (f) of [Article 6]({{< relref "#article6" >}}) (1) as the legal basis, you must **explicitly inform about the legitimate interests by you/or third parties**.
* If you **share personal data with third parties**, you must **list them by name or their categories** (e.g., mail server providers).
* If you **transfer personal data to non-EU states or international organizations**, you must inform about the **existence or absence of an adequacy decision by the EU Commission** (and/or about suitable safeguards under certain circumstances).

### Additional information
Additionally, tell private users about the following to ensure fair and transparent processing:

* **How long do you store personal data, or criteria to determine that period?**
* The **existence of certain rights** (access, rectification, erasure, restriction of processing, data portability, object).
* If you select point (a) of [Article 6]({{< relref "#article6" >}}) (1)/Article 9 (2) as the legal basis, you must explicitly inform about the **existence of the right to withdraw consent at any time, without affecting the lawfulness of processing based on consent before its withdrawal**.
* The **right to lodge a complaint with a supervisory authority**.
* Whether private individuals need to provide their personal data due to statutory or contractual requirements, or to enter into a contract; and **what happens if they do not provide this data**.
* If applicable, **information about automated individual decision making (including profiling), or the logic involved**.

If you not only process personal data for which you collected it, **inform the private individual about other purposes BEFORE processing it for any other purpose**

>Be as transparent as possible if you write your privacy policy, and only process personal data if necessary.

As you can see, you must provide lots of information to private individuals to comply with these basic requirements. The GDPR requires to provide this information "at the time when personal data are obtained".

Since web servers process personal data as soon as a client tries to connect to it, you must virtually provide this information instantly in a concise, transparent, intelligible and easily accessible form, using clear and plain language. This means:

* don't include vaporing
* mention everything you do with personal data
* write for normal people not for lawyers

Before we look at real privacy policies, we have to look at Article 6.

## Article 6 in detail {#article6}
Article 6 basically lists six reasons for lawfully processing personal data:

* **a private individual actively agreed with the processing**
* processing is necessary for the performance of a contract, or necessary prior to entering a contract
* you are legally required to process this personal data
* processing is necessary to protect your vital interests
* processing is in public interest
* **processing is in your legitimate interest**

In line with expectations, many privacy policies of non-commercial websites mention the first and the last point as the legal basis for processing. As seen in [Article 13]({{< relref "#article13" >}}), one must provide additional information in this case:

1. If a private individual actively agreed with the processing, **you must explicitly inform about the existence of the right to withdraw consent at any time, without affecting the lawfulness of processing based on consent before its withdrawal**
2. If processing is in your legitimate interest, **you must explicitly inform about the legitimate interests by you/or third parties**

In the following, we check if this information is provided in real privacy policies.

## 20 randomly chosen privacy policies {#twenty-privacy-policies}
We randomly chose privacy policies of 5 XMPP servers, 5 Matrix servers, 5 Mastodon servers, and 5 blogs. 18 were operated by private individuals (or this was claimed at least). Additionally, all of them were either operated by private individuals based in the EU, or the servers obviously processed personal data of EU citizens.

Only 1 (!) out of 20 servers provided information as required by the GDPR (Articles 12 and 13). Half of the servers didn't include any contact details, and/or didn't tell affected private users about the purposes of data processing and the legal basis for this. Furthermore, most servers didn't list the rights of affected private users according to the GDPR. The vast majority of servers didn't mention any (or some) third parties involved in processing personal data although third parties were obviously processing personal data on behalf of the server administrator.

### Privacy policies of XMPP servers {#pp-xmpp}
XMPP (Extensible Messaging and Presence Protocol) is a protocol mostly used for instant messaging.

As shown in our article "[XMPP: Admin-in-the-middle]({{< ref "/xmpp-aitm.md" >}})", XMPP servers play a vital part in XMPP-based communication and manage nearly everything, including contact lists, own contact details, group memberships, and presence data of devices. Therefore, we checked if privacy policies of XMPP servers mentioned this data apart from the typical "we process your IP address for security purposes" statements.

* One out of five XMPP servers met the GDPR requirements.
* Another XMPP server covered nearly everything, but used a policy template in German legalese. Furthermore, the privacy policy didn't mention any third party involved although the XMPP server is obviously hosted by a well-known server provider.
* The other three privacy policies only covered a small subset of requirements by the GDPR. The following information was missing: contact details, rights of affected private users, purpose of data processing and legal basis, and parties involved in processing personal data. Additionally, one privacy policy was hidden and hard to find.
* Most privacy policies didn't provide a comprehensive list of personal data processed by XMPP servers.

### Privacy policies of Matrix servers {#pp-matrix}
The Matrix protocol, also mainly used for instant messaging, is partially similar to XMPP. People can host their own servers, and there are some servers publicly available and hosted by private individuals. Recently, the reference server was breached. We covered this in "[5 lessons learned from the matrix.org breach]({{< ref "/matrix-vulnerabilities.md" >}})".

Again, we checked for information according to the GDPR requirements. These servers manage up to several thousands of user accounts:

* No privacy policy met the GDPR requirements.
* One privacy policy partially met the requirements, however, the privacy policy was hidden and there was obviously information missing about third parties involved in data processing (Amazon AWS, CloudFlare), and any information regarding obligatory adequacy decisions by the EU Commission.
* Another privacy policy, which partially met the requirements, neither listed any contact details nor the majority of rights of affected private users.
* The remaining two Matrix servers didn't have a privacy policy at all.

### Privacy policies of Mastodon servers {#pp-mastodon}
Mastodon is a social networking service, which can be hosted by private individuals. In August 2018, we already contacted dozens of server administrators since many big instances didn't update their Mastodon software, which contained publicly-known security vulnerabilities back then. We repeated this in October and November of 2018, however, most administrators contacted by us didn't react. So, apart from looking at privacy policies of Mastodon servers, also check their version numbers.

As before, we looked for information according to the GDPR requirements. These servers manage 5,000 to 300,000 user accounts:

* No privacy policy met the GDPR requirements.
* Obviously, all Mastodon privacy policies (checked by us) were adapted from Discourse, a discussion platform software.
  * The following information was missing: contact details, purposes of data processing and legal basis, parties involved in processing personal data, and (most) rights of the affected private users.
  * Only one privacy policy was slightly customized and contained contact details.
  * Another Mastodon server came with two privacy policies: one for the main website (this one was actually in conformance with the GDPR requirements), and one for the Mastodon instance (lacking information mentioned before).
  * All privacy policies state that the IP addresses of users are logged, and stored up to 12 months for unknown purposes. Moreover, the policies state that servers may log all IP addresses of every client request (for unknown reasons and without mentioning the legal basis for this).
  * As it seems, some parts of the original Discourse privacy policy were removed, so there are sentences that doesn't make sense anymore (as parts are missing).
  * In general, Mastodon privacy policies are rather vague.

### Privacy policies of blogs {#pp-blog}
Finally, we checked privacy policies of five different blogs (security/privacy blog, travel blog, technology blog, cooking blog, DIY blog). At least, blogs (as every other website) process the IP addresses of their readers. Additionally, blogs often embed third-party content like advertisements, fonts, JavaScript, or other files.

* No privacy policy fully met the GDPR requirements.
* No privacy policy included information about personal data processed by third parties although all blogs were hosted by well-known server providers.
* Three blogs embedded third-party content, effectively transferring personal data to third parties without mentioning this in their privacy policy.
* One privacy policy was extremely long, but very comprehensive. However, there was still crucial information missing (concerning third parties involved in data processing).
* One privacy policy listed all third parties involved in data processing, but there was no information regarding adequacy decisions by the EU Commission.
* One privacy policy was also very long and contained lots of vaporing instead of being concise. There was also information missing about the involvement of uriports.com, an error reporting service, that processes personal data on behalf of the blog operator. (This includes purposes of processing and the legal basis. Furthermore, affected users can't easily block this data transmission that happens without their consent.)
* The final privacy policy stated that the web server doesn't process any personal data, and that affected private individuals must contact Google Inc. if they want to know anything about processing of their data. Obviously, this blog was operated by a private individual and Google processed data on behalf of this person. However, this information wasn't included. Additionally, there was no information about any rights, or purposes of processing personal data, including the legal basis.

## Summary
If you consider the two-year transition period, the GDPR is actually more than three years old. However, as shown in the article, many privacy policies still lack obligatory information according to the GDPR.

As an affected private individual, inform the person of contact if there is no privacy policy, or if obligatory information is missing. If the person of contact doesn't reply, make use of your rights according to the GDPR and/or your national privacy laws. [We also provide a guide on identifying the completeness of privacy policies]({{< ref "/blog/guide-privacy-policy.md" >}}).

If you run your own (small) website, which is publicly accessible, at least read Articles 12 to 21 of the GDPR and provide information as required by the GDPR. In general, this is also true for websites operated in non-EU countries that process personal data of EU citizens. Only process personal data if it is absolutely necessary!

Moreover, we provide [basic guidance on writing (privacy) policies]({{< ref "/wss7-policies-contact.md#privacy-policy" >}}) in our [web server security series]({{< ref "/as-wss.md" >}}).

_Evaluated and written in collaboration with [Thorsten]({{< ref "/about.md#Thorsten" >}})._

{{< noticebox "warning" "And again: Contact your data protection lawyer if you have GDPR-specific questions. Online forums or other websites of people who aren't data protection lawyers aren't reliable sources for this topic." >}}
