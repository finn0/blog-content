+++
title = "Home network security – Part 3b: Turris Omnia as network-attached storage via Nextcloud"
author = "Benjamin"
date = "2018-08-11T07:46:17+02:00"
lastmod = "2020-05-16"
tags = [ "turris-omnia", "router", "lan", "nas", "privacy", "nextcloud" ]
categories = [ "Home network security" ]
ogdescription = "In this part, we show you how you can use your Turris Omnia as network-attached storage via Nextcloud."
slug = "hns3-nextcloud"
banner = "banners/as-hns"
notice = true
toc = true
syntax = true
+++

In this part of our [home network security series]({{< ref "/as-hns.md" >}}), we show you how you can use your Turris Omnia as network-attached storage (NAS) via Nextcloud. Your local Nextcloud server keeps your data within your home network. You can share files with your family members and other devices at home. There is no need for flash drives or online storage.

Compared with [NAS via SMB]({{< ref "/blog/hns3-nas.md" >}}), this option is easier to implement; however, the RAM usage may be higher.
<!--more-->
{{< rssbox >}}

## Requirements
For this guide, we need:

* our Turris Omnia that is connected with our computer and the internet
* an installed and empty mSATA drive (see [part 3a for installation instructions]({{< ref "/blog/hns3-nas.md#s1" >}}))
* an SSH client on our computer

## Step by step to your NAS using Nextcloud {#sbs-guide}
This time, we use the "Storage" module of Foris OS to format the mSATA drive and then install Nextcloud via LuCI and the CLI.

{{< noticebox "note" "Please note that the following guide is based on Turris OS 3.11.16 and Nextcloud 16.0.9. Your version of Turris OS or Nextcloud might differ. In case of doubt, read the official documentation." >}}

### Step 1: Format and set the mSATA (Foris) {#s1}
First of all, connect to your Turris Omnia using your web browser (e.g., https://192.168.1.1/foris/config/). Log in and go to "Storage."

There should be a red warning message like:

{{< samp "Device currently in use is … (internal flash)" >}}

Your mSATA drive should be selectable below (e.g., "sda").

Select your device and click on "Format & Set." After confirming this choice, your Omnia formats and sets your drive and then reboots. Log in again and check whether your drive is in use now (see image below). The file system is "btrfs" this time.

{{< webpimg "art-img/as-hns3-5storage.png" "Our mSATA drive after formatting and setting is as /srv location." "the storage page of Foris OS." >}}

### Step 2: Download and install Nextcloud (CLI) {#s2}
Now, we must download the Nextcloud packages via the CLI. Connect to your Turris via SSH: {{< kbd "ssh root@192.168.1.1" >}}. Use the password for "advanced administration" like [configured in the first part]({{< ref "/blog/hns1-hello-world.md#config-to" >}}) of this series.

Enter the following commands: {{< kbd "echo 'Install(\"nextcloud-install\")' >> /etc/updater/conf.d/auto.lua" >}} and {{< kbd "pkgupdate" >}}.

You should get a list of packages that your Omnia wants to install. Then, you should see the following output:

{{< samp "Press return to continue, CTRL+C to abort" >}}

Press {{< kbd "ENTER" >}} to proceed. The Turris downloads and installs Nextcloud from the Turris repository.

Start the installer by entering {{< kbd "sudo nextcloud_install" >}}. Enter {{< kbd "YES" >}} if you want to use the automatic setup. In our case, the configuration is stored in "/srv/www/nextcloud/config/config.php."

When asked, "What should be admins login?" enter a username for Nextcloud. Then, when asked, "What should be admins password?" enter the password for this user account that is used for Nextcloud.

### Step 3: Optionally secure the MySQL database (CLI) {#s3}
After installing Nextcloud, you may have to configure your MySQL database. Enter {{< kbd "sudo /usr/bin/mysql_secure_installation" >}}.

Confirm the initial password prompt with {{< kbd "ENTER" >}} since we haven't set a password before. Then you can set the "root password" of the database. After that, you have to press {{< kbd "Y" >}} several times:

* Remove anonymous users? [Y/n] {{< kbd "Y" >}}
* Disallow root login remotely? [Y/n] {{< kbd "Y" >}}
* Remove test database and access to it? [Y/n] {{< kbd "Y" >}}
* Reload privilege tables now? [Y/n] {{< kbd "Y" >}}

All done! Disconnect your terminal and close it.

### Step 4: Optionally enable hd-idle (LuCI) {#s4}
You can [optionally set the idle mode as described in our previous article]({{< ref "/blog/hns3-nas.md#s4" >}}) for your mSATA drive in LuCI:

* Go to Services / hd-idle.
* Enable hd-idle for "sda."
* Save & Apply your settings.
* Go to System / Startup.
* Check whether "hd-idle" is enabled and running.

### Step 5: Open Nextcloud in your web browser, and complete the setup {#s5}
Open your web browser and enter {{< kbd "https://192.168.1.1/nextcloud" >}} (change the IP address accordingly). Enter the username and password that you set before. You should see your Nextcloud instance (see image below).

{{< webpimg "art-img/as-hns3-6final.png" "Nextcloud is ready." "the default page of Nextcloud." >}}

Go to {{< kbd "https://192.168.1.1/nextcloud/index.php/settings/admin/overview" >}}, and look for any warnings. Carefully evaluate each warning.

{{< noticebox "warning" "You can use Nextcloud's auto updater to upgrade or update your Nextcloud version; however, future updates via the Turris repository might overwrite these updates or some configuration files. Keep this in mind when you use the auto updater." >}}

### Step 6: Enable advanced security settings of Nextcloud {#s6}
In your web browser, go to {{< kbd "https://192.168.1.1/nextcloud/index.php/settings/admin/security" >}}. On this page, you should enable server-side encryption by clicking "Enable server-side encryption." You may need an encryption module for this. We found it on {{< kbd "https://192.168.1.1/nextcloud/index.php/settings/apps/disabled" >}}, and set it to "Enable."

Then, go to {{< kbd "https://192.168.1.1/nextcloud/index.php/settings/admin/sharing" >}}, and disable options that are irrelevant for your setup. Check also the security apps page: {{< kbd "https://192.168.1.1/nextcloud/index.php/settings/apps/security" >}}.

## Tips
* [Enable and enforce **HTTPS**]({{< ref "/blog/hns2-tls-hardening.md" >}}) (encryption of data in transit).
* Check all settings of your Nextcloud instance.
* Keep in mind that users of the [guest network of your Turris Omnia]({{< ref "/blog/hns1-hello-world.md#to-guest-nw" >}}) can't access your Nextcloud instance since they are in another VLAN.
* If you want to access this Nextcloud instance remotely, use a VPN.

{{< hnsbox >}}

## Summary
Nextcloud on your Turris Omnia keeps your data within your local network. You don't need to trust any cloud server providers or other online parties. Besides, you don't need additional hardware at home.

## Sources
* {{< extlink "https://doc.turris.cz/doc/en/howto/storage_plugin" "Turris doc: Foris: Storage setting plugin" >}}
* {{< extlink "https://docs.turris.cz/geek/nextcloud/nextcloud/" "Nextcloud server on Turris" >}}

## Changelog
* May 16, 2020: Updated the installation guide and steps to secure Nextcloud. Removed notes on RAM usage since we didn't observe this anymore.
