+++
title = "Ask Us Anything (AMA): readers ask, we answer – Part 2"
author = "Benjamin"
date = "2020-07-21T08:21:11+02:00"
tags = [ "ama" ]
categories = [ "Ask Us Anything" ]
ogdescription = "We categorize and collect some questions by readers and our answers."
slug = "2020-7-ama-answers-part2"
banner = "banners/ish-ama"
toc = true
notice = true
+++

In July 2020, we hosted our first AMA (Ask Us Anything) event. In this article, we categorize and collect some questions and our answers.
<!--more-->
{{< rssbox >}}

Our first AMA event ended on July 23, 2020. Readers sent their questions via [e-mail]({{< ref "/contact-details.md" >}}) or in the Fediverse.

{{< noticebox "info" "We rephrased some questions to point to the crucial parts." >}}

## General questions about information security
### "You use the Signal messenger. Why? Why don't you use a decentralized messenger?"
While the Signal messenger is built on top of cryptography, many other messengers are generally in cleartext. Few messengers add some optional cryptography on top. If the cryptography fails for some reason, you go back to cleartext communication. Moreover, contrary to Signal, most messengers store all of your group memberships, contacts, and account settings in cleartext on the server. We don't want this cleartext world.

_So does decentralization surpass cryptography?_ No, we don't think so after various [tests with XMPP](/tags/xmpp/) and [Matrix](/tags/matrix/). "Decentralization" mostly exists in theory. In July 2019, we checked 1000 XMPP servers. Only seven companies in three countries controlled more than 50% of the XMPP servers. Besides, 50% of these XMPP servers were hosted in Germany, 10% in the USA, and 7% in France. Therefore, the vast majority of the network traffic passes a tiny number of bottlenecks on the internet. We saw similar numbers with Matrix, especially after the 2019 security breach. We aren't convinced that spreading your cleartext data on a small number of servers adds security to your data.

Moreover, side effects of decentralization are varying security levels, feature support, and privacy policies:

* Some people set up their decentralized server once. At most, they update the server software from time to time. However, there is no continuous security monitoring, alerting in case of incidents, or a security concept. In other words, these people likely never detect any security breaches of their servers. Due to the cleartext nature of the messengers, server-side attackers can monitor and modify nearly everything.
* Another contributor to de-facto centralization is the feature support: XMPP server admins can enable or disable extensions needed for modern messaging features. If the server doesn't support the extension, people need to migrate to another server to use the client features. "Compliance testers" exist for some messengers to help people finding servers that support most extensions. Due to this, people select from a small list of servers. On the other hand, only some clients support most modern messaging features. Moreover, some features are only partially supported, especially modern cryptography like OMEMO.
* Many privacy policies are either incomplete or missing. In our experience, deficient privacy policies originate from policy generators, or the website owner simply copied parts of other privacy policies without checking if the policy matches their use case. On the other hand, some people try to hide their identity and publish no policy or contact details at all. "No policy" means that you, as a user, can't contact the server operator to assume your privacy rights.

All in all, the decentralization of most messengers **in its current form** doesn't replace enforced cryptography and doesn't provide any additional privacy.

_Do you hate decentralization then?_ Using a centralized messengers doesn't mean that we "hate" decentralized messengers.

### "You recommend KeePass. How do you synchronize your password database?"
Some of us use Git for this; others store their KeePass database on a self-hosted Nextcloud instance.

### "Why do you say that privacy isn't the same as information security?"
**Privacy** (in terms of **data protection**) is about protecting personal data. The scope of data protection varies from country to country. There is no international definition of "data protection." Contrary to this, **information security** is about protecting information. "Information" includes internal data and intellectual property. There are international definitions of "information security" and best practices.

Data protection and information security may require similar measures to achieve protection, though. Of course, the difference may be irrelevant for private individuals.

### "Which security certifications do you recommend to boost my career?"
There are many different security certifications for diverse career paths, see [https://pauljerimy.com/security-certification-roadmap/](https://pauljerimy.com/security-certification-roadmap/). As a starting point, consider CompTIA's Security+.

### "Can you write a hardening tutorial?"
No, we can't. Hardening (e.g., disabling unused services and interfaces, applying strict rights management, and setting strong credentials) must be customized for hardware and software. We can't cover all possibilities here.

### "How important is social engineering in security?"
While social engineering (SE) describes attacks, "information security awareness" (ISA) protects against SE. ISA is as crucial as any other technical or processual measures to prevent or detect attacks. You can't avoid all attacks on your networks and systems by only focusing on technology. Therefore, everybody should understand common SE techniques. Keep in mind that SE perfectly works without technology since SE is about exploiting human characteristics.

## Questions about web security
### "Is online banking secure? Which are trustworthy banks?"
The level of security primarily depends on your bank. There is no binary answer. The same is true for "trustworthy" banks. Keep in mind that all banks share lots of information about your identity and transactions with law enforcement agencies and fraud prevention.

### "How do you do online banking securely?"
* Set a unique and strong password. Use a password manager to store this password securely.
* Use [two-factor authentication]({{< ref "/glossary.md#2fa" >}}), if available. The possibilities for this differ from country to country, and from bank to bank.
* Use notification services, if possible. For instance, some banks notify you when you pay with your credit card or transfer money.

### "Do you use a user.js file for Firefox?"
No, we use custom settings for Firefox.

### "You use codeberg.org. Is it secure for Git repos?"
We use it to host data that is already public (our content). For non-public data, we use Git locally.

## Questions about mobile security
### "Which mobile devices do you use?"
Xiaomi Mi A2 (Android 10), Apple iPhone XR (iOS 13), Google Pixel 4 (Android 10).

### "Can you test app XYZ, please? Can you test smartphone ABC, please?"
No, we don't test anything on request, including apps. On the one hand, testing hardware or software is time-consuming. We want to use our time for writing and improving content.

On the other hand, "testing" doesn't only include capturing some network traffic. A comprehensive app test includes static code analysis, fuzzing, server-side attacks, attacks on network protocols, binary analysis, etc. We can't conduct such extensive tests for free. (And many other bloggers won't provide this for free either.)

### "Can you recommend a password manager for mobile operating systems?"
At present, we don't use a password manager on mobile operating systems. However, we may look into it in the future.

## General questions about the InfoSec Handbook
### "Do you host any services yourself at home?"
We host some local services (Nextcloud, Bitwarden-rs, OpenVPN, and a customized MQTT broker for home automation) using Turris Omnia routers and a Raspberry Pi 4.

### "What do you think about hosting a Mastodon instance?"
At present, we don't plan to host anything but our website.
