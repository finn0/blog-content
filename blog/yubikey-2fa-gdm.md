+++
title = "Yubico Security Key: Local 2FA with PAM"
author = "Benjamin"
date = "2018-08-22T10:03:01+02:00"
lastmod = "2019-12-23"
tags = [ "2fa", "pam", "yubikey", "nitrokey", "gdm", "u2f" ]
categories = [ "authentication" ]
ogdescription = "We show you how you can use a YubiKey Security Key for 2FA with PAM."
slug = "yubikey-2fa-pam"
banner = "banners/yubikey-security-key"
toc = true
syntax = true
+++

Some time ago, we [compared the YubiKey 4C and the Nitrokey Pro]({{< ref "/blog/yubikey4c-nitrokeypro.md" >}}) that we both use on a daily basis. This time, we show you how you can use a Yubico Security Key with the pluggable authentication module (PAM) on Linux for local [two-factor authentication]({{< ref "/glossary.md#2fa" >}}) (2FA).
<!--more-->
{{< rssbox >}}

## What is U2F, FIDO, FIDO2, and WebAuthn? {#u2f-fido}
_Universal 2nd Factor_ (U2F) is an open authentication standard originally developed by Yubico and Google and now hosted by the FIDO Alliance. Security devices with U2F support allow you to use [two-factor authentication]({{< ref "/glossary.md#2fa" >}}) more easily since you don't need to manage additional credentials. [Read our comparison of the YubiKey 4C and Nitrokey Pro]({{< ref "/blog/yubikey4c-nitrokeypro.md#reasons-for-buying-dedicated-security-tokens" >}}) for more information on U2F for two-factor authentication.

The _Fast IDentity Online Alliance_ (FIDO Alliance) consists of different companies which recognized that security tokens for strong authentication lack interoperability. Their aim is to provide widely-accepted specifications to make the usage of online services more secure. The FIDO Alliance published two different specifications:

* UAF: Universal Authentication Framework (no passwords or user interaction involved)
* U2F: Universal 2nd Factor (using a security token as a second factor)

A newer project of the FIDO Alliance is _FIDO2_. FIDO2 connects the new W3C Web Authentication standard ([WebAuthn]({{< ref "/glossary.md#webauthn" >}})) with FIDO's Client-to-Authenticator Protocol (CTAP). The goal is to enable users to authenticate to online services by using their security devices (mobile and desktop). For more details, read our comparison of the [Yubico Security Key and Nitrokey FIDO2]({{< ref "/blog/yubico-security-key-nitrokey-fido2.md" >}}).

To put it in a nutshell, U2F offers [public key-based]({{< ref "/glossary.md#public-key-cryptography" >}}) two-factor authentication. In addition to this, FIDO2/WebAuthn offers strong single-factor authentication and different authenticator types.

## Requirements
For this tutorial, you need:

* a U2F security token. We use a Yubico Security Key, but you can use any other U2F token.
* your Linux device (we use Arch Linux in this guide).

## Step 1: Download and install pam_u2f {#s1}
First of all, we have to install "pam-u2f." This is a PAM module that implements U2F. You can use this module for all U2F security tokens not only for YubiKeys. Arch users can install it by entering: {{< kbd "sudo pacman -S pam-u2f" >}}.

At this point, you can check whether your system recognizes the YubiKey: {{< kbd "dmesg | grep -i \"Yubico Security Key\"" >}}. This shows something like:

{{< samp "… USB HID v1.10 Device [Yubico Security Key by Yubico] …" >}}

If your device doesn't recognize the YubiKey, reboot it. If you have the official YubiKey Manager on your system, you can also run: {{< kbd "ykman info" >}}.

## Step 2: Use pamu2fcfg to generate your config {#s2}
After installing "pam-u2f," you can use "pamu2fcfg" to easily generate a configuration file that we will add to the PAM configuration later.

Run {{< kbd "pamu2fcfg" >}}. The physical button of the Security Key blinks now. Press the button of the YubiKey to proceed. In your terminal, you see an output like:

{{< samp "[username]:[key-handle],[public-key]" >}}

Open your favorite text editor and save this output in a file which is accessible for the user (e.g., in the "home" folder of the user). We will put it in "~/.config/Yubico/u2f_keys" since there is already a Yubico folder and "u2f_keys" is the default location for this PAM module's auth file.

Advanced users can directly redirect the output of "pam-u2f" to the file by entering: {{< kbd "pamu2fcfg > ~/.config/Yubico/u2f_keys" >}}.

## Step 3: Update your PAM file {#s3}
Finally, we have to tell PAM to use our U2F security key. You can either create a new file in "/etc/pam.d/" or edit an existing file. We will edit "/etc/pam.d/gdm-password" here to use U2F with the GNOME Display Manager (GDM).

Add {{< kbd "auth sufficient pam_u2f.so debug" >}} to the file and save it. The "debug" keyword enables debug output in case of any errors. Log out and test whether you can log in with your U2F key and/or password.

In case of success, remove "debug," and change "sufficient" to "required." The difference is explained below:

* "sufficient": This is a PAM keyword telling PAM that if this module succeeds, all following sufficient modules are also satisfied. In our case, PAM will accept:
  * only our password (single factor)
  * only our U2F token (single factor)
  * both factors (if the U2F token is connected with the device when GDM asks for the password)
* "required": This enforces usage of the U2F token and enables true two-factor authentication.

Save the file, and log out again.

### Optional: Use U2F for sudo {#u2f-sudo}
In order to use U2F for sudo, you only need to add the line {{< kbd "auth required pam_u2f.so" >}} to "/etc/pam.d/sudo." This results in exactly the same behavior as with "gdm-password."

### Optional: Get prompted to insert your security token {#u2f-prompt}
Using the configuration above, there is no visual indication to insert your security token when you log in. This can be changed by adding the keyword {{< kbd "interactive" >}} to the end of the line. For instance: "auth required pam_u2f.so interactive." Using this for sudo results in the following prompt:

{{< samp "Insert your U2F device, then press ENTER." >}}

### Optional: Add a second U2F key {#2nd-u2f}
There is the risk of losing or destroying the U2F token. We recommend to use a second one as a backup. You have to run [step 2]({{< relref "#s2" >}}) again but this time you press the button of the backup device. The result is similar output as before. You need to append this to the previous output.

Extend the string in "~/.config/Yubico/u2f_keys" to {{< kbd "[username]:[key-handle],[public-key]:[backup-key-handle],[backup-public-key]" >}}, and test it again.

## Summary
In this article, we showed several examples for using a U2F security token. The new WebAuthn standard has the potential to spread acceptance of such tokens on the internet making credential management less painful.

## External links {#links}
* {{< extlink "https://www.yubico.com/" "Yubico" >}}
* {{< extlink "https://shop.solokeys.com/" "SoloKeys (alternative to Yubico Security Key)" >}}
* {{< extlink "https://developers.yubico.com/pam-u2f/" "pam-u2f" >}}
* {{< extlink "https://fidoalliance.org/" "FIDO Alliance" >}}
* {{< extlink "https://www.w3.org/TR/webauthn/" "Web Authentication: An API for accessing Public Key Credentials Level 1" >}}
* {{< extlink "https://twofactorauth.org/" "Information about favorite websites and their 2FA support" >}}
* {{< extlink "https://www.dongleauth.info/" "List of websites and their OTP/U2F support" >}}

## Changelog
* Dec 23, 2019: Removed aurman (package is now official). Clarified differences between U2F und FIDO2/WebAuthn. Changed "sufficient" to "required" as recommended (real two-factor authentication). Removed section on other U2F tokens since this is already mentioned.
* Apr 22, 2019: Added sections about using U2F for sudo, and getting a visual prompt.
* Nov 3, 2018: Updated information about Nitrokey FIDO U2F.
