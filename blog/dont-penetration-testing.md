+++
title = "3 Don'ts of penetration testing and security assessments"
author = "Benjamin"
date = "2019-12-20T13:17:18+01:00"
tags = [ "server-security", "assessment", "pentesting" ]
categories = [ "knowledge" ]
ogdescription = "We show 3 things good penetration testers don't do."
slug = "donts-penetration-testing"
banner = "banners/online-assessment-tools"
toc = true
+++

Penetration testing is a tool to find [security vulnerabilities]({{< ref "/glossary.md#vulnerability" >}}) and discover [security risks]({{< ref "/glossary.md#risk" >}}) systematically. However, if done wrong, penetration testing results in a list of arbitrary problems that aren't necessarily related to security.

In this article, we show three things good penetration testers don't do.
<!--more-->
{{< rssbox >}}

## The goal of penetration testing
Most penetration tests' primary goal is to get a list of verified security vulnerabilities and their risks for the tested environment.

A penetration tester works within a predefined scope, so a single penetration test commonly doesn't cover every possibility of penetrating a system. There are different testing concepts (white-box testing, gray-box testing, black-box testing) and perspectives (external attacker, internal attacker). Therefore, two penetration tests covering the same system can discover different risks.

## Actions you should avoid
In the following, we list three actions an experienced penetration tester should avoid.

### Action 1: Don't test anything without permission {#dont1}
You should never start testing without the permission of the system owner. In many countries, penetration testing without authorization will likely be considered illegal, so your actions may result in legal consequences.

There is also the danger of breaking something or triggering [intrusion detection/prevention systems]({{< ref "/glossary.md#intrusion-detection-system" >}}). This can result in damage or wrong testing results.

Thus, get the written permission of the system owner before you start. Know the target environment, and stay within the predefined scope of the test. One exception: Organizations may allow penetration testing without prior permission in some cases. Look for a security/disclosure policy. Some organizations even have a bug bounty program.

### Action 2: Don't be a script kiddie {#dont2}
Running arbitrary scripts and tools while hoping to find something is very unprofessional. The (inexperienced) customer hired you to work professionally. Running tools without knowing their functionality can result in damage of some kind: Your script could delete or modify files, or the targeted system can't process requests by the script and crashes.

Therefore, know your scripts and tools. Understand how they work and what they do. Understand their limitations. For instance, some online assessment tools like Webbkoll, Qualys SSL Labs, the Mozilla Observatory, can only test a tiny fraction of security-relevant configuration as explained in ["Limits of Webbkoll"]({{< ref "/blog/limits-webbkoll.md" >}}) and ["Pros and cons of online assessment tools for web server security,"]({{< ref "/blog/online-assessment-tools.md" >}}) or they show wrong results.

### Action 3: Don't report something without verifying it {#dont3}
Finally, don't write something in your report that you didn't verify. Compiling a list of 50 unconfirmed findings, which could—in theory—be exploited, doesn't help your customer in any way. For example, a penetration tester without any [OT]({{< ref "/glossary.md#ot" >}}) experience may come to wrong conclusions since most IT security vulnerabilities won't be exploitable in OT environments, or they are irrelevant to the customer.

The same is true for the online assessment tools mentioned above: If you run these tools, keep in mind that most findings aren't confirmed or may be irrelevant due to the system's environment. Verify your findings and consider the environment of the tested system.

And please, if you find something horrible, responsibly contact the owner of a system. Don't publish any "Attention please, I discovered lots of security vulnerabilities"-style posts based on unconfirmed assumptions. This not only looks unprofessional, but it can also damage your reputation.

## Summary
In the end, a good penetration test needs an experienced penetration tester, a systematic process, the right tools, and reporting skills. Get written permission, stay within the predefined scope, know your tools, and be confident when writing the report afterward.
