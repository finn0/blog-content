+++
title = "Web server security – Part 0: How to start"
author = "Benjamin"
date = "2019-07-14T19:02:31+02:00"
tags = [ "blogging", "server-security" ]
categories = [ "Web server security" ]
ogdescription = "In this article, we write about things to consider before starting your blog."
slug = "wss0-how-to-start"
banner = "banners/as-wss"
toc = true
+++

In part 0 of our web server security series, we discuss things to consider before setting up a server. These considerations are very important to ensure secure operation of your server in future.
<!--more-->
{{< rssbox >}}

## How not to start {#hnts}
Many server hardening or server security guides directly start with installing software packages and changing some configuration files. This is fine for experienced server administrators. However, people who try to set up their first server hit on problems and most importantly they very likely forget things that aren't covered by such guides.

So, please **do not start to set up your first server by blindly following any guide on the internet** (including our guides!).

## You are responsible for your server {#responsibility}
A server is just another computer connected to the internet. Contrary to your IT at home (e.g., smartphone, IoT devices, laptop), the server must be reachable from the internet to provide its services. Most likely, servers are located in data centers of server hosting companies. Oftentimes, customers can either get virtual servers or dedicated (physical) servers. In both cases, the customer is responsible for secure setup and operation of the server.

So, it is your responsibility to secure your server. Securing a server not only means to configure it once. Security includes technology, processes/organization and people. Image an office building with dozens of smoke detectors and fire extinguishers on each floor. In case of fire, this technology becomes useless if nobody knows how to use it and where these things are located.

The same is true for server security (and for information security, in general). For instance, server log files are useless if you don't check them regularly, or there could be patches for critical [security vulnerabilities]({{< ref "/glossary.md#vulnerability" >}}) that need to be installed while you are on vacation for a week. You need a basic security concept.

## Your basic (security) concept {#bsc}
In the following, we won't describe an extremely detailed all-in-one security concept. It is more about getting a basic idea what you want to achieve by operating your own server, and things you need to consider during setup and regular operation. A well-known approach here is the PDCA method: plan → do → check → act (and repeat).

It is not only important to think about server security, but also to write it down. You don't need a Configuration Management Database (CMDB) as a private user to manage one or two servers. You can simply use a common text editor.

Think about the following and write down all of your replies (especially mark questions without answers):

1. What do you want to achieve by self-hosting? Do you want to host your own website, network storage, or synchronization service for calendars and contacts?
2. Which services (software packages) do you need? Do you need these services anywhere or is it sufficient if they are available on your LAN only? Is the software you need maintained, or are there dozens of open issues on the internet and no one replies? How do you get security updates for the software? Are there any security features included (e.g., [2FA]({{< ref "/glossary.md#2fa" >}}), [TLS]({{< ref "/glossary.md#tls" >}})) or do you need to implement them using other technology (e.g., [VPN]({{< ref "/glossary.md#vpn" >}}), full-disk encryption)? How do you document any configuration changes? Are there any software dependencies that may break other software?
3. Which operating system do you need for these services? Do you know how to maintain these operating systems? How do you get security updates? How do you want to access the server (e.g., using VPN, or SSH)? How do you back up your data and prevent data loss? How do you conduct recovery tests to ensure that your backups are actually working?
4. How do you monitor log files and file integrity? How do you learn about security-relevant activities like failed log-in attempts? How do you get notified of suspicious events and possible security incidents? How do you quickly install security updates even if you are not available? How do you check if configuration files are up-to-date (e.g., removal of legacy parameters)?
5. Which are the most important security risks regarding your services and the server's operating system? How do you address the risks (e.g., reducing it by implementing additional security controls, or accepting it)? Again, keep in mind that security is much more than only one-time configuration of TLS cipher suites!
6. Do you plan to process [personal data]({{< ref "/glossary.md#personal-data" >}})? Are there any agreements necessary according to your national privacy laws? How do you protect this data? Do you plan to process data of other people? How do you ensure that they can assume their privacy rights?

The result of this is a basic concept. This should already help you to identify many issues before they actually occur. For instance, maybe you need to configure emergency access for a second person, or you need more knowledge to secure the database of a certain service on your server.

Regularly recheck and improve your basic concept. It doesn't need to be perfect. It is a working document and subject to change. Security isn't static, security needs care. The same is true for data protection/privacy.

## Finding the actual server {#ftas}
The next step is to find an appropriate server that meets your requirements:

* If you only need the defined services at home, you could simply set up a Raspberry Pi (or other hardware) that hosts these services locally. In this case, you even physically control your server.
* Or you may want to access your home server via the internet. Then, you may need a static IP address, and you should configure a [demilitarized zone]({{< ref "/glossary.md#dmz" >}}) in your network. This ensures that only your server is accessible via the internet while other local devices remain inaccessible.
* Or you may want to rent a server that is located in a data center and provided by a server hosting company. In this case, you don't need to buy additional hardware, and you don't need to expose your private network at home. Such hosting companies can be certified according to ISO/IEC 27001. This basically means that the company systematically manages information security, including risks, threats, and vulnerabilities. So, this certification is about security management, not about certain security technology.

In any case, you should add information about your server to your basic documentation. This includes:

* name of the server
* physical location of the server
* MAC, IPv4, and IPv6 addresses
* information about contracts, agreements, and costs
* information about backups and configuration
* information about people who access it

## Installing software {#is}
The final step is to install software on your server. A good practice for servers is to choose an operating system that only gets security updates for its packages. Common general-purpose operating systems for clients also provide regular feature updates. It is more likely that such feature updates—in theory—break your operating system. This could result in unexpected downtime after updating the server's operating system.

Common operating systems for servers (and clients) are Ubuntu, openSUSE, CentOS, Gentoo, Fedora, and Debian. There is no "best" or "most secure" operating system here. It is mainly a matter of taste.

After choosing your operating system, you should only install software packages that are needed afterward. For instance, you probably need software like OpenSSH to remotely access your server. However, you don't need LibreOffice or VLC media player on your server in most cases. Write down any software packages you install or use dedicated tools for documentation.

{{< wssbox >}}

## Summary
Many server security guides are only focused on technical configuration. While this may be sufficient for experienced server administrators, casual admins probably don't realize that their new server needs care.

During normal operation, you should at least regularly back up your data and configuration (after any changes). Monitor your server for security-relevant events. Check configuration files for legacy or new parameters from time to time. There are more things to consider, of course. However, this should provide some basic guidance.

In the [next part of this series]({{< ref "/blog/wss1-basic-hardening.md" >}}), we show the most basic steps to harden a freshly-installed Debian server.
