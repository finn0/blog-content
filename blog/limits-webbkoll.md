+++
title = "Limits of Webbkoll"
author = "Jakub"
date = "2018-03-28T09:52:00+02:00"
lastmod = "2018-12-11"
tags = [ "webbkoll", "privacy" ]
categories = [ "limits" ]
ogdescription = "Webbkoll is sometimes considered as the ultimate privacy tool. It doesn't live up its reputation."
slug = "limits-webbkoll"
banner = "banners/limits-webbkoll"
toc = true
+++

Webbkoll is sometimes considered as the ultimate privacy tool: You only have to enter a domain name and click on "Check". Then, you hopefully see three green check marks and two zeros. Everything is fine, isn't it?—No, because Webbkoll (like other online scanning tools) only scans a limited subset of privacy- and security-relevant configuration.

In this article, we show you reasons why Webbkoll can't be used as a primary source to classify how privacy-friendly a website is.
<!--more-->
{{< rssbox >}}

## What is Webbkoll? {#webbkoll-info}
Webbkoll is part of dataskydd.net. "Dataskydd" is Swedish and means "data protection", while "webbkoll" is a combination of "webb" and "koll" which basically means "web check". Dataskydd.net is a Swedish NGO founded by former Pirate Party EU parliamentarian Amelia Andersdotter.

Webbkoll is a scanning tool. You can enter a domain name and Webbkoll visits the website like "normal" people do. Then, Webbkoll presents you information about HTTPS configuration, [HSTS]({{< ref "/glossary.md#hsts" >}}), [CSP]({{< ref "/glossary.md#csp" >}}), Referrer Policy, [SRI]({{< ref "/glossary.md#subresource-integrity" >}}), localStorage and other security-relevant HTTP response headers. Moreover, it lists third-party requests and the approximate server location. Additionally, Webbkoll shows its users GDPR-relevant information.

## Webbkoll's limits {#limits}
First of all, Webbkoll only scans exactly the URL you entered. Subdomains or even other websites of the same domain aren't scanned. This means that the single website scanned by Webbkoll can be free of cookies, third-party content and so on, while subdomains or other websites actually send cookies, use third-party content etc. In order to get complete feedback, you must scan every website and all subdomains. Of course, this isn't really possible when this website uses dynamic content or requires you to be logged in (this can't be simulated by Webbkoll, too). [We show several examples below.]({{< relref "#examples" >}})

Secondly, even a website which is rated totally privacy-friendly (lots of zeros and green) can use databases or other servers (e.g., mail servers) which aren't accessed by your client but by the server which hosts the scanned website. For instance, a database which stores [personal data]({{< ref "/glossary.md#personal-data" >}}) of forum members can be easily accessible by criminals due to weak passwords while the website seems to be absolutely locked down. Webbkoll also doesn't check if software used by the website operator (like CMS) is up-to-date or contains known [security vulnerabilities]({{< ref "/glossary.md#vulnerability" >}}).

Thirdly, Webbkoll can't check whether personal data collected by the scanned website is lawfully collected. For instance, a form on a website can force you to enter your religion although there is no reason for the website operator to know this data. Furthermore, Webbkoll can't check whether personal data already stored by the website operator is sufficiently protected against attackers and isn't illegally processed. For example, website operators can analyze personal data or sell it to third parties while Webbkoll still says that everything is fine.

Fourthly, Webbkoll doesn't check forms or privacy policies. Forms can contain hidden fields which you can't see but their content is transmitted to the server. Besides, private website operators tend to include privacy policies only because they heard somewhere that they need one. These policies are often generated with the help of online tools and people simply adopt them. Webbkoll can't check privacy policies at all—neither their contents nor their 	presence or absence.

Fifthly, Webbkoll doesn't check for widely-known tracking techniques. For instance, there are no checks for JavaScript or CSS. Both can be used to clearly identify website visitors. Even PHP and CSS are sufficient for user tracking, however, since PHP is processed by the server and not by the client, external scans can't evaluate this.

Lastly, servers can generate lots of log files. These files can contain your IP address (already personal data), your user-agent and other information. External scans normally can't access these files. They even can't check whether website operators log your visits at all. Only using the log files, website operators can:

* see your approximate location (for example, Bratislava in Slovakia)
* see your client information (for example, you use Chrome 65 on Windows 10)
* see how often you access their website (for example, mostly in the evening between 7 and 8 p.m. on workdays)
* see how long you stay on their website (time between first and last access)
* see your interests (for example, you only read articles about the GDPR)
* …

Of course, website operators can easily combine this data with other data like your comments or personal data collected from you.

## Examples of inconsistent scanning results {#examples}
During testing we often found that different websites of the same domain show different results. We added the following three examples of well-known websites (at least for Czechs) to show you differences:

### Example 1: Observatory by Mozilla {#ex1}
We scanned the following pages:

* page 1a: https://observatory.mozilla.org/
* page 1b: https://observatory.mozilla.org/analyze/

Topic | Result for page 1a | Result for page 1b
------|-------------------|------------------
HTTPS | **Yes**           | **No; insecure**
CSP   | Good policy       | Good policy
Referrer Policy | Referrers partially leaked | Referrers partially leaked
Cookies | 0               | 0
Third-party requests | **8** requests to **4** unique hosts | **5** requests to **2** unique hosts
Server location | USA – **34.206.189.101** | USA – **52.20.90.255**

While page 1a embeds Google Fonts (fonts.googleapis.com), page 1b doesn't.

### Example 2: iDNES.cz {#ex2}
We scanned the following pages:

* page 2a: https://www.idnes.cz/
* page 2b: https://praha.idnes.cz/praha-zpravy.aspx

Topic | Result for page 2a | Result for page 2b
------|-------------------|------------------
HTTPS | Yes               | Yes
CSP   | Not implemented   | Not implemented
Referrer Policy | Referrers leaked | Referrers leaked
Cookies | **23** first-party; 131 third-party | **14** first-party; 131 third-party
Third-party requests | **318** requests to **95** unique hosts | **371** requests to **96** unique hosts
Server location | CZ – **185.17.117.32** | CZ – **185.17.117.45**

As of December 2018, page 2b sends the "content-security-policy-report-only" HTTP response header which isn't recognized by Webbkoll. ("Content Security Policy (CSP) header not implemented.") As in example 1, you silently navigate to another IP address if you go from page 2a to page 2b and the results of cookies and third-party requests differ.

### Example 3: The Office for Personal Data Protection {#ex3}
We scanned the following pages:

* page 3a: https://www.uoou.cz
* page 3b: https://www.uoou.cz/en/

Topic | Result for page 3a | Result for page 3b
------|-------------------|------------------
HTTPS | Yes               | Yes
CSP   | Not implemented   | Not implemented
Referrer Policy | Referrers leaked | Referrers leaked
Cookies | **8** first-party; **2** third-party | **7** first-party; **0** third-party
Third-party requests | **12** requests to **6** unique hosts | **7** requests to **4** unique hosts
Server location | CZ – 80.95.253.230 | CZ – 80.95.253.230

This example shows that even different language versions of the same website can make a difference when you scan them.

You can see that Webbkoll shows different results although we didn't change the domain. Why are these observations important? Because normally you wouldn't notice that there are new cookies set or additional third-party requests. Image the following scenario: You likely only check the homepage of a website, find nothing while all other pages of the domain set cookies and embed third-party content.

## External scanning is limited in general {#es}
One can argue that there are even more scanning tools like the Observatory by Mozilla, privacyscore.org etc. However, all scanning tools are somehow limited and don't offer a holistic view of the security or privacy level of a web server. The problem is that most configuration files and most types of processing of personal data remain hidden for external scanning. Webbkoll and other tools won't be able to access or evaluate these files or processes in future purely due to technical reasons. Our article "[Pros and cons of online assessment tools for web server security]({{< ref "/blog/online-assessment-tools.md" >}})" shows the most important pros and cons of these online assessment tools (there are more, of course).

Moreover, you have to keep in mind that privacy isn't only about one single web server or even website. Imagine a company, where your personal data is printed and accessible by everyone including cleaning staff. Their website could still be fine …

## Summary
Results of Webbkoll and other scanning tools are only one of many indicators to classify how privacy-friendly or secure websites are. On the one hand these tools don't include functionality to assess all important aspects of a website, on the other hand there are technical limitations which require internal scanning. Webbkoll can tell you that everything is fine, while website operators collect your personal data using log files and JavaScript and sell it to third parties. Keep this in mind when you read that someone scanned a website using Webbkoll and it showed lots of zeros and green check marks next time.

## Sources
* {{< extlink "https://webbkoll.dataskydd.net/en" "webbkoll.dataskydd.net" >}}
* {{< extlink "https://github.com/andersju/webbkoll" "webbkoll on GitHub" >}}
* {{< extlink "https://en.wikipedia.org/wiki/Amelia_Andersdotter" "Amelia Andersdotter on Wikipedia" >}}

## Changelog
* Dec 11, 2018: Updated this article including Webbkoll's update from 2018-11-30.
* May 23, 2018: Changed introduction due to feedback of Webbkoll developers/operators.
* May 8, 2018: Added examples to show differences in Webbkoll when analyzing different websites of the same domain.
