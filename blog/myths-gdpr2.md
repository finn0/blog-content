+++
title = "European GDPR myths – Part 2"
author = "Jakub"
date = "2019-12-15T10:05:05+01:00"
tags = [ "gdpr" ]
categories = [ "myths" ]
ogdescription = "There are still some GDPR myths that need to be debunked."
slug = "gdpr-myths-part2"
banner = "banners/gdpr-myths"
notice = true
toc = true
+++

In May 2018, [we debunked 3 GDPR myths]({{< ref "/blog/myths-gdpr.md" >}}). In this article, we discuss three other myths.
<!--more-->
{{< rssbox >}}

{{< noticebox "notice" "Please note: We aren't lawyers and don't pretend to be lawyers. Please do not rely on any information provided below. Contact your data protection lawyer if you have GDPR-specific questions." >}}

## Myth 1: In the case of a personal data breach, a company must inform all customers within 72 hours {#m1}
Some people are convinced that the GDPR enforces companies to notify all customers after learning about a data breach. **This is wrong.**

According to Article 34, "Communication of a personal data breach to the data subject" (data subject = an identified or identifiable natural person), the controller (the company) only needs to notify its customers (data subjects) "When the personal data breach **is likely to result in a high risk to the rights and freedoms of natural persons**." So it isn't mandatory to inform customers if there is medium or low risk. As you can see, it is up to the company to rate the severity of the data breach.

Then, there is no specific time frame for notifications in case of high-risk situations. The Article only says, "**without undue delay**." Recital 86 adds, "Such communications to data subjects should be made **as soon as reasonably feasible** and in close cooperation with the supervisory authority, respecting guidance provided by it or by other relevant authorities such as law-enforcement authorities." and "the need to implement appropriate measures against continuing or similar personal data breaches **may justify more time for communication**." No 72 hours here.

The mandatory notification in case of "high risk to the rights and freedoms of natural persons" is even more limited by Article 34, paragraph 3. **Companies don't need to notify people**:

* if they "implemented appropriate technical and organisational protection measures, and those measures were applied to the personal data affected by the personal data breach." For instance, if the affected personal data is encrypted.
* if they took "subsequent measures which ensure that the high risk to the rights and freedoms of data subjects referred to in paragraph 1 is no longer likely to materialise." In other words, they did something that reduced the previously high risk to medium or low risk.

The "72 hours" time frame likely originates from Article 33, "Notification of a personal data breach to the supervisory authority." Article 33 says, "In the case of a personal data breach, the controller shall without undue delay and, **where feasible**, not later than 72 hours after having become aware of it, notify the personal data breach to the supervisory authority competent in accordance with Article 55, **unless the personal data breach is unlikely to result in a risk to the rights and freedoms of natural persons**. Where the notification to the supervisory authority is not made within 72 hours, it shall be accompanied by reasons for the delay." Even this Article doesn't enforce 72 hours (and it is about notifying supervisory authorities, not about informing data subjects).

## Myth 2: Everybody can declare GDPR violations {#m2}
**Wrong.** Laws and regulations are complicated. In the case of the GDPR, you need to read and understand its Articles and the Recitals. Then, it may be necessary to consider translated versions of the GDPR, and of course, there are also national privacy laws. In summary, you (or random people on the internet) can't declare GDPR violations.

Suppose you think that somebody processes your personal data in some way that isn't allowed. In that case, you have the "right to **lodge a complaint** with a supervisory authority," as defined by Article 77. Keep in mind that you only have this right "if the data subject considers that **the processing of personal data relating to him or her** infringes" the GDPR. So you can't complain within the meaning of Article 77 if a company doesn't process any of your personal data.

## Myth 3: It is perfectly clear how to interpret GDPR Articles {#m3}
As written above, laws and regulations are complicated. It gets even more complicated if there are multiple authorities involved in data protection. For instance, in Germany, each of the 16 states has its own Data Protection Supervisor. There are even two authorities in Bavaria, one for the public and one for the private sector. Finally, there is a nation-level Data Protection Supervisor. In summary, there are at least 18 supervisory authorities in Germany.

These supervisory authorities don't interpret the GDPR in the same way. For example, in October 2018, the Bayerisches Landesamt für Datenschutzaufsicht (one of the Bavarian supervisory authorities) published a press release addressing whether doorbell nameplates installed by landlords are subject to the GDPR. They believe that installing such nameplates isn't automatic processing of personal data, and one can hardly argue that it is subject to the GDPR. Contrary to this, the Data Protection Supervisor of Baden-Württemberg (another German state) declared that installing doorbell nameplates is, in fact, data processing according to the GDPR.

So this myth is **wrong** since there are many different interpretations of the GDPR.

## Conclusion
As always, it is best to consult a lawyer in case of doubt. If you think that somebody processes your personal data in a shady way, contact a supervisory authority. However, please don't spread the wrong myths debunked in this article.

{{< noticebox "warning" "And again: Contact your data protection lawyer if you have GDPR-specific questions. Online forums or other websites of people who aren't data protection lawyers aren't reliable sources for this topic." >}}
