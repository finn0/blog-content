+++
title = "fscrypt"
author = "Benjamin"
date = "2019-11-30T14:10:30+01:00"
tags = [ "fscrypt" ]
categories = [ "tutorial" ]
ogdescription = "fscrypt is a high-level tool for managing Linux filesystem encryption."
slug = "tool-fscrypt"
banner = "banners/ish-tools"
notice = true
toc = true
syntax = true
+++

We look at [fscrypt]({{< relref "#external-links" >}}). fscrypt is a high-level tool for the management of Linux filesystem encryption. fscrypt manages metadata, key generation, key wrapping, PAM integration, and provides a uniform interface for creating and modifying encrypted directories.
<!--more-->
{{< rssbox >}}

{{< noticebox "warning" "fscrypt applies encryption at the directory level. Directory-level encryption isn't the same as full-disk encryption. If you need full-disk encryption on Linux, use LUKS." >}}

## Requirements for fscrypt
To use fscrypt, you need a filesystem with encryption support and a kernel supporting reading and writing from the filesystem:

* ext4 and Linux kernel 4.1 (or newer)
* F2FS (Flash-Friendly File System) and Linux kernel 4.2 (or newer)
* UBIFS (Unsorted Block Image File System) and Linux kernel 4.10 (or newer)

Since ext4 (the "fourth extended filesystem") is very common, we only talk about the ext4 in this post.

## Checking ext4 for encryption support
First of all, you need to check if your ext4 filesystem supports fscrypt. Check the following:

* Check if your filesystem is really ext4 formatted.
* In your terminal, enter {{< kbd "getconf PAGE_SIZE" >}} and {{< kbd "tune2fs -l /dev/device | grep 'Block size'" >}}. Both values MUST be identical.
* If you are using GRUB (GNU GRand Unified Bootloader), be sure that you use 2.04 or newer. GRUB older than 2.04 doesn't support booting ext4 filesystem with encryption enabled.

Proceed only if your system meets all requirements.

## Install fscrypt and set it up
Install fscrypt via your system's package manager. On Arch, you may run {{< kbd "yay -S fscrypt-git" >}}.

After this, enter {{< kbd "fscrypt status" >}}. The output shows whether your filesystem supports encryption and whether fscrypt is enabled. Check this carefully. If everything looks good, enter {{< kbd "sudo fscrypt setup" >}} to create the global configuration file. This is the only time you need root privileges.

After this, you have to decide on which filesystem you want to enable fscrypt. Let's assume you chose "/dev/sdb1." Enter {{< kbd "fscrypt setup /dev/sdb1" >}}. This command creates a hidden ".fscrypt" folder that contains relevant metadata.

The next step is already setting up a directory that should be encrypted. **Use a new folder for this!** Create a new folder (e.g., by entering {{< kbd "mkdir newfolder" >}}) and set it up by entering {{< kbd "fscrypt encrypt newfolder" >}}.

Afterward, fscrypt asks which protector it should use. A "protector" in fscrypt is just some kind of secret. fscrypt supports your login passphrase, a custom passphrase, or a raw key file. You can change your protector later. (In reality, it is more complicated; however, we won't cover this in detail.)

Let's tell fscrypt to use "A custom passphrase" here. Assign a name. Then enter and confirm your password. The final output should be something like:

{{< samp "'newfolder' is now encrypted, unlocked, and ready for use" >}}

Enter {{< kbd "fscrypt status newfolder" >}} to check again. The output should be similar to:

{{< samp "'newfolder' is encrypted with fscrypt" >}}

## Using your encrypted folder
After setting up the new folder, you can put some files in there. fscrypt only encrypts the files that you add to the folder after enabling the encryption. Due to this, you need to re-add files after enabling encryption or just use a new folder (as suggested above).

During regular use, you don't need to lock the folder. After a system restart, you can unlock your folder by entering {{< kbd "fscrypt unlock newfolder" >}}. You must enter your passphrase, and fscrypt will output:

{{< samp "'newfolder' is now unlocked and ready for use" >}}

Then, you can use it as any other folder on your system.

## Conclusion
In summary, fscrypt provides an easy way to set up folder-based encryption on most Linux systems. For more details, check [fscrypt's documentation]({{< relref "#external-links" >}}) on GitHub.

## External links
* {{< extlink "https://github.com/google/fscrypt/blob/master/README.md" "fscrypt" >}}
