+++
title = "European GDPR myths – Part 1"
author = "Benjamin"
date = "2018-05-09T12:19:01+02:00"
tags = [ "gdpr" ]
categories = [ "myths" ]
ogdescription = "The GDPR becomes enforceable on May 25 and there are many myths in circulation. We debunk three of them."
slug = "gdpr-myths"
banner = "banners/gdpr-myths"
notice = true
toc = true
+++

The European General Data Protection Regulation (GDPR) becomes enforceable on May 25, 2018, and replaces the European Data Protection Directive from 1995. More and more people talk about it every day, and there are many myths in circulation. In this article, we debunk three of them.
<!--more-->
{{< rssbox >}}

{{< noticebox "notice" "Please note: We aren't lawyers and don't pretend to be lawyers. Please do not rely on any information provided below. Contact your data protection lawyer if you have GDPR-specific questions." >}}

## Myth 1: The GDPR provides identical data protection for all EU member states {#m1}
**Wrong.** The keyword here is _harmonization_: One of the goals of the GDPR is to harmonize some data protection aspects "to ensure a consistent and high level of protection of natural persons and to remove the obstacles to flows of personal data within the Union, the level of protection of the rights and freedoms of natural persons with regard to the processing of such data should be equivalent in all Member States" (GDPR, Recital 10). In other words, the GDPR is like a foundation for data protection in the EU.

However, the GDPR "provides a margin of manoeuvre for Member States to specify its rules, including for the processing of special categories of personal data" (GDPR, Recital 10). These so-called opening clauses allow each member state to add additional regulations or modify some rules defined in the GDPR. For example, in Austria, there is the "Datenschutz-Anpassungsgesetz 2018," which revises the current "Datenschutzgesetz 2000," or in Germany, there is a new version of the "Bundesdatenschutzgesetz." These national laws differ from country to country, even when the GDPR becomes enforceable.

Additionally, you have to keep in mind that these national laws and the GDPR aren't the only regulations that deal with data protection. There are dozens of other laws and regulations addressing the broad topic of data protection.

## Myth 2: Private individuals don't have to comply with the GDPR {#m2}
**Wrong.** Some people seem to be convinced that the GDPR only affects companies and authorities. However, private individuals also have to comply with the GDPR. There is an exception, though: The GDPR "does not apply to the processing of personal data […] by a natural person in the course of a purely personal or household activity" (GDPR, Article 2).

This means that private individuals must comply with the GDPR when processing personal data of other natural persons for non-personal activities. Of course, "non-personal activities" is a matter of interpretation. However, is a website "purely personal" when it promotes products/services or embeds advertisements to make money? This is very questionable.

In the past, lawyers often interpreted "purely personal" very strictly. For example, using your friends' e-mail addresses to forward an advertisement could already be categorized as "non-personal" processing of personal data.

_Fun fact:_ Most national data protection laws already include this "purely personal or household activity" constraint (e.g., Austrian DSG 2000, German BDSG, Czech Act No. 101 / 2000 Coll.). This isn't a new aspect of the GDPR.

## Myth 3: Private individuals can sue their friends for discompliance {#m3}
**Wrong.** This opinion is the opposite of myth 2: People saying that they will sue you if you use services like WhatsApp because you shared their phone number with the company.

Keep in mind: The GDPR "does not apply to the processing of personal data […] by a natural person in the course of a purely personal or household activity" (GDPR, Article 2). This means that the GDPR—and most national data protection laws—don't apply if you process personal data of your family members or friends for "purely personal" purposes.

## Conclusion and further information {#conclusion-information}
No, data protection won't be 100% identical in all EU member states soon, and many aspects of the GDPR are already part of previous national laws. Moreover, don't assume that reading the GDPR is enough to know every aspect of data protection in the EU.

You should also keep in mind:

* The term "processing" within the scope of the GDPR means "any operation or set of operations which is performed on personal data or on sets of personal data, whether or not by automated means, such as":
  * collection
  * recording
  * organisation
  * structuring
  * storage
  * adaptation or alteration
  * retrieval
  * consultation
  * use
  * disclosure by transmission
  * dissemination or otherwise making available
  * alignment or combination
  * restriction
  * erasure or destruction
* The GDPR doesn't define the technical aspects of data protection. For example, Article 32 mentions "encryption of personal data"; however, there are no minimum requirements like "use [AES]({{< ref "/glossary.md#aes" >}})-256."
* There is another upcoming regulation (ePrivacy Regulation) that will replace the European "Regulation on Privacy and Electronic Communications" from 2002. This regulation focuses on electronic communications data that qualify as personal data.

In the meantime, [we published a second article debunking GDPR myths]({{< ref "/blog/myths-gdpr2.md" >}}).

{{< noticebox "warning" "And again: Contact your data protection lawyer if you have GDPR-specific questions. Online forums or other websites of people who aren't data protection lawyers aren't reliable sources for this topic." >}}
