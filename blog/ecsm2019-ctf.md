+++
title = "ECSM 2019: Preparing and hosting a security CTF contest"
author = "Benjamin"
date = "2019-10-19T14:10:04+02:00"
tags = [ "ecsm2019", "ctf", "ot-security" ]
categories = [ "knowledge" ]
ogdescription = "Our summary on preparing and hosting a security CTF contest."
slug = "ecsm2019-ctf"
banner = "banners/ecsm"
toc = true
+++

In recent weeks, some of us were heavily involved in preparing and hosting a public CTF contest in the context of this year's European Cyber Security Month (ECSM). More than 1,000 teams registered, nearly 600 solved at least one challenge. Unlike most other CTFs, this one was focused on industrial security.

In this article, we give an insight into the preparations and hosting of the CTF event and discuss some lessons learned.
<!--more-->
{{< rssbox >}}

## The Syskron Security CTF
The Syskron Security CTF contest was a free online cyber security competition for everyone, especially for school and university students. CTF means "Capture The Flag." Teams had to solve security challenges to retrieve flags (text strings). Then, they got points for submitting the flags.

### Preparing the contest
In the following, there are some main activities when preparing the contest:

* **Preparing the technical platform:** In this case, we set up and configured four servers (see [Notes on infrastructure]({{< relref "#notes-on-infrastructure" >}})). Then, the CTFd framework didn't meet GDPR requirements, so we needed time to customize it. Moreover, you need a domain name, DNS records, an e-mail address, a security concept, a testing environment, and so on.
* **Creating challenges:** Creating and testing challenges took the most time. It is not only about hiding a flag somehow but also about writing a story, creating hints that don't immediately disclose the flag, defining categories and points, and writing a full internal walkthrough. One challenge included a technical setup that needed monitoring, validation, and some scripting.
* **Announcing the event:** We announced the event on different platforms. However, the main lesson was that promoting your CTF on ctftime.org is essential.
* **Writing rules and policies:** You need rules and policies!

In total, we started preparations about two months before the actual event.

### The challenges
26 challenges were provided, split up into six categories:

{{< webpimg "art-img/ecsm2019-category.png" "The focus of the CTF was on forensics; however, we tried to create an equal number of challenges per category." "the categories of the CTF as a pie chart." >}}

#### OSINT
OSINT was all about gathering intelligence from public sources. This included identifying the location of three industrial sites and finding the default credentials of an industrial VPN solution.

#### Crypto
Crypto contained three challenges. They addressed the risks of rolling your own cryptographic scheme and using easy-to-guess credentials. Additionally, one challenge was about NaCl (the "Networking and Cryptography library") to show this library for network communication, encryption, decryption, and signatures.

{{< webpimg "art-img/ecsm2019-epes.png" "The challenge 'Enhanced PLC Encryption Standard' addressed the risk of rolling your own cryptographic scheme." "a challenge description of the CTF." >}}

#### Forensics
In total, eight forensics challenges were provided. One required connecting to an MQTT broker and then subscribing to a topic. Another one included data hidden in the Siemens S7 protocol. Some challenges required participants to analyze log files, find leaked data, and conduct a [dictionary attack]({{< ref "/glossary.md#dictionary-attack" >}}). Steganography (the practice of concealing something within a file) was also part of two challenges.

#### Trivia
Trivia contained four knowledge-based questions. One addressed the computer worm Stuxnet, another one the WannaCry ransomware attack, and two questions addressed security features of OPC UA, a machine-to-machine communication protocol.

#### Secret
The secret category was unlocked by solving the most straightforward OSINT challenge. It contained the industrial-themed 4-challenge-long story of Serra Raaphorst, an employee of a Dutch manufacturing company in Den Bosch. Raaphorst asked the CTF participant to discover the "dirty secrets" of her boss. Her story required people to decode DTMF (dual-tone multi-frequency signaling), solving a word search puzzle, and visiting the (digital) Library of Babel.

#### Fun
The final category featured three challenges, which weren't related to industrial security.

{{< webpimg "art-img/ecsm2019-solves.png" "436 teams solved 'Industrial sightseeing tour 1', the OSINT challenge that unlocked the 'secret' category. However, nobody solved 'My eyes hurt'." "the solve count of the CTF as a bar chart." >}}

### The (real) background
Of course, one of the main goals of this CTF is raising awareness of industrial security (or OT security). [OT]({{< ref "/glossary.md#ot" >}}) (operational technology) security is different from IT (information technology) security. Good practices of IT security don't apply:

* The overall security objective of OT environments is constant [availability]({{< ref "/glossary.md#availability" >}}) and [integrity]({{< ref "/glossary.md#integrity" >}}). In most IT environments, [confidentiality]({{< ref "/glossary.md#confidentiality" >}}) has a top priority. Then, there are also safety requirements for OT environments since they can cause significant harm to people.
* Many OT environments consist of special-purpose hardware running special-purpose software. Such hardware must be frequently available around the clock, and it runs for decades. Installing security updates means costly downtime. There is also the risk of changing configuration, breaking things (resulting in even more downtime), or there are just no security updates available for many different reasons. In some cases, updating this special-purpose equipment requires hardware upgrades that cost millions of euros.
* On the network level, some machine-to-machine (M2M) communication has to be in realtime. So packet inspection or other active analysis of network traffic can't be applied.
* Of course, most general-purpose IT tools (on the attacker's and the defender's side) don't work in OT environments. For instance, vulnerability scanners can't identify most vulnerabilities in OT environments, anti-malware software breaks availability, and host-based mitigations are often unavailable since most OT components run special-purpose operating systems.

There are many more differences (look on the internet if you are interested in this topic). Keep all of this in mind when you are reading about attacks on industrial environments next time.

### Notes on infrastructure
We won't go into detail here. The whole setup included four servers:

* **Submission server:** This server ran a modified CTFd 2.1.5 framework and Nginx as a proxy server. We moved resources hosted by third parties by default (like fonts and CSS files) to our own installation and added some security to the whole setup. We modified several CTFd files to meet the requirements of the European GDPR. One physical server with 8GB memory was sufficient to handle 1,5k users for five days.
* **Challenge server:** This server ran the MQTT broker that was part of a challenge. Initially, it was also in use to host a landing page.
* **Monitoring server:** The monitoring server was only internally accessible. It ran Icinga 2 and monitored system resources as well as user/team registrations, and other security-related properties of the public-facing servers. We used it for alerting in case of anomalies.
* **Mail server:** The mail server, yeah, was used to send and receive e-mails.

There was no tracking of users, so there aren't any nice graphs showing the origin of the CTF participants. At least, we can show the top 10 teams:

{{< webpimg "art-img/ecsm2019-scoreboard.png" "The French team 0x90r00t was the fastest team reaching about 85% of all points. The second-best team TeamPowerPrinter/Gutenberg from Denmark got the same amount of points about three days later. The academic team HgbSec from FH Oberösterreich (Campus Hagenberg) ranked third." "the top 10 teams over time." >}}

## Lessons learned
There are five lessons learned that we want to share:

1. In total, 1,500 users registered and created more than 1,000 teams. However, well-known top CTF teams only used one single user account during the contest. They shared their credentials and accessed their account from many different origins. Keep this in mind if you monitor your setup. Moreover, this means that the actual number of users was slightly higher.
2. Most flags were directly shown to participants after solving the challenge. However, there were also some challenges requiring users to write the flag. Only the flag format was given. For instance, people had to identify the company's name of an industrial structure in a picture. The lesson here was that many people didn't read (or understand) the flag format and general directions regarding these flags. Instructions were shown directly on the challenges page. Maybe, such directions must be part of the challenge's description.
3. Two different types of hints were provided: Free general hints and hints that helped solving the challenge. Teams had to "pay" 10% of the challenge's points for unlocking hints that helped them to solve a challenge. (For example, solving a challenge resulted in +500 pts. So they needed to "pay" 50 pts.) We think this approach is fair since teams that solve challenges without using hints get more points.
4. We decided to exclude some teams for violating one of the six rules of the CTF. Some teams tried to brute force the flag by entering hundreds of random strings; others tried to brute force the flag by systematically iterating over possible answers. One challenge was easily solvable by guessing since there were only six possible answers. Keep this in mind when designing your challenges and writing your rules.
5. While many participants liked the CTF (currently, the contest is rated 24.xx out of 25 on ctftime.org, ~45 teams voted), some of them reported solving challenges required guessing. At first glance, this looks like something that needs to be changed next time. However, most people likely didn't encounter such challenges before, so they didn't know how to solve this quickly. The lesson here is that you shouldn't back off from creating challenges that differ from common CTF challenges.

## Summary
The vast majority of teams rated this CTF as a challenging and enjoyable experience, and appreciated the event's industrial style. Our infrastructure worked as expected, and there were no disturbances. So maybe the Syskron Security CTF will be back in 2020. Anyway, hosting CTFs is a good way to raise awareness of information security.

## External links {#links}
* {{< extlink "https://ctftime.org/event/901" "Syskron Security CTF on ctftime.org (including write-ups, scoreboard, and tasks)" >}}
* {{< extlink "https://github.com/lepPwn/CTF-Games/tree/master/Syskron%20Security%20CTF" "Most challenges on GitHub (including descriptions and files)" >}}
* {{< extlink "https://github.com/CTFd/CTFd" "Most challenges on GitHub (CTFd, an open-source CTF framework)" >}}
