+++
title = "The current state of the LineageOS-based /e/ ROM"
author = "Jakub"
date = "2019-08-03T18:47:00+02:00"
tags = [ "android", "rom", "lineageos", "e-foundation" ]
categories = [ "Privacy" ]
ogdescription = "/e/ promises a privacy-enabled smartphone OS–we look at it in this article."
slug = "e-foundation-second-look"
banner = "banners/e-foundation"
notice = true
toc = true
syntax = true
+++

In March 2019, [we checked the LineageOS-based ROM]({{< ref "/blog/e-foundation-first-look.md" >}}) provided by the French /e/ Foundation.

In this article, we look at the /e/ Android ROM again to check whether issues identified by us are still present.
<!--more-->
{{< rssbox >}}

{{< noticebox "note" "Please note: Our articles on the /e/ ROM aren't comprehensive security tests or privacy checks of the /e/ ROM. Checking its security requires much more than only monitoring and analyzing some network traffic, and checking its privacy requires to consider legal aspects that we can't examine thoroughly." >}}

## Preparation
Like last time, we wiped our Moto G4. Then, we installed a fresh copy of /e/ using {{< kbd "adb sideload" >}}. Initially, we skipped most configuration. We only set the local time. The setup asks you to register for an /e/ account optionally. We didn't do this.

After the initial configuration, we connected the Moto G4 with a dedicated VLAN to monitor its entire network traffic.

## Features
Before we analyze the network traffic, we look at features of /e/ [like last time]({{< ref "/blog/e-foundation-first-look.md#features" >}}). The current ROM for the Moto G4 (build date August 3, 2019) is based on Android 7.1.2 like the previous time (Android 7.1.2 is still officially supported by Google). The Android security patch level is July 5, 2019, according to the settings. So it is the latest patch level for Android.

Preinstalled apps are, among others:

* Apps (/e/ app store)
* Bliss Launcher
* Chromium-based /e/ web browser
* LibreOffice Viewer
* Magic Earth (maps client)
* K-9 Mail-based mail client
* microG Services Core
* MuPDF mini (PDF viewer)
* Notes
* Open Camera
* OpenKeychain (OpenPGP client)
* OpenTasks
* QKSMS (SMS messenger, branded as foundation.e.message)
* Weather client
* several apps included in LOS/Android (Clock, Contacts, etc.)

They removed Signal and Telegram and added their own Apps store.

## Security and privacy {#sap}
Since this article also isn't focused on features of /e/, we again look at security and privacy aspects. The /e/ Foundation promotes its same-named mobile OS as "ungoogled," coming with "carefully selected apps."

### Communication with the internet {#communication}
In this section, we discuss several findings regarding /e/'s network traffic when connected to the internet.

#### Hello Google, you are still there {#google}
[Last time]({{< ref "/blog/e-foundation-first-look.md#google" >}}), we saw that /e/ connected to Google servers for its connectivity check. This time, we see exactly the same behavior:

{{< samp "GET /generate_204 HTTP/1.1" >}}
{{< samp "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.82 Safari/537.36" >}}
{{< samp "Host: connectivitycheck.gstatic.com" >}}
{{< samp "Connection: Keep-Alive" >}}
{{< samp "Accept-Encoding: gzip" >}}

Our G4 connects to "172.217.18.99," a Google server. There is a new [issue (#268)]({{< relref "#links" >}}) on /e/'s GitLab instance. Seemingly, they try to set up their own server for connectivity checks to get rid of Google here.

Last time, other users reported that /e/ used Google's DNS servers (e.g., 8.8.8.8) by default. We weren't able to reproduce this and [mentioned this in our last report]({{< ref "/blog/e-foundation-first-look.md#google" >}}). /e/ closed the related ticket #269, and stated: "This is not true […] The conclusion was that 8.8.8.8 and 2000:: are only used in Bionic to figure out if some IPv4 or IPv6 is available."

Besides, there is still network traffic from/to www.gstatic.com, clientservices.googleapis.com, and www.google.com via IPv4. This finding is tracked by [issue #271]({{< relref "#links" >}}). Issue #271 is still open, and there are no relevant comments.

#### NTP synchronization around the world {#ntp}
This finding didn't change at all. [See our last report.]({{< ref "/blog/e-foundation-first-look.md#ntp" >}})

The related [issue #272]({{< relref "#links" >}}) on /e/'s GitLab instance states: "This is something probably not extremely challenging in term of user’s data privacy protection and at least it doesn’t go to Google. Actually maybe we should sort out the list of NTP servers and keep only 'trusted ones', if this exists. And/or add some /e/ NTP servers."

#### Weather app still leaks personal data in cleartext {#weather}
[Last time]({{< ref "/blog/e-foundation-first-look.md#weather" >}}), the preinstalled weather app (foundation.e.weather, version 4.4) leaked our location in real-time. Each time, we searched for a location to get the current weather, the app sent a GET request to api.openweathermap.org in cleartext. One example was:

{{< samp "GET /data/2.5/find?q=lin&type=like&cnt=15&APPID=50[…]8 HTTP/1.1" >}}

If we used the device's GPS to get weather information, it leaked our current position.

The weather app still leaks data like your location in cleartext. Obviously, /e/ didn't add HTTPS to encrypt data in transit. However, they seemingly changed the user-agent to "okhttp/3.2.0." Last time, it was "Dalvik/2.1.0 (Linux; U; Android 7.1.2; Moto G4 Build/NJH47F)," leaking device information. This behavior is interesting since the version number didn't change.

The related [issue #273]({{< relref "#links" >}}) on /e/'s GitLab instance states: "The weather app will be improved, to use HTTPS and remove user-agent. […]." The issue is still open. The problem of leaking data is still present.

#### Magic Earth–again cleartext traffic {#magicearth}
We checked Magic Earth (com.generalmagic.magicearth, version 7.1.19.20…) again. [Last time]({{< ref "/blog/e-foundation-first-look.md#magicearth" >}}), we identified that the app talked to 12 different IP addresses owned by General Magic. Ten times the communication was in cleartext only.

This time, we experienced the same behavior. It is still unclear why only some traffic is encrypted.

The related [issue #274]({{< relref "#links" >}}) on /e/'s GitLab instance states: "MagicEarth provided us a quite comprehensive document regarding privacy, and we are working with them to improve this aspect. We will discuss with them the issue with unencrypted traffic." So there is no update here.

### The App store of /e/ {#appstore}
In March, there was no app store installed by default. This time, /e/ ships their own app store "Apps" (foundation.e.apps, version 1.1.5).

The app allows you to request new apps, search for apps, and also games, and you can download/install apps, of course. /e/ seemingly offers a broad mix of apps, contrary to F-Droid. So you also find apps like "SWAT and Zombies - Defense & Battle" that contain ads and trackers. Some people probably don't like this; however, there is a chance to get rid of third-party stores to access Google's infrastructure (like Aurora Store).

We checked for version information of 10 random apps for comparison:

| Name          | /e/ version | Google Play version |
|---------------|-------------|---------------------|
| Conversations | 2.5.3+fcr   | 2.5.5+pcr           |
| Firefox Klar  | 8.0.15      | device dependent    |
| Keybase       | 4.0.1       | 4.2.0               |
| Orfox¹        | 52.9.0esr   | 52.9.0esr           |
| Protonmail    | 1.11.4      | 1.11.4              |
| Signal        | 4.43.8      | 4.44.7              |
| Steam Chat    | 0.9         | 0.9                 |
| Telegram      | 5.7.1       | device dependent    |
| VLC           | 3.1.7       | device dependent    |
| WhatsApp      | 2.19.188    | device dependent    |

So some apps are slightly older than on Google Play. _¹Note that Orfox is totally outdated and insecure. Don't use it. We don't know why they added this app to their app store._

Using the Apps store results in traffic to/from "cleanapk.org." We don't know who operates this domain. The device downloaded two certificates (api.cleanapk.org and apk.cleanapk.org). Most communication was TLS 1.2 encrypted. It would be nice to get some information about "cleanapk.com." Maybe, this domain is operated by /e/ for their app store.

### /e/ Foundation website {#websites}
[Last time]({{< ref "/blog/e-foundation-first-look.md#websites" >}}), we also identified some issues with the website of /e/. For instance, their versions of WordPress and WooCommerce were outdated and contained publicly-known security vulnerabilities.

We checked their main website e.foundation again. This time, it runs WordPress 5.2.2 (the current version) and WooCommerce 3.6.5 (the current version). The server itself runs Apache 2.4.29 on Ubuntu 18.04 LTS (last time, it ran Apache 2.4.10 on Debian 8). Looking only at version numbers, this was improved by /e/.

However, nearly all modern security-related HTTP response headers like the Content Security Policy are still not set. The website still supports outdated TLS 1.0 and 1.1 protocols, but dropped support for old DES-based cipher suites. So there was only minor improvement here. We think that securing their website should take top priority, primarily due to 165 JavaScript files embedded on their main page. There is also some third-party content embedded, hosted on "cdn.jsdelivr.net" and "stackpath.bootstrapcdn.com."

Interestingly, the Google font is gone now. Last time, /e/ stated that removing the font hosted by Google isn't essential for them since their promise regarding being Google-free doesn't include their website. The related [issue #275]({{< relref "#links" >}}) talks about issues with the CMS, and it is still open.

## Summary
Compared with our last report, the overall situation is slightly improved. /e/'s website is based on up-to-date software now, and they removed the Google font. Additionally, they seemingly masked the user-agent of the Weather app. Most other findings are still unaddressed, though.

The new app store is a chance to get rid of Google Play completely; however, we don't know about the relationship with "cleanapk.com," and how they ship their apps (e.g., who builds and signs the apps?).

As before, /e/ looks promising, it isn't Google-free by now. For our final test, please read ["The state of the LineageOS-based /e/ ROM in December 2019"]({{< ref "/blog/e-foundation-final-look.md" >}}).

## External links {#links}
* {{< extlink "https://gitlab.e.foundation/e/management/issues/268" "GitLab issue #268 'Infosec Handbook Review Issues : “.... /e/ .. using Google for connectivity check:”'" >}}
* {{< extlink "https://gitlab.e.foundation/e/management/issues/271" "GitLab issue #271 'Infosec Handbook Review Issues : “...TLS 1.2-encrypted traffic from/to www.google.com.... via IPv4/IPv6.”'" >}}
* {{< extlink "https://gitlab.e.foundation/e/management/issues/272" "GitLab issue #272 'Infosec Handbook Review Issues : “NTP synchronization leaks IP Address to many different domain names”'" >}}
* {{< extlink "https://gitlab.e.foundation/e/management/issues/273" "GitLab issue #273 'Infosec Handbook Review Issues : “... Weather app leaks IP address and other details...in cleartext.”'" >}}
* {{< extlink "https://gitlab.e.foundation/e/management/issues/274" "GitLab issue #274 'Infosec Handbook Review Issues : “ Info is sent/received to/from General Magic in cleartext.”'" >}}
* {{< extlink "https://gitlab.e.foundation/e/management/issues/275" "GitLab issue #275 'Infosec Handbook Review Issues : “Info on usage of Google fonts shared in January, no response from /e/.”'" >}}
