+++
title = "How some 'security' blogs and websites actually cause insecurity"
author = "Benjamin"
date = "2018-12-01T10:45:00+01:00"
tags = [ "privacy", "blogging" ]
categories = [ "discussion" ]
ogdescription = "Many blogs and websites tell you something about 'security' but sometimes it's only about making money."
slug = "discussion-security-blogs"
banner = "banners/discussion-security-blogs"
toc = true
+++

In 2013, Edward Snowden revealed the global spy activities of the National Security Agency. In the years that followed, dozens of blogs and websites popped up to tell you about 'security' and how to defend against the evil US-American companies that passed your [personal data]({{< ref "/glossary.md#personal-data" >}}) to government agencies.

In practice, some of these blogs and websites only exist to spread FUD (fear, uncertainty, and doubt) to get your money eventually. We discuss several indicators for this phenomenon and its influence on your security in this article.
<!--more-->
{{< rssbox >}}

## Our Top 5 indicators for shady blogs and websites {#top5}
Consider at least the following five indicators if you read articles or search for new blogs worth reading.

### Indicator 1: 'Technology is everything in information security' {#i1}
We often see blogs and websites that are entirely focused on technology when it comes to information security. The aforementioned is okay as long as readers see that the focus of an InfoSec blog or website is technology. A famous example is Matthew Green's blog, "A Few Thoughts on Cryptographic Engineering." He focuses on cryptography, and we likely never look for information security awareness on his blog.

However, some blogs are telling you that information security is only about technology. According to these blogs and websites, InfoSec means algorithms, cryptography, configuration, network protocols, and so on. The authors never talk about people, processes, organization, risk management, security controls, incident response, disaster recovery, etc. They ignore other significant aspects of information security or actively tell their readers that these aspects are useless.

In our opinion, this leads to an incomplete impression of information security, and readers likely focus on technology. For instance, [social engineering]({{< ref "/glossary.md#social-engineering" >}}) (read ["The story of Jessika"]({{< ref "/blog/social-engineering-jessika.md" >}})), risk management, and processes are also very relevant to private individuals.

### Indicator 2: Articles are biased {#i2}
We often see biased articles. Biased articles either only contain lots of benefits of software/specific technology or lots of drawbacks. You won't find balanced assessments.

Do you read articles about instant messengers? Some people try to present their favorite messenger as the ultimate solution to all problems while traducing all other messaging solutions. However, no messenger fits all needs of everybody, and there is also [no secure instant messenger]({{< ref "/blog/discussion-secure.md#sm" >}}).

In our opinion, biased articles seem to be all about presenting the author's ideological beliefs as the "right ones." In reality, these articles result in myths and insecurity since many readers rely on the author's expertise. Subliminally presenting ideological beliefs or deliberately leaving out major drawbacks isn't proficient at all.

### Indicator 3: There are contradictory recommendations {#i3}
There are numerous reasons for contradictory recommendations on the internet: Good practices change, developers update software, organizations introduce new security features. For example, the statement "a 6-digit password is secure" was valid several years ago but isn't right in 2019.

Various legal and technical requirements in different countries also result in contradictory recommendations. The ISO/IEC 27001 or IEC 62443 standardize some security aspects, but there are no global information security regulations or privacy laws.

Some websites tell you that particular software or approaches are inadequate or insecure only to recommend the same in other articles written by the same author. Additionally, some authors recommend configuration or implementations based on any facts but paranoia or ideological beliefs (again).

Ultimately, you wonder what to do next since different websites recommend contradictory things.

### Indicator 4: Articles spread typical myths {#i4}
All indicators mentioned in this article lead to myths. Some readers repeat the false stories they read or heard somewhere. We debunked some of the most common myths in several articles:

* "[Private individuals can sue their friends for discompliance with the GDPR]({{< ref "/blog/myths-gdpr.md#m3" >}})."
* "[Open-source software is more secure than proprietary software]({{< ref "/blog/myths-software-security.md#m1" >}})."
* "[Externally scanning servers discovers all security and privacy issues]({{< ref "/blog/myths-web-security.md#m1" >}})."

Another prevailing myth is the "privacy-friendly and secure world of XMPP." In reality, [server admins (and server-side attackers) can log and modify nearly everything]({{< ref "/blog/xmpp-aitm.md" >}}), including cleartext passwords, contacts, reading statuses, and device activity.

In our opinion, the authors of InfoSec articles could avoid most myths by thoroughly researching their topics. Unfortunately, some authors only repeat stories written somewhere else without checking them.

### Indicator 5: Authors have no background in InfoSec {#i5}
Authors with no background in InfoSec are the last indicator. Does this mean that any author must be a Hollywood hacker? No.

In reality, there are many different domains of information security. A degree in Computer Science isn't necessarily a requirement to work for an InfoSec company. Typical InfoSec jobs are incident responders, security auditors, security awareness officers, penetration testers, chief information security officers, security code auditors, security analysts, forensics experts, security architects, security consultants, vulnerability assessors, and many more.

These jobs require different skills and diverse educational backgrounds. They have different focuses in the InfoSec world. Besides, the privacy or data protection world consists of many legal and contractual aspects in most countries and differs from information security.

Some authors of InfoSec articles don't have an InfoSec background at all. This is fine as long as they provide fact-based information. However, some authors create their own facts and continue to spread myths, leading to the problems mentioned above.

## Summary
Nowadays, information security affects everybody, but some people try to milk this topic to make money in a post-Snowden world. Articles based on ideological beliefs and hearsay quickly result in myths and insecurity.

We conclude this article with several tips:

* Keep in mind that information security is much more than only technology—also read about information security awareness, risk management, and processes/organization.
* Keep in mind that there are no international privacy laws or InfoSec regulations. Depending on the author's home country, tips and recommendations differ.
* Check the professional background of an author, if possible.
* Check whether the author only promotes his own articles and tries to create a filter bubble to make money.
* Challenge blanket statements and check different websites for more information—an alleged fact can quickly turn into another myth.
* Search for lists with well-known websites about information security.
* Help to debunk myths by carefully researching topics before sharing biased articles.
* Clearly define your goals when talking about information security. You can't implement everything, and there is no state of 100% security.
