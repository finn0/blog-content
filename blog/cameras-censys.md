+++
title = "Hi, I'm your insecure IP camera, broadcasting your life 24/7"
author = "Jakub"
date = "2018-05-12T11:48:19+02:00"
tags = [ "camera", "lan", "privacy", "wlan" ]
categories = [ "privacy" ]
ogdescription = "We show you several risks that come along with cheap IP cameras and provide tips how you can secure your device."
slug = "cameras-censys"
banner = "banners/cameras-censys"
notice = "true"
toc = true
syntax = true
+++

Do you own one of these cheap IP cameras from the local discount store, or did you buy one on Alza? Maybe you asked yourself why the camera is always looking around, autonomously moving, or even making noises? It is terrifying to see how many IP cameras are publicly accessible by everyone around the world. Its owners don't change default passwords, leave security configuration untouched, and seem happy that their camera broadcasts their family room around the clock.

In this article, we show you several risks that come with cheap IP cameras and tips on how you can secure your device.
<!--more-->
{{< rssbox >}}

{{< noticebox "note" "Please note: All pictures in this article show real publicly accessible pictures broadcasted by insecure IP cameras. We removed sensitive details and don't show any people or private areas." >}}

## They will find your device {#lookup}
There are 4.3 billion IPv4 addresses, so it's unlikely that "they" find your device, isn't it? It is **very likely** that your IP camera is already cataloged somewhere. Thousands of bots scan the internet around the clock for devices like insecure IP cameras. There are search engines that allow everybody to search for such devices.

Two well-known search engines are shodan.io and censys.io. Let's find cameras in the Czech Republic using censys.io. We only have to enter {{< kbd "location.country_code: CZ AND tags: camera" >}}:

{{< img "art-img/cameras-censys-ip-results.jpg" "censys.io shows 826 IP cameras in the Czech Republic." "IP addresses of 826 Czech cameras." >}}

censys.io shows 826 IP cameras in the Czech Republic. Since we know the IP address, it is easy to scan for additional information (e.g., using nmap).

censys.io is only one of many search engines or websites that provide initial information. Attackers don't have to guess your IP address for hours. They can use a search engine.

By the way, you can search for many other devices:

* Find Raspberry Pis in the Czech Republic: {{< kbd "location.country_code: CZ AND tags: raspberry pi" >}}
* Find NAS in the Czech Republic: {{< kbd "location.country_code: CZ AND tags: nas" >}}
* Find servers running Debian and OpenSSH on port 22: {{< kbd "22.ssh.v2.metadata.product: OpenSSH AND metadata.os: Debian" >}}
* …

Hopefully, you see that finding your devices is easy.

## They can pinpoint your home {#pinpoint}
Now you might think that attackers only have your IP address. IP addresses aren't sufficient to pinpoint your home, are they?

Attackers can access your camera even if they only know its IP address. They mostly need no additional information. Your camera itself discloses lots of information about your home (depending on your camera):

* Manufacturer and model of your camera
* Manufacturer and model of your router
* Internal IP addresses
* Your network topology
* Your timezone and NTP server
* The name of your video stream (e.g., "living room," "bathroom")
* Buildings around your home
* Your e-mail settings (including your e-mail address and e-mail password in some cases)
* Your WLAN settings, password, and SSID (e.g., "Sokolova999/1", "Pošta Brno")
* WLANs around your camera
* …

{{< img "art-img/cameras-censys-wireless-scan.jpg" "This camera discloses wireless LAN settings and shows WLANs near it." "WLAN information in camera settings." >}}

Attackers can combine this information to pinpoint your home. Pinpointing your home isn't only risky for your online devices but also for your "offline life":

* Attackers can use this information (especially live streams) to observe your daily routines
* Attackers can use their knowledge for [social engineering]({{< ref "/glossary.md#social-engineering" >}})
* Attackers can use this live information to commit burglary
* …

## They are in full control {#control}
Your camera not only discloses lots of information, but attackers can control it remotely. Attackers can:

* Rotate your camera to look around
* Start audio recording for eavesdropping
* Connect to your WLAN by using the password disclosed by the camera
* Hack other devices within your WLAN
* Disable recording at all
* Format your storage cards
* Delete log files to erase all traces
* …

Admittedly, that's pushing it a bit. Not every camera discloses everything by default or allows remote control.

{{< img "art-img/cameras-censys-cows.jpg" "Attackers can freely rotate this camera and see more than just cows." "cows as seen by a camera." >}}

## Secure your camera
After giving you a sermon, we want to provide some tips to secure your camera:

1. Don't buy cheap cameras that don't have any security features.
2. Change default usernames and passwords.
3. Enable HTTPS, if available.
4. Update your camera's firmware.
5. Disable port forwarding and UPnP.
6. Use WPA2-PSK-CCMP (sometimes called WPA2-AES) only.
7. Turn off your camera when you don't need it.
8. Regularly check your camera's log files
9. Use network segmentation, if available (e.g., by connecting your IP camera with your guest network only).
10. Regularly check its settings and change passwords.

Finally, always remember that no device on earth is or will ever be 100% secure.

## Summary
There is already an appropriate summary online:

{{< img "art-img/cameras-censys-hacked-camera.jpg" "This camera has a message for its owners." "a hacked live stream of a camera." >}}
