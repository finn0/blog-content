+++
title = "Home network security – Part 4: Turris Omnia as an ad blocker"
author = "Benjamin"
date = "2018-08-12T07:15:55+02:00"
tags = [ "turris-omnia", "router", "lan", "nas", "privacy", "ad-blocking", "malvertising", "tracking" ]
categories = [ "Home network security" ]
ogdescription = "This time, we will show you an easy way to use your Turris Omnia as an ad blocker in your home network."
slug = "hns4-adblocking"
banner = "banners/as-hns"
toc = true
syntax = true
+++

There seems to be an unspoken agreement when it comes to advertisements in the web browser: You can access web content for free and must see advertisements in return. This makes sense, however, advertisements are sometimes used for malicious purposes (malvertising). Furthermore, some companies use advertisements not only for making money, but for tracking. Using the Turris Omnia as an _network-level_ ad blocker is one of many possible solutions to block all these ads. We will show you a really easy way to configure Omnia's ad blocking capabilities.
<!--more-->
{{< rssbox >}}

## Requirements
For this part, we need:

* our Turris Omnia which is connected with our computer and the internet
* an SSH client on our computer

## Step by step to your network-level ad blocker {#sbs-guide}
This time, we install packages and configure them directly with LuCI. This makes things a lot easier since there is nearly no terminal involved.

We use Adblock here. It simply tells the [DNS resolver installed on the Omnia]({{< ref "/blog/hns5-dns-configuration.md" >}}) to not resolve certain domain names. If one of your devices in the network queries a blocked domain name, the Omnia will answer "NXDOMAIN" which basically means that this domain name doesn't exist. The result is that your device won't be able to connect to this domain name.

### Step 1: Download the packages {#s1}
First of all, connect to your Omnia using your web browser. Go to {{< kbd "https://192.168.1.1/cgi-bin/luci" >}} (change the IP address accordingly) and log in using the password for "advanced administration" like [configured in the first part]({{< ref "/blog/hns1-hello-world.md#config-to" >}}) of this series.

Go to System / Software and search for {{< kbd "adblock" >}} as shown in the picture below. Install "adblock" (the essential package) and "luci-app-adblock" to be able to use LuCI to configure Adblock later.

{{< webpimg "art-img/as-hns4-1package.png" "Get the Adblock packages using LuCI" "the software page of LuCI." >}}

### Step 2: Configure Adblock {#s2}
After installing, you have to enable and start Adblock. Go to System / Startup in LuCI and search for {{< kbd "adblock" >}}. Enable and start it by clicking the buttons.

Finally, we have to configure which block lists we want to use. There should be a new menu item "Services / Adblock" now. Select it and you will see the configuration screen as shown in the picture below.

{{< webpimg "art-img/as-hns4-2adblock.png" "Configure Adblock using LuCI" "the Adblock settings in LuCI." >}}

We don't have to change the defaults. Instead, scroll down and select block lists you want to use. Then, click "Save & Apply".

**Important:** One list is named "blacklist." This is your local custom [denylist]({{< relref "#denylist" >}}). If you want to use your own rules, don't disable it.

### Step 3: Add custom rules {#s3}
Maybe, you want to block more domains. This is possible by selecting the index "Advanced". There you can define your own lists:

* **Blacklist**: Define additional domains (one domain per line) which the Omnia must block. No wildcards are allowed. As an advanced user, you can directly edit the file "/etc/adblock/adblock.blacklist". All subdomains containing this domain will be also blocked.
* **Whitelist**: Define domains or subdomains which the Omnia must not block. For instance, you can allow a single subdomain here while you block all other subdomains. As an advanced user, you can directly edit the file "/etc/adblock/adblock.whitelist".

### Step 4: Restart Adblock and test it {#s4}
After enabling certain block lists and adding your custom rules, you have to restart Adblock. You can do so by restarting your Omnia, restarting the service using LuCI (System / Startup) or simply by using SSH: {{< kbd "/etc/init.d/adblock restart" >}}.

Go to Services / Adblock and back to "Advanced". There you can also query domain names. Enter a domain name which should be blocked. Blocked domain names appear below the text field.

Alternatively, you can enter a domain name on a device in this network. Simply enter: {{< kbd "nslookup [domain-name]" >}}. Successful blocking results in:

{{< samp "** server can't find [domain-name]: NXDOMAIN" >}}

{{< hnsbox >}}

## Summary
Using Adblock on your Turris Omnia (or router running OpenWrt) is very useful since all of these domain names will be blocked at network level. You don't have to configure and regularly update each and every device in this network. Keep in mind that some devices like smart TVs and IoT devices don't allow you to configure any host-based ad blocking.

Adblock also affects the [guest network]({{< ref "/blog/hns1-hello-world.md#to-guest-nw" >}}), of course, since this VLAN also uses your [Omnia as a DNS resolver]({{< ref "/blog/hns5-dns-configuration.md" >}}). Adblock enhances privacy and security in your network due to less tracking, malvertising and blocking of potential harmful domains.

However, as with all denylist-based filtering you must update your denylist from time to time and add new domain names.

## Sources
* {{< extlink "https://github.com/openwrt/packages/blob/master/net/adblock/files/README.md" "Adblock (OpenWrt)" >}}
