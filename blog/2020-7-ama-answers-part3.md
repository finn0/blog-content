+++
title = "Ask Us Anything (AMA): readers ask, we answer – Part 3"
author = "Benjamin"
date = "2020-07-25T10:17:31+02:00"
tags = [ "ama" ]
categories = [ "Ask Us Anything" ]
ogdescription = "We categorize and collect some questions by readers and our answers."
slug = "2020-7-ama-answers-part3"
banner = "banners/ish-ama"
toc = true
notice = true
+++

In July 2020, we hosted our first AMA (Ask Us Anything) event. In this article, we categorize and collect some questions and our answers.
<!--more-->
{{< rssbox >}}

Our first AMA event ended on July 23, 2020. Readers sent their questions via [e-mail]({{< ref "/contact-details.md" >}}) or in the Fediverse.

{{< noticebox "info" "We rephrased some questions to point to the crucial parts." >}}

## General questions about information security
### "You recommend Signal, but what about Jami or other P2P messengers? Aren't they more secure than server-based messengers?"
We used Jami when it was called Ring, and it didn't work reliably. There are some reports on forum.privacytools.io that this is still the case. We had the same experience with Tox. Therefore, we don't think that P2P messengers are viable for non-technical users who just want to chat.

Just for the record, most P2P messengers also rely on servers (DHT nodes). So, they aren't fully peer-to-peer in terms of "there are only clients involved."

And, of course, there are many different messengers and concepts. Every messenger and concept comes with pros and cons, and some of them are better for particular use cases. In contrast, others can't meet your requirements. See also ["What is 'secure'?"]({{< ref "/blog/discussion-secure.md#sm" >}})

### "Can you recommend free resources for awareness training?"
The EFF provides "free resources for digital security educators" at https://sec.eff.org. Our awareness activities are always hand-crafted, see ["Social engineering: The story of Jessika."]({{< ref "/blog/social-engineering-jessika.md" >}})

## Questions about web security
### "Does DNS-over-HTTPS replace the web browser add-on HTTPS Everywhere?"
No, DNS-over-HTTPS doesn't replace HTTPS Everywhere. DNS-over-HTTPS secures the DNS traffic between your web browser and the remote DNS resolver. It does not affect any other network traffic.

Contrary to this, HTTPS Everywhere tries to upgrade HTTP connections to HTTPS. If there is no HTTPS, your web browser loads the HTTP version of the website. However, HTTPS Everywhere doesn't affect DNS traffic.

### "Which is more secure: DNS-over-HTTPS or DNS-over-TLS?"
There is no binary answer here. We suggest reading our ["Beginner’s guide to DNS"]({{< ref "/blog/hns5-dns-configuration.md#dns-basics" >}}) that also explains DNSSEC and more.

### "How can I host my website anonymously?"
A solution that meets your requirements may be hosting an Onion Service v3 (aka prop224) to provide the website. In this case, the website is only available within the Tor network, making it hard to identify the server operator for regular users. You likely also need to choose an anonymous hosting provider and pay anonymously. Here, we can't provide any guidance.

Keep in mind that misconfiguration of the Onion Service or the server may leak information that helps to unveil your real identity. Besides, your writing style or posting habits may be sufficient to identify you.

### "Which blogging software do you recommend?"
In general, we recommend using [static site generators like Hugo]({{< ref "/blog/static-blogging.md" >}}) because the website itself requires less software on the server, which results in a reduced attack surface.

### "How can I stay up-to-date regarding recent developments to keep my servers and IT infrastructure secured?"
1. If available, subscribe to notification services to timely learn about new security vulnerabilities in the software you use on your server.
2. Subscribe to news feeds that cover security incidents in general.
3. Deploy monitoring software to monitor security-relevant events on your server (e.g., execution of sudo commands, logins/logouts, password changes).
4. Regularly recheck if the settings of your web server, SSH client, proxy server, and other server-side software are still up-to-date. There may be new or deprecated settings.

### "When I check the network traffic in my web browser, I see traffic to uriports.com or report-uri.com on some websites. Why?"
These are third-party providers for reporting directives, configured by the server. In the case of predefined errors, your web browser sends error reports to these providers.

Since the reports can contain personal data (your IP address, your user-agent), the third-party providers should be mentioned in the privacy policy of the website. Especially, Network Error Logging (NEL) reports can be misused to track users and are sent even if you never fully open the website: https://www.w3.org/TR/network-error-logging/#privacy-considerations.

You can also use [Webbkoll to detect these third-party providers]({{< ref "/news/2020-06-30-monthly-review.md#webbkoll-now-detects-reporting-directives" >}}); however, keep in mind that server admins can include or exclude particular pages or subdomains. Therefore, scanning a single webpage might not unveil the presence of reporting directives.

## Questions about mobile security
### "Can you write a privacy tutorial for LineageOS?"
We likely won't write a tutorial for this. Even if you switch to LineageOS, you still transmit data to third parties like Google. Besides, [switching the user operating system of your smartphone doesn't necessarily result in more security or privacy]({{< ref "/blog/2020-7-ama-answers-part1.md#is-my-lineageos-device-secure" >}}). Furthermore, [it isn't sufficient to analyze some network traffic]({{< ref "/blog/2020-7-ama-answers-part2.md#can-you-test-app-xyz-please-can-you-test-smartphone-abc-please" >}}) of the operating system or apps to rate security or privacy.

Just installing LineageOS and some F-Droid apps isn't enough to get a "privacy-friendly" smartphone or to "take back control."

### "Can you retest the /e/ ROM?"
We got a semi-official request from the developers of the /e/ ROM this time. Additionally, some readers ask us to retest the /e/ ROM. However, we won't do this.

As written before, testing hardware or software is time-consuming. We want to use our time for writing and improving content. "Testing" doesn't only include capturing some network traffic. A comprehensive app test includes static code analysis, fuzzing, server-side attacks, attacks on network protocols, binary analysis, etc. We can't conduct such extensive tests for free. (And many other bloggers won't provide this for free either.)

### "When you buy new hardware, do you look for long-term update support?"
Yes, update support for hardware is essential. Therefore, all of us primarily use hardware with long-term update support (including firmware like BIOS updates). If there are no updates anymore, we assess the risk and may replace the device.

## General questions about the InfoSec Handbook
### "Will you host another AMA event in the future?"
Yes. In the meantime, you can still send us questions regarding our content.

### "Can you write about your daily work in information security?"
Writing about our daily work is possible to a certain degree. We evaluate this.

### "How do you ensure that there is no conflict of interests when you recommend hardware or software?"
Our employers don't provide any B2C products or services. Therefore, we can't suggest anything that is sold by our employers. Regarding third parties, we do not accept any sponsoring (e.g., money, free product samples, invitations to events, articles).
