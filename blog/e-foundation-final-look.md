+++
title = "The state of the LineageOS-based /e/ ROM in December 2019"
author = "Benjamin"
date = "2019-12-25T11:00:02+01:00"
lastmod = "2020-08-09"
tags = [ "android", "rom", "lineageos", "e-foundation" ]
categories = [ "Privacy" ]
ogdescription = "/e/ promises a privacy-enabled smartphone OS–we look at it in this article."
slug = "e-foundation-final-look"
banner = "banners/e-foundation"
notice = true
toc = true
syntax = true
+++

More than four months ago, [we checked the LineageOS-based ROM]({{< ref "/blog/e-foundation-second-look.md" >}}) provided by the French /e/ Foundation for the second time.

In this article, we recheck our findings for the last time since Google dropped support for Android 7.1.2, the underlying Android version of the /e/ ROM for our device.
<!--more-->
{{< rssbox >}}

{{< noticebox "note" "Please note: Our articles on the /e/ ROM aren't comprehensive security tests or privacy checks of the /e/ ROM. Checking its security requires much more than only monitoring and analyzing some network traffic, and checking its privacy requires to consider legal aspects that we can't examine thoroughly." >}}

## Preparation
We again wipe our Moto G4. Then, we install a fresh copy of /e/ (e-0.7-n-2019121734423-dev-athene.zip) using {{< kbd "adb sideload" >}}. This version was released on December 17, 2019, and it is the current version for the Moto G4.

We choose the language (English USA), set the current date and time, and skip most other configuration (no WiFi, no SIM card, no PIN, no /e/ account). We only disable all location services.

After the initial configuration, we connect the Moto G4 with a dedicated VLAN to monitor its entire network traffic.

## Features
Before we analyze the network traffic, we look at features of /e/ [like last time]({{< ref "/blog/e-foundation-second-look.md#features" >}}).

### Patch level
The current /e/ ROM for the Moto G4 (build date December 17, 2019) is based on Android 7.1.2 like last time.

{{< noticebox "warning" "As of November 2019, Google does not support Android 7.1.2 anymore. Google does not provide any new security updates for this ROM." >}}

**Update (Dec 26, 2019)**: The Android security patch level is November 5, 2019, according to the settings. Literally minutes after we published this article, a newer version of the ROM became available. This version (December 25) reports patch level December 5, 2019. As mentioned above, Google dropped support for Android 7.1.2 and didn't release any security updates for Android 7 in November and December 2019. We don't know why the patch level is November/December in this case. It should show "October 2019."

A reader pointed to an ongoing discussion on community.e.foundation. There, the /e/ supports said: "Please note there has been a Security String bump on Github on the LineageOS sources. […] Patches have been backported from Oreo and Pie and the date updated as per the response given. Since /e/ forks the LineageOS code we are also showing similar patch dates. What this mean [sic] in exact terms for Nougat users is being checked by the development team."

Assuming that all security patches are [backported]({{< ref "/glossary.md#backporting" >}}), the Moto G4 is still not fully up-to-date since the firmware doesn't get any security updates. Of course, the vendor patch level depends on your device. Hopefully, the /e/ team will provide more transparency on security updates in their different ROM versions in the future.

### Preinstalled apps
Preinstalled apps are, among others:

* Apps (/e/ store) 1.1.6 ([read more about the Apps store below]({{< relref "#appstore" >}}))
* Bliss Launcher 1.3.0
* Chromium-based /e/ web browser 77.0.3865.104
* LibreOffice Viewer 6.1.0.0.alpha
* K-9 Mail-based mail client 5.600
* microG Services Core 0.2.10.19420
* MuPDF mini (PDF viewer) 1.13.0
* Notes 0.22.3
* Open Camera 1.47.2
* OpenKeychain (OpenPGP client) 5.4
* OpenTasks 1.2.3
* QKSMS (SMS messenger, branded as foundation.e.message) 3.7.4
* Weather client 4.4
* several apps included in LOS/Android (Clock, Contacts, etc.)

They removed Magic Earth (maps client) for unknown reasons. You can still install this app using the preinstalled Apps store.

## Security and privacy {#sap}
Since this article also isn't focused on features of /e/, we again look at security and privacy aspects. The /e/ Foundation promotes its same-named mobile OS as "ungoogled," coming with "carefully selected apps."

### Communication with the internet {#communication}
In this section, we discuss several findings regarding /e/'s network traffic when connected to the internet.

#### Connectivity check, now Google free {#google-connectivity-check}
[Last time]({{< ref "/blog/e-foundation-second-look.md#google" >}}) and during our initial test, we saw that /e/ connected to Google servers for its connectivity check. This time, they changed the connectivity check server:

{{< samp "GET / HTTP/1.1" >}}
{{< samp "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.82 Safari/537.36" >}}
{{< samp "Host: 204.ecloud.global" >}}
{{< samp "Connection: Keep-Alive" >}}
{{< samp "Accept-Encoding: gzip" >}}

Our G4 connects to "51.91.138.20" (204.ecloud.global), hosted by the French company OVH. The related [issue (#268)]({{< relref "#links" >}}) on /e/'s GitLab instance was closed in September 2019.

In summary, this is fixed.

#### Google is still in /e/'s web browser {#google-webbrowser}
Our initial findings regarding network traffic to Google are tracked by [issue #271]({{< relref "#links" >}}). Issue #271 is still open. The developers assume that the network traffic to Google servers is somehow connected to the /e/ web browser of the device.

We conducted two separate tests: We recorded all network traffic while opening all apps except the web browser. Then, we recorded the network traffic and only opened the web browser.

During the first test (without the web browser), we didn't record any network traffic to Google since the connectivity check is now done without Google, as mentioned above.

However, opening the web browser still results in Google traffic ("172.217.21.246," "216.58.208.35") and traffic to Microsoft ("13.107.21.200," likely related to bing.com).

In summary, this is still work in progress. /e/ seems to manually remove the sources of network traffic to Google in their web browser.

#### NTP synchronization around the world {#ntp}
This finding didn't change at all. [See our last report]({{< ref "/blog/e-foundation-second-look.md#ntp" >}}) and our [initial report]({{< ref "/blog/e-foundation-first-look.md#ntp" >}}).

The related [issue #272]({{< relref "#links" >}}) on /e/'s GitLab instance has new comments, for instance: "NTP servers are obiously [sic] catching IP adresses [sic], which is probably not a real privacy concern actually. But we can probably restrict the number of NTP servers used to main NTP server pools," written by /e/'s founder Duval.

In summary, this is still work in progress.

#### Weather app still leaks personal data in cleartext {#weather}
[Last time]({{< ref "/blog/e-foundation-second-look.md#weather" >}}), the preinstalled weather app (foundation.e.weather, version 4.4) still leaked our location in real-time. Each time, we searched for a location to get the current weather, the app sent a GET request to api.openweathermap.org in cleartext. One example was:

{{< samp "GET /data/2.5/find?q=lin&type=like&cnt=15&APPID=50[…]8 HTTP/1.1" >}}

If we used the device's GPS to get weather information, it leaked our current position.

We observed the same behavior this time. However, last time, the user-agent was "okhttp/3.2.0" while the version of the app was still 4.4.

This time, the user-agent is again "Dalvik/2.1.0 (Linux; U; Android 7.1.2; Moto G4 Build/NJH47F)," leaking device information. We initially observed this [during our first test]({{< ref "/blog/e-foundation-first-look.md#weather" >}}). During all three tests, the app's version was 4.4.

The related [issue #273]({{< relref "#links" >}}) on /e/'s GitLab instance stated: "The weather app will be improved, to use HTTPS and remove user-agent. […]." In the meantime, they closed this ticket and moved it to the issue tracker of the Weather app ([issue #15]({{< relref "#links" >}})). Issue #15 was also closed in September 2019. The last comment is: "We will remove the app, and replace it by another one later. Users can still download Good Weather from Apps."

In summary, this remains unfixed. The current Weather app only supports cleartext HTTP while leaking your device information and search queries.

#### Magic Earth–cleartext traffic disappeared, General Magic claims that there was never unencrypted traffic {#magicearth}
During our [initial test of Magic Earth]({{< ref "/blog/e-foundation-first-look.md#magicearth" >}}), we observed network traffic to 12 different IP addresses belonging to General Magic, the app's provider. Traffic to 10 IP addresses was in cleartext. [Last time]({{< ref "/blog/e-foundation-second-look.md#magicearth" >}}), we saw this behavior again.

Interestingly, Magic Earth is now removed from the default /e/ ROM. The related [issue #274]({{< relref "#links" >}}) on /e/'s GitLab instance is closed, quoting a reply of General Magic: "After checking with our IT Operations Specialist, I can tell you the information stated on the webpage is either outdated or incorrect. … All the connections from our application to our servers are fully protected, TLS encrypted." Based on this statement, /e/ closed the issue without rechecking these claims.

To recheck the app's behavior, we installed it using the preinstalled App store. The version is 7.1.19.51…, last time it was 7.1.19.20. The app connected to 8 different IP addresses owned by General Magic (via Telekom Romania). We did not observe unencrypted network traffic this time. We rechecked our pcap files of the second and first tests, and there was definitely cleartext traffic to most IP addresses belonging to General Magic.

In summary, General Magic obviously changed something while telling /e/ that our test "is either outdated or incorrect." The previous pcap files clearly show cleartext traffic. In our opinion, the fact that /e/ didn't recheck this but trusted in General Magic's claim is somewhat worrying, and it looks strange if you consider that they removed Magic Earth from the default /e/ image for our device. (There is an [issue #600]({{< relref "#links" >}}) about crashes of Magic Earth, and maybe this is the reason for the missing Magic Earth application in the first place.)

### The App store of /e/ {#appstore}
[Last time]({{< ref "/blog/e-foundation-second-look.md#appstore" >}}), there was a new app store installed by default (version 1.1.5). This time, "Apps" is still present (foundation.e.apps, version 1.1.6).

The app allows you to request new apps, search for apps, and also games, and you can download/install apps, of course. /e/ seemingly offers a broad mix of apps, contrary to F-Droid. So you also find apps that contain ads and trackers. Some people probably don't like this; however, there is a chance to get rid of third-party stores to access Google's infrastructure (like Aurora Store).

#### Version comparison
We again checked for version information of 11 random apps for comparison:

| Name          | /e/ version | Google Play version |
|---------------|-------------|---------------------|
| Conversations | 2.6.1+fcr   | 2.6.1+pcr           |
| Firefox Klar  | 8.0.15      | 8.0.23              |
| Keybase¹      | n/a         | 5.1.0               |
| Orfox²        | n/a         | 16.1.2-RC-2         |
| Protonmail    | 1.12.3      | 1.12.3              |
| Signal        | 4.51.6      | 4.52.4              |
| Steam Chat    | 0.9         | 0.9                 |
| Telegram      | 5.11.0      | device dependent    |
| Tutanota      | 3.66.5      | 3.66.2              |
| VLC           | 3.1.7       | device dependent    |
| WhatsApp      | 2.19.360    | device dependent    |

_¹Keybase is listed in Apps, but can't be installed due to an error. ²Orfox was removed from Apps._

Overall, the apps are mostly up-to-date this time.

#### cleanapk.com–the mysterious source of all apps {#cleanapk}
Like last time, using the Apps store results in traffic to/from "cleanapk.org." We still don't know who operates this domain.

There is now a website at info.cleanapk.org. It states: "Cleanapk.org's aim is to provide a clean, generic and up to date repository of free Android applications (open source and not open source) that are official packages from publishers. We do our best effort to provide pristine, unmodified Android applications packages (APKs)."

Moreover, it says: "Most of open source applications available through our API are mirrored from F-Droid. Packages are then analysed for integrity. Then we run Exodus privacy software analysis to collect the number of permission and trackers in each APK and deliver this information through our API."

In summary, the website doesn't state the source of all applications that don't originate from F-Droid. It only adds: "All the content of CleanAPK.org is either submitted by users, or available in various places on the Internet and supposed to be freely downloable [sic] and redistributable." Thus, the source of most applications remains a mystery.

>/e/'s Apps store uses cleanapk.com to get applications. However, nobody knows the operator of cleanapk.com, and their privacy policy doesn't comply with current GDPR requirements. Moreover, some users raised legal concerns regarding the applications that seem to originate from F-Droid and APKPure.

The concise privacy policy of cleanapk.com claims: "Our API is not tracking you: we are not using any cookie and IP adresses [sic] are just kept in service logs. They are rotated regularly and not used."

An /e/ user asked on community.e.foundation whether cleanapk.org is GDPR compliant. The /e/ support replied: "Please use the details on their site to contact them for this information.
We have tried to answer questions about Apps in the FAQ 4 there are no further clarifications that are available from our side." The FAQ of /e/ says: "Apps available in the /e/ Installer are open source apps that you can find on Fdroid, and free Android apps, same as you can find on Google Play Store. The /e/ app installer relies on a third-party application repository called cleanapk.org." So there is no additional information here. In another discussion on community.e.foundation, some users assume that one of cleanapk's sources is APKPure, another third-party provider of Android applications.

Overall, we think that the operators of cleanapk.com don't want to be identified. The privacy policy doesn't meet the requirements of the European GDPR (see ["GDPR: How to identify incomplete privacy policies?"]({{< ref "/blog/guide-privacy-policy.md" >}})), and the origin of most applications is unknown. Obviously, /e/ only says that cleanapk.com isn't connected to them.

## Summary
Compared with [our second report]({{< ref "/blog/e-foundation-second-look.md" >}}), network traffic to Google servers seems to be reduced to the web browser of /e/. The cleartext traffic of Magic Earth disappeared. The Weather app still leaks all traffic, including device information.

The new Apps store is a huge mystery, though. Last time, we already stated that we weren't able to find information about the operator of cleanapk.com. Nobody seemingly knows the operator of cleanapk.com. Cleanapk's privacy policy doesn't comply with current GDPR requirements. Moreover, some users raised legal concerns. Due to this, we don't trust this mysterious source for applications.

All in all, /e/ improved their ROM since our first test about nine months ago. There is still network traffic to Google; however, this has been reduced to a minimum. Unfortunately, /e/ never replied to any of our direct messages via e-mail or Mastodon during testing, and they never tried to contact us. Due to this, there was no coordinated issue handling possible. Even more concerning is the mysterious Apps store, preinstalled on /e/ devices, and the reaction of the /e/ support when somebody asks about cleanapk.com.

## Will we test the /e/ ROM again? (August 2020)
Months after we published this article, somebody related to the /e/ Foundation finally contacted us and asked for another follow-up test. While we would like to write about our findings' current status, we are also well aware that our tests aren't comprehensive security tests or privacy checks of the /e/ ROM.

As we don't have the resources to check the /e/ ROM in more detail (and this isn't the focus of our website), we suggested that the /e/ Foundation tasks a company specialized in testing mobile security with thoroughly checking their ROM. There should also be a team member dedicated to security, even if the target group of their ROM is the "average mom or dad."

## External links {#links}
* {{< extlink "https://gitlab.e.foundation/e/management/issues/268" "GitLab issue #268 'Infosec Handbook Review Issues : “.... /e/ .. using Google for connectivity check:”'" >}}
* {{< extlink "https://gitlab.e.foundation/e/management/issues/271" "GitLab issue #271 'Infosec Handbook Review Issues : “...TLS 1.2-encrypted traffic from/to www.google.com.... via IPv4/IPv6.”'" >}}
* {{< extlink "https://gitlab.e.foundation/e/management/issues/272" "GitLab issue #272 'Infosec Handbook Review Issues : “NTP synchronization leaks IP Address to many different domain names”'" >}}
* {{< extlink "https://gitlab.e.foundation/e/management/issues/273" "GitLab issue #273 'Infosec Handbook Review Issues : “... Weather app leaks IP address and other details...in cleartext.”'" >}}
* {{< extlink "https://gitlab.e.foundation/e/apps/Weather/issues/15" "GitLab issue #15 'Infosec Handbook Review Issues : “... Weather app leaks IP address and other details...in cleartext.”'" >}}
* {{< extlink "https://gitlab.e.foundation/e/management/issues/274" "GitLab issue #274 'Infosec Handbook Review Issues : “ Info is sent/received to/from General Magic in cleartext.”'" >}}
* {{< extlink "https://gitlab.e.foundation/e/management/issues/600" "GitLab issue #600 'Magic Earth error after 20191126 update'" >}}

## Changelog
* Aug 9, 2020: Added statement on follow-up tests.
* Dec 26, 2019: Added information regarding the patch level of the ROM. Added more section headings. Added links.
