+++
title = "YubiKey 4C vs. Nitrokey Pro: Stalemate"
author = "Benjamin"
date = "2018-04-11T19:27:36+02:00"
lastmod = "2019-12-22"
tags = [ "2fa", "e-mail", "openpgp", "password", "yubikey", "nitrokey" ]
categories = [ "authentication" ]
ogdescription = "YubiKey and Nitrokey can both bolster up your security but there are some differences."
slug = "yubikey4c-nitrokeypro"
banner = "banners/yubikey4c-nitrokeypro"
toc = true
+++

When it comes to [authentication]({{< ref "/glossary.md#authentication" >}}), most private individuals still rely on passwords or sometimes [biometrics]({{< ref "/glossary.md#biometrics" >}}). Attackers can easily access your accounts if they overcome this single hurdle.

This article focuses on two quite similar hardware products which can be used to add additional protection to your accounts: YubiKey 4C and Nitrokey Pro. We also talk about their successors (YubiKey 5 and Nitrokey Pro 2).
<!--more-->
{{< rssbox >}}

## Reasons for buying dedicated security tokens
The main reason for buying dedicated security tokens is being protected against [security vulnerabilities]({{< ref "/glossary.md#vulnerability" >}}) in multi-purpose apps and operating systems. Of course, security tokens also run software, but there is a lower risk of compromise if you use dedicated hardware tokens with special software. Some secrets on security tokens are read-only or even inaccessible for operating systems, so these secrets can't be compromised. Furthermore, most security tokens aren't connected to any network.

### 2FA with security tokens
[Two-factor authentication (2FA)]({{< ref "/glossary.md#2fa" >}}) (or multi-factor authentication (MFA)) tries to solve the problem of weak/reused passwords and problematic biometrics: Multiple factors are combined, so if an attacker compromises one factor, the other factor still protects your account. For more information on these factors, read our post ["Modern credential management: security tokens, password managers, and a simple spreadsheet"]({{< ref "/blog/modern-credential-management.md#basics" >}}).

On the internet, there are three major 2FA standards for private individuals as of this writing:

* [OATH-TOTP]({{< ref "/glossary.md#oath-totp" >}}): The Time-based One-Time Password algorithm was defined in 2011. It is based on a shared secret key that is stored by you and the online service. Since the online service has to store this secret, OATH-TOTP is less secure than U2F. TOTP is easy to implement though, thus it is widely supported by websites. Both the YubiKey 4C and the Nitrokey Pro support storing OATH-TOTP secrets. For TOTP, both tokens need an additional app since they need to know the current time for this algorithm.
* [U2F]({{< ref "/glossary.md#u2f" >}}): Universal 2nd Factor is generally the predecessor of WebAuthn (see below). It is based on [public-key cryptography]({{< ref "/glossary.md#public-key-cryptography" >}}). Due to this, the online service doesn't learn any secrets. Therefore, it is more secure than OATH-TOTP. Only the YubiKey 4C supports U2F. We explain U2F in detail in our post ["Yubico Security Key vs. Nitrokey FIDO U2F"]({{< ref "/blog/yubico-security-key-nitrokey-u2f.md#u2f-function" >}}).
* [WebAuthn]({{< ref "/glossary.md#webauthn" >}}): This is a new web standard that defines authentication based on public-key cryptography. Neither the YubiKey 4C nor the Nitrokey Pro support this, so we won't discuss WebAuthn in this post.

OATH-TOTP is widespread and easy to implement. As a result, there are many (free) apps for Android and iOS that support OATH-TOTP. Moreover, some password management tools like KeePass or KeePassXC support OATH-TOTP. On the other hand, storing your TOTP secrets in apps or password managers relies on the security of your operating system. Many older Android-based smartphones contain publicly-known security vulnerabilities. Even if you have an up-to-date LineageOS installed, it is likely that there are hardware components in your phone that have unpatched vulnerabilities in their firmware. Apps on your phone can be outdated and introduce additional vulnerabilities.

Furthermore, most phones are always connected to mobile networks and the internet. People use their phones for online shopping, browsing, messaging and so on. Needless to say that the risk of being compromised is higher in comparison with hardware that is only used for certain purposes.

Since U2F/WebAuthn is currently less common, we won't discuss the benefits and drawbacks of using dedicated security tokens here. In general, the same reasons apply.

Finally, we look at the support for the above-mentioned 2FA standards:

| 2FA standard | YubiKey 4C         | Nitrokey Pro      |
|--------------|--------------------|-------------------|
| OATH-TOTP    | up to 28 accounts  | up to 16 accounts |
| U2F          | unlimited accounts | not supported     |
| WebAuthn     | not supported      | not supported     |

And a comparison with their successors:

| 2FA standard | YubiKey 5C                      | Nitrokey Pro 2    |
|--------------|---------------------------------|-------------------|
| OATH-TOTP    | up to 32 accounts               | up to 15 accounts |
| U2F          | unlimited accounts              | not supported     |
| WebAuthn     | like U2F/up to 25 resident keys | not supported     |

### OpenPGP with security tokens
Some security tokens not only support 2FA, but they can also store [OpenPGP]({{< ref "/glossary.md#openpgp" >}}) keys. The YubiKey 4C and the Nitrokey Pro support generating and storing one OpenPGP key (that technically uses 3 subkeys).

Again, there are apps that allow you to store OpenPGP keys on your smartphone or laptop. However, YubiKeys and Nitrokeys aren't just normal USB flash drives. Both are implementations of the OpenPGP card standard. This means that cryptographic tasks like encryption, decryption, signing and authentication are performed within the token. The OpenPGP keys never leave these tokens, making it impossible for attackers to copy your private key. Normal USB flash drives or apps don't offer this protection.

In general, the support for OpenPGP key types and sizes is the same for the YubiKey and Nitrokey:

| OpenPGP key  | YubiKey 4C    | Nitrokey Pro  |
|--------------|---------------|---------------|
| RSA 2048 bit | supported     | supported     |
| RSA 4096 bit | supported     | supported     |
| Curve25519   | not supported | not supported |

## YubiKey and Nitrokey in comparison {#details}
YubiKeys as well as Nitrokeys are supported by most common operating systems. Both security tokens are almost equally expensive. They allow you to securely store your OpenPGP keys and can be used for two-factor authentication.

While all YubiKeys support [U2F (Universal 2nd Factor)]({{< ref "/glossary.md#u2f" >}}), only the [Nitrokey FIDO U2F]({{< ref "/blog/yubico-security-key-nitrokey-u2f.md#nitrokey-fido-u2f" >}}) and the newer [Nitrokey FIDO2]({{< ref "/blog/yubico-security-key-nitrokey-fido2.md" >}})
 support this authentication standard. Due to this, you need an additional Nitrokey if you want to use U2F or the newer WebAuthn. This is more expensive than using a single YubiKey, and managing the tokens becomes more complicated. The latter especially carries weight if you use a second security token as a backup. Then, you already need 4 Nitrokeys instead of 2 YubiKeys.

### YubiKey 4C
YubiKeys are produced by Yubico which was founded in 2007 and is based in the USA and Sweden.

Noticeable features of the YubiKey 4C are:

* support for static passwords (max. 64 characters)
* up to 28 [OATH-TOTP]({{< ref "/glossary.md#oath-totp" >}}) accounts (using the Yubico Authenticator or Yubikey Manager)
* generation and storage of [OpenPGP]({{< ref "/glossary.md#openpgp" >}}) keys (up to 4096 bits RSA)
* unlimited [U2F]({{< ref "/glossary.md#u2f" >}}) accounts
* 2 configurable slots:
  * Yubico [OTP]({{< ref "/glossary.md#otp" >}}) via YubicoCloud (uses 1 slot, configured for Yubico OTP at delivery)
  * [challenge-response authentication]({{< ref "/glossary.md#challenge-response-authentication" >}}) (uses 1 slot)

For some cryptographic actions, you can configure that one must press the physical button of the Yubikey to authorize the action for additional protection.

The official [Yubikey Manager]({{< ref "/terminal-tips.md#ykman" >}}) can be used to enable or disable certain features of a YubiKey or limit features to certain interfaces (USB/NFC). It can also be used to reset your U2F accounts.

While YubiKeys were partially open source in the past, Yubico decided to migrate to closed-source firmware. This doesn't mean that its firmware is less secure as explained in our post on [software security myths]({{< ref "/blog/myths-software-security.md#m1" >}}). Most software applications for YubiKeys are still open source, though. For security reasons, firmware of YubiKeys can't be modified. Due to this, firmware updates aren't possible.

### Nitrokey Pro
Nitrokeys are produced by Nitrokey UG which was founded in 2015 and is based in Germany. The people behind Nitrokey actually developed a predecessor, called Crypto Stick (2008–2010).

Noticeable features of the Nitrokey Pro are:

* up to 16 [OATH-TOTP]({{< ref "/glossary.md#oath-totp" >}}) accounts
* generation and storage of [OpenPGP]({{< ref "/glossary.md#openpgp" >}}) keys (up to 4096 bits RSA) or S/MIME (_We didn't test S/MIME since private users use OpenPGP if at all. Furthermore, Nitrokey officially states that problems occur when you want to use OpenPGP and S/MIME simultaneously._)
* password storage for up to 16 accounts (using the Nitrokey App)

The password storage offers only 16 slots. You can label each entry (max. 11 characters), store a username for each entry (max. 32 characters) and passwords can consist of up to 20 characters. Please note that some special characters are counted twice or more. For instance, the German characters ü or ß are counted twice. This means that some special characters effectively reduce the strength of passwords by forcing you to use less characters. Moreover, the 20 character limit makes it impossible to store long passphrases.

Nitrokeys consist of open hardware and open source software. If you want to spend more money, there are also three different models of Nitrokey Storage available. They offer secure storage (16 GB, 32 GB or 64 GB) and upgradeable firmware in addition to the features of Nitrokey Pro. We didn't test those models, though.

## Benefits
Both security tokens offer the following benefits:

* OpenPGP keys are securely stored on your security token. Moreover, it is easier to use OpenPGP because you only have to enter a short PIN instead of a long password when you want to use the stored key. The PIN is used authorize the cryptographic operation.
* OATH-TOTP secrets are securely stored on your security token.
* You can use your keys and access your passwords on the go. Especially your OpenPGP keys are protected, so malicious devices can't copy them.
* Both tokens are tamper-resistant. This means that you can't manipulate the hardware without destroying it.
* There are many websites and other use cases allowing you to use both tokens for two-factor authentication.
* Both tokens aren't connected to the internet and only serve special purposes.

## Problems
Of course, there are some problems. Some of them are typical for certain implementations/standards:

* In case of loss or destruction, you lose your stored keys and passwords. This means that you need backups and backups must be at least as secure as the primary device. We think that a second Nitrokey/YubiKey is inevitable. This is also widely recommended.
* Attackers can access several features of your Nitrokey/YubiKey without authentication when they have physical access to them. Keep this in mind and don't lose your Nitrokey/YubiKey!
* Some security tokens allow you to import an OpenPGP key that was generated on your normal computer. This is less secure than directly generating it on the hardware token since your OpenPGP key could be already compromised before. Therefore, directly generate your OpenPGP key on the security token instead of importing it.
* OATH-TOTP is useless if an attacker knows the secret used to generate the one-time password. Actually, this isn't a weakness of the implementations but of the algorithms. Configure OATH-TOTP only on trustworthy devices!
* OATH-TOTP requires a time source. However, synchronized clocks in security tokens require an energy source. Therefore, you must install additional software to use TOTP since neither YubiKeys nor Nitrokeys contain batteries.
* Sometimes, U2F secrets are generated by the manufacturer of the security token, then stored on the device and can't be replaced afterward. In theory, manufacturers could copy your secret U2F key that is used to derive U2F secrets.
* As mentioned above, YubiKeys don't allow firmware updates due to security reasons. In the past, it was necessary to replace YubiKeys with new ones after vulnerabilities were discovered (CVE-2015-3298, CVE-2017-15361). The replacement was for free.
* The firmware of the Nitrokey Pro can't be updated, too. We don't know if there is a free replacement program in case of security vulnerabilities.
* The Windows 10 update of September 2018 rendered the Nitrokey App useless. Due to this, Windows 10 users weren't able to access stored passwords and TOTP codes for about 10 months. As of October 2019, the Nitrokey App 1.4 (released in June 2019) runs again on Windows 10 1908.

## Upgrade to the next generation? {#ng}
In late summer of 2018, Nitrokey UG and Yubico announced their newest generation of security tokens. If you already own a YubiKey 4 or a Nitrokey Pro, you might ask yourself whether you should invest money for an upgrade.

### Upgrade to Nitrokey Pro 2? No. {#nitrokey-pro2}
The Nitrokey Pro 2 supports ECC-based OpenPGP keys. That's the main difference compared with its predecessor, the Nitrokey Pro. However, [Curve25519]({{< ref "/glossary.md#curve25519" >}}) (the current future default of [GnuPG]({{< ref "/glossary.md#gnupg" >}})) isn't supported. For us, there is no reason to switch from Nitrokey Pro to Nitrokey Pro 2.

### Upgrade to YubiKey 5C? It depends. {#yubikey-5}
The YubiKey 5 series supports [FIDO2]({{< ref "/glossary.md#fido2" >}}) and the new [WebAuthn]({{< ref "/glossary.md#webauthn" >}}) standard. Both technologies aren't widely adopted. U2F may be sufficient until 2020/2021. Besides, there are more variants of the YubiKey than before (USB-A with NFC, USB-A Nano, USB-C, USB-C Nano, USB-C/Lightning connector; and some upcoming models announced by Yubico).

With the release of the YubiKey 5Ci in August 2019, Yubico introduced their firmware 5.2.3 with support for Curve25519 and key attestation. If you want to use Curve25519 and/or WebAuthn, then you should upgrade to the YubiKey 5 series. If you don't need these standards, stay with your YubiKey 4.

## Conclusion
Both security tokens are quite similar except U2F support (YubiKey 4C) and their firmware model (closed source vs. open source). Therefore, our recommendation depends on your use case:

* If you only need a secure storage for your OpenPGP key, buy either a YubiKey or Nitrokey.
* If you hate closed-source software or if you need a hardware-based password storage (with the above-mentioned limitations), buy a Nitrokey.
* If you need U2F support or if you need to store lots of OATH-TOTP secrets, buy a YubiKey.

After using multiple YubiKey and Nitrokey models for several years, we—personally—prefer YubiKeys.

First of all, they are smaller in size, so they can be easily part of your physical keychain. Then, YubiKeys support more security standards like U2F. In case of Nitrokeys, you need multiple Nitrokeys to get the same features. This is more expensive, and multiple physical tokens are harder to manage (esp. keeping several Nitrokeys on your keychain). The same is true if you need to store lots of OATH-TOTP secrets. YubiKeys can store nearly twice as many TOTP secrets (4 series) or more than twice as many (5 series). Besides, Yubico is a well established company with good support and fast updates in case of issues. Contrary to this, we experienced slow or no support from Nitrokey (as mentioned above).

We are well aware of people seeing closed-source firmware on YubiKeys as bad. Both companies argue convincingly that their philosophy is better. However, this is our experience and our conclusion.

For more information on modern authentication, read our article ["Modern credential management: security tokens, password managers, and a simple spreadsheet"]({{< ref "/blog/modern-credential-management.md" >}}).

## Sources
* {{< extlink "https://www.yubico.com/" "Yubico" >}}
* {{< extlink "https://www.yubico.com/2016/05/secure-hardware-vs-open-source/" "Yubico – Secure Hardware vs. Open Source" >}}
* {{< extlink "https://www.nitrokey.com/" "Nitrokey" >}}
* {{< extlink "https://github.com/Nitrokey/wiki/wiki/Ideas" "Nitrokey – Development ideas" >}}
* {{< extlink "https://twofactorauth.org/" "Information about favorite websites and their 2FA support" >}}
* {{< extlink "https://www.dongleauth.info/" "List of websites and their OTP/U2F support" >}}

## Changelog
* Dec 22, 2019: Added information about Nitrokey FIDO2. See also ["Yubico Security Key vs. Nitrokey FIDO2"]({{< ref "/blog/yubico-security-key-nitrokey-fido2.md" >}}). Rewrote the beginning to clarify threat models and to cut some content that is already covered by other in-depth articles. Added some tables to make the article more readable. Updated our recommendation based on our experiences. Removed OATH-HOTP as it irrelevant for most private users.
* Nov 3, 2018: Updated information about Nitrokey FIDO U2F.
* Oct 31, 2018: Reordered information and added information about U2F, FIDO2. Added information about the fifth generation of YubiKeys and the second generation of Nitrokeys. Included feedback of the CEO of Nitrokey. Added information about U2F problems, TOTP's time source and Windows 10 patch (0918). Clarified conclusions.
