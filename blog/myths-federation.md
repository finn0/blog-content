+++
title = "Federation myths"
author = "Benjamin"
date = "2018-08-14T09:53:02+02:00"
lastmod = "2019-07-10"
tags = [ "open-source", "federation", "mastodon", "xmpp" ]
categories = [ "myths" ]
ogdescription = "Federation is good and there are many use cases for it. However, it isn't a magic bullet."
slug = "federation-myths"
banner = "banners/federation-myths"
toc = true
+++

Federated services are popular among people who care about their online privacy. However, several myths are rattling around that you read over and over again. We exemplarily discuss Mastodon and XMPP in this article.
<!--more-->
{{< rssbox >}}

## Myth 1: Federated services aren't user-friendly {#m1}
People telling you this myth may have tested federated services and decided that federation is just bad: outdated software, no regular updates, many bugs, hard to use, bad user experience. We think **it depends**.

There is an excellent example of federated services with usability in mind: **Mastodon**. There are regular updates, and it's easy to use. More and more people like and use it every day. It looks modern, it feels modern, and you don't need additional software when using your web browser. For instance, to register for Mastodon, you can choose any server (instance), enter your credentials, and sign up. That's it.

However, there is also the other side of the coin: **XMPP messaging**. Wait? XMPP? You may have just read that using XMPP is as easy as using e-mail! (By the way, this statement is too sketchy and another myth!)

What's wrong here? While some messaging services like WhatsApp use customized XMPP and are very popular, many leisure projects emerged on GitHub and other repository hosting platforms over the years. Sometimes these projects consist of half-baked and buggy code with no updates for months. The biggest problem in this example is that XMPP itself is somewhat standardized, but dozens of so-called XEPs introduce new features. Most messengers support basic XMPP, but the implementation of XEPs is arbitrary. Missing support for certain XEPs leads to a handful of messengers left if you want to use some features. This subset of messengers may not contain any messengers which run on your cellphone's operating system.

Finding an appropriate messenger for your operating system that supports all features you want to use requires research and time. But that's not all. You have to find a server! Yes, some websites list XMPP servers. Yes, some clients even suggest servers. Yes, there are blogs around telling you which server you should use. But there are these XEPs again! Servers also have to support XEPs. You not only have to look for the "right" client but for the "right" server, too. (And many of these XEPs are "experimental proposals" or "drafts," not standards!) Strong advocates of the XMPP community then come up with dozens of guides to tell you how you have to use XMPP. Finally, you found a server, registered, and the next update of the server or client makes chatting impossible for days due to protocol issues.

You can't call this "user-friendly," and this is only the tip of the iceberg. No one wants to read dozens of articles, guides, and learn about network protocols only to be able to send a message. You can spend hours configuring your XMPP setup only to realize that feature XYZ is not yet implemented. (XMPP has more downsides that we don't discuss in this article.)

In sum, some federated services are as user-friendly as any other non-federated services, but some are a PITA.

## Myth 2: Federation is a privacy feature {#m2}
We read this over and over again: Federation automatically means privacy! Why? You may well ask since there is no categorical answer. Federated services use ordinary servers, standard network protocols, common software. There is no unique privacy feature implemented.

_Federation_ means that different clients, servers, and networks understand each other. On the other hand, _decentralization_ means that there is no central authority controlling all servers in a (federated) network.

Does this mean privacy? **We don't think so.**

First of all, most federated services like Mastodon and XMPP require registration to use it. You have to provide more or less [personal data]({{< ref "/glossary.md#personal-data" >}}): your username, your e-mail address, your IP address. Then, this data is stored somewhere on the internet and mostly in cleartext.

Secondly, there is [metadata]({{< ref "/glossary.md#metadata" >}}):

* Timestamps of your logins
* Timestamps of certain actions like sending or reading messages
* The number of messages sent
* The size of data sent

Federation doesn't mean that there is more or less metadata in comparison with non-federated services.

Thirdly, few companies either host many servers or provide internet access for users of the federated services. These companies can see who connects to which server, how often, and so on. Using federated services doesn't mean that your traffic is invisible. We showed this for more than 1000 XMPP servers in July 2019: Only seven companies in three countries controlled more than 50% of XMPP servers. Besides, German companies hosted 50% of XMPP servers, US-American companies hosted 10%, and French server providers hosted 7%. This clearly shows the problem of the physical centralization of federated services.

Fourthly, there is the server. Who controls this server? Who can access your data stored on this server? Does the admin comply with current data protection laws? Did the admin harden the server's configuration, and is the server's software kept up-to-date? You likely don't know, and using [online assessment tools]({{< ref "/blog/online-assessment-tools.md" >}}) to check this covers only a small fraction of security measures.

Let's return to our exemplary services: Mastodon and XMPP. Servers store most of your data in cleartext. Unencrypted storage is normal and doesn't differ from non-federated services. Mastodon requires a valid e-mail address and some XMPP servers also need you to provide one.

The **big difference** here is that you don't have to trust a global player like Google or Facebook but smaller companies, friendly societies, or individuals.

In sum, federation shifts trust from one institution to another. Federation is about trusting other parties. You hope that admins don't access or sell your data. You hope that they don't merge their federated instance with global players anytime soon.

## Myth 3: Federation is more important than encryption {#m3}
Most people who spread myth 2 ("Federation is a privacy feature") also spread this myth. They argue, while encryption isn't that significant, using federated and decentralized services is crucial.

This is another sketchy statement. Let's differentiate between:

* Transport encryption (encrypting traffic between your client and your server)
* Server-side encryption at rest (encrypting your data on the server if not in use)
* End-to-end encryption (encrypting traffic between your client and the client your dialog partner)

We think you agree that end-to-end encryption is the supreme discipline here. End-to-end encryption based on state-of-the-art cryptographic algorithms ensures that you don't have to trust any server between you and your dialog partner. Enforced end-to-end encryption supporting [forward secrecy]({{< ref "/glossary.md#forward-secrecy" >}}) (FS) ensures that nobody can access encrypted data anytime soon as long as the endpoints aren't compromised.

Transport encryption only ensures partial security, even if modern encryption and FS are in use. A compromised server puts your data at risk. Server-side encryption reduces this risk but doesn't eliminate it, and you can hardly check whether server-side encryption is permanently enabled or supported at all. There may be unencrypted backups or configuration issues that effectively disable server-side encryption.

Does federation on its own provide this level of security? No. Federation is about the interoperability of protocols, clients, and networks, not about security like mentioned before. This myth is **wrong**. Yes, there are federated services without use cases for end-to-end encryption, but transport encryption is important nearly everywhere.

## Myth 4: Oh, you don't trust your admin? Run your own server! {#m4}
This is half a myth and mostly stated to bid defiance: Federation advocates tell you that federation is a privacy feature, you insist on the opposite due to [reasons mentioned above]({{< relref "#m2" >}}), and then they tell you: "Oh, you don't trust your admin? Run your own server!"

Running your own server isn't a magic bullet, either. You not only have to rent a server, but you also have to install server software, harden your server, implement security measures, regularly update its software, and keep it secure. This implies that you understand web server security, check log files, and monitor your server permanently.

However, your own server doesn't guarantee that your data stays on your server when using federation. For instance, some Mastodon instances mirror messages of other instances. Moreover, people can share your content, of course. When using XMPP, some of your account data is shared with other servers to be able to chat. Furthermore, your server provider most likely logs your server usage and can track who connects to your server.

In summary, you get a little more control over your data and metadata, but you have to maintain your own server and pay for it.

## Summary
You read these myths over and over again. Discussions turn into matters of faith almost every time. You read that federation implies less tracking, more control, and everything is so easy.

In our opinion, the most significant advantage for users of federated services is higher [availability]({{< ref "/glossary.md#availability" >}}) if using several decentralized servers. In this context, "availability" doesn't necessarily mean that everybody can use this federated service all the time, but it is much harder to shut down every server of the whole network at the same time. This likely implies censorship resistance.

On the other hand, federation neither automatically means more security nor privacy.

## External links {#links}
* {{< extlink "https://codeberg.org/infosechandbook/scripts/raw/branch/master/other/xmpp-server-hosters.txt" "List of hosters of 1000 XMPP servers (codeberg.org)" >}}

## Changelog
* Jul 10, 2019: Added information about 1000 XMPP servers that are physically centralized.
