+++
title = "KeePassXC for beginners – setup and basic usage"
author = "Benjamin"
date = "2020-07-16T20:26:07+02:00"
tags = [ "keepassxc", "password", "yubikey" ]
categories = [ "tutorial" ]
ogdescription = "This tutorial shows the setup and basic usage of KeePassXC."
slug = "keepassxc-password-management-basics"
banner = "banners/modern-credential-management"
toc = true
notice = true
syntax = true
+++

Weak passwords, reused passwords, or passwords based on "magic formulas" weaken your online security significantly. We suggest password management software to generate strong passwords, store passwords securely, and keep track of passwords.

In this tutorial for beginners, we set up and show a typical use case of KeePassXC, an open-source password manager.
<!--more-->
{{< rssbox >}}

## Requirements
The following steps are required before creating your first database:

1. Download KeePassXC and install it on your operating system. For this tutorial, we use **KeePassXC 2.6.0**. If you install another version of KeePassXC, the setup and usage might differ.
2. Start KeePassXC.
3. If your operating system manages the KeePassXC package, select "No" when asked, "Would you like KeePassXC to check for updates on startup?" If you update it manually, or you are unsure, select "Yes."

## Create your first database
After starting KeePassXC, click "Create new database." Set the database name and a description, as shown in the picture below. Click "Continue."

{{< webpimg "art-img/keepassxc-basics-3.png" "Set a database name and a description." "the 'general database information' dialogue in KeePassXC." >}}

### Keep or change the encryption settings
After setting a name and description, you can change the encryption settings. We recommend staying with the defaults if there are no particular reasons to change them.

* You can set the "Decryption Time" to up to 5 seconds. This value slows down the decryption of your locked database to make [brute-force attacks]({{< ref "/glossary.md#brute-force-attack" >}}) time-consuming.
* We recommend using "KDBX 4.0" (default) as the database format.

If you want to change the encryption algorithm, [key derivation function]({{< ref "/glossary.md#kdf" >}}), or other advanced settings, click "Advanced Settings," as shown in the picture below:

{{< webpimg "art-img/keepassxc-basics-4.png" "You can either keep the default encryption settings or click 'Advanced Settings.'" "the 'encryption settings' dialogue in KeePassXC." >}}

The "Advanced Settings" allow you to choose:

* Encryption Algorithm: [AES]({{< ref "/glossary.md#aes" >}}) 256-bit (default), Twofish 256-bit, ChaCha20 256-bit.
* Key Derivation Function: [Argon2]({{< ref "/glossary.md#argon2" >}}) (KDBX 4 – recommended; default), AES-KDF (KDBX 4), AES-KDF (KDBX 3.1).
* Transformation rounds: 10 (default).
* Memory Usage: 64 MiB (default).
* Parallelism: 2 threads (default).

**We recommend AES-256 and Argon2.** The remaining parameters (transformation rounds, memory usage, parallelism) can be increased to slow down your database's decryption time.

The defaults are shown in the image below:

{{< webpimg "art-img/keepassxc-basics-5.png" "We recommend AES-256 and Argon2. You can increase the number of transformation rounds to slow down brute-force attacks." "the advanced 'encryption settings' dialogue in KeePassXC." >}}

After setting the "Encryption settings," click "Continue."

### Set a password or passphrase
Finally, you have to set a password or passphrase that is used to protect all database entries. We recommend that you click the small _dice_ icon (🎲) in the password field to generate a random passphrase.

Clicking the _dice_ icon opens the "Generate Password" window, as shown in the picture below:

{{< webpimg "art-img/keepassxc-basics-9.png" "You can generate a random password to protect your database." "the 'generate password' dialogue in KeePassXC." >}}

Alternatively, you can select "Passphrase." **We recommend setting a 9-word passphrase** since it is easier to avoid spelling mistakes when unlocking your database. You can write down the passphrase and store it offline.

The following screenshot shows the default setting (7-word passphrase):

{{< webpimg "art-img/keepassxc-basics-10.png" "Alternatively, you can generate a random passphrase." "the 'generate passphrase' dialogue in KeePassXC." >}}

After setting your password or passphrase, click "Done." If you want to add a key file or YubiKey, see [Next steps]({{< relref "#next-steps" >}}).

## Check the settings
You created your first database. Now, we recommend checking the default settings of KeePassXC. Go to "Tools" → "Settings."

We recommend selecting "Backup database file before saving," as shown in the next picture. Enable or disable the remaining settings as required.

{{< webpimg "art-img/keepassxc-basics-18.png" "In the application settings, we recommend enabling 'Backup database file before saving'." "the 'basic settings' dialogue in KeePassXC." >}}

The security settings allow you to customize timeouts and lock events:

{{< webpimg "art-img/keepassxc-basics-19.png" "In the application settings, you can set timeouts and events that automatically lock your database." "the 'security settings' dialogue in KeePassXC." >}}

## Create your first entry
After creating the database itself, you can add your first entry. You should see an empty "Root" folder, as shown below:

{{< webpimg "art-img/keepassxc-basics-12.png" "After opening your new database, you should see a 'Root' folder." "the unlocked database in KeePassXC." >}}

Click "Entries" → "New Entry." Alternatively, press {{< kbd "CRTL" >}} + {{< kbd "N" >}}, or click the "plus" icon.

Enter the title of the account, your username for the account, your password, and the URL. If you didn't set a password for the account before, click the "dice" icon to generate a random password or passphrase again.

Moreover, you can set a date when your password expires. There are some presets for the expiration (click "Presets").

If you click the "download" icon (to the right of the URL field), KeePassXC downloads the favicon of the URL. Then, the favicon is shown in front of the password entry in your database. The favicon makes it easier to find the account you search visually.

An example entry is shown below:

{{< webpimg "art-img/keepassxc-basics-13.png" "For each entry, we recommend setting a title, username, password, and URL." "the 'add entry' dialogue in KeePassXC." >}}

Additionally, you can set attributes or add attachments. These features can be helpful in some use cases:

{{< webpimg "art-img/keepassxc-basics-15.png" "You can add additional attributes to each entry." "the 'additional attributes' dialogue in KeePassXC." >}}

Instead of downloading the favicon, you can select default icons or manually add icons, as shown below:

{{< webpimg "art-img/keepassxc-basics-14.png" "You can set predefined or custom icons per entry." "the 'select icon' dialogue in KeePassXC." >}}

Done. You created your first database and your first entry. Now, you can add more entries, migrate to strong passwords, and fully discover all settings of KeePassXC.

Tip: Use the "Auto-Type" feature that automatically types your username and password into a form. On Linux, select the form, switch to KeePassXC, select the correct entry and press {{< kbd "SHIFT" >}} + {{< kbd "CTRL" >}} + {{< kbd "V" >}}.

{{< webpimg "art-img/keepassxc-basics-16.png" "After creating your first entry, you should see it and its details." "the unlocked database in KeePassXC with a single entry." >}}

## Next steps
Customize KeePassXC for your use cases. You can read our article ["Modern credential management"]({{< ref "/blog/modern-credential-management.md" >}}) for advanced credential management.

Besides, you can add a key file or YubiKey for additional protection. Finally, have a look at the statistics, as explained below:

### Add a key file or YubiKey/OnlyKey
KeePassXC supports adding a key file or a YubiKey for additional protection. Understand the pros and cons of both options.

{{< noticebox "warning" "KeePassXC and KeePass 2 implement the support for key files and YubiKey differently. The differing implementation means that you can't open a kdbx file created with KeePassXC in KeePass 2, and vice versa (assuming that you added a key file or YubiKey)." >}}

#### Key file
A key file can be any file. However, you should use a file that contains random bytes. For instance, on some Linux systems, you can enter the following command to create a key file, containing about 10MB of random bytes: {{< kbd "dd if=/dev/urandom of=keyfile bs=1M count=10" >}}.

* Some benefits: It is free (no cost). You can easily back up your key file. It is beginner-friendly.
* Some drawbacks: Malware or an attacker can easily copy your key file. You can accidentally modify or remove the key file.

#### YubiKey or OnlyKey
A YubiKey is a physical security token (see our [YubiKey articles](/tags/yubikey/)). **We recommend setting up two YubiKeys (primary + backup).**

* Some benefits: It drastically improves the encryption key. It can't be accidentally modified.
* Some drawbacks: It isn't free. You need a backup in case you lose your YubiKey. Setting it up may be hard for beginners.

If you want to set up a YubiKey, [read our tutorial on setting up the challenge-response mode]({{< ref "/blog/keepassxc-yubikey-challenge-response.md" >}}).

### Statistics
KeePassXC gives an insight into your passwords, as shown in the picture below. Check these statistics regularly to improve the strength of your passwords:

{{< webpimg "art-img/keepassxc-basics-17.png" "KeePassXC gives an insight into your passwords." "the 'statistics' dialogue in KeePassXC." >}}

## Summary
_Is KeePassXC the perfect and most secure password manager?_ No, it isn't. For instance, attackers can steal your passwords by installing malware on your system, even if you set a 50-digit password and use a YubiKey. Furthermore, other password managers are more appropriate for specific use cases.

Our message is: Use a password manager of your choice. KeePassXC and KeePass 2 generate strong passwords for you and store them securely. Moreover, they help you to use your passwords every day.

## External links {#links}
* {{< extlink "https://keepassxc.org/" "KeePassXC: Homepage" >}}
* {{< extlink "https://keepassxc.org/docs/KeePassXC_GettingStarted.html" "KeePassXC: Getting Started Guide" >}}
* {{< extlink "https://keepass.info/" "KeePass Password Safe 2" >}} (the original KeePass)
