+++
title = "Home network security – Part 5: Client-side DNS security features"
author = "Benjamin"
date = "2019-01-05T20:20:00+01:00"
tags = [ "turris-omnia", "router", "lan", "privacy", "dns", "dnssec", "dot", "doh", "kresd" ]
categories = [ "Home network security" ]
ogdescription = "The Turris Omnia allows you to use DNS-over-TLS and comes with DNSSEC, securing your DNS traffic."
slug = "hns5-dns-configuration"
banner = "banners/as-hns"
toc = true
syntax = true
+++

The Turris Omnia allows you to use DNS-over-TLS and comes with DNSSEC. If configured correctly, your DNS traffic between the local Turris Omnia and your remote DNS server is encrypted and authenticated.

In this article, we discuss client-side DNS security features and show how you can configure your Turris Omnia (or other routers) to use these features.
<!--more-->
{{< rssbox >}}

## Requirements
For this part, we need:

* our Turris Omnia which is connected with our computer and the internet (or a local DNS resolver with support for DNS-over-TLS and DNSSEC)
* an SSH client on our computer
* a remote and trusted DNS server that supports DNS-over-TLS as well as DNSSEC
* optionally: Wireshark on a local computer to test the setup

## Beginner's guide to DNS {#dns-basics}
There are many different DNS guides on the internet. In the following, we only give a basic overview on DNS and modern security features:

### What's DNS? {#dns-definition}
Some people describe the Domain Name System (DNS) as "the phone book of the internet". This comparison shows the basic idea of DNS: Internet users know domain names like "infosec-handbook.eu" while computers know IP addresses like "85.235.66.222". In summary, DNS allows computers to translate human-friendly domain names into computer-friendly IP addresses.

DNS achieves this by using a complex system of recursive and authoritative DNS servers. Querying for DNS information looks basically like:

1. Local client: Is DNS response in local cache (e.g., operating system, web browser)? No → ask recursive DNS server
2. Recursive DNS server: Is DNS response in local cache and still valid? No → recursively go through the authoritative DNS hierarchy
3. Authoritative DNS servers: each authoritative DNS server returns information for the recursive DNS server to get the right DNS response
4. Recursive DNS server: After getting the response, it is cached and sent to the local client
5. Local client: Receives the DNS response, and may cache it

Normally, your devices never directly talk with authoritative DNS servers but only recursive DNS servers. Recursive DNS servers are provided by your internet service provider (ISP), public WiFi provider, or other parties. In the following, we always mean a public, recursive DNS server when talking about remote DNS servers.

### What about DNS security and privacy? {#dns-security-privacy}
In November 2018, the original DNS turned 35. Being 35 years old means that DNS is one of the early internet protocols. Most protocols developed decades ago don't implement any security or privacy features, and are clearly focused on functionality.

By default, DNS traffic is unencrypted and unauthenticated. Third parties (like your ISP, attackers, or state actors) are able to analyze your DNS traffic. Unencrypted DNS traffic exposes all domain names entered on the devices of your home network. In August 2018, researches published the paper "[Who Is Answering My Queries: Understanding and Characterizing Interception of the DNS Resolution Path]({{< relref "#links" >}})" showing that DNS interception happens not only in theory but 8.5% of inspected AS (autonomous systems) exhibit DNS interception. Moreover, they report that nearly 28% of DNS packets sent from China to Google's public DNS servers were intercepted.

Furthermore, the researchers showed that all DNS servers used for interception ran outdated DNS server software.

Sometimes, third parties are even able to modify DNS traffic to redirect you to malicious websites. For instance, you enter "name-of-your-bank.eu" in your web browser. Your computer asks your DNS resolver for the corresponding IP address. Let's assume that "100.100.100.100" is the actual IP address of your bank. Instead of "100.100.100.100", you computer gets "200.200.200.200". Of course, your web browser navigates to "200.200.200.200"—the IP address of an attacker-controlled server. On the other hand, third parties cannot only redirect you but block name resolution for specific domain names.

Recent developments to cope with these security and privacy issues are DNSSEC and DNS-over-TLS/DNS-over-HTTPS.

### What's DNSSEC? {#dnssec-definition}
DNSSEC stands for "Domain Name System Security Extensions" and is a set of standardized specifications for securing certain kinds of DNS information. DNSSEC actually uses multiple DNS resource records to provide [digitally signed]({{< ref "/glossary.md#digital-signature" >}}) resource records. The DNS resolver installed on your Turris Omnia can verify these signatures which provides [authenticity]({{< ref "/glossary.md#authenticity" >}}) and [integrity]({{< ref "/glossary.md#integrity" >}}).

However, you can't just turn on client-side DNSSEC. In order to work, DNSSEC must also be configured and supported server-side. Additionally, there is a complex signing and trust system with multiple hierarchy levels. In the end, you must rely on people that they configured DNSSEC properly and protect their signing keys.

Furthermore, enabled DNSSEC doesn't prevent third parties from analyzing your complete DNS traffic. As mentioned above, DNSSEC only provides authenticity and integrity. There is no [confidentiality]({{< ref "/glossary.md#confidentiality" >}}). Spoofing attempts of third parties will fail, however, they can still analyze your DNS traffic.

### What's DNS-over-TLS/DNS-over-HTTPS? {#dot-doh-definition}
DNS-over-TLS and DNS-over-HTTPS are both standardized and look similar, however, they incorporate different ideas to provide confidentiality by encrypting DNS traffic.

#### DNS-over-TLS {#dot-definition}
DNS-over-TLS ([RFC 7858]({{< relref "#links" >}})) uses the dedicated TCP port 853 while common DNS traffic uses UDP port 53. It is slower due to the TCP overhead but it benefits from TLS 1.3 since TLS 1.3 is faster than TLS 1.2.

On the other hand, third parties can easily identify DoT traffic due to the specific port number. In some cases, all ports but 80 and 443 may be blocked (for instance, if you use public WiFi) which effectively renders DoT useless.

DNS-over-TLS is supported by PowerDNS, Knot Resolver, BIND, Unbound and Stubby, and there are several public DNS servers with support for DoT and DNSSEC. Android 9 (Android Pie) also supports DoT.

#### DNS-over-HTTPS {#doh-definition}
DNS-over-HTTPS ([RFC 8484]({{< relref "#links" >}})) uses HTTPS and HTTP/2 to transfer DNS traffic. Due to this, HTTPS port 443 is used. Third parties can't differentiate between "normal" HTTPS traffic like website contents and DNS traffic. So, it's hard to censor DoH traffic. Another benefit is that web servers could provide DNS responses by themselves, providing decentralized DNS resolving.

Using HTTPS allows applications to handle domain name resolving while "normal" DNS traffic is handled by the operating system. DoH is supported by Firefox, several server implementations, and specific apps like Cloudflare's DoH app for Android/iOS.

#### What's better?
Some people tell you that DoT is better since it uses a dedicated port and is widely supported. On the other hand, DoH is only a "hack" and not a real protocol.

Other people tell you that DoT is bad since it uses a dedicated port and can be easily blocked. According to them, DoT is also insecure and its traffic could be easily intercepted.

You probably see that there is no simple answer. It solely depends on whom you ask.

### What about DNSCurve, DNSCrypt, and DNS over QUIC? {#dnscrypt-dnscurve-dnsoverquic}
There are more projects to secure DNS traffic. DNSCurve, DNSCrypt, and DNS over QUIC are also known but not standardized at the moment:

* DNSCurve: DNSCurve is one of the first attempts to secure DNS, designed by cryptologist Daniel J. Bernstein. It incorporates several cryptographic primitives that were designed by Bernstein like Curve25519, Salsa20, and Poly1305. However, it isn't widely supported or deployed.
* DNSCrypt: DNSCrypt is based on DNSCurve. However, it was never proposed to the Internet Engineering Task Force (IETF) to become standardized. It uses UDP or TCP, and port 443. Several public DNS servers support DNSCrypt. It doesn't rely on CA-signed certificates but on trusting public signing keys of the DNS server.
* DNS over QUIC: QUIC is a new network protocol at transport layer, designed by Google. It will become HTTP/3 in future. There is an expired draft "Specification of DNS over Dedicated QUIC Connections" that describes the use of QUIC to transport DNS traffic. QUIC offers security and privacy for DNS traffic similar to DNS-over-TLS while being faster.

We won't go into detail about the three protocols mentioned above.

## Step by step to encrypted and authenticated DNS traffic {#sbs-guide}
Let's proceed to the practical part: encrypted and authenticated DNS traffic for your home network.

The Turris Omnia runs Knot Resolver (_kresd_) by default. Knot Resolver is a full, caching DNS resolver, developed by CZ.NIC. CZ.NIC also develops the Turris Omnia and other routers. _kresd_ supports DNSSEC as well as DNS-over-TLS.

The idea is to tell _kresd_ on the Turris Omnia (or your home router) to use DNSSEC, and DNS-over-TLS. Using your home router as a local DNS resolver and cache provides authenticated DNS responses for all devices that are connected to your home network. DNS-over-TLS will be used between your home router and the remote DNS server as shown in the picture below.

{{< webpimg "art-img/as-hns5-dns-traffic.png" "After configuring, the DNS traffic between our Turris Omnia and the remote DNS server is encrypted and authenticated. Local DNS traffic between clients and the Turris Omnia is authenticated but unencrypted." "the network layout with DoT enabled." >}}

Keep in mind:

* Only DNS traffic between your router and the remote DNS server is encrypted while local DNS traffic remains unencrypted
* DNS traffic between the remote, recursive DNS server and any authoritative DNS server can't be influenced by private individuals
* We assume that all devices in your home network are trustworthy
* We assume that you selected a trustworthy, remote DNS server with support for DoT and DNSSEC

### Step 1: Enabling DNSSEC {#s1}
The good news: DNSSEC is enabled by default. If you didn't turn it off, you can proceed to configure DoT.

You can test if your client gets authenticated DNS responses. Open your terminal and enter {{< kbd "dig infosec-handbook.eu +multiline +dnssec" >}}. The output looks like:

{{< samp "; <<>> DiG 9.23.2-ISH <<>> infosec-handbook.eu +multiline +dnssec" >}}
{{< samp "…" >}}
{{< samp ";; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 63943" >}}
{{< samp ";; flags: qr rd ra ad; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1" >}}

Check the response for "status: NOERROR" and "flags: qr rd ra **ad**". "ad" means "authentic data".

Another example is {{< kbd "dig orf.at +multiline +dnssec" >}}. orf.at isn't signed, so you won't see an "ad" flag. This response isn't authenticated.

### Step 2: Understanding DNS forwarding {#s2}
This isn't all about DNSSEC. It's important to understand which party actually validates signed DNS resource records:

* Turning "DNS forwarding" off enables DNSSEC validation on your Turris Omnia. DNSSEC validation is done by _kresd_ locally.
* Turning "DNS forwarding" on disables DNSSEC validation on your Turris Omnia. Then, DNSSEC validation is done by the remote DNS server.

However, DNS-over-TLS requires you to enable DNS forwarding. This means that your local _kresd_ and all local clients will still get authenticated responses (due to TLS) but won't validate DNS responses.

### Step 3: Enabling DNS-over-TLS {#s3}
Turris OS 3.11 (released in December 2018) ships a new and easy way to turn on DNS-over-TLS. It is directly accessible [via the web GUI now]({{< relref "#s3a" >}}). If you don't run Turris OS 3.11 or don't want to use the web GUI, you can [use SSH]({{< relref "#s3b" >}}) to connect to your Turris Omnia.

#### Step 3a: Enabling DNS-over-TLS using the web GUI {#s3a}
Open your web browser and enter {{< kbd "https://192.168.1.1/foris/config/main/dns/" >}} (change the IP address accordingly).

Check the checkbox "Use forwarding". There should be a "select" menu now. The menu shows the following DNS servers:

* Use provider's DNS resolver (your ISP, unencrypted)
* CZ.NIC (TLS)
* Cloudflare (TLS)
* Google (unencrypted)
* Quad9 (TLS)

Google started to support DoT, however, the configuration file on the Omnia isn't updated for Google's DoT support. You either have to wait for an update or you can create your own custom configuration file.

CZ.NIC, Cloudflare and Quad9 are preconfigured DNS servers with support for DNS-over-TLS and DNSSEC validation. Select one of them and click on "Save changes". Then, the web GUI shows "Configuration was successfully saved." Finally, [you can check your setup]({{< relref "#s4" >}}).

#### Step 3b: Enabling DNS-over-TLS using the terminal {#s3b}
Alternatively, you can directly change the resolver's configuration. Connect to your Turris Omnia using SSH: {{< kbd "ssh root@[turris-router-ip]" >}}.

Open the configuration file of the resolver: {{< kbd "vi /etc/config/resolver" >}}. The first section is "config resolver 'common'".

* Change "option forward_upstream '**0**'" to "option forward_upstream '**1**'" (enabling DNS forwarding, turning off local DNSSEC validation)
* Add "option forward_custom '**00_odvr-cznic**'" (if you want to use the CZ.NIC DNS server)

Other options are:

* 99_google (unencrypted)
* 99_cloudflare (DNS-over-TLS)
* 99_quad9 (DNS-over-TLS)

Save and quit vi. Finally, restart the resolver: {{< kbd "/etc/init.d/resolver restart" >}}. If the Turris Omnia runs [adblock as presented in part 4]({{< ref "/blog/hns4-adblocking.md" >}}), you must probably restart it, too: {{< kbd "/etc/init.d/adblock restart" >}}.

Before Turris OS 3.11, configuring DNS-over-TLS is harder but possible if your router runs _kresd_ ≥ 2.0.0. Go to [DNS tricks for Omnia and knot-resolver]({{< relref "#links" >}}) to see how to configure older versions of _kresd_.

### Optional: Add a custom DNS server {#so}
If you don't want to use one of the preconfigured DNS servers, you can add your own configuration.

On your Turris Omnia, go to {{< kbd "cd /etc/resolver/dns_servers" >}}. This folder contains above-mentioned configuration files. Let's have a closer look at "00_odvr-cznic". Enter {{< kbd "cat 00_odvr-cznic.conf" >}}. The output of "00_odvr-cznic.conf" is:

```bash
name="00_odvr-cznic.conf"
description="CZ.NIC (TLS)"
enable_tls="1"
port="853"
ipv4="217.31.204.133"
ipv6="2001:1488:800:400::2:133"
ca_file="/etc/resolver/CZ.NIC2-cacert.pem"
hostname="odvr.nic.cz"
```

You can simply copy the file and change all values accordingly. The website "[DNS Privacy Test Servers]({{< relref "#links" >}})" lists more DNS servers like UncensoredDNS, securedns.eu, or ns1.dnsprivacy.at.

Back up your custom configuration file and never change one of the default configuration files. Updates may overwrite your changes.

### Step 4: Check if DNS-over-TLS is in use {#s4}
Finally, we want to check if DNS-over-TLS is actually in use. Since all clients in our local network send their DNS queries to the Turris Omnia, we can't just start Wireshark or tcpdump on a client to verify this. We only see local unencrypted DNS traffic between a client and our Turris Omnia.

However, we can do the following to monitor all traffic between our Turris Omnia and the local ISP router:

* On your computer, open a terminal and enter: {{< kbd "mkfifo /tmp/pcap_file" >}}.
* After that, enter {{< kbd "ssh root@[turris-router-ip] \"tcpdump -n -w - -i eth1 '(port 53) or (port 853)'\" > /tmp/pcap_file" >}}.
* Open a second terminal window on your computer, and enter: {{< kbd "wireshark -k -i /tmp/pcap_file" >}}.

Wireshark should be started now. Wireshark will show all packets between your Turris Omnia and the router of the ISP that use port 53 (unencrypted DNS) or port 853 (DNS-over-TLS). This is also the view of third parties.

**If configured correctly, you only see TCP and TLSv1.2/TLSv1.3 packets between your Turris Omnia and the remote DNS server. All traffic goes to remote port 853.** If configured incorrectly, you will see unencrypted DNS queries and responses. Traffic goes to remote port 53.

You can also run the "Extended test" on dnsleaktest.com to see which DNS servers are used by your home network.

After testing, remove the named pipe: {{< kbd "rm /tmp/pcap_file" >}}.

{{< hnsbox >}}

## Summary
Enabling DNS-over-TLS and DNSSEC is quite easy after updating to Turris OS 3.11. Advanced users can define their own configuration files to use other DNS servers. Please note that DNS forwarding turns off local DNSSEC validation as explained above. Furthermore, local DNS traffic remains unencrypted.

After turning on both features, third parties can't analyze your DNS traffic anymore (as long as the remote DNS server isn't controlled by these parties). However, they can still see how often your home network queries for DNS information. They can also log IP addresses when your devices connect to remote servers. Moreover, some servers rely on "Server Name Indication" (SNI) to host multiple encrypted websites on a single IP address. SNI exposes domain names since it is unencrypted. Currently, there is a draft called "Encrypted Server Name Indication for TLS 1.3" that could fix this in future. _(infosec-handbook doesn't use SNI!)_

So, even if everything is encrypted, third parties can monitor your traffic. However, it becomes harder. Of course, the remote DNS server can be misused to log all of your queries. This is always the case.

## External links {#links}
* {{< extlink "https://doc.turris.cz/doc/en/public/dns_knot_misc" "DNS tricks for Omnia and knot-resolver" >}}
* {{< extlink "https://internet.nl/" "Internet.nl—allows you to test your internet connection and whether DNSSEC is enabled" >}}
* {{< extlink "https://tools.ietf.org/html/rfc7626" "RFC 7626: DNS Privacy Considerations" >}}
* {{< extlink "https://tools.ietf.org/html/rfc7858" "RFC 7858: Specification for DNS over Transport Layer Security (TLS)" >}}
* {{< extlink "https://tools.ietf.org/html/rfc8484" "RFC 8484: DNS Queries over HTTPS (DoH)" >}}
* {{< extlink "https://dnsprivacy.org/wiki/display/DP/DNS+Privacy+Public+Resolvers" "DNS Privacy Public Resolvers" >}}
* {{< extlink "https://dnsprivacy.org/wiki/display/DP/DNS+Privacy+Test+Servers" "DNS Privacy Test Servers" >}}
* {{< extlink "https://dnsprivacy.org/wiki/" "DNS Privacy Project Homepage" >}}
* {{< extlink "https://www.usenix.org/system/files/conference/usenixsecurity18/sec18-liu_0.pdf" "Who Is Answering My Queries: Understanding and Characterizing Interception of the DNS Resolution Path (PDF file)" >}}
