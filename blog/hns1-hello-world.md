+++
title = "Home network security – Part 1: Hello World"
author = "Benjamin"
date = "2018-05-11T11:52:30+02:00"
lastmod = "2020-06-09"
tags = [ "turris-omnia", "router", "lan", "wlan", "wpa2", "wpa3" ]
categories = [ "Home network security" ]
ogdescription = "We talk about several network basics, install our Turris Omnia and show the basic configuration."
slug = "hns1-hello-world"
banner = "banners/as-hns"
toc = true
syntax = true
+++

In this series, we show ways to secure your network at home. In the first part, we introduce the Czech open-source router Turris Omnia. Besides, we talk about network basics, install the Turris Omnia, and improve its basic configuration.
<!--more-->
{{< rssbox >}}

## Network basics
First of all, we want to talk about some network basics briefly. You can skip this section if you know IP addresses, MAC addresses, and routers. While this isn't a full guide for all aspects of networks, we want to talk about the essential basics. There are [useful resources]({{< ref "/recommendations.md#hn" >}}) that explain network basics in detail.

A computer network consists of two or more devices that are connected. The physical or logical connection of two or more devices allows the devices to communicate using _network protocols_ on different _layers_. A common network protocol used by IT devices is HTTPS (Hypertext Transfer Protocol Secure). You very likely used HTTPS to navigate to this article. However, there are not only protocols that are used for communicating with the internet. Some network protocols are only used in a local area network. In this series, we mostly refer to your local area network at home as the "home network."

In most cases, your home network uses a single _router_ to allow devices to connect to the internet. Some of your devices use wireless connections; others use a wired connection. Your router is typically connected with your _internet service provider_ (ISP). Your home network may look like the network in the following picture:

{{< webpimg "art-img/as-hns1-basicnetwork.png" "Basic network configuration (star topology). Dashed lines are wireless connections. The dotted line to the internet is the connection to your ISP. Solid lines are wired connections." "the star topology of a basic home network." >}}

Every device in a network needs an _IP address_ (IP means Internet Protocol). In your home network, your devices commonly get dynamically-assigned IP addresses from your router. These IP addresses are only used within your home network. Your router gets its public IP address from your ISP. As soon as you use a device on your home network to communicate with the internet, your router relays so-called _packages_ from your device to the internet and vice versa.

Besides, every device has a unique _MAC address_ (MAC means media access control). The MAC address is the physical address of a device while the IP address is its logical address. The MAC address is also needed for communication but only within a local network. Since you usually never change the MAC address (the manufacturer of the device presets it), we don't go into detail here.

We look closer at different terms:

### Router
The router is a very versatile device within your network and provides several services like local distribution of IP addresses, firewalls, or VPN functionality (a virtual private network). Your router is a single point of failure in terms of the availability of your internet connection and your network security.

If your ISP provides your router, the router is likely limited in its functionality. You should check whether you can do the following:

* Regularly update the firmware of your router. Keep in mind that cheap routers may never get any security updates.
* Change default passwords of your router, especially any admin or root password.
* Disable functionality that you don't need, especially WPS, UPnP, and NAT-PMP.
* Restrict access to the management interface of your router to wired connections only.
* Check whether your router supports HTTPS-only and SSH for connections for its management interface.

Instead of using the router provided by your ISP, you can also buy dedicated hardware, and install an open-source operating system for routers. Popular Linux-based distributions are OpenWrt and IPFire. Other distributions are pfSense (based on FreeBSD), and OPNsense (based on HardenedBSD/FreeBSD).

Setting up your custom router from scratch costs time and money, and you need to know a lot about networks and Linux. Due to this, we use the [Turris Omnia]({{< relref "#install-to" >}}) in this series. In our opinion, the Turris Omnia is a great compromise.

### LAN
In this series, we always mean "any devices connected to your router at home using cables" when talking about a LAN (local area network).

When it comes to choosing Ethernet cables for your home network, use Cat 5e or Cat 6 cables. The best variant depends on your use case:

* If you only lay one or two cables side by side, you can either use "U/UTP" or "U/FTP."
* If you lay dozens of cables closely to each other, use "S/UTP," "F/FTP," or "SF/FTP."

The difference between these variants is their shielding that is needed to reduce electromagnetic interference.

For physical security, several companies sell locks that you "plug" into the unused network ports of a device. These locks aim to prevent attackers from connecting their devices with the unused network ports. Keep in mind that attackers can easily remove already-attached network cables to use these ports. A more secure alternative is a server cabinet with a lock.

### WLAN
In this series, we always mean "any devices connected to your router at home using [Wi-Fi]({{< ref "/glossary.md#wi-fi" >}})" when discussing a WLAN (wireless local area network).

Compared to wired connections, wireless connections are frequently exposed. People want robust wireless connections, so most routers use high transmission power by default. High transmission power likely means that your WLAN is exposed far beyond your home's or apartment's physical borders. Due to this, you need to protect your WLAN.

#### WLAN protection mode
In general, there are two sets of protection modes: personal (SAE/PSK) and enterprise (EAP). The personal modes rely on a pre-shared key and are typically used for home networks. The enterprise modes rely on an additional authentication server and are more appropriate for corporate networks.

For strong protection of your wireless network at home, use WPA3-SAE. WPA3-SAE is also called WPA3-Personal. It uses SAE (Simultaneous Authentication of Equals) for key agreement and password-based authentication. SAE replaces the Pre-Shared Key method that you may know from WPA2-PSK.

If you use WPA3-SAE and a device fails to connect to the WLAN, switch to WPA2-PSK-CCMP. Ensure that you use "CCMP" (Counter-Mode/CBC-MAC Protocol) for WPA2. Some routers may call this mode WPA2-AES. Do not set any mode that relies on TKIP.

There is also a mixed mode that provides WPA3-Personal and WPA2-Personal at the same time (WPA3-Personal Transition Mode). Keep in mind that the WPA3-Personal Transition Mode only offers the security of WPA2-PSK since it is vulnerable to [downgrade attacks]({{< ref "/glossary.md#downgrade-attack" >}}).

Other protection modes are considered legacy and may be insecure. Such legacy modes are all TKIP-based modes (including WPA2-PSK-TKIP), all WPA-based modes, and WEP. TKIP (Temporal Key Integrity Protocol) uses the insecure RC4 algorithm for encryption. TKIP was an interim solution to replace WEP.

#### WLAN password
We recommend that you set a long password for your wireless network, mainly when you use WPA2-PSK-CCMP.

For WPA3-SAE and WPA2-PSK-CCMP, you can set a password up to 63 characters. You can generate a QR code that contains your WLAN settings instead of entering the password on all of your devices manually. The Turris Omnia creates such QR codes for you.

#### Additional settings
Depending on your router, you can enable additional security features for your WLAN, or disable features that you don't need. Since we can't cover everything in this article, we recommend that you carefully read any available documentation of your router.

In general, we recommend the following:

* Reduce the transmission power of your router, if possible. Reducing the transmission power limits the range of your WLAN.
* If your router offers different Wi-Fi protocols, use Wi-Fi 5 (IEEE 802.11ac) on 5 GHz. Keep in mind that some old devices might only support 2.4 GHz networks. In rare cases, your router might already support Wi-Fi 6 (802.11ax).

### Basic network security {#bns}
We recommend that you document all devices that you connect with your home network. Your documentation should answer the following questions:

* Which devices are connected to your router? (e.g., name, IP address, MAC address, owner)
* Did you replace all default passwords of the devices with individual passwords?
* Did you check the network settings of every device and disable unused features?
* Which services are provided by these devices? (e.g., a local XMPP server)
* Do these devices need to connect to the internet or only to local devices?
* Did you change your router's configuration for some reason?

You should regularly revise your document. At first glance, documentation may look superfluous; however, the documentation introduces structure and forces you to think about your current setup.

## Unpacking and installing the Turris Omnia {#install-to}
As mentioned above, we continue the series with a Turris Omnia. The Omnia is a crowdfunded router and manufactured by CZ.NIC, a Czech interest association. The Omnia's hardware is powerful enough to be used as a standalone home server. The Omnia runs a customized version of OpenWrt, called Turris OS. As of June 2020, the current version of Turris OS is 5.x, based on OpenWrt 19.07.

In January 2018, we bought the most expensive version (RTROM01-2G), which provides 2GB DDR3 RAM and Wi-Fi 5, for about €260 in the Czech Republic. The RTROM01-2G variant includes the Turris Omnia with three antennas, one network cable (Cat 5e U/UTP), several power adapters, an instruction manual, and a wall mounting bracket as pictured:

{{< img "art-img/as-hns1-turris-omnia-content.jpg" "Turris Omnia and its accessories." "the unboxed Turris Omnia with accessories." >}}

Assembling the Omnia is very straightforward: Screw the antennas onto the Omnia, connect its WAN port with a free network port of your current router, and plug it into the wall socket.

## The basic configuration of the Turris Omnia {#config-to}
Connect the Omnia with a computer using the network cable. Enter "192.168.1.1" in your web browser. You should see the setup wizard that helps you to set up your Omnia. Please note that the following steps may be different on your router due to another version of Turris OS:

* Change passwords.
  * There is a password for Foris OS. Foris OS is appropriate for most users.
  * The second password is for advanced administration. This password is used for LUCI and SSH. Both interfaces allow you to configure many advanced settings.
* Configure WAN/DHCP if necessary.
* Test the connectivity.
* Set the region for time synchronization.
* Enable automatic updates.
* Configure your LAN settings (DHCP and router IP address) if needed.
* Configure your WLAN settings (passwords, SSIDs, etc.). We enabled the 5 GHz network and its guest network only.
* Reboot your Omnia.

Your Omnia gets updates automatically. You can set up e-mail notifications to get notified each time the Omnia installs updates.

### Improve WLAN security {#to-wlan}
We recommend that you use WPA2-PSK-CCMP. You can check your current protection mode via LuCI in your web browser, or via SSH in your terminal.

If your Omnia runs Turris 5.x, you can set up WPA3-SAE. Check the version of Turris OS first. Connect to your Omnia via SSH: {{< kbd "ssh root@192.168.1.1" >}}. Enter the password for advanced administration. You may see the version of Turris OS in the banner text. If you do not see any version number, enter {{< kbd "cat /etc/turris-version" >}}.

To set up WPA3-SAE, remove the package "wpad" and install either wpad-openssl or hostapd-openssl. We use hostapd-openssl in the following. Remove wpad ({{< kbd "opkg remove wpad" >}}), and install hostapd-openssl ({{< kbd "opkg install hostapd-openssl" >}}). Finally, enter {{< kbd "reboot" >}}.

After rebooting, go to the wireless setting in LuCI, and set the protection mode to "WPA3-SAE (strong security)". Alternatively, connect to your Omnia via SSH, enter {{< kbd "vi /etc/config/wireless" >}}, and set the parameter "option encryption" of your WLAN to "sae."

You may enable or disable additional features for your WLAN. Please read the official documentation of OpenWrt to understand all settings (see [Links]({{< relref "#links" >}}) below).

### Understand the purpose of the guest network {#to-guest-nw}
The guest network is a separate VLAN. VLAN means _virtual LAN_. Instead of being physically separated, a VLAN is logically separated. So, you have two different networks by default:

* VLAN 1 (guest network) uses the IPv4 address range "10.111.222.1/24."
* VLAN 2 ("normal" network) uses the IPv4 address range "192.168.1.1/24."

Devices connected to the Omnia's guest network (VLAN 1) are only allowed to communicate with the internet. They can't access the management interface of your Omnia. Furthermore, devices in VLAN 1 can't connect to other devices in VLAN 1 or 2.

We recommend that you use VLAN 1 for devices that only need to connect to the internet, but don't need to communicate with other local devices. Of course, you can share the password of the guest network with guests, as the name implies.

We recommend that you customize the default configuration according to your needs if you know how to configure VLANs. You can also use the private 172.16.0.0/12 IP address range for additional VLANs.

At this stage, your home network can look like this:

{{< webpimg "art-img/as-hns1-turris-network.png" "The home network with a Turris Omnia (192.168.1.1) and your former router (192.168.0.1). Dashed lines are wireless connections. The dotted line to the internet is the connection to your ISP, and solid lines are wired connections. Devices in VLAN 1 can only connect to the internet; devices in VLAN 2 can connect to your router and other devices in VLAN 2." "the final network layout with the Turris Omnia." >}}

{{< hnsbox >}}

## Summary
Installing and configuring the Turris Omnia is easy, and you don't have to learn advanced Linux commands or spend hours to get it to work. The Omnia's defaults allow fast integration in your home network and a good level of security.

We already run three Omnias and didn't encounter any problems during more than two years of operation.

[The next part of this series]({{< ref "/blog/hns2-tls-hardening.md" >}}) is about securing the HTTP connection to the management interface of the Omnia.

## Links
* {{< extlink "https://www.turris.cz/en/omnia/" "Turris Omnia – The open source center of your home." >}}
* {{< extlink "https://docs.turris.cz/" "Turris Documentation" >}}
* {{< extlink "https://openwrt.org/docs/guide-user/network/wifi/basic" "OpenWrt - WLAN settings in /etc/config/wireless" >}}
* {{< extlink "https://www.openwrt.org/" "OpenWrt" >}}

## Changelog
* Jun 9, 2020: Updated the content for Turris OS 5.x.
* Jul 28, 2019: Removed redundant section. Added are more detailed explanation of networks. Added information about Ethernet cables.
