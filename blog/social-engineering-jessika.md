+++
title = "Social engineering: The story of Jessika"
author = "Benjamin"
date = "2020-03-15T09:53:07+01:00"
tags = [ "social-engineering", "OSINT", "hygiene", "photo" ]
categories = [ "knowledge" ]
ogdescription = "Jessika showed Master students the power of social engineering."
slug = "social-engineering-jessika"
banner = "banners/social-engineering-jessika"
toc = true
+++

In January 2020, we gave another lecture on social engineering and awareness at the University of Regensburg, Germany. During our interactive session, Master students had to invade the privacy of our fictional character Jessika. Each student became a social engineer for this course, and some of them managed to "hack" into Jessika's GMX mailbox without using "hacking tools."

This article is about becoming aware of the human factor that is crucial for information security.
<!--more-->
{{< rssbox >}}

## Humans can be hacked, but there is no patch
Often, we see blogs and websites that are entirely focused on technology when it comes to information security. They don't talk about the management of information security or the human factor at all (read ["How some 'security' blogs and websites actually cause insecurity"]({{< ref "/blog/discussion-security-blogs.md#i1" >}})).

However, the human factor is crucial when it comes to information security. A skilled [social engineer]({{< ref "/glossary.md#social-engineering" >}}) can remotely control other humans without getting caught. Victims of social engineering attacks either never realize that they helped the social engineer, or it is already too late. Most of today's attacks on information security heavily rely on social engineering.

The main reason for the success of social engineering is the "social" part of it: Humans are social beings with certain character traits. And character traits can be exploited. For instance, social engineers can exploit curiosity, anxiety, and boredom. Look at a bunch of emojis to get an idea of exploitable characteristics.

Humans need workarounds since you can't patch them. You can't remove the character traits of a human with an update. However, you can frequently make people aware of the dangers of social engineering. In the following, this is the mission of Jessika.

## I'm Jessika, and you don't know me (so far)
The students started with a single link to Jessika's account on mastodon.social. Their task was to invade Jessika's privacy:

* What is her full name?
* Where does she live?
* When is her birthday?
* What are the names of her current and former employers?
* Does she have any pets? If yes, what are the names of her pets?
* …

>I'm Jessika. I'm using some social network to post my thoughts and to connect with other interesting people. Recently, I switched from Twitter to Mastodon. I don't have that many followers there, but it looks interesting.

Finding targets of interest (= exploitable humans) and collecting information about them is the first social engineering step. Nowadays, there are hundreds of social networks for different purposes. Most social networks expose information about you to the public internet. Some communities name this OSINT, open-source intelligence.

### Jessika's public activity on mastodon.social
After getting the link, the students opened mastodon.social. This is one of many Mastodon instances on the internet used by people to share short posts. Of course, most posts are publicly accessible.

By scrolling up and down, the students found different pieces of Jessika's life. For instance, she was on vacation in Innsbruck, Austria, and traveled by train. She wasn't alone because she wrote, "Hopefully, _we_ will reach the connecting train to Regensburg." This likely exposes her current place of residence, which could be Regensburg:

{{< img "art-img/se-jessika-mastodon2.jpg" "Detailed posts expose Jessika's movements, position, and interests." "several social media posts about train delays and traveling from Innsbruck to Germany." >}}

Later that day, Jessika posted about eating pizza. After this, she took a picture of an artificial owl (just a decorative item) and her cat. While many students found the cat's name (Xarina), some students even realized that the owl photo shows a GMX account in the background. GMX is an e-mail provider:

{{< img "art-img/se-jessika-mastodon1.jpg" "Lovely owl; however, social engineers see the GMX mailbox in the background, and cat's name." "a photo of an artificial owl and a GMX mailbox in the background, and a cat." >}}

### Jessika likes traewelling (pronounced like traveling)
In Jessika's Mastodon feed, there were several crossposts from traewelling.de. traewelling is a German website for recording all of your movements using public transport (trains, buses, trams, subways).

In Jessika's case, the students quickly realized the privacy implications of a public feed of all of Jessika's movements. You can guess her place of work, you can guess the area where she lives, you can predict her daily activities, etc. This data is also precious for the average burglar since everybody can see that Jessika isn't at home:

{{< img "art-img/se-jessika-traewelling.jpg" "Websites like traewelling.de can be misused to expose all of your movements with public transport." "a feed on the German traveling page traewelling.de." >}}

### Jessika's professional profile on XING
Another link from mastodon.social pointed to XING, a social network for work-related activities (similar to LinkedIn). There, the students found Jessika's full name (Jessika Vogel) and the current and previous employers:

{{< img "art-img/se-jessika-xing1.jpg" "Websites like XING can expose your current and previous jobs/employers as well as current and former colleagues." "the job history of Jessika on XING." >}}

Then, the students looked at her courses of studies:

{{< img "art-img/se-jessika-xing2.jpg" "Another part of Jessika's CV on XING shows her studies." "the course of studies of Jessika on XING." >}}

Finally, they found Jessika's hobbies, interests, and date of birth:

{{< img "art-img/se-jessika-xing3.jpg" "Hobbies, interests, and date of birth are excellent information for social engineers." "hobbies, interests, and the date of birth of Jessika." >}}

## Putting everything together
After the students collected public information about Jessika, we put everything together:

_Jessika Vogel is 28 years old. She lives in Regensburg. Last weekend, she traveled back from Innsbruck via Munich. She wasn't alone on the train. She has a cat (Xarina), and she loves animals as well as hiking._

_Jessika works as an IT project manager for two years. Previously, she did an internship in IT project management. She holds Master of Science and Bachelor of Science degrees in business informatics (University of Regensburg). She speaks German, English, and a little Portuguese and French. Today, she commuted to work using bus number 31, shortly after 7 am. She misses her two-week long vacation in Austria._

Some students used this information to access Jessika's mailbox on GMX. Accessing her account was possible because she used a very weak and easy-to-guess password ("Xarina1992"). Of course, accessing somebody's mailbox exposes another load of helpful information:

{{< img "art-img/se-jessika-gmx.jpg" "Some students were able to access Jessika's inbox on GMX." "the GMX inbox of Jessika." >}}

This exercise was only the first step of a likely successful social engineering attack. Attackers can use this information to connect with Jessika, exploiting her interests and characteristics, or they can collect more details about Jessika's family, friends, colleagues, and other important people in Jessika's life. It is also easy to [impersonate]({{< ref "/glossary.md#impersonation" >}}) Jessika due to the full access to her mailbox.

## Lessons learned
Jessika doesn't exist. We crafted her identity and accounts so that the Master students could conduct social engineering. However, real-world attacks show that many of such social media profiles expose information about people. Attackers can use this information to exploit you and other people around you. For instance, look for "Robin Sage," a fictional American cyber threat analyst, on the internet.

Keep the following in mind:

* Just migrating from social network A to social network B doesn't protect your data. Be aware that publicly posted information is public and automated tools can find it quickly.
* Online archives may keep your publicly posted information on the internet forever. So think twice before posting anything.
* Even single pieces of useless information can become valuable when combined with other pieces of information.
* Using the same username for many different accounts makes it easier for social engineers to find you.

## Conclusion {#conclusion}
Addressing the human factor is vital for information security. IIn terms of Jessika's story, remember that every public activity on the internet can be misused for social engineering attacks. Social networks, which claim that they value your privacy, won't protect your publicly posted information. Frequently check your social media profiles, delete unused accounts, and use strong credentials for authentication.

_Some photos used for "Jessika's" awareness campaign are from unsplash.com (CC0 license)._
