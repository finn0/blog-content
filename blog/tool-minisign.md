+++
title = "Minisign"
author = "Benjamin"
date = "2019-08-31T15:56:30+02:00"
tags = [ "minisign" ]
categories = [ "tutorial" ]
ogdescription = "Minisign can be used to sign and verify files."
slug = "tool-minisign"
banner = "banners/ish-tools"
notice = true
syntax = true
+++

We look at ["Minisign – A dead simple tool to sign files and verify signatures."]({{< relref "#external-links" >}}) You can use Minisign as an alternative to [GnuPG]({{< ref "/glossary.md#gnupg" >}}) for [signing]({{< ref "/glossary.md#digital-signature" >}}) and verifying files.
<!--more-->
{{< rssbox >}}

Minisign uses [Ed25519]({{< ref "/glossary.md#ed25519" >}}) for cryptographic signing and verification. Several projects like the very popular crypto libraries libsodium or dnscrypt-proxy use Minisign to sign their releases. There are also libraries and implementations in Golang (go-minisign) and Rust (rsign2) available. The current version of Minisign is 0.8, released in February 2018.

After downloading and installing Minisign on your platform, you enter {{< kbd "minisign -G" >}} to create a key pair. On Linux, the private, password-protected key is stored in "~/.minisign/minisign.key" and the public key in "~/minisign.pub." We suggest using a password manager to store your password.

{{< noticebox "warning" "If you create a new key pair, you get two files: The file that ends with \".key\" is your private signing key. Do not share it. The file that ends with \".pub\" is your public verification key. Share this file with everybody who needs to verify your signatures." >}}

The contents of {{< kbd "cat minisign.pub" >}} look like:

{{< samp "untrusted comment: current minisign public key of InfoSec Handbook" >}}
{{< samp "RWTobCZNZpK7QlEBFPj+eGxRxUrsF/wW+Rrm/XOL+RXaC1C6ZLplTsVL" >}}

The first line is an "untrusted" comment. "Untrusted" means that it isn't signed and can be changed. The second line is the Base64 encoded public key.

After creating a key pair, the workflow is similar to tools like GnuPG: You publish your public key "minisign.pub" and use your local private key "minisign.key" to sign files.

To sign files, just enter: {{< kbd "minisign -Sm [file-to-sign]" >}}. After entering the password for the private key, a second file is created, named "[file-to-sign].minisig." Another person can verify the file's signature by entering: {{< kbd "minisign -Vm [file-to-verify] -p minisign.pub" >}}. "[file-to-verify].minisig" must be in the same folder.

Furthermore, you can add "trusted" comments. Trusted comments are signed. Enter {{< kbd "minisign -Sm [file-to-sign] -t '[a-trusted-comment]'" >}}.

The result looks like:

{{< samp "untrusted comment: signature from minisign secret key" >}}
{{< samp "RWTobCZNZpK7QnVLb7KjgV0QB+MaYemn/rjDMwIJUcnUyYwHqgCq5JQqwDDEbOAuk2f8WqDpQsYF15ZVgISJcC+NLPaD/WDG4wc=" >}}
{{< samp "trusted comment: a trusted comment by InfoSec Handbook" >}}
{{< samp "JTbwBH2GAtnYBbGq484em05IF9/PLY97mhsdqWSUbZP8UYOHDn0YZGKdQNImBHcyHwhKkQrW5kgsio1ixLltAw==" >}}

Rename your "minisign.pub" and "minisign.key" files if you need numerous key pairs.

## Conclusion
Minisign is a modern tool that allows you to sign and verify files. Besides, you can verify Minisign's signatures using [OpenBSD's Signify]({{< ref "/blog/tool-signify.md" >}}) tool. The public keys and signatures are compatible.

## External links
* {{< extlink "https://jedisct1.github.io/minisign/" "Minisign – A dead simple tool to sign files and verify signatures." >}}
