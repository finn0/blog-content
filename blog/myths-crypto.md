+++
title = "Cryptography myths"
author = "Jakub"
date = "2019-12-30T12:31:11+01:00"
tags = [ "encryption", "privacy" ]
categories = [ "myths" ]
ogdescription = "We debunk some myths regarding cryptography."
slug = "crypto-myths"
banner = "banners/crypto-myths"
toc = true
syntax = true
+++

Some people are convinced that applying cryptography to their product or service solves all security and privacy problems. Of course, this isn't the case, as shown in this article.
<!--more-->
{{< rssbox >}}

## Myth 1: Applying cryptography makes everything secure {#m1}
For encryption, you need the original unencrypted text, a key, and an encryption algorithm. Let us assume that your original text has no cryptographic relevance.

Depending on your encryption algorithm, you either need the [public key of the recipient of your message]({{< ref "/glossary.md#public-key-cryptography" >}}) (asymmetric key ciphers) or a [shared key]({{< ref "/glossary.md#symmetric-cryptography" >}}) (symmetric key ciphers). Keys can be leaked, keys can be weak, and keys can be guessed using certain attacks like [brute-force attacks]({{< ref "/glossary.md#brute-force-attack" >}}). Stream ciphers, a subclass of symmetric key ciphers, can be attacked if you ever use the same key twice.

Moreover, most encryption algorithms are only secure as long as you meet particular requirements. For instance, some rely on unique values, and some rely on random values. So even if an encryption algorithm is secure in theory, it can be implemented incorrectly and become insecure.

In summary, cryptography adds some security depending on your threat model, your implementation, and your key management. **Cryptography doesn't make everything secure.**

## Myth 2: Applying cryptography makes everything private {#m2}
Applying **cryptography doesn't make everything private**.

For instance, if you use [OpenPGP]({{< ref "/glossary.md#openpgp" >}}) for all of your e-mails, there is still lots of unencrypted [metadata]({{< ref "/glossary.md#metadata" >}}) like senders and recipients of e-mails, length of the content, timestamps, mail servers, and subject lines.

Another example is XMPP: If you always use [OMEMO]({{< ref "/glossary.md#omemo" >}}) for your messages, the XMPP server still stores your contacts, group memberships, and device information in cleartext (also see ["XMPP: Admin-in-the-middle"]({{< ref "/blog/xmpp-aitm.md" >}})).

Even HTTPS doesn't keep everything private since third parties can see which servers you connect to, including how often you access individual servers and the time of connection.

In many cases, cryptography only protects the content of messages while metadata remains unencrypted and accessible to third parties.

## Myth 3: AES is secure {#m3}
Don't get us wrong here! The Advanced Encryption Standard (AES), introduced 20 years ago, is still considered sufficiently secure for most purposes. However, as a block cipher, AES supports the operation modes _Electronic Codebook_ (ECB) and _Cipher Block Chaining_ (CBC) that are considered insecure in some cases. Therefore, it is **wrong** to claim that AES is secure in general. It is only secure if you use secure modes of operation (like GCM) and if everything is implemented correctly.

Block ciphers like AES transform fixed-length groups (blocks) into groups of the same size. ECB takes a block and always applies the same operation to it. Two identical blocks as input result in two equal blocks as output.

For instance, you want to encrypt two messages with ECB mode: "This is my secret text" and "This is no secret book." For simplicity, we assume that 1 block equals 4 characters, and zeros are appended to get a full block at the end. The resulting two messages look like: "This ismy secr ette xt00" and "This isno secr etbo ok00."

ECB encrypts both messages, resulting in something like:

{{< samp "01ab def9 321c c992 8877" >}}
{{< samp "01ab 7722 321c aaa2 551f" >}}

As you can see, there are two identical blocks in both encrypted messages: "01ab" (This) and "321c" (secr). ECB handles each block separately, which is one of the mode's weaknesses.

Another weakness is directly resulting from this: You can modify parts of ECB-encrypted text without knowing the secret key used for encryption. In our example, you can change "01ab def9 321c c992 8877" to "01ab 7722 321c c992 8877." If decrypted, the modified message reads, "This is no secret text" instead of "This is my secret text."

On the other hand, the CBC mode is prone to some padding oracle attacks (e.g., POODLE) and timing attacks (e.g., Lucky13).

## Conclusion
In most cases, applying cryptography adds [confidentiality]({{< ref "/glossary.md#confidentiality" >}}) and [integrity]({{< ref "/glossary.md#integrity" >}}), plus achieves other [security goals]({{< ref "/glossary.md#security-goal" >}}). However, applying cryptography neither makes everything secure nor private. Developers need to implement cryptography correctly. Cryptography requires secure key management (see our article on ["Modern credential management"]({{< ref "/blog/modern-credential-management.md" >}})), and you need to consider that there is still unencrypted data and metadata.

Claiming that a product or service uses some cryptography or even "military-grade encryption" doesn't mean that it is secure (see also our article ["What is 'secure'?"]({{< ref "/blog/discussion-secure.md" >}})).
