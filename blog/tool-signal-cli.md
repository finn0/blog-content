+++
title = "signal-cli"
author = "Benjamin"
date = "2019-09-30T19:16:30+02:00"
tags = [ "signal" ]
categories = [ "tutorial" ]
ogdescription = "With signal-cli, you can use Signal in your terminal."
slug = "tool-signal-cli"
banner = "banners/ish-tools"
toc = true
syntax = true
+++

We look at [signal-cli]({{< relref "#external-links" >}}), a Java-based CLI tool. You can use signal-cli as a Signal messenger client in your terminal. In this post, we briefly introduce the basic configuration of signal-cli.
<!--more-->
{{< rssbox >}}

## Register a phone number
First of all, you need to download and install the tool. The [official "Quickstart" guide]({{< relref "#external-links" >}}) on GitHub suggests different possibilities. Arch Linux users can use the AUR package. There is also a guide for Ubuntu users. signal-cli requires at least Java Runtime Environment (JRE) 8 installed.

After installing the tool, you need to register a phone number. You don't need to register it on your phone beforehand.

The command for this step is {{< kbd "signal-cli -u [phone-number] register" >}}. This command triggers Signal to send a verification SMS to the phone number. So you must be able to receive the SMS on your phone or via an SMS service on the internet. If you can't receive SMS, use {{< kbd "signal-cli -u [phone-number] register --voice" >}}.

Then, you need to verify your phone number by entering: {{< kbd "signal-cli -u [phone-number] verify [verification-code]" >}}.

## Send a message
Now, you can use your phone number with the CLI tool. To send your first message, type: {{< kbd "signal-cli -u [phone-number] send -m '[message]' [phone-number-recipient]" >}}. You second device should receive the message.

## Set a PIN
You should always set a Registration Lock PIN. This PIN is required if somebody tries to re-register your phone number for Signal. The command for the step is: {{< kbd "signal-cli -u [phone-number] setPin [registration-lock-pin]" >}}. We suggest storing the PIN in a password manager.

## Verify safety numbers
The final step is to establish verified trust between both phone numbers (devices). Type {{< kbd "signal-cli -u [phone-number] listIdentities" >}}.

The output should be similar to:

{{< samp "[phone-number-recipient]: TRUSTED_UNVERIFIED Added: Wed Sep 18 12:56:44 CEST 2019 Fingerprint: [fingerprint]  Safety Number: [safety-number]" >}}

As you can see, this identity is marked as "TRUSTED_UNVERIFIED."

Retrieve the safety number of the second device/phone number, and compare it. If both numbers match, type {{< kbd "signal-cli -u [phone-number] trust -v [fingerprint] [phone-number-recipient]" >}}. Note that you enter the "[fingerprint]" of the other device. You don't need to enter the "[safety-number]."

Recheck your setup by entering {{< kbd "signal-cli -u [phone-number] listIdentities" >}} again. Now, the output should be similar to:

{{< samp "[phone-number-recipient]: TRUSTED_VERIFIED Added: Wed Sep 18 12:56:44 CEST 2019 Fingerprint: [fingerprint]  Safety Number: [safety-number]" >}}

Finally, mark the safety number as "verified" on the other device.

## Conclusion
We use signal-cli to send alerts and notifications from numerous servers. For daily usage, the official Signal clients are better. Keep in mind that you must install Java on your device to use signal-cli.

## External links
* {{< extlink "https://github.com/AsamK/signal-cli" "signal-cli" >}}
* {{< extlink "https://github.com/AsamK/signal-cli/wiki/Quickstart" "signal-cli – Quickstart" >}}
