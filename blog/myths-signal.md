+++
title = "Signal messenger myths"
author = "Benjamin"
date = "2018-03-23T04:21:17+01:00"
lastmod = "2020-11-05"
tags = [ "signal" ]
categories = [ "myths" ]
ogdescription = "Signal is one of the best end-to-end encrypted communication apps but there are some myths in circulation."
slug = "signal-myths"
banner = "banners/signal-myths"
toc = true
+++

When it comes to security, Signal is one of the best [end-to-end encrypted]({{< ref "/glossary.md#end-to-end-encryption" >}}) communication apps. There is no unencrypted fallback, and servers process very little [metadata]({{< ref "/glossary.md#metadata" >}}). However, there are some myths about Signal rattling around, mostly spread by people who never used Signal before or who want to promote their favored communication apps.

In this article, we discuss three myths about Signal and show why they are wrong.
<!--more-->
{{< rssbox >}}

## Myth 1: Signal needs access to your contacts {#m1}
**Wrong**. Signal works without accessing any of your contacts. You can deny access to your address book and storage. Denying access comes with at least two drawbacks:

1. You have to manually enter your friends' phone numbers when you contact them for the first time.
2. Signal can't notify you if one of your contacts joins Signal for the first time.

_In the (near) future, Signal may introduce in-app contact management._

Regarding your Signal Profile (the name and picture that you can set in Signal): Signal encrypts your profile, and you can only restore it with your optional Signal PIN. Signal shares your profile with contacts in your address book and members of the groups that you create. In other cases, you must explicitly allow sharing your Signal Profile.

## Myth 2: You have to disclose your cellphone number to use Signal {#m2}
**Wrong**. You can register an arbitrary phone number, but you must confirm that you have access to this phone number during the initial setup. Disclosing your cellphone number isn't necessary to use Signal.

However, keep in mind that somebody could try to re-register for Signal when you use a public phone number. We recommend setting the Registration Lock PIN or Signal PIN.

_In the (near) future, Signal may introduce either accounts without phone numbers or communicating with others without sharing your phone number._

## Myth 3: Signal is just like WhatsApp {#m3}
**Wrong**. Signal developers went to great lengths to fully implement modern end-to-end encryption. Servers process only very little metadata, as mentioned previously.

For instance, the "sealed sender" feature hides the message senders' identity, so Signal servers don't learn about the sender of a message. Client implementations and server code are open source. Signal would show prominent security warnings if the contact's safety number changed.

On the other hand, WhatsApp collects much more metadata, and there is the widely discussed data exchange with Facebook. Additionally, WhatsApp isn't fully open source and allows storing (unencrypted) backups in the cloud.

Of course, there are more differences between the messengers. Signal isn't just another WhatsApp.

## Conclusion
Unfortunately, some people spread these myths without ever checking if they are right. These myths confuse potential users who ultimately stay with their messenger or migrate to less secure solutions.

## Sources
* {{< extlink "https://signal.org/" "signal.org" >}}
* {{< extlink "https://support.signal.org/hc/en-us/articles/360007459591-Signal-Profiles-and-Message-Requests" "Signal Profiles and Message Requests" >}}

## Changelog
* Nov 5, 2020: Updated information about Signal Profiles and future plans.
* Oct 31, 2018: Added information about "sealed sender."
* Apr 3, 2018: Added Registration Lock PIN (> 4.17.5)
