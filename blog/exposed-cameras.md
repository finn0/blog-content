+++
title = "Christmas holidays: more exposed IP cameras will go online within the next few days"
author = "Jakub"
date = "2018-12-24T07:14:00+01:00"
tags = [ "camera", "lan", "privacy", "wlan" ]
categories = [ "privacy" ]
ogdescription = "There may be an insecure IP camera for you underneath the Christmas tree."
slug = "exposed-cameras"
banner = "banners/exposed-cameras"
toc = true
syntax = true
+++

It's Christmas time, and there may be an IP camera for you underneath the Christmas tree. Besides unpacking and connecting it with your WLAN, you should also check its configuration and ensure that unwanted third parties can't access it.

In this article, we show you how attackers can quickly locate your camera's physical location and provide tips on how you can secure your device.
<!--more-->
{{< rssbox >}}

## Last Christmas {#lc}
Back in May, [we already showed risks that come along with IP cameras]({{< ref "/blog/cameras-censys.md" >}}). We won't repeat everything of this article, but give you two examples of how attackers locate your home.

As of now, querying censys.io for IPv4 addresses tagged with "camera" in the Czech Republic ({{< kbd "location.country_code: CZ AND tags: camera" >}}) reveals 718 cameras. In total, censys.io lists nearly 78,000 cameras. Of course, censys.io is only one search engine to find publicly accessible IP cameras. There are likely many more IP cameras showing private areas like living rooms, bedrooms, or front yards.

In summary, attackers will likely find your IP camera if there are no protective measures in place. There are obvious risks of publicly available live streams:

* Attackers can use this information to observe your daily routines.
* Attackers can use their knowledge for [social engineering]({{< ref "/glossary.md#social-engineering" >}}).
* Attackers try to use default credentials to log into your camera and learn about your home network.

Moreover, attackers can try to pinpoint your home using the live stream of your camera. We will show you two examples below.

## Two examples of pinpointing cameras {#pinpointing-examples}
It's easy to physically locate most cameras given their live image and approximate location based on their IP address. Let's have a look at two publicly accessible live streams that show public areas.

### Example 1: Camera somewhere in Prague, Czech Republic {#ex1}
According to its IP address, the following camera is located somewhere in Prague, Czech Republic. Prague is home to about 1.3 million people and has an urban area of 298 km². Prominent features in the picture are tram tracks, the sidewalk patterns, and the park on the other side of the street.

{{< img "art-img/exposed-cameras-1a.jpg" "Example 1a: This camera is located somewhere in Prague, Czech Republic. Prominent features in the picture are tram tracks, the sidewalk patterns, and the park on the other side of the street." "several cars on a street in Prague as seen by a camera." >}}

Of course, an IP address pointing to Prague doesn't mean that this camera is located in Prague. However, there are only seven cities in the Czech Republic that operate trams. Every city has its design and colors for trams. An observer only has to wait several minutes until the next tram passes. The tram design clearly points to Prague.

Furthermore, an observer sees the tram lines 3 and 8. The lines limit the camera's possible location to a 3 km long part of Sokolovská street in Karlín, Prague. There is only one location with a park on the other side of the street, shown in the following picture.

{{< img "art-img/exposed-cameras-1b.jpg" "Example 1b: The design and colors of passing trams point to Prague. The tram lines point to Sokolovská street in Karlín. The park on the other side of the street points to one remaining location. You can confirm the location by looking at the patterns of the sidewalk." "the satellite view of the camera's position." >}}

Using the "panorama" feature of mapy.cz finally reveals the camera's physical location, as shown below:

{{< img "art-img/exposed-cameras-1c.jpg" "Example 1c: Using the 'panorama' feature of mapy.cz finally reveals the camera's physical location." "the camera on the wall." >}}

Locating this camera took about 5 minutes. Moreover, there are two additional live streams accessible via the IP address. There is another camera on the front facade and one camera in the building's entrance hall, filming all residents and visitors.

### Example 2: Camera somewhere in the Czech Republic {#ex2}
Another camera is located somewhere in the Czech Republic, according to its IP address. The approximate location of the IP address is also Prague. However, the real area can't be Prague according to the small size of the houses in the picture.

In the upper left corner, the stream shows "Zdirec n.D." This label points to the city of "Ždírec nad Doubravou" in the Czech Republic. A prominent feature in the picture is a small park surrounded by apartment blocks.

{{< img "art-img/exposed-cameras-2a.jpg" "Example 2a: Another camera is located somewhere in the Czech Republic according to its IP address. In the upper left corner, the stream shows 'Zdirec n.D.'. This label points to the city 'Ždírec nad Doubravou' in the Czech Republic." "the perspective of the camera." >}}

Ždírec nad Doubravou is a town in the Vysočina Region that lies between Pardubice and Jihlava and has a population of 3,120. Looking at the aerial map of the town reveals only one possible location: a park in the Northern part of the town.

{{< img "art-img/exposed-cameras-2b.jpg" "Example 2b: Looking at the aerial map of the town reveals only one possible location: a park in the Northern part of the town. The screenshot is rotated through 180°." "the satellite image of the camera's location." >}}

Finally, we switch to the "panorama" feature of mapy.cz to digitally stand in front of the house with the camera.

{{< img "art-img/exposed-cameras-2c.jpg" "Example 2c: We switch to the 'panorama' feature of mapy.cz to digitally stand in front of the house with the camera." "some cars on the street in front of the camera's position." >}}

## Sometimes, attackers are in full control {#control}
The two examples above show how attackers can quickly pinpoint the physical locations of cameras. Besides, they can learn about the camera manufacturer, model, configuration, and much more. We discussed other examples in our [article about metadata in image files]({{< ref "/blog/image-metadata.md#visible-info" >}}).

Sometimes, attackers can:

* Rotate your camera to look around.
* Start audio recording for eavesdropping.
* Connect to your WLAN by using the password disclosed by the camera.
* Hack other devices within your WLAN.
* Disable recording at all.
* Format your storage cards.
* Delete log files to erase all traces.

Don't let attackers in your home!

## Secure your camera
We provide some tips to secure your camera:

1. Don't buy cheap cameras that don't have any security features (hopefully, you didn't get a cheap camera for Christmas).
2. Change default usernames and passwords.
3. Enable HTTPS, if available.
4. Update your camera's firmware.
5. Disable port forwarding and UPnP.
6. Use WPA2-PSK-CCMP (sometimes called WPA2-AES) only.
7. Turn off your camera when you don't need it.
8. Regularly check its log files.
9. Use network segmentation, if available (e.g., by connecting your IP camera with your guest network only).
10. Regularly check its settings and change passwords.

Finally, always remember that no device on earth is or will ever be 100% secure. Regularly check news feeds to learn about disclosed [security vulnerabilities]({{< ref "/glossary.md#vulnerability" >}}) that may affect the security of your camera.

## Summary
Keep in mind that IP cameras are just another IP device in your home network. Some of them are publicly accessible while their owners never learn about this. Talk about this problem with people who own cameras and help to secure cameras, if possible.

Thank you, and Merry Christmas 2018!
