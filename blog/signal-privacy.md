+++
title = "How to use Signal more privacy-friendly"
author = "Benjamin"
date = "2018-04-21T09:02:36+02:00"
lastmod = "2020-05-24"
tags = [ "metadata", "privacy", "signal" ]
categories = [ "privacy" ]
ogdescription = "When it comes to security, Signal is one of the best messengers available. We show you some additional privacy tips."
slug = "signal-privacy"
banner = "banners/signal-privacy"
toc = true
notice = true
+++

Signal is one of the most secure messengers available, making it practically impossible for server operators or other [men-in-the-middle]({{< ref "/glossary.md#man-in-the-middle-attack" >}}) to decrypt your conversations, learn about your group memberships, or spy on you in another way.

However, some people raise privacy concerns due to Signal's need for an arbitrary phone number. This time, we show you how to use Signal even more privacy-friendly.
<!--more-->
{{< rssbox >}}

## Setup
The Signal messenger is primarily developed for Android and iOS. Thus, most of the features mentioned below are available in the mobile version of Signal. There is also Signal Desktop and "signal-cli," but these clients come with a limited set of features. While you must link Signal Desktop with the mobile app, you can use signal-cli as a standalone Signal client.

### Avoiding a smartphone
Some people don't want to install Signal on their smartphones. In this case, you can either install Signal on a virtual machine or use signal-cli. We installed and tested multiple versions of Signal on Android x86, using Oracle's VirtualBox. Of course, some features aren't available due to the technical limitations of the virtual environment.

Another possibility is to use the tool "signal-cli," as shown in our [article on it]({{< ref "/blog/tool-signal-cli.md" >}}). We primarily use signal-cli as a standalone solution for sending security notifications from servers to clients.

### Hiding your real phone number
Some people argue that Signal can't be private if you have to provide your real phone number. However, [you don't have to register your actual cellphone number]({{< ref "/blog/myths-signal.md#m2" >}}). Alternatively, you can buy a single-use SIM card to register this phone number, register a VoIP number, or use an online service that allows you to receive SMS. You can even access this SMS service using the Tor Browser. We registered six phone numbers for multiple purposes without exposing our "real" phone numbers.

Besides, there may be other options than registering a phone number to use Signal in the future.

### Hiding your contacts
Despite Signal asking for access to your contacts for improved usability, you can deny this request. There is [no need to let Signal access your contacts]({{< ref "/blog/myths-signal.md#m1" >}}). You can use Signal without any contacts on your phone (or virtualized operating system).

## Configuration
We recommend the following configuration steps. Depending on your use cases and threat model, you might choose other settings.

{{< noticebox "note" "Please note that the following section is based on Signal Android 4.60.x. Depending on your version, some features may be missing or have other names. In case of doubt, read the official Signal documentation." >}}

### Set a Signal PIN and enable Registration Lock
After registering your identifier for Signal, you should immediately set your Signal PIN and enable Registration Lock. The is no character limit for the Signal PIN.

The Registration Lock addresses [SIM swapping attacks]({{< ref "/glossary.md#sim-swapping" >}}). So an attacker who is in full control of your phone number can't register this phone number again. Keep in mind that the Registration Lock expires after seven days of inactivity. "Inactivity" means that your Signal client does not connect to Signal servers within the period. Even if an attacker manages to register your phone number again, previous messages can't be accessed due to [forward secrecy]({{< ref "/glossary.md#forward-secrecy" >}}).

Besides, the Signal PIN is used to restore your profile, settings, and contacts if you lose your device. The Signal PIN replaces the previous Registration Lock PIN. Please note that this feature was introduced in early 2020, so the Signal PIN's functionality might change soon.

### Set your Signal Profile
Signal requires you to set a name for your profile. Besides, you can optionally set an avatar. The Signal Profile is end-to-end encrypted and can't be accessed by Signal servers. So only your contacts see your name and (optionally) an avatar.

### Disable support for SMS and MMS
In the past, Signal offered encrypted SMS. In 2015, Signal developers decided to drop the SMS encryption feature. Nowadays, Signal can still send and receive SMS; however, they are always in cleartext.

We recommend not to use Signal as your default SMS app since you can get confused, and accidentally send cleartext SMS to someone while expecting to send an end-to-end encrypted message. Besides, SMS produces tons of [metadata]({{< ref "/glossary.md#metadata" >}}). Use a dedicated app for insecure SMS.

### Set the "Privacy" options
This configuration section contains the Signal PIN mentioned above. Besides, you can configure the app access, communication settings, and the sealed sender feature.

We recommend:

* Enable "screen lock" (locks Signal using the screen lock of your device).
* Enable "screen security" (screenshots are blocked).
* Enable "incognito keyboard" (avoids that your keyboard app stores your conversations due to personalization).
* Disable "read receipts" (so others can't see if you read their messages).
* Disable "typing indicators" (so others can't see that you enter text).
* Disable "send link previews" (so there are no additional network connections when you enter certain links).
* Enable "display indicators" of the sealed sender feature, and keep the "allow from anyone" setting disabled.

### Optionally set disappearing messages and view-once media
Signal allows you to set disappearing messages per contact/group. For the sender, the timer starts immediately after sending a message. For the recipient, the timer starts when reading the message for the first time. Afterward, the Signal client deletes the message.

Another feature is called "view-once media." You can set it before you send a media attachment. The sender's client doesn't store the view-once media in the conversation's history. The recipient's client deletes the view-once media after the recipient accessed it. Signal automatically removes view-once media after 30 days if the recipient doesn't access it.

Depending on your threat model, you may enable this feature.

### Understand the checkmarks
Signal doesn't disclose your online status. Your contacts can't see whether you are online at the moment, or the last time you were online. However, Signal shows one checkmark (in a circle) when a Signal server receives your message and two checkmarks (in two circles) when the recipient receives your message. If read receipts are enabled, the checkmarks are in filled circles after the recipient opened the conversation.

## Operation
After setup and configuration, you can use Signal in day-to-day life. Finally, you should verify all of your contacts and understand the limits of Signal.

### Verify your contacts
Verify all of your contacts to avoid [man-in-the-middle attacks]({{< ref "/glossary.md#man-in-the-middle-attack" >}}). For verification, Signal provides per-contact safety numbers. You can share the safety numbers via channels that can't be easily altered. For instance, you can share them during a phone call.

### Understand the limits of Signal's security features
Never forget that there is no 100% security. For instance, if somebody infects your device with [malware]({{< ref "/glossary.md#malware" >}}), this party could easily read all of your messages and wiretap your Signal calls. Furthermore, someone could steal your device, and law enforcement could seize it. Enforced [end-to-end encryption]({{< ref "/glossary.md#end-to-end-encryption" >}}) only ensures that the entire connection between your device and the device of the recipient is secure.

## Conclusions
Signal is already really secure and privacy-friendly by default; however, you can improve this by using a strict configuration and a phone number that can't be connected with your real identity. You don't need a phone or SIM card to use Signal, and Signal doesn't need to access your contacts.

## Sources
* {{< extlink "http://www.android-x86.org/" "Android x86" >}}
* {{< extlink "https://signal.org/android/apk/" "Signal APK download" >}}
* {{< extlink "https://support.signal.org/hc/en-us/articles/360007320751" "How do I know if my message was delivered or read?" >}}
* {{< extlink "https://support.signal.org/hc/en-us/articles/360007059792-Signal-PINs" "Signal PINs (including Registration Lock)" >}}

## Changelog
* May 24, 2020: Added information about "Signal PINs," and updated links.
* Jan 1, 2020: Added signal-cli as another possibility to use Signal without a smartphone.
* Apr 21, 2019: Clarified information about Signal's SMS feature. Thanks to @se7en via Pleroma. Added "link previews." Removed "Silence" app.
* Dec 16, 2018: Added recommendation for "typing indicator."
* Nov 19, 2018: Added information about "sealed sender."
