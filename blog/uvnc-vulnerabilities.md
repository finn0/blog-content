+++
title = "UltraVNC – a security nightmare"
author = "Benjamin"
date = "2019-03-30T09:31:00+01:00"
lastmod = "2019-04-03"
tags = [ "ultravnc", "vnc", "remote-access", "joomla", "phpbb" ]
categories = [ "vulnerability" ]
ogdescription = "This month, Kaspersky published 22 vulnerabilities in UltraVNC client and server. We looked at UltraVNC's website that is also insecure."
slug = "uvnc-vulnerabilities"
banner = "banners/security-risk"
toc = true
syntax = true
+++

UltraVNC is open-source software to control other systems and visually share desktops remotely. If you look at its track record, it looks great: only seven security [vulnerabilities]({{< ref "/glossary.md#vulnerability" >}}) in 13 years. However, this month, Kaspersky published not only one newly-found vulnerability in UltraVNC, not two, not five, but 22 security vulnerabilities (KLCERT-19-003 to KLCERT-19-024) that all have their own [CVE]({{< ref "/glossary.md#cve" >}}) identifiers. Most vulnerabilities come with a [CVSS]({{< ref "/glossary.md#cvss" >}}) v3.0 base score of 10.0 out of 10.0, which means that it can't be worse anymore.

The official website and forum of UltraVNC aren't better: There is no HTTPS, there are no modern security features, there was a file containing secrets, and the CMS is outdated. In this article, we show several vulnerabilities of uvnc.com to raise awareness about insecure websites.
<!--more-->
{{< rssbox >}}

## A common problem: security by assumption, hope, and belief {#sbh}
You know the term "security by obscurity" (or "security through obscurity"): Someone creates something and keeps its details secret to "secure" it. However, this doesn't work, and most people know it these days.

Then, a set of people repeatedly state that open-source software is secure "since everybody can check the source code for security vulnerabilities." We already wrote in other articles that this doesn't work either. A problem exists:

* Developers hope that someone checks their source code for security vulnerabilities.
* Third-party developers assume that someone already checked the source code for security vulnerabilities.
* End users believe that the application is secure since it is open source, and someone indeed checked the source code for security vulnerabilities.

Ultimately, nobody checks the source code; all parties assume it's "secure."

We call this "security by assumption, hope, and belief." This problem isn't limited to UltraVNC, of course. In January, a cryptographic weakness in 7-Zip was found. 7-Zip is an open-source file archiver. 7-Zip's source code is also available; however, "security by assumption, hope, and belief" kicked in. Nobody looked at the vulnerable crypto part of the code so far.

In summary, never assume that any code is "secure." In case of doubt, nobody checked it. And no, not "everybody can check open-source code for security vulnerabilities." Most people aren't developers, and developers neither "speak" every programming language possible nor master bug-free secure coding. Forget this urban legend.

## Vulnerabilities in UltraVNC's code {#code}
As mentioned above, Kaspersky found 22, mostly critical, security vulnerabilities in the UltraVNC source code (affecting client and server). According to Kaspersky, all UltraVNC versions are affected. They found the vulnerabilities in several revisions of UltraVNC (1198 to 1211) and point to revision 1212 that addresses all vulnerabilities so far. Revision 1212 is part of UltraVNC 1.2.2.4 that is available for about two weeks. Versions before 1.2.2.4 come with the already mentioned vulnerabilities.

We tried to contact the UltraVNC team to verify that they fixed all 22 vulnerabilities. However, there is no security contact, and they didn't include detailed information about fixed vulnerabilities in their changelogs.

## UltraVNC's website {#website}
Going to UltraVNC's website uncovers a new problem: there is little to no security. The website looks like somebody set it up many years ago, and nobody maintained it afterward. However, a forum exists (which we will discuss below) and developers still publish posts there. So, the developers are there but seemingly don't care about security.

### JavaScript everywhere {#js}
When you navigate to uvnc.com, the website will tell you that you must enable JavaScript (if you turned it off by default). As soon as you turn on JavaScript, all the nasty scripts will be executed: Google Analytics, Ads by Google, Facebook Connect, and more. The website also sets tracking [cookies]({{< ref "/glossary.md#cookie" >}}) and modifies the localStorage of the web browser (thanks to Google AdSense). There is no Subresource Integrity (SRI) that would allow the website to embed third-party content securely. Third parties (or attackers) can change the embedded content—uvnc.com will happily deliver it to your web browser. Malvertising, anyone?

### No HTTPS, no security headers {#https}
The second obvious observation is the absence of HTTPS. There is a 2048 bit Let's Encrypt HTTPS [certificate]({{< ref "/glossary.md#certificate" >}}) valid for www.uvnc.com, not for uvnc.com (no www part). However, the website doesn't redirect non-www to www requests (or vice versa). It also doesn't redirect HTTP to HTTPS. So, the HTTPS certificate is useless, and the certificate chain is in an incorrect order, too. Even worse: The web server accepts outdated and insecure SSLv3, plus outdated TLS 1.0 and 1.1 protocols. Moreover, it accepts stone-age cipher suites that use 3DES, RC4, IDEA, and RSA-based key exchange.

There is more! The web server installed on a server of Kansas-based APH Inc. (operating as Codero) is vulnerable to POODLE (Padding Oracle On Downgraded Legacy Encryption, CVE-2014-3566) due to its support for stone-age crypto: attackers can downgrade TLS 1.x to SSLv3. Additionally, there is no modern security header present (HTTP Strict Transport Security, Content Security Policy, X-Content-Type-Options, Referrer Policy, etc.).

By the way, this is all also true for the admin access of uvnc's CMS.

### Publicly-accessible secrets {#secrets}
The list of weaknesses and security problems continues. There is a file that contained configuration details and secrets. Since the UltraVNC team changed the file's content to "index.php" after we contacted them. We show the remaining default content of the file in the screenshot below to raise awareness. Such publicly-accessible files are a valuable source for any attacker.

{{< webpimg "art-img/uvnc-vulnerabilities-secret.png" "One file on uvnc.com disclosed configuration details and secrets (we removed all secrets in the screenshot). The UltraVNC team changed the content of the file after we contacted them." "a file with configuration data." >}}

### Joomla 1.5.15 {#joomla}
Furthermore, we looked at their CMS. Joomla powers their website. Joomla is a famous content management system (CMS) nowadays. Most content management systems (e.g., Drupal, WordPress, Joomla) are incredibly complex software packages that contain security vulnerabilities. Security vulnerabilities are common. There were more than 130 vulnerabilities with CVE identifiers in Joomla alone. The critical part for administrators of websites is that they update their content management systems as soon as possible.

Let's go back to uvnc.com. Its CMS identifies itself as "Joomla! 1.5 - Open Source Content Management." Using fingerprinting techniques to determine the actual version number results in "Joomla 1.5.15." Joomla 1.5.15 was released in November 2009. If uvnc.com uses Joomla 1.5.15, it uses an outdated CMS with up to 100 publicly-known security vulnerabilities. This poses a high risk for any visitor of the website.

### Exposed web statistics {#webstats}
Finally, we found a publicly-accessible instance of Webalizer 2.01-10 containing stats from April 2017 to March 2018. These stats include IP addresses in cleartext. It is unclear whether these IP addresses belong to individuals or bots. If they belong to private users, UltraVNC leaks personal data, too.

{{< webpimg "art-img/uvnc-vulnerabilities-webstats.png" "A publicly-accessible instance of Webalizer 2.01-10 contains stats of one year. These stats include IP addresses." "statistics about website visitors." >}}

## UltraVNC's forum {#forum}
UltraVNC's phpBB-powered forum is accessible on uvnc.com. If you click links on the forum page, its URL doesn't change. The source code tells us the reason: The forum (forum.ultravnc.info) is embedded with the help of an {{< kbd "<iframe>" >}}. Inline frames can be used to embed a document within HTML pages. HTML 5 introduced additional controls (using the "sandbox" parameter) to restrict inline frames. However, inline frames pose a security risk. The embedded website could serve malware, and there is the risk of [clickjacking]({{< ref "/glossary.md#clickjacking" >}}).

There are the same security vulnerabilities as before: no HTTPS, no security headers, and maybe an outdated version of phpBB. Any website can likely embed the forum and conduct clickjacking. Moreover, the PHP version of the forum identifies itself as "PHP/5.3.3-7+squeeze19." This implies that the forum's operating system is Debian 6 "Squeeze," and it is likely that "apache2 2.2.16-6+squeeze12" is in use. The LTS support for Debian 6 ended in February 2016, while Apache and PHP are unsupported since 2014.

The website's overall (in)security could also be an indicator of poorly-secured databases used by Joomla and phpBB. Hopefully, we won't observe just another data breach containing [personal data]({{< ref "/glossary.md#personal-data" >}}) and passwords of people who used the UltraVNC forum in the future.

## UltraVNC's (non) reaction {#reaction}
We reached out to UltraVNC and asked for a comment on the most severe findings. At the moment, they only removed the secrets mentioned above from the publicly-accessible file. We didn't get a reply or observed any other reaction so far. Later, the UltraVNC team removed the {{< kbd "<meta name=\"generator\" … />" >}} tags from their HTML code to hide their Joomla version without updating anything.

## Summary
This article shows that you should never assume that somebody already checked something for security vulnerabilities. This isn't only true for UltraVNC or open-source software but also for your personal website, forum, or other digital content. Several "security" blogs recommend UltraVNC because it is open source (= secure and trustworthy, according to them).

If you run a web server, care about it—install updates, set up technical security measures (see our [web server security series]({{< ref "/as-wss.md" >}})), monitor it. Just setting up a web server under the motto "never touch a running system" is a significant risk for every visitor of your website. Even if you don't host valuable data or don't process others' personal data, your server could become part of a [botnet]({{< ref "/glossary.md#botnet" >}}) to conduct illegal activities. Additionally, at least reply to security-related/privacy-related e-mails!

Finally, keep in mind that Virtual Network Computing (VNC) uses the Remote Framebuffer Protocol (RFB, RFC 6143). RFB 3.8 is insecure by design. The only optional security mechanism is a legacy [challenge–response authentication]({{< ref "/glossary.md#challenge-response-authentication" >}}) method, using 8-digit passwords and DES. If you use VNC, run it only in controlled networks (e.g., your LAN or via VPN). Some VNC clients like UltraVNC bring their own encryption features that aren't compatible with other organizations' clients/servers.

## Changelog
* Apr 3, 2019: Updated Joomla version, added information about PHP/Debian/Apache in use, added information about publicly-accessible statistics website.
