+++
title = "Web server security – Part 5: Server-side DNS security configuration"
author = "Jakub"
date = "2018-12-08T13:50:00+01:00"
tags = [ "blogging", "server-security", "dns", "caa", "dnssec", "ssh" ]
categories = [ "Web server security" ]
ogdescription = "In this article, we introduce server-side DNS security configuration."
slug = "wss5-dns-configuration"
banner = "banners/as-wss"
toc = true
syntax = true
+++

In this part of the Web server security series, we introduce server-side DNS security configuration. This doesn't include setting up and operating your own DNS server but understanding and setting security-relevant DNS resource records.
<!--more-->
{{< rssbox >}}

## Requirements
* authorization to change DNS configuration
* basic knowledge about DNS resource records

## DNS
The Domain Name System originates from the need for mapping human-friendly host names to computer-friendly numerical addresses. This was once realized by maintaining a centralized HOSTS file and manual address assignment. The centralized solution evolved into the decentralized and hierarchical Domain Name System. In November 2018, the original DNS turned 35.

Nowadays, DNS is not only used for address resolution but also for mapping services and providing further information. One of the biggest problems with DNS is its insecurity: Like many other protocols designed decades ago, original DNS traffic doesn't provide any security features. Raw DNS traffic is unsigned and in cleartext, making it easy for third parties to tamper with DNS packets and servers. However, this situation changes since DNSSEC (for [authenticity]({{< ref "/glossary.md#authenticity" >}})) and DNS-over-TLS/DNS-over-HTTPS (for [confidentiality]({{< ref "/glossary.md#confidentiality" >}})) become more widespread.

## DNS resource records {#rr}
Resource records (RR) are the basic elements of the Domain Name System. RR fields consist of:

* NAME: fully qualified domain name of the node in the tree
* TYPE: type of RR in numeric form
* CLASS: class of RR
* TTL: time interval that the resource record may be cached before the source of the information should again be consulted
* RDLENGTH: length of the RDATA field
* RDATA: additional information about the RR

You don't have to learn this since most of these fields are optional or your DNS hosting provider already configured defaults.

## Adding security-relevant DNS resource records {#sr-dns-rr}
Check each of the following DNS resource records to decide if you want to implement them. Please note that each DNS hosting provider has its own interface for changing DNS resource records. Due to this, we can't provide a universal step-by-step guide for setting each RR. The following configuration applies to the interface of our provider (netcup GmbH).

### CAA
CAA means "DNS Certification Authority Authorization". It is a policy that tells certificate authorities (CA) if they are authorized to issue [certificates]({{< ref "/glossary.md#certificate" >}}) for a given domain name. The idea is that a CA fetches the CAA resource records of a domain name and checks whether it is authorized prior to issuing a certificate. In September 2017, CAA became mandatory for CAs to implement.

CAA records consist of:

* flags: Flag for future use. You must set this flag to 0 according to RFC 6844.
* tag: Different property tags like "issue", "issuewild" or "iodef".
* value: Character string.

#### Example 1: No CA authorized
In the first example, no CA is authorized to issue certificates for [your-domain-name]:

{{< samp "[your-domain-name] CAA 0 issue \";\"" >}}

#### Example 2: Authorize certain CAs
In the second example, only Let's Encrypt is authorized to issue certificates for [your-domain-name]:

{{< samp "[your-domain-name] CAA 0 issue \"letsencrypt.org\"" >}}

#### Example 3: Set an e-mail address for reporting
In the third example, the CA can send violation reports to [your-email-address] using the Incident Object Description Exchange Format (IODEF):

{{< samp "[your-domain-name] CAA 0 iodef \"mailto:[your-email-address]\"" >}}

netcup users can set CAA by selecting "Type: CAA", "Host: @" and "Destination: 0 issue [CAs]".

Please note that CAA records can be spoofed if [DNSSEC]({{< relref "#dnssec" >}}) isn't present and CAs don't perform DNSSEC validation. This step isn't mandatory for CAs. Moreover, CAA doesn't directly protect visitors of your website.

### DNSSEC
DNSSEC means "Domain Name System Security Extensions" and is a set of standardized specifications for securing certain kinds of DNS information. DNSSEC actually uses multiple DNS resource records to provide [digitally signed]({{< ref "/glossary.md#digital-signature" >}}) resource records. Client-side DNS resolvers can verify these signatures which provides [authenticity]({{< ref "/glossary.md#authenticity" >}}) and [integrity]({{< ref "/glossary.md#integrity" >}}).

The most important DNSSEC-related resource records are:

* DNSKEY: Contains the public key used by the DNS resolver to verify RRSIG records
* RRSIG (resource record signature): Contains a single DNSSEC signature for a record set
* DS (delegation signer): Name of a delegated zone with reference to a DNSKEY record in the sub-delegated zone. The DS record is placed in the parent zone along with the delegating NS records.

DNSSEC has several requirements:

* Your parent zone (e.g., "cz" or "eu") and all of its parent zones must support DNSSEC. You can check this using a terminal: {{< kbd "dig +short DNSKEY [parent-zone]" >}}. No results mean that your parent zone doesn't support DNSSEC and you can't use it.
* Your authoritative DNS servers must support DNSSEC.
* Your registrar must allow uploading DS records to the parent zone.

DNSSEC is extensible when it comes to algorithms used for signing. See [IANA – Domain Name System Security (DNSSEC) Algorithm Numbers]({{< relref "#links" >}}) for more information.

netcup allows you to enable/disable DNSSEC by checking a single check box (check "DNSSEC status"). If you disable DNSSEC, you must wait 48 hours until it can be enabled again. Please note that any change of signed resource records requires to update RRSIGs. Providing old RRSIGs and updated resource records likely leads to an unreachable website.

### OPENPGPKEY
OPENPGPKEY is used to associate an [OpenPGP]({{< ref "/glossary.md#openpgp" >}}) public key with an e-mail address, thus forming an "OpenPGP public key association". The resource record is described in RFC 7929 "DNS-Based Authentication of Named Entities (DANE) Bindings for OpenPGP". [DANE is described in the TLSA section]({{< relref "#tlsa" >}}).

An OPENPGPKEY resource record consists of:

* your modified e-mail address
* the complete armored OpenPGP public key

Your e-mail address (let's take "abc@example.eu") is modified in the following way:

1. Take the part in front of @ → "abc."
2. [Hash]({{< ref "/glossary.md#hash-function" >}}) "abc" using SHA-256 and cut the hash value after 56 hexadecimal characters. You can do this in your terminal entering: {{< kbd "echo -n \"abc\" | sha256sum | cut -c -56" >}}.
3. Take the hash value "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61" and append ".\_openpgpkey." as well as the domain name of your e-mail server and a closing dot

The result looks like:

{{< samp "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61._openpgpkey.example.eu." >}}

To get the complete armored OpenPGP key using [GnuPG]({{< ref "/glossary.md#gnupg" >}}), enter {{< kbd "gpg -a --output public-key.asc --export [keyid]" >}}. Copy the content of "public-key.asc" as needed.

We didn't implement this for reasons explained in [GnuPG for e-mail encryption and signing]({{< ref "/blog/gpg-for-emails.md" >}}). Moreover, most clients don't seem to support OPENPGPKEY at the moment.

According to RFC 7929, [DNSSEC]({{< relref "#dnssec" >}}) must be present and DNSSEC validation is mandatory for clients. This means that you must implement DNSSEC in order to provide a verifiable OPENPGPKEY resource record.

### SSHFP
SSHFP means "Secure Shell fingerprint record" and can be used to store an SSH fingerprint in the DNS zone.

An SSHFP resource records consists of:

* algorithm number: 1 = [RSA]({{< ref "/glossary.md#rsa" >}}), 2 = DSS, 3 = [ECDSA]({{< ref "/glossary.md#ecdsa" >}}), 4 = [Ed25519]({{< ref "/glossary.md#ed25519" >}})
* fingerprint type: 1 = SHA-1, 2 = SHA-256
* fingerprint: hash value of the SSH public key using the hash algorithm defined in "fingerprint type"

To easily generate an SSHFP resource record, connect to your remote server, and enter: {{< kbd "ssh-keygen -r [your-domain-name] -f /etc/ssh/ssh_host_ed25519_key.pub" >}} (if you want to use the Ed25519 key).

You get two lines:

{{< samp "[your-domain-name] SSHFP 4 1 [sha1-hash-value]" >}}
{{< samp "[your-domain-name] SSHFP 4 2 [sha256-hash-value]" >}}

Only add the second line (SHA-256) to your DNS zone by selecting "Type: SSHFP", "Host: @" and "4 2 [sha256-hash-value]" (example for netcup users).

More examples of SSHFP records are shown in the RFCs 4255, 6594 and 7479.

You should implement DNSSEC to sign SSHFP records, however, DNSSEC validation isn't mandatory for clients.

### TLSA
TLSA means "Transport Layer Security Authentication". TLSA resource records can be used to specify the keys used in a domain's TLS servers as a way to authenticate TLS servers without a certificate authority (CA). It is defined in RFC 6698 "The DNS-Based Authentication of Named Entities (DANE) Transport Layer Security (TLS) Protocol: TLSA".

DNS-based Authentication of Named Entities (DANE) has the goal of developing protocols and techniques that allow internet applications to establish cryptographically secured communications with different protocols based on DNSSEC.

As of December 2018, there are no widespread clients with support for TLSA. Due to this, we didn't implement TLSA and don't discuss it in this article.

### TXT
TXT (text) records can be used to add arbitrary information to the DNS zone. For instance, Keybase users can use DNS TXT records to make their websites verifiable.

Open your terminal and enter {{< kbd "keybase prove dns [your-domain-name]" >}}. You get a verification string that looks like:

{{< samp "keybase-site-verification=[characters]" >}}

Add this to your DNS zone by selecting "Type: TXT", "Host: @" and "Destination: keybase-site-verification=[characters]" (example for netcup users). Keybase will regularly check for this record.

## Testing the configuration {#test}
There are several websites that provide tests. For example, DNSSEC can be tested (and debugged) using the [DNSSEC Analyzer by Verisign]({{< relref "#links" >}}). For other tests, simply use your favorite search engine to find such websites.

Furthermore, you can use the terminal of your local Linux client to test your configuration:

* CAA: {{< kbd "dig +short CAA [your-domain-name]" >}} returns your CAA records. No result means that there are none.
* DNSSEC:
  * {{< kbd "dig +short +dnssec NS [your-domain-name] | grep \"NS\"" >}} returns nothing if there are no NS records. If NS records are present, this command shows your NS records plus RRSIGs.
  * {{< kbd "dig +short DNSKEY [your-domain-name]" >}} shows the DNSKEYs of your domain name. No result means that there is none.
* TXT: {{< kbd "dig +short txt [your-domain-name]" >}} shows TXT records like the Keybase verification string mentioned above.

{{< wssbox >}}

## Summary
Keep in mind that DNS resource records must be supported by clients to be effective. This is also true for HTTP response headers as mentioned in previous parts of this series. Adding arbitrary DNS resource records that aren't supported by clients doesn't add security at all.

Furthermore, the structure of resource records may change over time, so regularly check whether you need to update your records.

## External links {#links}
* {{< extlink "https://dnssec-debugger.verisignlabs.com/" "VERISIGN LABS – DNSSEC Analyzer" >}}
* {{< extlink "https://caatest.co.uk/" "DNS CAA Tester" >}}
* {{< extlink "https://openpgpkey.info/" "OPENPGPKEY.info (German)" >}}
* {{< extlink "https://www.iana.org/assignments/dns-sec-alg-numbers/dns-sec-alg-numbers.xhtml" "IANA – Domain Name System Security (DNSSEC) Algorithm Numbers" >}}
