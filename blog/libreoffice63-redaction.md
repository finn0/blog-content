+++
title = "How to redact sensitive information with LibreOffice 6.3+"
author = "Jakub"
date = "2019-11-10T09:36:24+01:00"
tags = [ "libreoffice", "redaction" ]
categories = [ "tutorial" ]
ogdescription = "This tutorial introduces the new redaction feature of LibreOffice."
slug = "libreoffice63-redaction"
banner = "banners/libreoffice63-redaction"
notice = true
toc = true
+++

LibreOffice 6.3 contains a new "redaction" feature. This feature allows editors to quickly redact (= remove) sensitive or confidential information from documents.

In this article, we show the risks of leaking information in documents, and the new redaction feature of LibreOffice.
<!--more-->
{{< rssbox >}}

## Wrongly redacted means leaked
In general, the process of removing sensitive information from documents is called "sanitization". "Redaction" is about blacking out sensitive text in documents. As a moviegoer, you likely saw sanitized documents since there are shown in many movies about espionage, secret government activities, and other 007s.

However, even as a private individual you may want to remove sensitive information from your documents. For instance, you are a club member and your task is to write and publish meeting minutes. Such meeting minutes might contain sensitive information like names or locations. While this information needs to be there for internal purposes, you likely want to remove it before you publish documents online. This can go wrong as shown in the screenshot below.

{{< webpimg "art-img/libreoffice63-redact-wrong.png" "These Czech meeting minutes contain visually redacted text. However, if you highlight the black parts, you can read the blackened text. (We removed some parts in the screenshot.)" "meeting minutes with blackened text." >}}

In fact, you leak sensitive information if it is not correctly redacted. In 2014, the New York Times accidentally exposed the name of an NSA agent due to the same error.

## The redaction feature of LibreOffice {#feature}
LibreOffice 6.3 introduced a new feature to help you redacting content. The feature is available in multiple applications of LibreOffice, e.g., in Writer, Calc, and Impress. In the following, we show the feature in Writer.

### Step 1: Start the redaction feature {#s1}
After writing or opening your document, which contains sensitive information, start the redaction feature. The location of it depends on your user interface, for example:

* User Interface Standard Toolbar: Go to "Tools". There you find "Redact".
* User Interface Tabbed: Go to "Review". There you find "Redact".

{{< webpimg "art-img/libreoffice63-redact1.png" "Step 1: You have your document that contains sensitive information. Open the redaction tool." "an arbitrary document." >}}

### Step 2: Select the appropriate tool {#s2}
After clicking on "redact", a new window is opened since the redaction tool actually starts LibreOffice Draw, and a small toolbar. Here you can select (from left to right):

* Rectangle Redaction
* Freeform Redaction
* Redacted Export (exports a sanitized version)
* Export Directly as PDF (exports an insecure version for review purposes, see [warning in step 4]({{< relref "#s4" >}}))

To redact content, either use the Rectangle Redaction or Freeform Redaction tool.

{{< webpimg "art-img/libreoffice63-redact2.png" "Step 2: The redaction tool actually starts LibreOffice Draw and a small toolbar. Here you can select either Rectangle Redaction or Freeform Redaction." "the redaction tool of LibreOffice." >}}

### Step 3: Redact sensitive information {#s3}
After selecting the appropriate tool, redact your text. In the screenshot, we used _Rectangle Redaction_. Normally, these boxes are 50% transparent, and automatically made opaque later.

{{< webpimg "art-img/libreoffice63-redact3.png" "Step 3: You simply add some rectangles on top of the text. These rectangles are 50% transparent in the beginning, so you can see what is below them. Later, LibreOffice automatically makes them opaque. In the screenshot, they are already opaque." "a document with redacted text." >}}

### Step 4: Export your redacted version {#s4}
Finally, you export your document to a PDF file by choosing either _Redacted Export (Black)_ or _Redacted Export (White)_. Depending on your choice, LibreOffice will change transparent redaction markings to opaque black/white shapes.

The result is a PDF file containing sanitized and compressed images of your text. Keep in mind that there is no selectable text in it.

{{< noticebox "warning" "Selecting \"Export Directly as PDF\" also exports a PDF file with opaque shapes, however, if you highlight these shapes, you can read the text below them. This feature is only for reviewing documents after sanitization. Do not publish documents using \"Export Directly as PDF\"!" >}}

{{< webpimg "art-img/libreoffice63-redact4.png" "Step 4: The result is an exported PDF file containing sanitized and compressed images of your text. There is no selectable text in it." "a PDF file with redacted text." >}}

## A simple trick: Redact, print, scan {#rps}
If you don't know how to redact information or if you don't have the right tools for it, simply highlight black text with black color or add black rectangles over the text. Then, physically print your document, and scan it again. Afterward, you also get a sanitized document. Don't forget to check your sanitized version again for sensitive information!

## Summary
If you (or your company) frequently must redact information, define a process for this. Which types of information need to be redacted? How should this information be redacted? How is the sanitized document reviewed?

Raise awareness of wrong redaction and possible data leaks. Moreover, keep in mind that even a correctly sanitized document may leak information unintentionally due to metadata, context of redacted information, and/or the length of redacted passages in the text.

Happy sanitizing, and never forget to check your sanitized version again for sensitive information!

## External links {#links}
* {{< extlink "https://wiki.documentfoundation.org/ReleaseNotes/6.3" "LibreOffice 6.3: Release Notes" >}}
