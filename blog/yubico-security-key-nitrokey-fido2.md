+++
title = "Yubico Security Key vs. Nitrokey FIDO2"
author = "Benjamin"
date = "2019-12-22T14:16:38+01:00"
tags = [ "2fa", "password", "yubikey", "nitrokey", "u2f", "fido2", "webauthn" ]
categories = [ "authentication" ]
ogdescription = "The Yubico Security Key and Nitrokey FIDO2 are affordable solutions to make identity management on the internet less painful."
slug = "yubico-security-key-nitrokey-fido2"
banner = "banners/yubicosecuritykey-nitrokeyfido2"
toc = true
+++

WebAuthn, _Web Authentication: An API for accessing Public Key Credentials Level 1_, is a W3C Recommendation released in March 2019. It defines creation and use of strong, attested, scoped, public key-based credentials by web applications. It is very similar to the [Universal 2nd Factor (U2F)]({{< ref "/glossary.md#u2f" >}}) standard, but extended and customized for online services.

Security devices with WebAuthn support allow you to use [two-factor authentication]({{< ref "/glossary.md#2fa" >}}) more easily since they contain a secret key that provides a second factor only by pressing the device's button. They can also be used as a single factor in some cases, storing your credentials for you.

We already compared the [Yubico Security Key and Nitrokey FIDO U2F]({{< ref "/blog/yubico-security-key-nitrokey-u2f.md" >}}). In this article, we again compare the Yubico Security Key with the Nitrokey FIDO2, the successor of the Nitrokey FIDO U2F. If you need more features like support for OpenPGP, read our comparison ["YubiKey 4C vs. Nitrokey Pro: Stalemate"]({{< ref "/blog/yubikey4c-nitrokeypro.md" >}}).
<!--more-->
{{< rssbox >}}

## The basic idea of WebAuthn
The basic idea of WebAuthn is very similar to U2F as described in [Yubico Security Key and Nitrokey FIDO U2F]({{< ref "/blog/yubico-security-key-nitrokey-u2f.md#u2f-function" >}}).

A new concept of WebAuthn is the "Client-side-resident Public Key Credential Source", or **resident credentials**/resident keys. If you use resident keys, your security token stores a private key for an online service. Using resident credentials allows securely logging in without entering any other credentials like passwords. However, unlike U2F, the token needs persistent memory. Therefore, the number of resident credentials that can be stored on a security token is limited. Some devices allow you to protect resident credentials with a PIN or biometrics. In theory, online services can implement this to get rid of passwords and usernames. Logging into an online services could consist of only pressing the physical button of your token in this case. In this ideal world, compromising an online service would only leak public keys stored by the service. Attackers can't use the public keys for anything. We don't know any real-world online services that already implemented this, though. This might change in the years ahead.

## Yubico Security Key and Nitrokey FIDO2 in comparison {#details}
YubiKeys as well as Nitrokeys are supported by most common operating systems. You don't have to install additional software to use U2F tokens except a web browser with U2F support. Nowadays, U2F is supported by most web browsers, however, it is already legacy technology superseded by the [W3C WebAuthn API]({{< ref "/glossary.md#webauthn" >}}). WebAuthn is backward compatible with U2F.

{{< img "art-img/yubico-security-keys-nitrokey-fido2.jpg" "A Nitrokey is around twice as big as a Yubikey. Multiple Nitrokeys on the same keychain can become annoying on a daily basis." "a Nitrokey and two Yubico Security Keys on a keychain." >}}

Both the Yubico Security Key and Nitrokey FIDO2 support unlimited U2F credentials, and unlimited WebAuthn credentials if you use them like U2F. When it comes to storing resident credentials (RC), the Nitrokey FIDO2 can store twice as many keys as shown in the table below:

| Standard     | Yubico Security Key | Nitrokey FIDO2     |
|--------------|---------------------|--------------------|
| U2F          | unlimited accounts  | unlimited accounts |
| WebAuthn 2FA | unlimited accounts  | unlimited accounts |
| WebAuthn RC | up to 25 accounts   | up to 50 accounts  |

### Yubico Security Key
YubiKeys are produced by Yubico which was founded in 2007 and is based in the USA and Sweden. All current YubiKeys support WebAuthn.

In April 2018, Yubico introduced the Security Key that is less expensive than other YubiKeys while only supporting U2F and WebAuthn. The physical design is identically equal to USB-A YubiKeys. Like other YubiKeys, it is shipped containing closed-source firmware.

The Yubico Security Key supports U2F and WebAuthn (included in FIDO2). It can store up to 25 resident credentials. 3 out of 25 can be RSA-based credentials, all other credentials must be based on [elliptic-curve cryptography]({{< ref "/glossary.md#ecc" >}}). There is also an NFC version of the Security Key.

The official [Yubikey Manager]({{< ref "/terminal-tips.md#ykman" >}}) can be used to reset all U2F and WebAuthn secrets. It can also be used to list resident credentials, and set a PIN (4 and 128 characters) to protect them. If you enter a wrong PIN 8 times in a row, your resident credentials are permanently locked, and you need to reset the FIDO2 application of the token. In this case, you lose all of your resident credentials. You also need to re-register with U2F websites.

### Nitrokey FIDO2
Nitrokeys are produced by Nitrokey UG which was founded in 2015 and is based in Germany. The people behind Nitrokey actually developed a predecessor, called Crypto Stick (2008–2010).

The Nitrokey FIDO2 is based on the Solokey. The differences are another casing and touch sensor.

In November 2019, Nitrokey UG released the Nitrokey FIDO2. It is the second Nitrokey with U2F support and doesn't support other technologies like [OpenPGP]({{< ref "/glossary.md#openpgp" >}}). In contrast to YubiKeys, you must buy a Nitrokey FIDO2 and another Nitrokey if you want to use more features (not only U2F or OpenPGP etc.). The physical design of the Nitrokey FIDO2 is identically equal to the Nitrokey Pro, or Nitrokey FIDO U2F. Like other Nitrokeys, it is shipped containing open-source firmware and open hardware.

The Nitrokey FIDO2 allows you to update its firmware in theory. Currently, there seems to be no management software for the Nitrokey FIDO2. So there is no application to update its firmware.

The same is true for setting a PIN to protect resident credentials: There is no official application. Ironically, we used the Yubico demo website (via Chromium 79) to set a PIN on our Nitrokey FIDO2. Firefox 71 didn't work. According to the Nitrokey support forum, you can also directly set a PIN in Chrome/Chromium or Windows 10.

The Nitrokey FIDO2 doesn't have an AAGUID, a 128-bit identifier indicating the type of the token. The AAGUID is required for attestation and defined by WebAuthn. Additionally, it fails several tests written by the original Solokey project. We don't know whether these issues result in practical drawbacks at the moment, however, there are some users reporting that they can't store resident credentials on their Nitrokey FIDO2.

In summary, the Nitrokey FIDO2 is quite similar to the Yubico Security Key, but there are some unresolved issues. There is no NFC version of this token, but it seems to be planned for 2020.

## Benefits
WebAuthn tokens are based on [public-key cryptography]({{< ref "/glossary.md#public-key-cryptography" >}}). Due to this, online services never learn or store any secrets. The secret key of the token or resident credentials can't be extracted, and cryptographic operations are performed by the token. To avoid unwanted operations by the token, users must press the device's button to authorize a single operation. This is far more secure than using apps on mobile devices for [two-factor authentication]({{< ref "/glossary.md#2fa" >}}), and more convenient than entering a one-time password (like OATH-TOTP). Registering and logging in consists of pressing a button.

Moreover, web browsers check the identity of websites using their [certificates]({{< ref "/glossary.md#certificate" >}}) and websites have to provide the correct key handle. This offers basic [phishing]({{< ref "/glossary.md#phishing" >}}) protection, because wrong key handles result in wrong checksums. Therefore, further cryptographic operations are aborted.

Furthermore, a manipulated web browser or operating system can't access the secret key of the token. They could only log successful login attempts but the challenge involved protects against [replay attacks]({{< ref "/glossary.md#replay-attack" >}}).

Finally, your token can't be fingerprinted by a website since there is no unique property that is transmitted to the website.

## Problems
Of course, there are some problems:

First of all, WebAuthn requires a client with WebAuthn support. As of today, most major web browsers support WebAuthn, which is backward compatible with U2F. However, if your web browser doesn't support it, you can't use your WebAuthn token. The same is true for server-side support: Many websites don't support WebAuthn, but seemingly WebAuthn becomes more wide-spread.

Secondly, WebAuthn tokens aren't for free. The Yubico Security Key costs about €20, while the Nitrokey FIDO2 costs €29. Given the fact that you can use them for an unlimited amount of accounts, the price is manageable. If resident credentials become widespread, a Nitrokey FIDO2 can store twice as many credentials, making it more cost-effective.

Thirdly, if you lose your token, you lose access to your accounts. Some websites require you to also configure [OATH-TOTP]({{< ref "/glossary.md#oath-totp" >}}) as a backup solution. More secure is a second WebAuthn token as a backup token. Most websites should allow you to register multiple WebAuthn tokens.

Fourthly, if someone steals your token, you lose access to your accounts and this person may access your accounts if other credentials are also known to him. Resident credentials can be PIN-protected.

Sometimes, WebAuthn secrets are generated by the manufacturer of the security token, then stored on the device and can't be replaced afterward. In theory, manufacturers could copy your secret WebAuthn key during manufacturing.

Finally, most WebAuthn tokens only come with an LED indicating that you should press its physical button. There is no display that shows the operation that you should authorize. This can be confusing.

## Conclusions
The main difference is that the hardware/firmware of YubiKeys is closed source while Nitrokeys are based on open hardware/open-source software. Both producers argue convincingly that their philosophy is better. Both WebAuthn tokens are almost identical in size as mentioned in the other posts.

Currently, we recommend buying a Yubico Security Key. It is cheaper, it comes with an official management application, and we didn't encounter any problems so far. If you want to buy a Nitrokey, buy the Nitrokey FIDO2. There is no reason to buy its predecessor, the Nitrokey FIDO U2F.

Just as a footnote, some operating systems started supporting WebAuthn, so you don't need any additional tokens in this case. We don't recommend such solutions. Use a dedicated security token.

## Sources
* {{< extlink "https://www.yubico.com/" "Yubico" >}}
* {{< extlink "https://www.nitrokey.com/" "Nitrokey" >}}
* {{< extlink "https://github.com/Nitrokey/nitrokey-fido2-firmware" "Nitrokey FIDO2 firmware" >}}
* {{< extlink "https://demo.yubico.com/playground" "Yubico demo website" >}}
* {{< extlink "https://www.dongleauth.info/" "List of websites and their OTP/U2F/WebAuthn support" >}}
