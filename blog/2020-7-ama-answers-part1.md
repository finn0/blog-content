+++
title = "Ask Us Anything (AMA): readers ask, we answer – Part 1"
author = "Benjamin"
date = "2020-07-14T17:25:20+02:00"
tags = [ "ama" ]
categories = [ "Ask Us Anything" ]
ogdescription = "We categorize and collect some questions by readers and our answers."
slug = "2020-7-ama-answers-part1"
banner = "banners/ish-ama"
toc = true
notice = true
+++

In July 2020, we hosted our first AMA (Ask Us Anything) event. In this article, we categorize and collect some questions and our answers.
<!--more-->
{{< rssbox >}}

Our first AMA event ended on July 23, 2020. Readers sent their questions via [e-mail]({{< ref "/contact-details.md" >}}) or in the Fediverse.

{{< noticebox "info" "We rephrased some questions to point to the crucial parts." >}}

## General questions about information security
### "What is the difference between IT security, cyber security, OT security, and information security?"
All of these terms are interchangeable but may describe some particular aspects:

* **Information security** (InfoSec) is the umbrella term describing that you want to protect information (so you aren't only focused on technical aspects, but also on processes and humans).
* **IT security** focuses on technical aspects of information technology (e.g., how to secure network traffic in an office, or how to secure a PC?).
* **OT security** is focused on technical aspects of operational technology (e.g., how to secure network traffic in industrial production lines, or how to secure programmable logic controllers?)
* **Cyber security** is just another term for information security. Some people consider this term more modern than InfoSec; others hate it.

### "How do you communicate with others securely?"
We mostly use the Signal messenger.

### "Which e-mail provider do you recommend?"
We do not recommend any particular e-mail provider. E-mail remains an insecure medium, even if you add some encryption on top. If you don't trust your e-mail provider, you can use OpenPGP implementations like GnuPG to encrypt/decrypt the content of your e-mails offline. We recommend using a modern instant messenger instead of e-mail.

### "Are password managers trustworthy? Which one do you recommend?"
We use KeePass 2 and KeePassXC (Simplified explanation: KeePassXC is based on KeePass 2. Some features are identical; some features differ). Both password managers can be used to manage passwords offline. You can also put the kdbx file (the file that contains your password database) on a local shared drive.

_Are they trustworthy?_ Especially KeePass 2 is a well-known password manager that got audited. So, yes, we think both are trustworthy.

### "Which operating system is the most secure?"
The most secure operating system has no security vulnerabilities, no connections to anything, and all data is permanently encrypted. Of course, this doesn't make sense; however, the answer to this question depends on a threat model.

When it comes to a "high level of technical security," then operating systems like Qubes OS can be appealing. Keep in mind that the human factor can undermine technical security, and even Qubes OS isn't "perfectly" secure.

In general, we recommend installing a minimal operating system (= only installing the software you need) and disabling any service and interface that you don't need.

### "What is the most secure? AES, Twofish, or Serpent?"
The technical differences between them don't matter for real-world use cases. However, we recommend AES since it is well-tested, accelerated by hardware implementations, and "state of the art." Keep in mind that AES can still be insecure due to insecure cipher modes (e.g., ECB), vulnerable implementations, or weak keys.

### "Why are companies hacked when they are ISO 27001 certified?"
The ISO/IEC 27001 is a standard that specifies a management system for information security (ISMS). The management of information security is required to organize security controls, define and revise processes, and conduct risk management continuously. Information security management is as vital as implementing technical security measures or securing the human factor. However, having a certified ISMS isn't sufficient to "be secure." On the other hand, random, disorganized technical security measures are also not enough. Since you can't ever reach a state of 100% security, there is always the likelihood of being hacked.

## Questions about web security
### "Which is more secure? The Tor Browser, Firefox ESR, or Firefox?"
There is no simple answer here (as always). Firefox (without ESR) comes with new security features; however, these new features may introduce additional security vulnerabilities. Firefox ESR delays these new features while getting frequent security updates. The Tor Browser is a customized version of the Firefox ESR; however, there are considerations to switch from Firefox ESR to Firefox.

### "Which web browser add-ons do you recommend?"
Primarily uBlock Origin. We recommend that you reduce the number of add-ons. Every add-on living in your web browser can introduce new security vulnerabilities or spy on your internet traffic.

### "I want to start my career in web application security; how to start?"
* Understand basic network concepts like the TCP/IP model.
* Understand common security vulnerabilities and mitigations (see OWASP Top Ten Web Application Security Risks).
* Train your skills (e.g., see OWASP Mutillidae II and HackTheBox).

### "Should I use GPG? What is better? GPG or PGP?"
In our opinion, OpenPGP (including GPG) is still to complex for most people. If there is no particular reason for you, we don't recommend that you set it up.

_What is better? GPG or PGP?_

* **PGP** is proprietary software, initially developed by Phil Zimmermann (American cryptographer). Nowadays, Symantec owns PGP.
* **OpenPGP** is an open standard, introduced several years after PGP was made. It specifies the use of different algorithms like SHA-1, AES-128, or RSA for e-mail security.
* **GPG** (GNU Privacy Guard) is an open-source implementation of OpenPGP, available for most platforms, and supports elliptic curve cryptography.
* Other implementations/libraries are more or less compliant with OpenPGP (e.g., NeoPG, Sequoia PGP, or OpenPGP.js).

So, this isn't directly comparable.

## Questions about mobile security
### "Is my LineageOS device secure?"
There is also no binary answer to this. In your typical smartphone are lots of different hardware chips. They run their own firmware (e.g., cellular baseband, cellular RF receiver, Bluetooth, WLAN, GPS, SIM card, SD card, screen, power supply). Your smartphone operating system like Android or iOS runs on top of this, using its dedicated application processor. As always, software may contain security vulnerabilities, and migrating from Android to LOS doesn't fix security vulnerabilities in your WLAN chip, for example. Besides, all current standards for mobile connectivity (3G, 4G, 5G) come with weaknesses, Bluetooth includes vulnerabilities, and WPA (securing your WLAN) contains flaws.

### "What do you think about the Librem 5?"
We won't use it. The last reports we read didn't sound promising.

### "Which messenger is the most secure?"
There is no "secure" messenger without a threat model. See ["What is 'secure'?"]({{< ref "/blog/discussion-secure.md#sm" >}})

## Questions about network security
### "Which router do you recommend?"
We recommend the Turris Omnia. It is a modular router manufactured by a Czech company, mostly consisting of open hardware and running open-source software.

### "Is the Turris Omnia secure?"
As written before, nothing is "secure." Software may contain security vulnerabilities, cryptography may become outdated, or the current configuration may increase the attack surface. However, in comparison with other routers, the Omnia gets frequent security updates, its operating system is based on the well-supported OpenWrt, and you can further harden the router by disabling services.

### "Should I buy a new router that supports WPA3?"
If your router supports WPA2-PSK-CCMP, then you can still use it. Set a long and random password (up to 63 characters). You can use QR codes to share this password with your devices (e.g., smartphone).

If you need a new router, then you should buy one that supports WPA3-SAE. Keep in mind that some of your devices may not support WPA3-SAE at this time. We recommend setting a strong password, too. See the [section "WLAN" in part 1 of our Home network security series.]({{< ref "/blog/hns1-hello-world.md#wlan" >}})

### "Which VPN provider should I use?"
We do not recommend any particular VPN provider. In general, don't use VPN providers for "anonymity." (Use the Tor network.) If you look for a VPN provider, choose one that allows you to use open-source clients (instead of provider-specific software).

## General questions about the InfoSec Handbook
### "Where are you from?"
We are from different European countries (Czech Republic, Germany, Austria). See the map below:

{{< picture "img/ish-contributor-map.png" "Locations of contributors of the InfoSec Handbook" >}}

### "I want to start a blog; what should I do first?"
From a security perspective, try to avoid content management systems since they come with a large attack surface, especially WordPress (literally the Windows of content management systems). Try to use [static website generators like Hugo]({{< ref "/blog/static-blogging.md" >}}). If you run your own server, [secure it, and keep it secure](/as-wss/).

Additionally, minimize processing of personal data of your readers, and provide a comprehensive privacy policy. Keep in mind that there may be more legal requirements in some countries when you run your own website.

### "You say ABC, but other bloggers say XYZ. What is true?"
Information security is a vast topic. For instance, many good practices in IT security (e.g., office environments) don't work in OT security (industrial environments). An incident responder has other tasks than somebody who analyzes mobile apps. Developing security concepts for IT services differs from developing secure network protocols … If you consider this, you see that there isn't a binary answer in most cases. Therefore, other people may recommend different solutions.
