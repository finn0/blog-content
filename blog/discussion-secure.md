+++
title = "What is 'secure'?"
author = "Benjamin"
date = "2018-09-02T11:19:01+02:00"
lastmod = "2019-06-04"
tags = [ "privacy", "encryption", "audit" ]
categories = [ "discussion" ]
ogdescription = "Many products and services claim to be 'secure'. What does this mean?"
slug = "discussion-secure"
banner = "banners/discussion-secure"
toc = true
+++

Secure crypto, secure passwords, secure messaging, secure e-mail, secure browsing—we see "secure something" everywhere, but no one defines this term. On closer inspection, "secure" even becomes a catchword most of the time. We discuss different examples in this article.
<!--more-->
{{< rssbox >}}

## The "secure" something {#something}
Let's have a look at different topics and the term "secure" in their context:

### Secure cryptographic algorithms {#sa}
There are at least two golden rules in cryptography:

* Never roll your own cryptography.
* Only use well-known and audited cryptographic algorithms.

We can say that "secure" means "unbreakable." "Unbreakable" is relative to the capabilities of an attacker. A single attacker probably doesn't have the technical resources, the knowledge, or the time to break cryptography, while an intelligence agency may be capable of doing this.

However, "secure" cryptographic algorithms can become insecure due to technological advances over time. For instance, the Data Encryption Standard (DES) was considered "secure" and "unbreakable" once. As computers became faster and faster, its key lengths became insufficient. Therefore, even a single attacker might be able to break DES nowadays.

Even if developers follow the two rules mentioned above, their software isn't automatically secure. There are different reasons for this, for example:

* They implement cryptography incorrectly, making it possible to bypass it.
* They use insecure modes of operation for block ciphers (e.g., ECB).
* They use an outdated crypto library that contains security vulnerabilities.

As a developer, you should always use well-known and audited cryptographic libraries. A third party and vulnerability scanners should audit your complete code before publishing it. Fix any discovered security vulnerabilities, document your changes, and check your changes for side effects. Be aware that [audited software isn't automatically more secure than software without any audit]({{< ref "/blog/myths-software-security.md#m2" >}}).

As an "end user," use well-known cryptographic primitives like [AES]({{< ref "/glossary.md#aes" >}}) instead of unknown algorithms. See also our article on ["Cryptography myths."]({{< ref "/blog/myths-crypto.md" >}})

### Secure passwords {#sp}
Discussing password security is another unloved topic. Ask ten different security experts to get 11 distinct opinions about password length, complexity, and change interval. Some statements from the internet:

* "You only need a strong password for your password manager. Use random passwords everywhere. Changing them isn't necessary."
* "Even the NIST says that you should not change passwords anymore."
* "Don't use a password manager as it is a single point of failure."

From a technical perspective, the "secure" password doesn't exist. A password can be stronger or weaker. Ultimately, you can break any password using [brute force]({{< ref "/glossary.md#brute-force-attack" >}}). Moreover, the technical progress weakens underlying cryptography and, therefore, passwords.

>Use two-factor authentication if avaiable, and stick with a well-known, maintained, and regularly audited password manager.

Keep it simple: Use [two-factor authentication]({{< ref "/glossary.md#2fa" >}}) if available and stick with a well-known, maintained, and regularly audited password manager. We covered credential management (and passwords) in "[Modern credential management: security tokens, password managers, and a simple spreadsheet]({{< ref "/blog/modern-credential-management.md" >}})."

### Secure authentication {#sauth}
Besides passwords, there are many other less-known possibilities to authenticate yourself: Cryptographic keys (e.g., SSH and OpenPGP keys), digital certificates, passphrases, time-based one-time passwords ([OATH-TOTP]({{< ref "/glossary.md#oath-totp" >}})), Universal 2nd Factor ([U2F]({{< ref "/glossary.md#u2f" >}})), or [WebAuthn]({{< ref "/glossary.md#webauthn" >}}).

As before, you can't say that something is "secure" or "insecure." For instance, why is an SMS-based one-time password insecure or secure? Is a wannabe hacker from your neighborhood able to intercept an OTP by "hacking" your device or the cellular network? This may be very unlikely. Contrary to this, an intelligence agency or law enforcement agencies may easily bypass this by directly accessing the server—no authentication is needed in this case.

Many attacks are primarily successful due to [social engineering]({{< ref "/glossary.md#social-engineering" >}}), not due to successful technical exploitation. It doesn't matter whether you send your SMS-based or app-based OTP to an attacker due to successful social engineering. Even if you do everything right, the server's connection could be compromised, or—as already mentioned—an attacker could directly access the server-side database.

>Define typical attackers for your use cases and deploy security as needed.

Hence, as before, "security" depends on your threat model. Define typical attackers for your use cases and deploy security as needed.

### Secure messaging {#sm}
The Electronic Frontier Foundation (EFF) published a Secure Messaging Scorecard in 2014. The scorecard consisted of:

* transport encryption
* [end-to-end encryption]({{< ref "/glossary.md#end-to-end-encryption" >}}) (E2EE)
* verification of identities
* [forward secrecy]({{< ref "/glossary.md#forward-secrecy" >}})
* proper documentation of the security design
* third-party audits
* recent code audit

In 2018, the [EFF told us]({{< relref "#sources" >}}) that there "is no such thing as a perfect or one-size-fits-all messaging app" because the scorecard "oversimplified the complex question of how various messengers stack up from a security perspective" and "it wasn’t possible for [the EFF] to clearly describe the security features of many popular messaging apps, in a consistent and complete way, while considering the varied situations and security concerns of [the EFF's] audience."

As a result, they didn't update their scorecard. The EFF published a series of articles to discuss the nature of "secure" messaging so users can develop "an understanding of secure messaging that is deeper than a simple recommendation."

We think "security" means "somewhat encrypted" for most users. However, there is a big difference between "encrypted client-server communication" and "end-to-end encrypted communication," for example. Then, there is optional encryption with unencrypted fallback in some messengers or messengers that store tons of [personal data]({{< ref "/glossary.md#personal-data" >}}) in cleartext on servers exposing this data to admins and server-side attackers.

The EFF also says that an "app with great security features is worthless if none of your friends and contacts use it, and the most popular and widely used apps can vary significantly by country and community." They argue that E2EE is "great for preventing companies and governments from accessing your messages." However, "if someone is worried about a spouse, parent, or employer with physical access to their device, the ability to send ephemeral, 'disappearing' messages might be their deciding factor in choosing a messenger."

>To sum up, we can say that there is no one-size-fits-all "secure" messenger. Different users want different security features.

Another EFF article lists some security features:

* end-to-end encryption (many messaging apps use or are based on the Signal protocol)
* code quality (using "secure" algorithms [doesn't mean that they are implemented securely]({{< relref "#sa" >}}))
* user experience (can users easily send and receive encrypted messages?)
* service availability
* encrypted cloud backups (some messengers store unencrypted backups on the internet, rendering E2EE useless)
* secure auto-updating mechanisms
* messenger of sufficiently high popularity that its use is not suspicious
* indicators of compromise that are recognizable to an end-user
* verification of identities
* aliases instead of phone numbers
* avoidance of network [metadata]({{< ref "/glossary.md#metadata" >}})
* contact discovery without disclosing your contacts to the service provider
* reproducible builds
* binary transparency
* the same level of security even in group chats

No instant messenger provides all of these features. You have to be aware of these features and find the messenger which fits your needs.

### Secure e-mail {#se}
If you look for "secure" e-mail, you will mostly find implementations of [OpenPGP]({{< ref "/glossary.md#openpgp" >}}) like [GnuPG]({{< ref "/glossary.md#gnupg" >}}). Tools like GPG are around for years but lack usability. Some services try to make them more usable by implemented encryption directly in the web browser, but they force you and others to use their services in to benefit from encryption.

Similar to "secure" messaging, most people seem to consider "encrypted" e-mails to be secure, and we face similar problems again:

* End-to-end encryption using OpenPGP isn't widespread.
* OpenPGP isn't easy to use.
* Metadata and personal data stored on servers remain unencrypted.
* [And there are more issues with OpenPGP in general.]({{< ref "/blog/gpg-for-emails.md" >}})

Since messaging apps are far more prevalent when it comes to private communication nowadays, we don't discuss these issues here. However, we think that the term "secure e-mail" is more than vague.

### Secure browsing {#sb}
For most users, "security" in the web browser means [HTTPS/[TLS]({{< ref "/glossary.md#tls" >}}) nowadays. However, the sheer presence of HTTPS/TLS doesn't guarantee "security": Self-signed, revoked, or expired [certificates]({{< ref "/glossary.md#certificate" >}}) are already issues that many non-technical users ignore. Your HTTPS connection can also use insecure TLS protocol versions (< 1.2) or legacy TLS cipher suites.

When we look at other aspects of web security, there is a long list of factors to consider: Cookie security, third-party content, security-relevant HTTP response headers, web server security vulnerabilities, outdated crypto libraries, exposed databases, etc.

Keep in mind that HTTPS is only there to protect data in transit. HTTPS doesn't protect your data stored on the server. It also doesn't protect data in the main memory of your device or the server.

All in all, "secure" browsing is far more than only enabling HTTPS and hoping for the best.

### Secure devices {#sd}
Finally, there is often an ongoing highly charged debate about "secure" devices. Some people state that all smartphones are "insecure"; others tell you that all "internet of things" devices are insecure. However, is your laptop, desktop computer, or server "secure"?

What means "security" in this context? All security updates installed? Full-disk encryption enabled? All unused interfaces disabled? A non-root user account, which is used by default? No known security vulnerabilities in software installed on the device, including firmware?

There is no "secure device" without a proper definition. You (again) must define your threat model and your criteria to classify your devices' security. Blanket statements are frequently misleading and don't consider your specific use cases.

## Summary
"Security" isn't a property that you can turn on or off. Keep in mind:

* Security isn't clearly defined in most cases.
* Security changes over time.
* Security heavily depends on the capabilities of an attacker.
* Security requires a holistic view instead of detached assessments of distinct security features.
* Security isn't "one size fits all."

## Sources
* {{< extlink "https://www.eff.org/deeplinks/2018/03/secure-messaging-more-secure-mess" "EFF: Secure Messaging? More Like A Secure Mess." >}}

## Changelog
* Jun 4, 2019: Added "secure authentication" and "secure devices." Revised the section "secure cryptographic algorithms."
