+++
title = "KeePassXC and YubiKeys – Setting up the challenge-response mode"
author = "Benjamin"
date = "2020-12-12T09:01:41+01:00"
tags = [ "keepassxc", "password", "yubikey" ]
categories = [ "tutorial" ]
ogdescription = "This tutorial shows the setup of YubiKeys for KeePassXC."
slug = "keepassxc-yubikey-challenge-response"
banner = "banners/keepassxc-yubikey"
toc = true
notice = true
syntax = true
+++

In our tutorial for beginners, [we set up KeePassXC]({{< ref "/blog/keepassxc-password-management-basics.md" >}}) for daily use. KeePassXC is a famous open-source password manager, helping you to combat weak passwords and password reuse.

In this tutorial for advanced users, we set up two YubiKeys to further increase your KeePassXC database's overall security.
<!--more-->
{{< rssbox >}}

## Requirements
The following steps are required before proceeding:

1. Set up KeePassXC, as shown in [our KeePassXC tutorial]({{< ref "/blog/keepassxc-password-management-basics.md" >}}). For this tutorial, we use **KeePassXC 2.7.0**. If you install another version of KeePassXC, the setup and usage might differ.
2. Create and save your first KeePassXC database. In the following, we assume that you already have a KeePassXC database.
3. Get at least one YubiKey 5 (or a similar security token). We recommend getting two YubiKeys (one primary, one backup). You need a free configuration slot per YubiKey for this tutorial.
4. Install the "YubiKey Manager" (ykman) to configure the YubiKeys. For this tutorial, we use the **YubiKey Manager 3.1.1**. If you install another version of the YubiKey Manager, the setup and usage might differ.

{{< noticebox "note" "While the original KeePass and KeePassXC use the same database format, they implement the challenge-response mode differently. If you set up the mode in KeePassXC, you can't open the database in KeePass anymore (and vice versa)." >}}

## Configuring the YubiKey(s)
We use the YubiKey Manager to configure the YubiKey(s).

### Configure your primary YubiKey
In the following, we assume that the second configuration slot of your YubiKey is unconfigured and free.

1. Plug in the primary YubiKey.
2. Enter {{< kbd "ykman info" >}} to check its status.
3. Enter {{< kbd "ykman otp info" >}} to check both configuration slots. By default, "Slot 1" is already "programmed."
4. Set up slot 2 for the challenge-response mode: {{< kbd "ykman otp chalresp -t -g 2" >}}. The parameters are "require touching the physical button to generate the response" (-t) and "generate a random secret" (-g).

You should see output similar to the following:

{{< samp "Using a randomly generated key: abcd…6789" >}}
{{< samp "Program a challenge-response credential in slot 2? [y/N]:" >}}

Press {{< kbd "y" >}} to set up slot 2. Done.

### Configure additional YubiKeys
For any additional YubiKey, you need to configure the same secret (the "randomly generated key").

1. Plug in another YubiKey.
2. Enter {{< kbd "ykman info" >}} to check its status.
3. Enter {{< kbd "ykman otp info" >}} to check both configuration slots. By default, "Slot 1" is already "programmed."
4. Set up slot 2 for the challenge-response mode: {{< kbd "ykman otp chalresp -t 2 [secret]" >}}. This time, you need to enter the secret key ("abcd…6789") instead of using the parameter "-g."

You should see output similar to the following:

{{< samp "Program a challenge-response credential in slot 2? [y/N]:" >}}

Press {{< kbd "y" >}} to set up slot 2. Done.

Repeat this for every other YubiKey you want to use as a backup.

## Reconfiguring your KeePassXC database
After setting up the YubiKey(s), we need to reconfigure the KeePassXC database to use the YubiKey challenge-response mode.

{{< noticebox "warning" "It is unlikely that something bad happens. However, we recommend to back up your unmodified database before proceeding." >}}

Reconfiguring your KeePassXC database is straightforward:

1. Plug in any of the prepared YubiKey.
2. Unlock your KeePassXC database by entering the corresponding password.
3. Go to "Database" → "Database Security."
4. Click "Add additional protection…" (see the image below).

{{< webpimg "art-img/keepassxc-basics-11.png" "Besides the password, you can add a key file or YubiKey to protect your database further." "the 'database credentials' dialogue in KeePassXC." >}}

5. Click "Add YubiKey Challenge-Response." KeePassXC should automatically detect your YubiKey, showing "YubiKey [serialnumber] Challenge-Response - Slot 2 - Active Button." If KeePassXC doesn't detect your YubiKey, click "Refresh."
6. Click "Okay."
7. Save your KeePassXC database. Done.

Since you configured the same secret on each YubiKey, you only need to do this step once.

## Testing your new setup
Finally, test your new setup:

1. Lock your KeePassXC database (e.g, press {{< kbd "CTRL" >}} + {{< kbd "W" >}}).
2. Select your database to unlock it.
3. Enter your password and select the YubiKey. You might need to click "Refresh."
4. Click "OK."
5. KeePassXC asks you to press the physical button of your YubiKey. Press it.
6. Use your unlocked database.

If you have two YubiKeys, don't forget to test both.


## Summary
A YubiKey additionally protects the KeePassXC database, depending on your threat model and use cases. We recommend to set up two YubiKeys.

## External links {#links}
* {{< extlink "https://keepassxc.org/" "KeePassXC: Homepage" >}}
* {{< extlink "https://keepass.info/" "KeePass Password Safe 2" >}} (the original KeePass)
