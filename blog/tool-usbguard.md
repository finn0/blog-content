+++
title = "USBGuard"
author = "Benjamin"
date = "2020-03-31T20:53:30+02:00"
tags = [ "usbguard" ]
categories = [ "tutorial" ]
ogdescription = "USBGuard protects against malicious USB devices."
slug = "tool-usbguard"
banner = "banners/ish-tools"
syntax = true
+++

We briefly look at [USBGuard]({{< relref "#external-links" >}}). USBGuard is a Linux-only framework that protects a system against malicious USB devices. The current version is 0.7.6, released in November 2019. USBGuard should be in the repositories of most Linux distributions. Usually, it isn't installed since non-technical users can be confused by this tool.
<!--more-->
{{< rssbox >}}

USBGuard is handy when your system is in an exposed environment. For instance, somebody could connect their USB drive with an unattended or public computer. One option is to remove or disable exposed USB ports physically. However, this solution becomes impractical if you need the USB ports for legitimate USB devices. USBGuard can be used to create an [allowlist]({{< relref "#allowlist" >}}) of authorized USB devices.

After installation, you must enable the USBGuard service. In your terminal, enter {{< kbd "sudo systemctl start usbguard" >}} and {{< kbd "sudo systemctl enable usbguard" >}}. By default, USBGuard allows all USB devices that are currently connected to your machine. The initial allowlist includes internal USB devices!

In your terminal, enter {{< kbd "usbguard list-devices" >}} to see all USB devices that are connected to the system.

If you connect another USB device to the machine, USBGuard blocks it by default. Enter {{< kbd "usbguard list-devices -b" >}}. USBGuard should list the blocked device. You can enter {{< kbd "usbguard allow-device [id]" >}} to allow the device. Moreover, you can enter {{< kbd "usbguard block-device [id]" >}} to block an authorized device on the allowlist.

## Conclusion
USBGuard allows you to define rules specific to the USB ports of your machine. You can also set user- or group-specific rules. We may cover this in an upcoming article.

## External links
* {{< extlink "https://usbguard.github.io/" "USBGuard" >}}
