+++
title = "GDPR: How to identify incomplete privacy policies?"
author = "Benjamin"
date = "2019-06-02T10:41:09+02:00"
tags = [ "gdpr", "privacy-policy", "policy" ]
categories = [ "privacy" ]
ogdescription = "In this guide, we show how to identify incomplete privacy policies."
slug = "guide-privacy-policy"
banner = "banners/gdpr-myths"
notice = true
toc = true
+++

After publishing our article about [incomplete privacy policies of mostly private servers]({{< ref "/blog/one-year-gdpr.md" >}}), several readers asked us for guidance on identifying incomplete privacy policies.

In this guide, we show how you can quickly check if a privacy policy contains mandatory information according to the European GDPR.
<!--more-->
{{< rssbox >}}

{{< noticebox "warning" "Please note: We aren't lawyers and don't pretend to be lawyers. Please do not rely on any information provided below. Contact your data protection lawyer if you have GDPR-specific questions." >}}

## General information according to Article 12 {#info-article12}
We already discussed the question whether privacy policies are always necessary in our previous article. [The answer is "Yes" in many cases.]({{< ref "/blog/one-year-gdpr.md#need" >}})

Let us look at Article 12 of the GDPR to see what is mandatory. According to Article 12 (1) (GDPR), the **data controller** (e.g., web server operator, mail server operator, XMPP server admin) "shall take appropriate measures to provide **any** information referred to in Articles 13 and 14 and **any** communication under Articles 15 to 22 and 34 **relating to processing** to the data subject in a **concise, transparent, intelligible and easily accessible form, using clear and plain language**, in particular for any information addressed specifically to a child".

So, check for the following:

### Do you find a privacy policy at all?
* Some websites checked by us didn't have a privacy policy at all.
* If you can't find one, mandatory information is very likely not provided.

### Does the privacy policy match all general requirements?
* "concise": Is the policy short (= concise) or extremely long and contains lots of vaporing (like telling you about what data controllers don't do with your [personal data]({{< ref "/glossary.md#personal-data" >}}) instead of telling you what they do!)
* "transparent": This is hard to determine. In general, check whether information according to Articles 13 and 6 is provided (as shown below). If the policy only consists of several sentences or tells you that no personal data is ever processed, then something is wrong.
* "intelligible and easily accessible form": Is the policy easy to understand? Can you easily find it without searching for hours? Bad signs are well-hidden policies with embedded JavaScript to obfuscate information.
* "using clear and plain language": Is the policy written in everyday language or in legalese? Legalese is obviously bad since the privacy policy's purpose is to inform affected people and not to satisfy law students.

If the privacy policy doesn't match some of these requirements, become suspicious. It is your personal data at last.

As written above, the privacy policy must include "**any** information referred to in Articles 13 and 14 and **any** communication under Articles 15 to 22 and 34". So, check this afterward.

## Information according to Articles 13 and 6 {#info-article13-article6}
Article 13 tells you about information data controllers must provide if they get your personal data **directly** from you. Examples are:

* You access their service (e.g., website, forum, XMPP server, mail server) while they log your IP address and user-agent string. _Actually, logging isn't necessary here since the GDPR speaks about "processing", which means doing anything with personal data!_
* You access their website while they use APIs of your web browser to get unique identifiers for marketing purposes.
* You fill out an HTML form and send personal data like your e-mail address to the server.
* You create and use an account on a server provided by someone on the internet.

The basic rule of thumb is: As soon as you make us of anything on the internet, which isn't provided by your friends or family, you very likely transfer your personal data to a data controller (wittingly or unwittingly). Data controllers are not only international conglomerates but also private individuals who provide small decentralized services, which can be used by other people.

{{< noticebox "info" "Contrary to Article 13, Article 14 is about information data controllers must provide if they get your personal data indirectly! We won't discuss Article 14 in this article. Check the GDPR for further information." >}}

Check for the following to see whether the privacy policy matches requirements of Article 13:

### Does the privacy policy contain contact details?
* There is no clear definition of "contact details" in the GDPR. However, we think the policy should at least contain a valid e-mail address.
* Some countries like Germany or Austria require website operators to provide additional information like their name, full physical address (no P.O. box!), and a second fast channel for communication (e.g., phone number registered in the country of the website operator). Check for this, if applicable.
* Besides, for-profit operators need to provide additional information like their trade ID.

### Does the privacy policy contain contact details of a data protection officer (DPO)?
* Public authorities or public bodies need a DPO (expect "courts acting in their judicial capacity").
* Data controllers need a DPO if one of their regular core activities is processing personal data on a large scale.
* Data controllers need a DPO if one of their regular core activities is processing personal data of special categories (ethnic origin, political opinions, religious or philosophical beliefs, trade union membership, genetic/biometric data, health data, data about sex life/sexual orientation) or data relating to criminal convictions and offenses.
* Some countries may require DPOs in more scenarios. For instance, in Germany companies need a DPO if at least 10 employees regularly process personal data. Check §38 of the BDSG (Germany's federal data protection act) for more information.

### Are purposes of processing personal data clearly defined?
* This check requires that the data controller actually lists all cases of processing data (full transparency).
* Dependent on the type of server, check for information about IP addresses, names (user names, real names), addresses (physical addresses, e-mail addresses), phone numbers, third parties involved (e.g., server providers, hosting companies, other services that get your personal data), and so on.
* If a data controller also uses your data for other purposes than originally defined, there must be information about these additional processing activities provided prior to actually processing the data for these other purposes.

### Does the data controller clearly inform about the legal basis for processing?
* Article 6 lists six general reasons for legally processing data. Article 13 requires data controllers to explicitly list such reasons for every data processing activity.
* If the data controller justifies processing with your consent (Article 6 (1) a), there must also be information about your right to withdraw your consent at any time!
* If the data controller justifies processing with reference to contracts (Article 6 (1) b) or legal requirements (Article 6 (1) c), data controllers must tell you if you are obliged to provide personal data and any consequences if you don't provide it.
* If the data controller justifies processing with the controller's legitimate interest (Article 6 (1) f), there must also be information about every interest (not only the reference to Article 6)!

### Does the data controller list all third parties involved in data processing?
* For instance, is information about hosting companies, server providers, content delivery networks etc. provided?
* The GDPR requires either listing their names or their categories.
* While tools like [Webbkoll]({{< ref "/limits-webbkoll.md" >}}) help you to identify some third-party dependencies, you can't identify all of them. For example, you won't be able to identify the whole server infrastructure behind a web application. While the application accessed via your web browser can be hosted on a European server, your personal data can actually be stored on several international database servers that are only visible to the application server, not to your web browser.

### Is information about data transfer to non-EU states or international organizations provided?
* If the data controller transfers data to non-EU states or international organizations, there must be information about the existence or absence of an adequacy decision by the EU Commission.
* Dependent on the type of transfer, data controllers must provided more information (see Article 13 (1) f of the GDPR).

### Does the privacy policy tell you about the period of time for which your personal data is stored?
* This can be stated in absolute values (e.g., one month) or as criteria used to determine this period.
* Check if the period looks reasonable for you. For instance, a storage period of one year for debugging purposes looks suspicious.

### Does the privacy policy mention your rights according to the GDPR?
* Your rights are defined in Articles 15–21 and 77.
* It is not clearly defined by the GDPR to what extent data controllers have to list these rights. However, check if they are mentioned at least.
* National privacy laws/regulations may add more rights!

### Does the privacy policy contain information about automated individual decision making (including profiling), or the logic involved?
It is hard to check if the data controller makes use of this.

If most requirements aren't met, carefully check the whole service covered by the policy.

## Your rights
The Articles 15 to 21 contain your rights (e.g., access, rectification, erasure, restriction of processing, notification obligation, data portability, object). We won't go into detail here. Keep in mind that not only the GDPR contains information about your rights but also national laws.

So, check the GDPR and your national privacy law(s) to get an idea of all of your rights. You explicitly have the right to "lodge a complaint with a supervisory authority, in particular in the Member State of [your] habitual residence, place of work or place of the alleged infringement if [you consider that processing your personal data] infringes [the GDPR]" (see Article 77 (1)).

{{< noticebox "info" "Our guide isn't about identifying \"violations of the GDPR\" (or any other privacy laws/regulations), but about identifying information that is likely not provided by website operators. It is not up to private individuals to determine such violations. This is the job of the judiciary in most countries." >}}

Instead of jumping on the "GDPR violations everywhere" bandwagon, contact the responsible data controller first. Ask about information that is likely missing. Maybe, the data controller only forgot to add this. Nobody is perfect at last.

However, if there is no reaction and the data controller obviously ignores requests while processing personal data on a large scale, lodge a complaint with a supervisory authority. The judiciary has to decide if the data controller violates the GDPR (and/or other privacy laws/regulations). This isn't the job of online forums or blogs since laws and regulations are oftentimes very complicated and matter of interpretation.

## Quick check
Finally, we provide a very basic quick check for you:

1. Is there a privacy policy that actually covers the service you check?
2. Is the policy written in everyday language and tells you about what is done with your personal data (not about what isn't done with it)?
3. Does the privacy policy contain contact details (e.g., an e-mail address)?
4. Does the privacy policy clearly mention the purposes of data processing?
5. Does the privacy policy clearly mention the legal basis for processing?
6. Does the privacy policy mention third parties involved in processing?
7. Does the privacy policy clearly state how long your personal data is stored?
8. Does the privacy policy mention your rights (Articles 15–21 and 77)?
9. If applicable: Is there information about data transferred to non-EU countries or international organizations?
10. If applicable: Is there information automated individual decision making, including profiling?

If several requirements aren't met, read the detailed descriptions and examples provided above, and carefully check the service/product in scope of the privacy policy.

## Summary
This basic list provides guidance on identifying whether a privacy policy is complete or incomplete. If you think that something is extremely wrong and your personal data is unlawfully processed, contact your data protection lawyer, and/or lodge a complaint with a supervisory authority. Online forums or other websites of people who aren't data protection lawyers aren't reliable sources for this topic.
