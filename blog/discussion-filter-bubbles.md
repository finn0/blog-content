+++
title = "Binary thinking is for boolean data types, not for information security or privacy"
author = "Benjamin"
date = "2019-08-11T16:03:57+02:00"
tags = [ "privacy", "ethics", "appeals" ]
categories = [ "discussion" ]
ogdescription = "Double standards, pseudo ethics, and binary thinking won't improve security."
slug = "discussion-filter-bubbles"
banner = "banners/discussion-filter-bubble"
toc = true
+++

When it comes to information security and privacy, some people specialized in spreading binary thinking. According to them, there is either 100% security/privacy, or no security/privacy at all. Such a binary view is not only wrong but results in a toxic "us vs. them" mentality.

In this article, we briefly show related problems and the pointlessness of proclaimed behavior rules.
<!--more-->
{{< rssbox >}}

## Common binary statements
If we look at binary statements, we mostly see the same declarations:

* "Oh, you are using Microsoft Windows? Seriously? You must use Linux!"
* "Oh, you are using banking apps on your smartphone? Your smartphone is totally insecure!"
* "Oh, you are using DuckDuckGo as a search engine? It is hosted on Amazon's AWS, and privacy-conscious people don't use AWS! (Besides, AWS supports the CIA!!)"
* "Oh, you are using PayPal? PayPal is so evil! You must only pay cash!"
* "Oh, you are using Google/Facebook/Microsoft/(any other US-American company)? They are so evil! How dare you?"
* "Oh, you are using proprietary software? Proprietary software is so bad for security! Only open-source software can be secure since everybody can check the source code!"
* …

Most of these statements reflect a binary view. There is only "right" or "wrong," no alternative to following such people's unwritten rules. However, the vast majority of these statements are either far from reality, biased, or come without threat models, usability considerations, or other in-depth analysis.

No, Linux isn't magically more secure, and no, there is no single Linux for everybody. No, somebody on the internet can't magically hack your smartphone by just entering its IP address in some hacking tool. And while some people claim that they never use Google/Facebook/Microsoft in their life, they are using technology invented by people who work for these companies daily.

If you dig deeper, you even discover more strange things. For instance, a guy claims that he—as a privacy-conscious something—would never use DuckDuckGo due to being hosted by AWS (as mentioned above). At the same time, he uses Amazon to buy goods, was a member of the Amazon PartnerNet to collect "donations" for years, and recommends other services hosted by AWS like the Signal messenger, projects on GitHub, or the Observatory by Mozilla. Another example is a guy claiming that PayPal is extremely bad for privacy, but entering his publicly posted account number on PayPal reveals that his banking account is registered with PayPal. A final example: A person claiming that only open-source software and hardware can be secure. At the same time, this person posts about their new proprietary router, their proprietary smartphone, and their proprietary laptop.

So why do such people spread binary thinking but violate their own rules? We don't know. However, it seems to be a common scheme to make like-minded people happy. The result is often a new filter bubble that only accepts "right" or "wrong." Welcome to the "us vs. them" mentality.

## Switching filter bubbles
The new filter bubble, inflated by binary thinking of like-minded people, is proclaimed as the only truth. The final step is to convert people who are unsure about the "right" way to address things to "the free world." As shown below, they are just switching filter bubbles.

{{< webpimg "art-img/discussion-filter-bubbles.png" "Sometimes, leaving your alleged filter bubble and entering the 'free world' (as described by your 'savior') means nothing but switching filter bubbles." "a user who switches from one filter bubble to another one." >}}

Is it more secure and more "privacy-friendly" to switch from a closed-source instant messenger to an open-source instant messenger hosted by unknown people and stores tons of metadata? Is it more secure to host your own Nextcloud instance instead of using a well-known hosting provider? Is your smartphone so insecure that you shouldn't use any banking apps but end-to-end encrypted messengers? Obviously, most of these blanket statements don't make any sense, or there is no binary answer.

## The world is non-binary
Our message is that people should stop falling prey to the binary world that doesn't exist. The world of information security and privacy is non-binary. There isn't only one truth. There isn't a "good" and a "bad" way. There is more than merely a binary state of secure/insecure or privacy-friendly/not that privacy-friendly. ("Privacy-friendly" isn't even a defined term.)

The same is true for the legal point of view: There is no global law or regulation for information security or privacy. There are hundreds, if not thousands, of different laws and regulations. Even the European GDPR, which was introduced to harmonize data protection laws across the European Union, comes with several opening clauses that allow national customization. So even the legal situation isn't "right" or "wrong."

## Conclusions
It is essential to talk with everyone, and not only with people who have precisely the same point of view. People who use services provided by Google/Facebook/Microsoft aren't better or worse than people who host everything on their own. People who use their smartphone for everything—including online banking—aren't better or worse than people who always pay cash.

Considering that even the purists don't follow their own rules, their statements look like a way of social influencing to create the "us vs. them" mentality. Then, the hardliners of these groups start expelling everyone who eludes their binary world. This world is non-binary, though.
