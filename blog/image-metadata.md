+++
title = "A picture paints a thousand words"
author = "Benjamin"
date = "2018-04-04T19:44:12+02:00"
lastmod = "2019-06-03"
tags = [ "metadata", "photo", "exif" ]
categories = [ "privacy" ]
ogdescription = "Photos can put your privacy at risk."
slug = "image-metadata"
banner = "banners/image-metadata"
toc = true
syntax = true
+++

Nearly all of us upload images to share them with friends or family members. However, others can use pictures to gather information about us. This article contains several examples so that you think twice when you are about to upload an image next time.
<!--more-->
{{< rssbox >}}

## Metadata in image files {#metadata}
Most likely, you think of Exif when you read "[metadata]({{< ref "/glossary.md#metadata" >}})." Exif stands for "Exchangeable image file format." More standards like IPTC, PLUS, and Dublin Core exist. All of these standards can add metadata to your photos, which isn't apparent. Additionally, smartphones and cameras can automatically add your location using GPS.

While devices add most metadata automatically, you can also add, modify, or remove metadata manually. Automatically added metadata is, for instance, camera model and manufacturer, information about lens and flash configuration. Nowadays, most image viewers can show metadata embedded in photos. You can also use [ExifTool]({{< relref "#links" >}}) to view and modify Exif data.

Of course, websites can process metadata automatically. For instance, Wikimedia Commons processes and stores metadata of all uploaded images. While this processing can be useful (e.g., you want to see similar pictures or other pictures taken by this particular camera model), it also puts your privacy at risk. After all, embedded metadata can consist of more than 300 different data sets.

Besides the exact time, data, and location (where and when the photo was taken), attackers learn about your device due to metadata. They can estimate how valuable your photo equipment is. Furthermore, they can gather information about your smartphone and learn about unpatched [security vulnerabilities]({{< ref "/glossary.md#vulnerability" >}}) in your device's OS.

## Visible information {#visible-info}
So far, we have talked about metadata like Exif data or GPS data. However, attackers can also process visible information in a photo. This information includes:

* approximate daytime (e.g., in the morning, in the evening)
* season (e.g., vegetation, snow, clothing)
* environment (flora and fauna)
* position of the photographer/camera
* faces and gestures of photographed individuals
* clothing (e.g., logos of universities)
* street signs (they can reveal street names or city names)
* billboards (they can reveal company names and distances)
* sounds (in case of videos)
* and so on

Even a single photo without visible people and embedded metadata can reveal dozens of reference points so that attackers can locate where the photo was taken. Several real examples:

* A photo showing a flower box on a balcony was sufficient to pinpoint the exact apartment (floor, location in the apartment building) of the photographer within 30 minutes. In addition to the photo, we knew the city's name where the building was located (24,000 inhabitants).
* Another photo showing the view from a window of an apartment building in a city with 290,000 inhabitants was sufficient to locate the actual construction and approximate floor within 45 minutes.
* One second of a live video stream showing the view from a balcony covered by a curtain was sufficient to locate the exact building and approximate floor in Budapest (1.7 million inhabitants) within several hours. We only knew that the building was located in the northeast of Budapest.
* A live video stream of two distinct locations in an undisclosed small Czech town showed the facade of a shop selling glasses and a chapel at a cemetery. A quick search for the name of the shop revealed 80 possible locations. Ignoring all in-door sites left a small subset of possible towns only. After several minutes, we found the town's name and the exact locations of the shop, chapel, and camera.

We only used satellite imagery to spot buildings that came into question and then used Google Street View (or mapy.cz resp.) to pinpoint the exact buildings. There is also a paper describing this process: [Accurate Image Localization Based on Google Maps Street View]({{< relref "#links" >}}).

Nowadays, Facebook and other websites can locate your position automatically, using machine learning and algorithms. Of course, automatic determination of the position is more exact when you take pictures of well-known structures like the St. Mary's Cathedral in Linz or the AZ Tower in Brno.

Additionally, reverse image search engines like [TinEye]({{< relref "#links" >}}) process images to find these images (or similar ones) on the internet. Using these engines can reveal other profiles of you, and attackers can learn more about you.

Finally, keep in mind that operating IP cameras at home may also be bad for your privacy. It is easy to locate cameras in the physical world by just looking at the video feed in many cases. See our article "[Christmas holidays: more exposed IP cameras will go online within the next few days]({{< ref "/blog/exposed-cameras.md#pinpointing-examples" >}})" for more information.

## Conclusions
In the best case, attackers only need one photo to gather lots of information about you. Of course, they can combine this data with information from other public sources or photos.

You should always rename files and strip metadata before you upload them. Please note that even renamed files without metadata can disclose information like your address, interests, location, etc. An interesting project is "I Know Where Your Cat Lives," which visualizes publicly-available photos of cats on a world map using embedded metadata.

If you have ImageMagick installed (this is often the case when you run a Linux distro), you can use the following commands in the terminal to view metadata or strip it:

* {{< kbd "identify -format '%[EXIF:*]' abc.jpg" >}}: Shows stored Exif data in abc.jpg.
* {{< kbd "mogrify -strip abc.jpg" >}}: Removes all Exif data in abc.jpg.

## External links {#links}
* {{< extlink "http://visionnas2.cs.ucf.edu/news/Zamir_ECCV_2010.pdf" "Accurate Image Localization Based on Google Maps Street View" >}}
* {{< extlink "https://iknowwhereyourcatlives.com/" "iknowwhereyourcatlives.com" >}}
* {{< extlink "http://www.sno.phy.queensu.ca/~phil/exiftool/" "ExifTool" >}}
* {{< extlink "https://www.tineye.com/" "TinEye" >}}

## Changelog
* Jun 3, 2019: Added information that is directly visible. Added one example of pinpointing locations.
* Apr 5, 2018: Added two ImageMagick commands
