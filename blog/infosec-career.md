+++
title = "Your career in information security"
author = "Benjamin"
date = "2019-04-22T09:01:01+02:00"
tags = [ "career", "comptia", "certifications" ]
categories = [ "knowledge" ]
ogdescription = "In this article, we talk about careers in information security."
slug = "infosec-career"
banner = "banners/infosec-career"
toc = true
+++

Some readers want to start their career in information security. However, they don't know how to start. In this article, we talk about careers in information security.
<!--more-->
{{< rssbox >}}

## InfoSec isn't mostly about Hollywood-style "hacking" {#hacking}
Many people who are totally new to the topic "information security" know Hollywood-style "hacking": A hoodie-wearing guy sits in front of dozens of monitors, the room is dark, information everywhere, and he types commands at the speed of light. Another extreme are some nerdy cryptography experts who break codes by just looking at them.

In reality, there isn't that much Hollywood in information security. Depending on the company and the job, InfoSec people wear suits or business casual. Most InfoSec people aren't hardcore coders who know every single programming language. They are normal people specialized in securing information. So, first of all, forget the Hollywood action, and start with a solid foundation.

## A solid foundation {#foundation}
The first step on the road to becoming an information security expert is oftentimes a bachelor's degree. There may be companies that require other qualification, of course. Contrary to popular belief, you don't need a Computer Science degree in any case. Let's look at the three basic domains of information security: technology, processes/organization, and humans.

### Technology
As you can see, technology (the "hackers") is only 33% of InfoSec. However, technology also differs: There is "traditional" information technology (IT), focusing on securing IT components typically found in office environments. Then, there is operational technology ([OT]({{< ref "/glossary.md#ot" >}})) that focuses on OT components like [industrial control systems]({{< ref "/glossary.md#industrial-control-systems" >}}) in production/manufacturing environments. IT and OT differ in many aspects. For instance, OT components may conduct time-critical operations. Oftentimes, [availability]({{< ref "/glossary.md#availability" >}}) of OT components has the highest priority while [confidentiality]({{< ref "/glossary.md#confidentiality" >}}) isn't important. Moreover, OT components can be special hardware, only developed for a specific purpose, and may run special operating systems that can't be easily updated.

Many OT networks are traditionally isolated from the internet. In recent times, OT networks get more and more connected, and operators become aware of security aspects in their networks. This is why there is a need for OT security professionals who understand the special implications of industrial networks.

Starting your InfoSec career with an emphasis on technology typically requires a STEM degree (science, technology, engineering, and mathematics) in most cases. No, you don't need a Bachelor of Science in Computer Science. There are companies that happily recruit people with a degree in mathematics, or engineering. Some universities also offer special study programs in information security.

Some InfoSec job titles with an emphasis on technology are forensics experts, incident responders, vulnerability assessors, penetration testers, code auditors, threat analysts, security engineers, security consultants, security architects, security analysts, and security administrators.

### Processes and organization
Besides technology, there are processes and organization. Technology on its own isn't sufficient to secure anything. A company needs guidelines, policies, and processes. Someone has to define clear rules for humans and technology. How to react in case of security incidents? How to store backups? What are acceptance criteria for new software? How to manage vulnerabilities? These are typical questions of information security management, needed to govern information security in companies.

A career in information security management doesn't necessarily require a Bachelor of Science in Computer Science. For instance, a degree in business information systems may be suitable. Some universities also offer special study programs in information security management.

Some job titles in information security management are (chief) information security officers, security specialists, security software developers, security managers, security directors, and security auditors.

### Humans
Finally, there are humans. There is the need to raise awareness for information security since technology and processes/organization can never offer 100% security. Humans can bypass technical security measures, or ignore defined rules. Then, there is human error. It's even worse: Many [social engineering]({{< ref "/glossary.md#social-engineering" >}}) techniques are possible without technology being involved. Therefore, someone must address humans as being vital for information security.

Starting a career related to information security awareness isn't that easy. The main reason for this is that many companies are focused on technology and processes/organization when it comes to information security. However, more and more companies realize the importance of full-time employees in InfoSec awareness. These people must understand other humans, and need a decent amount of well-developed soft skills. InfoSec awareness doesn't necessarily require STEM degrees.

A common job title in information security awareness is (chief) security awareness officer.

### What you can do in general {#general}
If you are uncertain about your InfoSec career path, you can still decide later. To get an idea of the real day-to-day business, you can intern at companies, or work as a student employee. Make use of these possibilities to gain experience.

Keep in mind that getting a degree in something doesn't imply that you get your hands on the latest technology, or findings. Studying is about learning the basics.

Besides, we recommend to learn at least one scripting language, and basic Linux commands. While many study programs already include one or more programming languages (like Java, C, or C++), you should learn languages like Python, or JavaScript. In our opinion, JS is mostly important if you want to focus on web security later. Python is universally usable, and easy to understand if you already learned a programming language.

Furthermore, soft skills are important. Most InfoSec folks aren't hoodie people but social creatures. So, make use of any possibility to improve your soft skills.

Finally, learn and improve your English. English is needed nearly everywhere nowadays. Do you need a certificate? There is no rule of thumb for this, however, it isn't good if your interviewer switches to English during a job interview, and you can't answer one single question.

## Get specialized {#specialization}
After getting a solid foundation for your chosen career path, you can specialize in your domain by getting a Master's degree. For instance, some universities offer study programs focused on information security management, others offer programs focused on security in industrial or automotive environments.

We don't recommend to just get a Master's degree in Computer Science after getting a Bachelor's degree in Computer Science since it is very broad and not focused on information security. Use specialized study programs to gain experience, and focus on information security. Some companies offer financial support to get a Master's degree.

Keep in mind that academic degrees aren't always necessary to reach your favorite job position. People with several years of relevant job experience can get the same jobs.

## Get certified {#certification}
Then, there is the big market of InfoSec certifications. Some people consider certificates to be absolutely useless. In our opinion, certificates offer a comparable level of qualification, and they are generally accepted by companies. However, they aren't just beneficial for your CV, but also for your employer. For instance, if you work as a security consultant, your certificates clearly show that you reached a certain level of qualification and specialization. Some companies offer financial support to get certified.

Generally accepted certifications are CompTIA's certifications in cyber security (Network+, Security+, CySA+, CASP+, PenTest+).

Then, there is the "International Information System Security Certification Consortium", also known as (ISC)², famously known for the Certified Information Systems Security Professional (CISSP) certification.

Moreover, there is the "Global Information Assurance Certification", also known as GIAC, founded by the SANS Institute. GIAC offers certifications for professionals like Web Application Penetration Tester (GWAPT), Certified Incident Handler (GCIH), Global Industrial Cyber Security Professional (GICSP), or Response and Industrial Defense (GRID).

The "International Council of Electronic Commerce Consultants" (EC-Council) is famously known for the Certified Ethical Hacker (CEH) certification. Finally, "Offensive Security" offers the Offensive Security Certified Professional (OSCP), and Offensive Security Certified Expert (OSCE) certifications.

Most certificates need to be regularly renewed by earning continuous education points. This totally makes sense since a decade-old certificate without any related work experience isn't really meaningful. In general, we recommend to go for vendor-independent certifications. Keep in mind that getting certified doesn't imply that you use the latest tools, and try out the newest hacks. It is about learning basics to reach a certain level of qualification which is comparable.

## Build a (social) network {#networking}
As mentioned above, social skills are really important. Another important part is your social network (no, we don't think of Facebook here). There are many well-known security conferences and workshops each year. Use these opportunities to get in touch with other security professionals to exchange ideas and knowledge.

You can also be a more active part of such events. For instance, look for "call for papers" and apply for giving a talk. Or just help to organize events.

## Summary
If you still don't know your ultimate dream job in information security, intern at companies, learn scripting languages, and improve your soft skills. Gain work experience as a student employee, and find an appropriate career path. And no, you don't need to get a Computer Science degree in any case, you don't need to code in any case, and it isn't all about penetration testing and breaking codes.

If you are about to write your bachelor/master thesis, choose an InfoSec topic, and use these opportunities for specialization. Finally, get vendor-independent and generally accepted certificates to demonstrate your knowledge.
