+++
title = "ECSM 2019: Securing emerging technology (IoT) at home"
author = "Benjamin"
date = "2019-10-30T19:44:35+01:00"
tags = [ "ecsm2019", "iot" ]
categories = [ "privacy" ]
ogdescription = "Ways to secure IoT at home."
slug = "ecsm2019-emerging-technology"
banner = "banners/ecsm"
toc = true
+++

The second topic of this year's European Cyber Security Month is "Emerging Technology." We decided to address IoT (Internet of Things) devices at your home since many different devices are connected to the internet nowadays.

In this short article, we present several problems with IoT devices and ways to secure them.
<!--more-->
{{< rssbox >}}

## IoT everywhere
Nowadays, there are dozens of IoT devices available for private users/consumers. Examples are:

* air conditioners
* Bluetooth trackers
* carbon monoxide monitors
* cookers
* digital scales
* fridges
* garage controls
* heating controls
* IP cameras
* sensors (light, wind, rain)
* smart locks
* sockets
* sprinklers
* toothbrushes
* vents
* voice assistants
* washing machines
* and many more "smart" things

Apart from these internet-connected devices, there is very likely a router at your home. Finally, you find the "classic" IT devices like laptops, desktop computers, and smartphones. All of these devices probably contain IT and run software.

## An example of insecure IoT devices
We already published [two articles showing the dangers of IP cameras](/tags/camera/) that are directly exposed to the internet.

This time, we want to share the following picture. It shows the configuration page of a publicly-exposed Mobotix camera. As mentioned in the other posts, some IP cameras also leak your WiFi password and additional sensitive information.

{{< webpimg "art-img/ecsm2019-mobotix-camera.png" "This is the configuration page of a publicly-exposed Mobotix camera. Of course, some cameras also leak your WiFi password and other sensitive information." "the settings of an exposed camera." >}}

If you closely look at the picture, you see that this camera runs the Mobotix firmware MX-V4.0.4.28, released on November 25, 2011. So the firmware of this IoT device is nearly eight years old. The current version is MX-V4.7.2.21, released on June 6, 2019. Several updates contain important security updates for the camera.

The question here is: Whose fault is that? Is the manufacturer of the device responsible for updating the device? Or is the private user who operates the camera responsible? Of course, there is no easy answer. Keep in mind that information security is a shared responsibility.

## Ways to secure IoT devices
After our brief look at an exposed camera configuration page, we present several tips to secure your IoT devices.

1. **Create an asset inventory**: This seems to be trivial; however, manufacturers of IoT devices want to make their devices more comfortable for customers. Some devices are automatically connected to the internet without additional configuration. Therefore, you need to identify all devices that are connected to the internet.
2. **Use your router as a guard**: Your router is very likely the central gateway to the internet for most of your IoT devices. Use it to control and monitor your network traffic. Check our [Home network security series]({{< ref "/as-hns.md" >}}).
3. **Update firmware of your IoT devices**: Of course, there isn't a possibility to update every IoT device. Some devices don't come with an update feature. However, the example above shows your responsibility to update your devices. The same is true for "classic" IT, like your router, laptops, and smartphones.
4. **Set strong credentials instead of default ones**: Especially older IoT devices come with default credentials and don't force you to change them. Go to your devices and change their passwords. If you don't know about managing credentials, check our article on [modern credential management]({{< ref "/blog/modern-credential-management.md" >}}). The same is true for your router and your WiFi password. There are many lists with default credentials on the internet, making it very easy to access your weakly-protected devices.
5. **Check security and privacy settings**: Open the settings of your IoT devices and check whether there are dedicated security and privacy settings. Some devices allow you to enable HTTPS for transport encryption. Use such features and try to understand their purpose.
6. **Disable features (and IoT devices) you do not need**: Go to your device's settings and carefully check their features. Disable features you never use (e.g., Bluetooth). If they can't be disabled, evaluate whether you can block them at the network level using your router.
7. **Be aware of data in the cloud**: Some IoT devices may upload your data to the cloud. Disable such features if you don't need them.
8. **Read news feeds**: Regular check [news feeds](/news/) to learn about newly-discovered security vulnerabilities.

As an advanced, tech-savvy user, you can create dedicated virtual LANs for your IoT devices to isolate their network traffic. Use network scanners like nmap to identify open ports and services on your home network.

## Summary
IoT devices are everywhere nowadays. The most significant task is to become aware of IT in your home network. Many people don't realize that the live feed of their IP camera is publicly accessible. Others don't know that their smart vacuum cleaner always uploads their homes' indoor layout to the internet or that their smart locks can be easily unlocked within seconds. Learn about your IoT devices and particularly about their security features.
