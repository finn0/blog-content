+++
title = "About us"
ogdescription = "Information about our project and contributors."
nodateline = true
noprevnext = true
notice = true
+++

## Who we are
We are a growing community of European information security professionals and privacy activists who like to share their knowledge for free. We started the "InfoSec Handbook" in January 2017. In March 2018, we decided to redesign/rename the website entirely and switched to English only. We reach a global audience and are in close contact with InfoSec professionals from all over the world.

We are not available for hire!

## What we do
First of all, we are full-time employees working closely with many other information security experts every day. Our employers are well-known international companies, which are active in various InfoSec domains. Being active in distinct InfoSec domains ensures that our website covers a wide range of different InfoSec topics. Our professional daily work includes conducting penetration tests, analyzing malware, designing industrial networks, inspecting network traffic, hardening operating systems and applications, creating tools for particular tasks, writing InfoSec policies and guidelines, and much more. Kindly note that our families and principal employers have priority over our website.

We operate the non-profit (100% self-funded) InfoSec Handbook as a team, focusing on a privacy-friendly and secure design. We process the least amount of personal data possible to provide our website, and regularly reconfigure and closely monitor our web servers to ensure secure operation.

Besides, some of us conduct workshops and give lectures. For instance, we were at:

* Vysoké učení technické (University of Technology) in Brno, Czech Republic
* České vysoké učení technické (Czech Technical University) in Prague, Czech Republic
* Universität Regensburg (University of Regensburg), Germany
* Zentrum Digitalisierung.Bayern (Center for Digitalization Bavaria), Germany
* IT-Cluster Linz, Austria

To acquire additional skills and meet with other professionals, we regularly attend security summits and workshops. You can meet us in person if you attend events like the Gulaschprogrammiernacht, BSides Munich, C3W PrivacyWeek, or Honeynet Project Workshops. 😉

Finally, we sometimes look for security vulnerabilities in websites/web applications/software and report findings to administrators/developers. As of May 2020, we sent reports to about 166 companies, organizations, and private individuals. We also privately report incomplete privacy policies; however, this isn't the main focus of our voluntary work.

## What we don't do
As mentioned above, we do not promote any services or products of our employers on our website.

Since we like to read and write honest and neutral articles, we do not promote any services or products of companies, organizations, or private individuals in connection with sponsoring. For the same reason, we do not promote any services or products solely based on hearsay or assumptions.

Lastly, we do not contact the media or any other third party to share or sponsor our website or content.

## Contributors
All contributors are either information security professionals or privacy activists who want to share their knowledge with you for free.

{{< picture "img/ish-contributor-map.png" "Locations of contributors of the InfoSec Handbook" >}}

### Jakub {#Jakub}
Jakub holds a Master of Information Technology Security degree, in addition to a Bachelor of Science degree in Computer Science (Computer Systems and Data Processing). He currently works as a Cyber Threat Intelligence Analyst for a Czech company in Prague. Jakub operates infosec-handbook.eu and frequently provides ideas and practical threat hunting knowledge. During leisure time, he cares for his children, wanders through Czech woods, or goes fishing.

### Benjamin {#Benjamin}
Benjamin is an [ICS]({{< ref "glossary.md#industrial-control-systems" >}})/[OT]({{< ref "glossary.md#ot" >}}) security consultant, counseling companies of the beverage and liquid food sector (including critical infrastructure). He holds a Bachelor of Science degree in Computer Science. He worked for an internal information security department with a focus on information security awareness as well as a security-sensitive European government organization for years. Benjamin’s experience includes the areas of network security, network-level anomaly detection, and visualization of security-relevant information using graph databases. He likes traveling, hiking, and contributing to Wikipedia. He is also an active member of Codeberg e.V. (Germany).

### Thorsten {#Thorsten}
Thorsten is passionate about helping like-minded as well as non-technical people when it comes to privacy and data protection. He holds a degree in Information Technology. Thorsten is known for his privacy-related articles and easy to understand tutorials in German that he published on free and open social networks.

### Verena {#Verena}
Verena liked the idea of our free InfoSec Handbook and decided to create several icons for us. Moreover, she provides pictures for our website. She is snap-happy and loves creative activities.

## Official accounts {#oa}
We operate accounts on the following publicly-accessible websites:

* {{< extlink "https://github.com/infosec-handbook" "Github.com" >}}
* {{< extlink "https://www.reddit.com/user/infosechandbook" "Reddit.com" >}}
* {{< extlink "https://codeberg.org/infosechandbook" "codeberg.org" >}}

Besides, we operate groups in 4 different corporate networks that are closed for the public.

{{< noticebox "warning" "Unsigned comments or accounts on websites that aren't mentioned above are very likely faked. Please report any unsigned comments or accounts." >}}
